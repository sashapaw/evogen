subject_type.get_next_name do |c| 
  "#{get_value('Analytes').map(&:name).join(', ')} L-J Plots from #{get_value('Start Date').strftime('%m/%d/%Y')}" + "-" + "#{get_value('End Date').strftime('%m/%d/%Y')}" +  (" -- %07d" % c)
end