d = Date.today
subject_type.get_next_name do |c|
  if get_value('Analytes').size == 1
    "#{get_value('Analytes')[0].name}-#{get_value('Control Type')} Control #{get_value('Lot Number')}-" + (c)
  else 
    "#{get_value('Control Type')} Control #{get_value('Lot Number')}-" + (c)
  end 
end