# Generate name like: Ethyl Glucuoride - Low Control
analyte  = get_value('Analyte')
if get_value('Lot Number').present?
 "#{analyte.name} - #{get_value('Control Type')} - Lot # #{get_value('Lot Number')} "
else 
 "#{analyte.name} - #{get_value('Control Type')} "
end