def after_create_final 
  raise "There could be only one!" if self.subject_type.subjects.count > 1
end