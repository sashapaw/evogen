def after_create_final
  validate
end

def after_update_final
  validate
end

def validate
  analyte = get_value('Analyte')
  raise '"Analyte" is required for Range' unless analyte
  low = get_value('Low')
  raise '"Low" value is required for Range' unless low
  high = get_value('High')
  raise '"High" value is required for Range' unless high
  raise '"High" cannot be less than "Low"' if high < low
  critical_low = get_value('Critical Low')
  raise "\"Critical Low\" (#{critical_low}) shall be less than \"Low\" (#{low}) or empty" if critical_low && critical_low >= low
  critical_high = get_value('Critical High')
  raise "\"Critical High\" (#{critical_high}) shall be greater than \"High\" (#{high}) or empty" if critical_high && critical_high <= high
  age_from = get_value('Age From')
  age_to = get_value('Age To')
  age_unit = get_value('Age Unit')
  raise '"Age Unit" is required if "Age From" or "Age To" are set' if (age_from || age_to) && !age_unit
  raise '"Age Unit" is set but "Age From" and "Age To" are empty' if age_unit && !age_from && !age_to
  raise '"Age From" cannot be negative' if age_from && age_from < 0
  raise '"Age To" cannot be negative' if age_to && age_to < 0
  raise "\"Age From\" (#{age_from}) cannot be greater than \"Age To\" (#{age_to})" if age_from && age_to && age_from > age_to
  low_limit = analyte.get_value('Low Limit')
  if low_limit
    raise "\"Critical Low\" (#{critical_low}) cannot be less than \"Low Limit\" (#{low_limit}) of analyte" if critical_low && critical_low < low_limit
    raise "\"Low\" (#{low}) cannot be less than \"Low Limit\" (#{low_limit}) of analyte" if low < low_limit
  end
  high_limit = analyte.get_value('High Limit')
  if high_limit
    raise "\"Critical High\" (#{critical_high}) cannot be greater than \"High Limit\" (#{high_limit}) of analyte" if critical_high && critical_high > high_limit
    raise "\"High\" (#{high}) cannot be greater than \"High Limit\" (#{high_limit}) of analyte" if high > high_limit
  end
end