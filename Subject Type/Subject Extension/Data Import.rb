def add_data(sample, test_result)
  add_value("Associated Test Results", test_result) if test_result.present?
  if sample.present?
    if sample.qc?
      add_value("QC Samples", sample)
    else
      add_value("Imported Samples", sample)
    end
  end
end