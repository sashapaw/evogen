def after_load
  message_types = self.get_value('HL7 Message Type')
  provider_name = self.name
  if message_types && message_types.include?('ORU')
    class_eval {
      include Limfinity::LISHL7::Providers::EHR
      include Limfinity::LISHL7::Providers::EHR.const_get(provider_name)
    }
  end
  if message_types && message_types.include?('DFT')
    class_eval {
      include Limfinity::LISHL7::Providers::Billing
      include Limfinity::LISHL7::Providers::Billing.const_get(provider_name)
    }
  end
end

def hl7_version
  get_value('HL7 Version')
end
