def custom_view(context, params={})
  pocr = get_value('Point of Care Test Result Entries', :order=>'name ASC') || []

  data = pocr.map{|u|
    r = u.get_value('PoC Result')
    [u.id, u.get_value('Point of Care Test').name, r=='Positive' ? 'X' : '', 
    r=='Negative' ? 'X' : '', 
    r=='Not Tested' ? 'X' : '']
  }

  {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, items: [
    {xtype: 'subjectprops', subjectId: id},
    {xtype: 'label', html:'<div style="padding:5px;color:white;">Test Results:</div>'},
    Limfinity::Requisition.poc_view(data, flex: 1)
  ]}.to_json
end
