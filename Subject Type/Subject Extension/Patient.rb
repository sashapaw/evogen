def my_users
  qb = QE::Builder.new
  q = qb.prop('Patient').eq(self)
  q = QE::QOM::Query.new(nil,User,q)
  
  qom2arel = QE::QOMToArel.new(User)
  arel = q.accept(qom2arel)
  qom2arel.target_class.find_by_sql(arel)
end

def after_create_final
  #flow = SubjFlow.where(:subject_type_id=>self.subject_type_id).first
  #stdef = SubjStateDef.where(subj_flow_id: flow.id, name: 'Active').first
  #WflowEngine.force_advance_ignore_permissions(self, User.curr_user, stdef, User.curr_user)
  self.set_value('Facility', User.curr_user.get_value('Facility') ) unless self.get_value('Facility').present?
  validate
  # automatically generate the MRN, if it's not provided in the UI
  if !self.get_value('MRN').present?
	system_settings = ::Limfinity::LIS.system_settings
    mrn_prefix = system_settings.get_value('Patient MRN Prefix')
    
    self.set_value('MRN', "#{mrn_prefix}#{rand(10 ** 10)}" % subject_type.get_next_name_number)
    self.send(:gen_name)
  end
  context.advance_workflow('Patient', 'Active', self)
  ::Limfinity::SystemHooks.patient_created(self)
end

def after_update_final
  validate
  self.send(:gen_name)
end

def validate
  ::Limfinity::LIS.validate_address(self['Address'])
  ::Limfinity::Patient.insurance_check(self)
  npi = get_value('Primary Physician NPI')
  raise "Primary Physician NPI shall have 10 digits but set to '#{npi}'" if npi && !/^\d{10}$/.match(npi)
  ::Limfinity::SystemHooks.validate_patient(self)
end

def custom_view(context, params={})
  if User.curr_user.get_value('Patient').present?
    prop_id = context.find_udf('Report File').id
    data = get_value('Released Requisitions', {limit:10, :order=>'created_at DESC'}).map{
      |u| [u.id, prop_id, u.name, 
           SysUtils.format_date(u.get_value('Report Date'), User.curr_user), 
           u.get_value('Report File').present? ? u.get_value('Report File').filename : '']
    }
    
    {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, items: [
      {xtype: 'subjectprops', subjectId: id, standardFields:[], includeFields:['First Name', 'Last Name', 'DOB', 'Address', 'Phone Number'] },
      {xtype: 'label', html:'<div style="padding:5px;color:white;">Test Results (last 10 results shown):</div>'},
      {xtype: 'grid', flex: 1, viewConfig: {forceFit: true, deferEmptyText:false},emptyText:'<div align="center">No Test Results To Display</div>', 
        autoExpandColumn:'file',
        columns:[
            {dataIndex:'req', header:'Requisition', width:'20%'}, 
            {dataIndex:'date', header:'Report Date', width:'15%'}, 
            {dataIndex:'file', header:'Report File (click to download)', renderer:UIUtils.raw_js("function(val, params, rec) { return rec.get('file') ? '<a href=\"/subjects/attachment/?id='+rec.get('id')+'&pid='+rec.get('prop_id')+'\" target=\"_blank\">'+rec.get('file')+'</a>' : '' }"), width:'65%'}
        ],
        store: {type: 'array', fields:['id', 'prop_id', 'req', 'date', 'file'], data: data}}
    ]}.to_json
  end
end

def dob
  @dob ||= get_value('DOB')
end

def age_in_days
  unless @age_in_days
  	@age_in_days = (Time.now.to_i - dob.to_time.to_i)/60/60/24
  end
  @age_in_days
end

def age_in_months
  _calc_ages unless @age_in_months
  @age_in_months
end

def age_in_years
  _calc_ages unless @age_in_years
  @age_in_years
end

def _calc_ages
  days_in_months  = [nil, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  borrowed_month = false

  current_date = Time.new
  birth_date = dob

  # Get days for this year
  if current_date.to_date.leap?
    days_in_months[2] = 29
  end

  day   = current_date.day - birth_date.day
  month = current_date.month - birth_date.month
  year  = current_date.year - birth_date.year

  if day < 0
    # subtract month, get positive # for day
    day = days_in_months[birth_date.month] - birth_date.day + current_date.day
    month -= 1
    borrowed_month = true
  end

  if month < 0
    # subtract year, get positive # for month
    month = 12 - birth_date.month + current_date.month
    if borrowed_month == true
      month -= 1
    end
    year -= 1
  end
  @age_in_years = year
  @age_in_months = month + (year*12)
end