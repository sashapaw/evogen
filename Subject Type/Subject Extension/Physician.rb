def after_create_final
  validate
  self.set_value('Facility', User.curr_user.get_value('Facility') ) unless self.get_value('Facility').present?
  context.advance_workflow('Physician', 'Active', self)

  ::Limfinity::SystemHooks.physician_created(self)
end

def after_update_final
  validate
  self.send(:gen_name)
end

def validate
  npi = get_value('NPI Number')
  raise 'NPI Number is required for Physician' if npi.nil?
  raise "NPI Number shall have 10 digits but set to '#{npi}'" unless /^\d{10}$/.match(npi)
  ::Limfinity::LIS.validate_address(self['Address'])
  ::Limfinity::LIS.validate_email(self['Contact Email'])
end