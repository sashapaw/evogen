def after_create_final
  validate
  ScriptRunner.context.advance_workflow('Contact', 'Active', self, User.curr_user)
end

def after_update_final
  self.send(:gen_name)
  validate
end

def validate
  ::Limfinity::LIS.validate_address(self['Address'])
  ::Limfinity::LIS.validate_email(self['EMail'])
end
