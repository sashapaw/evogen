def integration_class
  instrument_class_name = get_value('Integration Class Name')
  raise "Integration Class is not configured for instrument type #{name}" if instrument_class_name.blank?
  Limfinity::Instrument.const_get(instrument_class_name)
end
