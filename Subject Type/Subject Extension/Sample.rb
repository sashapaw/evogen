def before_create_callback
  self.name = self.barcode_tag = "#{Time.now.strftime('%y%m%d')}%06d" % self.serial_number
  Limfinity::SystemHooks.sample_preinit(self)
end

def after_create_final 
  Limfinity::SystemHooks.sample_init(self)
  validate()
end

def after_update_final
  validate()
end

def qc?
  false
end

def validate()
  spec_type = self.get_value('Specimen Type')
  raise "Specimen Type is not set on #{name} sample" if spec_type.nil?
  tests = self.get_value('Tests')
  tests.each{|t|
    t_types = t.get_value('Specimen Types')
    if t_types.present? && !t_types.include?(spec_type)
      raise "Specimen Types (#{t_types.map(&:name).join(', ')}) of '#{t.name}' test are incompatible with Specimen Type (#{spec_type.name}) of '#{self.name}' sample"
    end
  }
  
  low = get_value('Low Limit')
  high = get_value('High Limit')
  if low && high
    raise "Low Limit (#{low}) shall not be greater than High Limit (#{high})" if low > high
  end
end

def create_test_result_for_analyte(a)
  c = context
  unit = a.get_value('Unit')
  code = a.get_value('Code')

  res = c.create_subject('Test Result') { |obj|
    obj.set_value('Sample', self)
    obj.set_value('Analyte', a)
    obj.set_value('Unit', unit) if unit.present?
    obj.set_value('Code', code) if code.present?

    obj.advance_workflow('Test Results', 'Data Entry')
  }
end

def add_tests_and_create_test_results(add_tests)
  curr_analytes = get_value('Tests').map {|t| t.get_value('Analytes')}.flatten.uniq
  add_tests.map {|t| t.get_value('Analytes')}.flatten.select {|a| !curr_analytes.include?(a)}.
    each {|a| create_test_result_for_analyte(a)}
  add_value('Tests', add_tests)
end

def create_all_test_results
  self.get_value('Tests').map {|t| t.get_value('Analytes')}.flatten.uniq.
    map {|a| create_test_result_for_analyte(a)}
end

def delete_test_results(keep_analytes: nil, delete_analytes: nil)
  trs = get_value('Test Results')
  trs.keep_if{|tr|
    tranal = tr.get_value('Analyte')
    (keep_analytes.nil? || !keep_analytes.include?(tranal)) &&
    (delete_analytes.nil? || delete_analytes.include?(tranal))
  }
  trs.each {|tr| tr.destroy}
end

def delete_tests_and_test_results(del_tests)
  curr_tests = get_value('Tests')
  remaining_tests = curr_tests - del_tests
  remaining_analytes = remaining_tests.map {|t| t.get_value('Analytes')}.flatten.uniq
  delete_test_results(keep_analytes: remaining_analytes)
  remove_value('Tests', del_tests)
end

def test_results_by_test(tests = nil)
  tests = self.get_value('Tests') if tests.nil?
  return [] if tests.blank?
  tests.sort_by!{|t| t.get_value('Report Order') || 1000000}
  a = []
  c = ScriptRunner.context
  tests.each{|t|
    results = c.find_subjects(query: c.search_query(from: 'Test Result') {|qb|
      q = qb.and(
        qb.prop(:terminated).eq(false),
        qb.prop('Sample').eq(self),
        qb.prop('Analyte').eq(t.get_value('Analytes'))
      )
    }, order: {'Analyte->"Analyte Type"->name'=>:asc, 'Analyte->name'=>:asc})
    a << {'test'=> t, 'results'=> results} if results.present?
  }
  a
end

def cancel
  delete_test_results
  context.advance_workflow('Sample', 'Cancelled', self)
end

def custom_view(context, params = {})
  user = User.curr_user
  ug = UserGroup.find_by_name('Lab Managers')
  lu = UserGroup.find_by_name('Lab Users')
  ru = UserGroup.find_by_name('RURO Admins')
  can_results = ug.users.include?(user) || lu.users.include?(user) || ru.users.include?(user)

  #If LabUser or LabManager or RuRo Admin
  return nil unless can_results
  
  if params[:node]
    result_groups = self.test_results_by_test
    nodes = result_groups.map{|group|
      t = group['test']
      tid = t ? t.id : 0
      test_type = t.get_value('Test Type')
      test_type_suffix = test_type ? " <span style=\"color:#999\">(#{test_type.name})</span>" : ''
      subnodes = group['results'].map{|tr|
        analyte = tr.get_value('Analyte')
        value = tr.get_value('Value').to_s
        unit = tr.get_value('Unit').to_s
        outcome = tr.get_value('Outcome').to_s
        flag = tr.get_value('Flag')
        color = flag && outcome == 'CONSISTENT' ? flag.get_value('Color for Consistent') : flag && outcome == 'INCONSISTENT' ? flag.get_value('Color for Inconsistent') : '000'
        range = tr.get_value('Range').to_s
        comment = tr.get_value('Comments').to_s
        {
          id: "#{self.id}t#{tid}r#{tr.id}",
          c0: UIUtils::h(analyte.to_s), 
          a0: {oid: analyte.id},
          c1: UIUtils::h(value),
          a1: {oid: tr.id},
          c2: UIUtils::h(unit), 
          a2: {oid: tr.id},
          c3: UIUtils::h(range),
          a3: {oid: tr.id},
          c4: "<span style='color:##{color};'>#{flag}</span>",
          a4: {oid: (flag || tr).id},
          c5: "<span style='color:##{color};'>#{outcome}</span>",
          a5: {oid: tr.id},
          c6: tr.get_value('Finalized') ? 'Yes' : 'No', 
          a6: {oid: tr.id},
          c7: UIUtils::h(comment), 
          a7: {oid: tr.id},
          leaf: true
        }
      }
      {
        id: "#{self.id}t#{tid}",
        c0: t ? "#{UIUtils::h(t.name)}#{test_type_suffix}" : '<span style="color:#999">Non-ordered</span>',
        a0: t ? {oid: t.id} : nil,
        expanded: true,
        children: subnodes,
        leaf: subnodes.empty?
      }
    }
    return nodes.to_json
  end

  tree_only = params[:tree_only] == true || params[:tree_only] == 'true'
  view_only = params[:view_only] == true || params[:view_only] == 'true'
  columns = [
    {text: 'Test / Analyte', width: 250},
    {text: 'Value', width: 70},
    {text: 'Unit', width: 70},
    {text: 'Range', width: 100},
    {text: 'Flag', width: 60},
    {text: 'Outcome', width: 120},
    {text: 'Finalized', width: 80},
    {text: 'Comment'}
  ]
  
  treegrid = {
    xtype: 'treegrid',
	title: 'Test Results', 
    viewConfig: {emptyText: '<p align="center">No Test Results To Display</p>', deferEmptyText: true},
    columns: columns,
    ajaxParams: {subject_id: id}
  }.to_json
  tree_only ? treegrid : <<-EOS
  {
    items: [
      {xtype: 'subjectprops', subjectId: #{id}},#{treegrid}
    ]
  }
EOS
  
end
