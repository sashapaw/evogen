def after_create_final
  ::Limfinity::LIS.validate_address(self['Address'])
  ::Limfinity::LIS.validate_email(self['Contact Email'])

  if not get_value('RURO_CLIENT').present?
    portal = context.create_subject('Facility Portal', name: 'Welcome, ' + self.name)
    portal.set_value('Facility', self)
    context.start_workflow('Facility Portal', portal)
  end
  context.advance_workflow('Facility', 'Active', self)
end

def after_update_final
  ::Limfinity::LIS.validate_address(self['Address'])
  ::Limfinity::LIS.validate_email(self['Contact Email'])
end

def custom_view(context, params={})
    msg = "Client\'s Portal User Accounts:"
    msg = "Lab User Accounts:" if get_value('RURO_CLIENT').present?
  
    data = my_users.map{|u|
      ug = UserGroup.find_by_name('Clients Report Access')
      username = "#{u.username}"
      if ug.users.include?(u)
        username = "<i class='fa fa-user-plus'></i> #{u.username}"
      end

      [username, u.fullname, u.email, SysUtils.format_datetime(u.last_login, User.curr_user), u.disabled? ? 'No' : 'Yes']
    }
    {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, items: [
      {xtype: 'subjectprops', subjectId: id, exclude_fields:'Use Patient Testing Scheduler'},
      {xtype: 'label', html:'<div style="padding:5px;color:white;">'+msg+'</div>'},
      {xtype: 'grid', flex: 1, viewConfig: {forceFit: true, deferEmptyText:false},emptyText:'<div align="center">No Users To Display</div>', 
        columns:[
            {dataIndex:'login', header:'Login', width:'15%'}, 
            {dataIndex:'fullname', header:'Full Name', width:'30%', renderer:UIUtils.raw_js("function(val, obj, rec) {if (rec.get('active')=='Yes') return val; else return '<span style=\"text-decoration:line-through;color:gray;\">' + val + '</span>';}")},
            {dataIndex:'email', header:'EMail', width:'22%'},
            {dataIndex:'last_login', header:'Last Login', width:'23%'},
            {dataIndex:'active', header:'Active', width:'15%'}
        ],
        store: {type: 'array', fields:['login', 'fullname', 'email', 'last_login', 'active'], data: data}}
    ]}.to_json
end

def my_users
  qb = QE::Builder.new
  q = qb.and(qb.prop('Facility').eq(self), qb.condition('Patient is null'))
  q = QE::QOM::Query.new(nil,User,q)
  
  qom2arel = QE::QOMToArel.new(User)
  arel = q.accept(qom2arel)
  qom2arel.target_class.find_by_sql(arel)
end