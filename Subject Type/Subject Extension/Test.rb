def after_create_final
  validate
  context.advance_workflow('Test', 'Active', self)
end

def after_update_final
  validate
  gen_name
end

def validate
  non_reportable = self.get_value('Non-Reportable')
  analytes = self.get_value('Analytes') || []
  raise "Non-Reportable '#{name}' test cannot contain more than one analyte" if non_reportable && analytes.size > 1
  analytes.each{|analyte|
    non_reportable_analyte = analyte.get_value('Non-Reportable')
    raise "#{non_reportable ? 'Non-' : ''}Reportable '#{name}' test cannot contain #{non_reportable_analyte ? 'Non-' : ''}Reportable '#{analyte.name}' analyte" if non_reportable != non_reportable_analyte 
  }
  
  non_orderable = self.get_value('Non-Orderable')
  if non_orderable
    orderable_panels = context.find_subjects(query: "FROM \"Test Panel\" WHERE \"Tests\" = #{self.id}")
    raise "Non-Orderable '#{name}' test cannot be present in Test Panels: #{orderable_panels.map(&:name).join(', ')}" if orderable_panels.present?
  end
  
  spec_types = self.get_value('Specimen Types')
  if spec_types.present?
    analytes.each{|analyte|
      spec_type = analyte.get_value('Specimen Type')
      if spec_type && !spec_types.include?(spec_type)
        specs = spec_types.map(&:name).join("', '")
        raise "'#{analyte.name}' analyte is only for '#{spec_type.name}' specimen type and cannot be included in '#{self.name}' test which is only for '#{specs}'"
      end
    }
  end
  
  facility = get_value('Facility')
  if facility
    panels = context.find_subjects(query: "FROM \"Test Panel\" WHERE \"Tests\" = #{self.id} AND (Facility = null OR Facility != #{facility.id})")
    raise "'#{name}' test for '#{facility.name}' facility cannot be present in Test Panels: #{panels.map(&:name).join(', ')}" if panels.present?
  end
end

def custom_view(context, params={})
  context.require_script('view_helper')
  tree_only = params[:tree_only] == true || params[:tree_only] == 'true'
  view_only = params[:view_only] == true || params[:view_only] == 'true'
  ans = (get_value('Analytes') || []).sort{|a1,a2|a1.name <=> a2.name}
  nodes = ans.map{|an|
      an_code = an.get_value('Instrument Code')
      unit = an.get_value('Unit')
      type = an.get_value('Qualitative')
    {id: an.id, 
      c0: UIUtils::h(an.name), 
      a0: {oid: an.id}, 
      expanded: false, 
      c1: UIUtils::h(an_code || ''), 
      c2: UIUtils::h(type.present? ? 'Yes' : 'No'), 
      c3: UIUtils::h(unit ? unit.name : ''),
      leaf: true}
    }
  to_display = nodes.select{|node| node}
  columns = [
    { text: 'Analyte Name' },
    { text: 'Instrument Code', width: 150},
    { text: 'Qualitative', width: 110},
    { text: 'Default Unit', width: 120}
  ]
  
  treegrid = 
    {
        xtype: 'treegrid',
        title: 'TEST ANALYTES',
		viewConfig: {emptyText: '<div align="center">No Analytes To Display</div>', deferEmptyText: true},
        columns: columns,
        data:to_display
    }.to_json

  tree_only ? treegrid : <<-EOS
{
  items: [
    {
      xtype: 'subjectprops',
      subjectId: #{id}
    },
    #{treegrid}
  ]
}
EOS
end 