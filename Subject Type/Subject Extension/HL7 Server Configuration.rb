def provider_type
  self.get_value('HL7 Provider')
end

def upload_hl7_file(f, remote_file_path)
  host = get_value('HL7 Host Name')
  port = get_value('HL7 Host Port')
  port = nil if !port.nil? && port.blank?
  port = Integer(port) unless port.nil?
  username = get_value('HL7 Username')
  password = get_value('HL7 Password')
  type = get_value('HL7 Server Type')

  case type
  when "SFTP"
    Limfinity::LISHL7::Transmission::SFTP::upload_file(host, username, password, f.path, remote_file_path + '/' + File.basename(f.path), port)
  when "FTP"
    Limfinity::LISHL7::Transmission::FTP::upload_file(host, username, password, f, remote_file_path, port)
  when "FTPS"
    Limfinity::LISHL7::Transmission::FTPS::upload_file(host, username, password, f, remote_file_path, port)
  when 'MLLP'
    Limfinity::LISHL7::Transmission::MLLP::upload_file(host, port, f)
  when 'REST'
    Limfinity::LISHL7::Transmission::HTTP::upload_file(host, username, password, f)
  else
    "Server type #{type} not configured"
  end
end

def download_hl7_files
  host = get_value('HL7 Host Name')
  port = get_value('HL7 Host Port')
  port = nil if !port.nil? && port.blank?
  port = Integer(port) unless port.nil?
  username = get_value('HL7 Username')
  password = get_value('HL7 Password')
  type = get_value('HL7 Server Type')
  remote_file_path = get_value('Orders File Path')

  case type
  when "SFTP"
    Limfinity::LISHL7::Transmission::SFTP::get_files(host, username, password, remote_file_path, port)
  when "FTP"
    Limfinity::LISHL7::Transmission::FTP::get_files(host, username, password, remote_file_path, port)
  when "FTPS"
    Limfinity::LISHL7::Transmission::FTPS::get_files(host, username, password, remote_file_path, port)
  when 'MLLP'
    Limfinity::LISHL7::Transmission::MLLP::get_files(host, port)
  when 'REST'
    Limfinity::LISHL7::Transmission::HTTP::get_files(host, username, password)
  else
    "Server type #{type} not configured"
  end
end
