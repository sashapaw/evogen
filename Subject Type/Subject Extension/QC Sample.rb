def after_create_final
  self.barcode_tag = self.name
  validate
  self.save!
end

def after_update_final
  validate
end

def validate
  raise "Control Type is required for sample #{self.name}" if self.get_value('Control Type').blank?
end

def qc?
  true
end
