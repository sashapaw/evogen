def after_create_final
  validate
  context.start_workflow('Test Panel',self)
end

def after_update_final
  validate
  ctx = ScriptRunner.context
  pcode = ctx.find_udf('Code')
  pdesc = ctx.find_udf('Description')
  if changes.include?(pcode) || changes.include?(pdesc)
    update_column(:name, "#{get_value(pcode)} #{get_value(pdesc)}")
  end
end

def validate
  non_orderable_tests = context.find_subjects(query: "FROM Test WHERE \"Non-Orderable\" is true and <-\"Test Panel\"::\"Tests\" = #{self.id}")
  raise "Non-Orderable Tests #{non_orderable_tests.map(&:name).join(', ')} cannot be added to Test Panel" if non_orderable_tests.present?
  
  facility = get_value('Facility')
  tests = if facility
    context.find_subjects(query: "FROM Test WHERE <-\"Test Panel\"::\"Tests\" = #{self.id} AND (\"Facility\" = null OR \"Facility\" != #{facility.id})")
  else
    context.find_subjects(query: "FROM Test WHERE <-\"Test Panel\"::\"Tests\" = #{self.id} AND \"Facility\" != null")
  end
end