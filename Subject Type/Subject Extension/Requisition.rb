def before_create_callback
  self.name = self.barcode_tag = 'R%08i' % self.serial_number
  Limfinity::SystemHooks.requisition_preinit(self)
end

def after_create_final
  Limfinity::SystemHooks.requisition_init(self)
  validate
end

def after_update_final
  validate
end

def validate
  req_types = self.get_value('Specimen Types')
  tests = self.get_value('Tests')
  tests.each{|t|
    t_types = t.get_value('Specimen Types')
    if t_types.present? && (t_types & req_types).blank?
      raise "Specimen Types (#{t_types.map(&:name).join(', ')}) of '#{t.name}' test are incompatible with Specimen Types (#{req_types.map(&:name).join(', ')}) of '#{self.name}' requisition"
    end
  }
  ::Limfinity::Patient.insurance_check(self)
  npi = get_value('Test Performer NPI Number')
  raise "Test Performer NPI Number shall have 10 digits but set to '#{npi}'" if npi && !/^\d{10}$/.match(npi)
end

def attachments_dir(create=false)
  dir = "attachments/#{self.created_at.strftime("%Y_%m")}/subject_R#{('%08i' % self.serial_number)}"
  FileUtils.mkdir_p(dir) if create === true && !File.exists?(dir)
  dir
end

def attachments_view(context, bref_name, file_udf_name, udf_names)
  bref = context.find_udf(bref_name)
  return nil unless context.can_read(bref)
  st = SubjectType.find_by_name('Attachment') #FIXME get from bref
  file_udf = context.find_udf(file_udf_name)
  udfs = (udf_names || []).map{|udf_name| context.find_udf(udf_name)}
  {
    title: UIUtils.h(bref_name),
    xtype: 'attachments',
    limit: 100,
    subject_type_id: st.id,
    subj_id: id,
    bref_id: bref.id,
    file_field: {id: file_udf.id, name: file_udf.display_name},
    user_fields: udfs.map{|udf| {id: udf.id, name: udf.display_name}}
  }
end

def custom_view(context, params = {})
  user = User.curr_user
  ug = UserGroup.find_by_name('Lab Managers')
  lu = UserGroup.find_by_name('Lab Users')
  ru = UserGroup.find_by_name('RURO Admins')
  can_results = ug.users.include?(user) || lu.users.include?(user) || ru.users.include?(user)

  #If LabUser or LabManager or RuRo Admin
  return nil unless can_results

  if params[:node]
    sample = context.find_subject(uid: params[:node])
    raise "No Sample by ID=#{params[:node]}" if !sample || sample.subject_type.name != 'Sample'
    return sample.custom_view(context, params)
  end
  
  tree_only = params[:tree_only] == true || params[:tree_only] == 'true'
  columns = [
    {text: 'Sample / Test / Analyte', width: 270},
    {text: 'Value', width: 70},
    {text: 'Unit', width: 70},
    {text: 'Range', width: 90},
    {text: 'Flag', width: 60},
    {text: 'Outcome', width: 120},
    {text: 'Finalized', width: 80},
    {text: 'Comment'}
  ]
  samples = get_value('Samples') || []
  treegrid = {
    xtype: 'treegrid',
    title: 'Test Results',
    viewConfig: {emptyText: '<p align="center">No Test Results To Display</p>'},
    columns: columns,
    data: samples.map{|sample|
      {
        id: "#{sample.id}",
        c0: "<b>#{UIUtils::h(sample.name)}</b> (#{sample.get_value('Specimen Type')})",
        a0: {oid: sample.id},
        expanded: samples.size == 1
        }
    },
    ajaxParams: {subject_id: id}
  }
  return treegrid.to_json if tree_only
  
  items = [
    {xtype: 'subjectprops', title: 'General', subjectId: id}
  ]
  items << treegrid if samples.present?
  pocs = get_value('Point of Care Test Results')
  if pocs.present?
    entries = pocs.first.get_value('Point of Care Test Result Entries') || []
    data = entries.map{|u|
      r = u.get_value('PoC Result')
      [u.id, u.get_value('Point of Care Test').name, r=='Positive' ? 'X' : '', 
        r=='Negative' ? 'X' : '', 
        r=='Not Tested' ? 'X' : '']
    }
    items << Limfinity::Requisition.poc_view(data, title: 'POC Test Results')
  end
  confirmation = get_value('Confirmation Test Results')
  if confirmation.present?
    data = confirmation.map{|u|
      r = u.get_value('PoC Result')
      [u.id, u.get_value('Confirmation Test').name, r=='Positive' ? 'X' : '', 
        r=='Negative' ? 'X' : '', 
        r=='Not Tested' ? 'X' : '']
    }
    items << Limfinity::Requisition.poc_view(data, title: 'Confirmation Test Results')
  end
  if get_value('Attachments').present?
    att_cmp = attachments_view(context, 'Attachments', 'File', [])
    items << att_cmp if att_cmp
  end
  {
    xtype: 'tabpanel',
    items: items
  }.to_json
end

def patient
  @__patient ||= self.get_value('Patient')
end

def test_result_by_analyte(analyte)
  ctx = ScriptRunner.context
  q = ctx.search_query(subject_type:'Test Result') do |qb|
    qb.and(
      qb.compare('Sample->Requisition', :eq, self),
      qb.compare('Analyte', :eq, analyte)
      )
  end
  ctx.find_subjects(query:q, limit:1 ).first
end

def test_results_by_flag(flag)
  ctx = ScriptRunner.context
  q = ctx.search_query(subject_type:'Test Result') do |qb|
    qb.and(
      qb.compare('Sample->Requisition', :eq, self),
      qb.compare('Flag', :eq, flag)
      )
  end
  ctx.find_subjects(query:q)
end


def history_results_by_analytes(analytes, limit: 5)
  ctx = ScriptRunner.context
  q = ctx.search_query(subject_type:'Requisition') do |qb|
    qb.and(
      qb.compare('Patient', :eq, patient),
      qb.compare('"Tests"->"Analytes"', :eq, analytes),
      qb.compare('"Collection Date"', :lt, self.get_value('Collection Date'))
      )
  end
  subjs = ctx.find_subjects(query:q, order:{'Collection Date'=>[:desc, 'nulls last']}, limit:limit )
  subjs.map do |r|
    {requisition:r, test_results: analytes.inject({}) {|h,a| h[a] = r.test_result_by_analyte(a); h}}
  end
end

def history_results_by_flags(flags, limit: 5)
  ctx = ScriptRunner.context
  q = ctx.search_query(subject_type:'Requisition') do |qb|
    qb.and(
      qb.compare('Patient', :eq, patient),
      qb.compare('"Collection Date"', :lt, self.get_value('Collection Date'))
      )
  end
  subjs = ctx.find_subjects(query:q, order:{'Collection Date'=>[:desc, 'nulls last']}, limit:limit )
  subjs.map do |r|
    {requisition:r, test_results: flags.inject({}) {|h,f| h[f] = r.test_results_by_flag(f); h}}
  end
end