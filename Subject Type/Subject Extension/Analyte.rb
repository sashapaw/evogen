def after_create_final
  validate
  context.start_workflow('Analyte',self)
end

def after_update_final
  validate
end

def validate
  # Make sure both or neither of Instrument Type and Instrument Code are specified
  instrument_type = self.get_value('Instrument Type')
  instrument_code = self.get_value('Instrument Code')
  raise "Both Instrument Type and Instrument Code fields should be completed or left blank" unless (instrument_type.blank? && instrument_code.blank?) || (instrument_type.present? && instrument_code.present?)
  
  # Check compatibility with Tests by Non-Reportable flag
  non_reportable = self.get_value('Non-Reportable')
  tests = self.get_value('In Tests') || []
  tests.each{|t|
    non_reportable_test = t.get_value('Non-Reportable')
    raise "#{non_reportable_test ? 'Non-' : ''}Reportable '#{t.name}' test cannot contain #{non_reportable ? 'Non-' : ''}Reportable '#{self.name}' analyte" if  non_reportable != non_reportable_test
  }
  
  # Check compatibility with Tests by Specimen Type
  spec_type = self.get_value('Specimen Type')
  if spec_type.present?
    tests.each{|t|
      spec_types = t.get_value('Specimen Types')
      if spec_types.present? && !spec_types.include?(spec_type)
        specs = spec_types.map(&:name).join("', '")
        raise "'#{self.name}' analyte is only for '#{spec_type.name}' specimen type and cannot be included in '#{t.name}' test which is only for '#{specs}'" 
      end
    }
  end
  
  low = get_value('Low Limit')
  high = get_value('High Limit')
  if low && high
    raise "Low Limit (#{low}) shall not be greater than High Limit (#{high})" if low > high
  end
end

def custom_view(context, params = {})
  if params[:node]
    trs = get_value('Applicable Ranges') || [] #.sort{|r1,r2|r1.name <=> r2.name}    
    
    nodes = trs.map{|tr|
    {
        id: tr.id,

        c0: ::Limfinity::LIS.format_value(tr.get_value('Critical Low'), self),
        c1: ::Limfinity::LIS.format_value(tr.get_value('Low'), self),
        c2: ::Limfinity::LIS.format_value(tr.get_value('High'), self), 
        c3: ::Limfinity::LIS.format_value(tr.get_value('Critical High'), self), 
        c4: tr.get_value('Age From'), 
        c5: tr.get_value('Age To'), 
        c6: tr.get_value('Age Unit'), 
        c7: tr.get_value('Gender'), 
        leaf: true}
    }
    to_display = nodes.select{|node| node}
    data = to_display.to_json
    return data
  end

  tree_only = params[:tree_only] == true || params[:tree_only] == 'true'
  columns = [
    {text: 'Critical Low',width:'12.5%', iconCls:''},
    {text: 'Low*', width:'12.5%', },
    {text: 'High*', width:'12.5%'},
    {text: 'Critical High',width:'12.5%'},
    {text: 'Age From',width:'12.5%'},
    {text: 'Age To',width:'12.5%'},
    {text: 'Age Unit',width:'12.5%'},
    {text: 'Gender',width:'12.5%'}
  ]
    
  treegrid = <<-EOS
  {xtype: 'treegrid', title: 'APPLICABLE RANGES', 
    flex: 1, viewConfig: {forceFit: true, deferEmptyText:false}, enableColumnMove:false, 
    emptyText:'<div align="center">No Ranges To Display</div>', enableColumnResize:false,
    columns: #{columns.to_json}, ajaxParams: {subject_id: #{id}}
  }
EOS
  tree_only ? treegrid : <<-EOS
  {
    items: [
      {xtype: 'subjectprops', subjectId: #{id}},#{treegrid}
    ]
  }
EOS
  
end

def result_choices
  unless @result_choices
    raise "Result Choices applicable only to Qualitative Analytes" unless get_value('Qualitative')
    @result_choices = (get_value('Result Choices') || '').split(/[;\n]/).map(&:strip).reject {|v| v.blank?}
  end
  @result_choices
end

def flags
  unless @flags
    ctx = ScriptRunner.context
    @flags = ctx.find_subjects(query:ctx.search_query(subject_type:'Flag') do |qb| 
      #qb.or qb.compare('Analyte',:eq,self), qb.condition("Analyte is null")
      qb.compare('Analytes',:eq,self)
    end)
    @flags = Limfinity::StandardFlags.flags if @flags.blank?
  end
  @flags
end