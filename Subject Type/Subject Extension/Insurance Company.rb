def after_create_final
  validate
  ScriptRunner.context.start_workflow('Insurance Company', self)
end

def after_update_final
  validate
end

def validate
  code = get_value('Code')
  raise "Code shall have up to 4 digits but set to '#{code}'" if code && !/^\d{1,4}$/.match(code)
  pid = get_value('Payer ID')
  raise "Payer ID shall alphanumeric string but set to '#{pid}'" if pid && !/[[:alnum:]]+/.match(pid)
  ::Limfinity::LIS.validate_address(self['Address'])
end