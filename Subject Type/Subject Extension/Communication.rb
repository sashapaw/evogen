def custom_view(context, params={})
  comms = get_value('Replies', :order=>'created_at DESC') || []
  data = comms.map{|u| ["<a href=\"javascript:void(0)\" onclick=\"RURO.viewSubject(#{u.id})\">#{u.get_value('Reply')}</a>", u.get_value('User'), 
    u.get_value('Reply By').present? ? u.get_value('Reply By').name : 'Lab Team', 
    SysUtils.format_date(u.created_at, User.curr_user)]}

  {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, items: [
    {xtype: 'subjectprops', subjectId: id},
    {xtype: 'label', html:'<div style="padding:5px;color:white;">Replies:</div>'},
    {xtype: 'grid', flex: 1, viewConfig: {forceFit: true, deferEmptyText:false},emptyText:'<div align="center">No Replies To Display</div>',
      columns:[
        {dataIndex:'reply', header:'Reply', width:'60%'}, 
          {dataIndex:'by', header:'By', width:'20%'},
          {dataIndex:'date', header:'Date', width:'15%'}
      ],
      store: {type: 'array', fields:['reply', 'user', 'by', 'date'], data: data}
      }
  ]}.to_json
end