def after_create_final
  context.advance_workflow('Point of Care Test', 'Active', self)
end

def custom_view(context, params={})
  rules = (get_value('Reflex Rules') || []).sort{|r1,r2|r1.name <=> r2.name}
    data = rules.map{|rg| [
      rg.name,
      rg.get_value('Primary POC Test') ? rg.get_value('Primary POC Test').name : '', 
      rg.get_value('Operator'), 
      rg.get_value('Value'), 
      rg.get_value('Reflex Packaged Tests') ? rg.get_value('Reflex Packaged Tests').map(&:name).join(',') : ''
      ]}
    {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, items: [
      {xtype: 'subjectprops', subjectId: id},
      {xtype: 'label', html:'<div style="padding:5px;color:white;">REFLEX RULES</div>'},
      {xtype: 'grid', flex: 1, viewConfig: {forceFit: true, deferEmptyText:false},emptyText:'<div align="center">No Reflex Rules To Display</div>', 
        columns:[
          {dataIndex:'poct', header:'Primary POC Test', width:'20%'},
          {dataIndex:'operator', header:'Operator', width:'10%'},
          {dataIndex:'value', header:'Value', width:'10%'},
          {dataIndex:'followups', header:'Followup Test(s)', width:'60%'}
          ],
        store: {type: 'array', fields:['name', 'poct', 'operator','value','followups'], data: data}}
      ]}.to_json
end