colors = ['#001f3f', '#0074D9', '#7FDBFF', '#39CCCC', '#3D9970', '#2ECC40', '#01FF70', '#FFDC00', '#FF851B', '#FF4136', '#85144b', '#F012BE', '#B10DC9', '#111111', '#AAAAAA', '#DDDDDD']

photo = get_value('Patient Photo File')
if photo
  prop = Property.find_by_display_name('Patient Photo File')
  # position: fixed;top:160;right:240px;
  "<img style='float:right;border-radius:50%;width:45px;height:45px;' src='/subjects/attachment/?id=#{subj.id}&pid=#{prop.id}'>".html_safe
else
  idx = subj.id % colors.size
  name = subj.get_value('First Name')[0] + subj.get_value('Last Name')[0]
  s = "<div style='float:right;background-color:#{colors[idx]}; border-radius: 50%; height: 45px; text-align: center; width: 45px;'>" +
  "<span style='color: #fff; font-family: sans-serif; font-size: 22.5px; line-height: 1; position: relative; top:11.25;'>#{name}</span>"+
  "</div>"
  s.html_safe
end