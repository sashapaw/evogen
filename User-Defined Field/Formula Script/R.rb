rep = get_value('Replies').last

if rep
  fac = rep.get_value('User').get_value('Facility')
  '<i data-qtip="Reply Required" class="fa fa-envelope-square"></i>'.html_safe if fac != @user.get_value('Facility')
end
