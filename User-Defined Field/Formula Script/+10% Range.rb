target_val = self.get_value('Target Value')
target_val.blank? ? nil : Float(target_val)+(self.get_value('SD') || 0.0)