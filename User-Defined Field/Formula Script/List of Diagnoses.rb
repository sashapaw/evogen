# Display a printable list of test codes to put on a barcode label
subj.get_value('Diagnoses').collect{|s| s.get_value('Code')}.join(', ')