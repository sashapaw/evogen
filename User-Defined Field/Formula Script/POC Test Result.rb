if (subj.get_value('Point of Care Test Results').present?)
  poc_test = subj.get_value('Point of Care Test Results').first
  poc_entries = poc_test.get_value('Point of Care Test Result Entries')
  value = 'N'
  poc_entries.each do |entry|
    if entry.get_value('PoC Result') == 'Positive'
      value = 'P'
    end
  end
  value
end
