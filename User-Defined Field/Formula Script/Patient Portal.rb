if subj.my_users.present? && !subj.my_users.first.disabled?
  "<span style='color:green;'><b>ENABLED</b></span>".html_safe
end