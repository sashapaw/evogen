cd = get_value('Date Closed')
if cd
  od = get_value('Date Opened')
  d = (cd - od)/3600/24
  if d < 1
    'Less than a day'
  elsif d < 2
    "1 day"
  else
    "#{d.to_i} days"
  end
else
  ''
end