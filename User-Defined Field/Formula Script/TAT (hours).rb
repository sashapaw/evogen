dateReceived = get_value('Received Date')
dateReleased = get_value('Released Date')
if dateReceived && dateReleased
  tat =  ((dateReleased.to_time - dateReceived) / 1.hours).round
  if tat <=0 
    tat = 1
  end
  tat
else
  ''
end