dateStored = get_value('Date Stored')
if dateStored
  today = Date.today
  days = (today - dateStored)
  if days < 1
    'Less than a day'
  elsif days < 2
    "1 day"
  else
    "#{days.to_i} days"
  end
else
  ''
end

