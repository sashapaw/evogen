cd = get_value('Errors')
if cd.present?
  "<span style='color:red;font-weight:bold;'>Yes</span>".html_safe
else
  "No"
end