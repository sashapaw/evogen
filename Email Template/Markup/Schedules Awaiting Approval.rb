<html>
<head>
	<title></title>
</head>
<body>This is a daily reminder. You have {{ params['count'] }} Testing Schedules to approve.<br />
<br />
To approve, modify or reject, please go to our Physician Portal at <a href="{{ params['base_url'] }}">{{ params['base_url'] }}</a></body>
</html>
