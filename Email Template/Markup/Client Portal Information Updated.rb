Dear {{ params["Full Name"] }},
<br><br>
   We've updated your account information in our Client Portal.
<br><br>

To use the system, please go to our Client Portal at <a href="{{ params['base_url'] }}">{{ params["base_url"] }}</a><br><br>

Sincerely,<br>
{{ subj.name }}<br/>