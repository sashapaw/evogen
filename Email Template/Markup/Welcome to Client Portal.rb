Dear {{ params["Full Name"] }},
<br><br>
   We've created a new account for you in our Client Portal.
<br><br>

To use the system, please go to our Client Portal at <a href="{{ params['base_url'] }}">{{ params["base_url"] }}</a><br><br>

Your username is: <b>{{ params["Login"] }}</b><br>

Your password will be in a separate email.
<br><br>

Sincerely,<br>
{{ subj.name }}<br/>