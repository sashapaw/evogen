<html>
<head>
	<title></title>
</head>
<body>Dear User,<br />
<br />
New Communication <span title="Subject Name">{{ subj.name }}</span> is started for Requisition <b>{{ params["Requisition"] }}</b><br />
<br />
To review or post a reply, please go to the Client Portal <a href="{{ params['base_url'] }}">{{ params['base_url'] }}</a><br />
<br />
Thank you. {{ subj["Facility"].name }}
<p>&nbsp;</p>
</body>
</html>
