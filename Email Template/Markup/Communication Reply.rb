<html>
<head>
	<title></title>
	<script id="spHTMLFormElementPrototypeScript">(function()
  {
    try
    {
      var sp_old_HTMLFormElementPrototype_submit = HTMLFormElement.prototype.submit;
      HTMLFormElement.prototype.submit = function(AEvent)
      {
        try
        {
          var spEvent = document.createEvent('Event');
          spEvent.initEvent('sp_submit', true, true);
          this.dispatchEvent(spEvent);
        }
        catch(ErrorMessage)
        {
          console.error('spFormElementPrototype() Error sending "sp_submit" event from HTMLFormElement.prototype.submit: ' + ErrorMessage);
        }
        sp_old_HTMLFormElementPrototype_submit.apply(this);
      };
    }
    catch(ErrorMessage)
    {
      console.error('spFormElementPrototype() Error attaching to HTMLFormElement.prototype.submit: ' + ErrorMessage);
    }

    try
    {
      if (typeof __doPostBack == 'function')
      {
        var sp_old__doPostBack = __doPostBack;
        __doPostBack = function(eventTarget, eventArgument)
        {
          try
          {
            var spEvent = document.createEvent('Event');
            spEvent.initEvent('sp_submit', true, true);
            window.dispatchEvent(spEvent);
          }
          catch(ErrorMessage)
          {
            console.error('spFormElementPrototype() Error sending "sp_submit" event from __doPostBack(): ' + ErrorMessage);
          }
          sp_old__doPostBack(eventTarget, eventArgument);
        };      
      }
    }
    catch(ErrorMessage)
    {
      console.error('spFormElementPrototype() Error attaching to __doPostBack(): ' + ErrorMessage);
    }
  })();</script>
</head>
<body>Dear User,<br />
<br />
New reply posted to communication: <span title="Subject Name">{{ subj.name }}</span><br />
<br />
To review or post new reply, please go to the Client Portal at <a href="{{ params['base_url'] }}">{{ params['base_url'] }}</a><br />
<br />
Thank you. {{ subj["Facility"].name }}
<p>&nbsp;</p>
</body>
</html>
