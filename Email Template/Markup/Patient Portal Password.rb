Dear {{ subj["First Name"] }} {{ subj["Last Name"] }},
<br><br>

Your password for the Patient Portal is: <b>{{ params["Password"] }}</b><br>
<br><br>

Sincerely,<br>
{{ subj["Facility"].name }}<br/>