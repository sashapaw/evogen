Dear {{ subj["First Name"] }} {{ subj["Last Name"] }},
<br><br>
   We've created a new account for you in our Patient  Portal.
<br><br>

To use the system, please go to our Patient Portal at <a href="{{ params['base_url'] }}">{{ params['base_url'] }}</a><br><br>

Your username is: <b>{{ params['EMail'] }}</b><br>

Your password will be in a separate email.
<br><br>

Sincerely,<br>
{{ subj["Facility"].name }}<br/>