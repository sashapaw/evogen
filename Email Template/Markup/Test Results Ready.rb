<html>
<head>
	<title></title>
</head>
<body>
<p>Dear {{ params["greeting"] }},<br />
<br />
Results are ready for Requisition <b>{{ subj.name }}</b><br />
<br />
To download the report, please go to our Client Portal at <a href="{{ subj.url }}">{{ subj.url }}</a><br />
<br />
<br />
<br />
Sincerely,</p>
{{ params["labname"] }}

<p><br />
<i>This information is considered patient confidential, is restricted by law, or has been specifically restricted by a patient/client in a signed HIPAA consent form. Information is only used as is reasonably necessary to process your testing or results that may require communication between {{params["labname"]}} and other health care providers necessary to complete our testing of specimens and return them to you.</i></p>
</body>
</html>
