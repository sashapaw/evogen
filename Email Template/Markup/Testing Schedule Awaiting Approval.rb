<html>
<head>
	<title></title>
</head>
<body>This is a notification to inform you that your action is required for Testing Schedule for Patient with MRN: {{ subj["MRN"] }}<br />
<br />
To approve, modify or reject, please go to our Physician Portal at <a href="{{ params['base_url'] }}">{{ params['base_url'] }}</a></body>
</html>

