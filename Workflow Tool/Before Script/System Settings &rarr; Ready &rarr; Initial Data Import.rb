extend ScriptRunner::UI
radio_values = ['Skip', 'Update']
sheets = SpreadsheetImporter.sheets

params[:custom_fields] = encode_fields([
  {xtype: 'box', html:"<a href='/udf/attachment?id=#{LIS.common_config.id}&pid=#{find_prop('Import Template File').id}'>Download Template</a>", padding: '0 0 5 0'},
  udf('File', nil, fieldLabel: 'File to Upload', required: true),
  {xtype: 'container', layout: {type: 'table', columns: 2}, defaults: {padding: '5 40 0 0'}, items: [
    {xtype: 'box', html: '<b>Sheets to Import</b>'},
    {xtype: 'box', html: '<b>What to do with the existing records?</b>'},
    {xtype: 'checkbox', udfName: 'All Enabled', boxLabel: 'All', checked: true},
    custom_radio_group(name:"custom[All]", udfName: 'All', values: radio_values, value: radio_values[1], width: 300)
  ] + sheets.map{|sheet| [
      prepare_react(xtype: 'checkbox', udfName: "#{sheet} Enabled", boxLabel: sheet, checked: sheet != 'Insurance Companies', react: {
        change: 'this.setValue(value)',
        only: 'All Enabled'
      }),
      custom_radio_group(name:"custom[#{sheet}]", disabled: sheet == 'Insurance Companies', width: 300,
        values: sheet == 'Reference Ranges' ? ['Delete'] : radio_values,
        value: sheet == 'Reference Ranges' ? 'Delete' : radio_values[1],
        react: {
          change: 'if (name == "All") {this.setValue(value)} else {this.setDisabled(!value)}',
          only: ['All', "#{sheet} Enabled"]
      })
    ]}.flatten
  }
])

params[:no_transaction] = true
params[:background_execution] = true
params[:show_progress] = true