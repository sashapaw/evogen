params[:required] = {
  'Analyte'=> true,
  'Control Type'=>true,
  'Target Value'=>true
}

params[:udf_config] = {
  'Analyte' => { filter: 'terminated is false'}
}