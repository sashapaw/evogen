raise 'Select Exactly One QC Report' if subjects.size != 1

report = subjects[0]

raise 'No report file exists to preview' if report.get_value('File').nil?

prop = Property.find_by_display_name('File')

params[:custom_fields] = {
  xtype:'panel',
  height: 500,
  anchor:'100%',
  html:"<iframe width=100% frameborder=0 height=100%  src=\"/subjects/file/?id=#{report.id}&prop_id=#{prop.id}&inline=on\"></iframe>"
}.to_json
