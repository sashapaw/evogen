finalized = true
subj.get_value('Test Results').each do |result|
  if !result.get_value('Finalized')
    finalized = false
    break
  end
end

if finalized
  raise "There are No Unfinalized Test Results to Edit."
end