extend UI
params[:custom_fields] = UIUtils.encode_fields([
  udf('Facility', subj, filter:'terminated is false', labelWidth:200, allowCreate:false, required:true),
  udf('Requisition', subj, labelWidth:200, allowCreate:false, required:false, defaultValue: subj),
  udf('External Source ID', subj, labelWidth:200,required:false, defaultValue: subj.get_value('External Source ID')),
  udf('Message Template', subj, labelWidth:200, allowCreate:true,
    hidden:!SubjectType.find_by_name('Message Template').subjects.present?),
  udf('Message', subj, labelWidth:200, anchor:'100%', height:160),
  udf('File', subj, labelWidth:200),
  udf('Also Send Email to the Support Representative', subj, labelWidth:200)
])