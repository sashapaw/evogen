lab = subj.get_value('Lab Test')
if lab  # full requisition
  params[:custom_fields] = Requisition::LayoutPortal.requisition_layout_portal(subj, subj.get_value('Tests'))
  params[:submit_disabled_as_empty] = true
else # just PoC tests
  extend UI
  patient = subj.get_value('Patient')
  facility = patient.get_value('Facility')
  defaultSpecimenTypes = LIS.system_settings.get_value('Default Specimen Types') || []
  #raise defaultSpecimenTypes.inspect
  params[:custom_fields] = encode_fields([
    {xtype:'label', anchor:'100%', html:"<b>Patient: #{subj.get_value('Patient')}</b>"}, 
    udf('Requesting Physician', subj, addCustomFields: Physician::Layout.physician_layout(facility, 2), required:true,
      filter:"Facility = #{facility.id} and terminated is false",
      allowCreate:facility.get_value('Allow Physicians Management')),
    udf('Testing Date', subj, defaultValue:Date.today, required:true),
    udf('Specimen Types', subj, value: defaultSpecimenTypes, required:true),
    {xtype:'hidden', name:'custom[poc_grid_results]', itemId:'poc_grid_results_id'}, 
    Requisition.poct_results_tab(subj, 350)
  ])	
end
