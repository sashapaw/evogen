params[:allowCreate]={'Analytes'=>false}
=begin
extend UI
params[:custom_fields] = encode_fields([
  {xtype:'container', height: 50, anchor: '100%', layout: 'fit', items:[
    udf('Analytes', subj, required: true, allowCreate: false, width: '100%')
  ]}
])
=end

params[:udf_config] = {
  'Analytes'=> {
      filter: "terminated is false"
  }
}