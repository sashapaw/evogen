raise "You must select atleast one Requisition" if subjects.size == 0
extend UI
system_setting = SubjectType.find_by_name('System Settings').subjects.first

params[:background_execution] = true
params[:custom_fields] = encode_fields([
  udf('Released Date',subj, defaultValue:Time.now, required:true),
  udf('Released By',subj, required:true, anchor:'100%'),
  field_set(title:'Test Performed By', items:[
    field_row([
      udf('Test Performer First Name', subj, required:true, fieldLabel:"First Name", defaultValue:system_setting.get_value('Lab Performer First Name')),
      udf('Test Performer Last Name', subj, required:true, fieldLabel:"Last Name", defaultValue:system_setting.get_value('Lab Performer Last Name'))
    ]),
    udf('Test Performer NPI Number', subj, required:true, fieldLabel:"NPI Number", defaultValue:system_setting.get_value('Lab Performer NPI Number'))
  ])
])