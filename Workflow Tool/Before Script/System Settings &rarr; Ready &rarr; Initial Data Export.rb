extend ScriptRunner::UI
sheets = SpreadsheetImporter.sheets

params[:custom_fields] = encode_fields([
  {xtype: 'container', items: [
    {xtype: 'checkbox', udfName: 'All Enabled', boxLabel: '<b>All</b>', checked: true},
  ] + sheets.map{|sheet|
      prepare_react(xtype: 'checkbox', name:"custom[#{sheet}]", boxLabel: sheet, checked: true, react: {
        change: 'this.setValue(value)',
        only: 'All Enabled'
      })
    }
  }
])

params[:background_execution] = true
params[:show_progress] = true