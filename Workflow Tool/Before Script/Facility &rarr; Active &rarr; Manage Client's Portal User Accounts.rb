unless LIS.system_settings.get_value('Client Portal Enabled')
  raise "This module is not available under your current LimitLIS license. Please contact your account representative or email sales@ruro.com."
end

extend UI
params[:custom_fields] = encode_fields([
    custom_radio_group(name:'custom[user-action]', udfName:'rb-auto', margin:'0 0 20 0', items: [
        {boxLabel:'Add User', inputValue:'add', checked:true},
        {boxLabel:'Edit User', inputValue:'edit'},
        {boxLabel:'Archive User', inputValue:'archive'},
        #{boxLabel:'Restore User', inputValue:'restore'},
        {boxLabel:'Password Reset', inputValue:'reset'}
    ]),

    udf('User', nil, anchor:'100%', required:true, margin:'0 0 20 0', filter:"Facility = #{subj.id} and Patient is null",
      react: {
        shown_when: 'data["rb-auto"] != "add"',
        change: '
          if (name == "User" && value) {
            RURO.runHelperScript("user_info_lookup_helper", {id:value}, function(result) {
              if (!result.result) {
                form.setValue("Full Name", result.fullname);
                form.setValue("EMail", result.email);
                form.setValue("Login", result.username);
                form.setValue("Allow to View Test Results", result.viewTestResult);
              }
            });
          }',
        only: ['rb-auto', 'User']
      }
    ),
  
    field_container([
        udf('Login', nil, anchor:'100%', required:true),
        udf('Full Name', nil, anchor:'100%', required:true),
        udf('EMail', nil, anchor:'100%', vtype:'email', required:true),
        udf('Allow to View Test Results', nil, anchor:'100%')
      ], react: {
        shown_when: 'value == "add" || value == "edit"',
        only: 'rb-auto'
    })
])

