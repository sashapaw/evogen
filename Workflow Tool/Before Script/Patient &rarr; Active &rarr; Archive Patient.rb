raise "Select at least one Patient" unless subjects.length>0
msg = "<b>ATTENTION</b>: Patient's data will be archived, access to Patient Portal will be suspended."
params[:tool_message] = msg.html_safe