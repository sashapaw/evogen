raise "Select exactly one Analyte" unless subjects.length == 1
params[:custom_fields] = Analyte::Layout.analyte_layout( subjects[0] )
params[:submit_disabled_as_empty] = true
