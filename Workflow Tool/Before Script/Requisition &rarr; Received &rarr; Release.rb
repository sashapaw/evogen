raise "Select One or More Requisitions" if subjects.size == 0

extend UI
system_setting = LIS.system_settings
show_release_signature = system_setting.get_value("Require requisition approval on release")
require_lab_npi = system_setting.get_value('Require lab performer NPI on release')
lab_npi = system_setting.get_value('Lab Performer NPI Number')


subjects.each do |s|
  if s.get_value('Pre-Registered Samples').present?
    msg = "<b>ATTENTION</b>: All Pre-registered Samples associated with Requisitions will be deleted."
    params[:tool_message] = msg.html_safe	  
    break
  end
end

params[:custom_fields] = UIUtils.encode_fields([
  udf('Released Date',subj, defaultValue:Time.now, required:true),
  udf('Released By',subj, disabled: true, required:true, anchor:'100%'),
  udf('Requisition Release Approval', subj, hidden:!show_release_signature, required: show_release_signature,
    invalidText: 'Please confirm that you have reviewed and approved this report for release',
    fieldLabel: 'I have reviewed and approved this report for release',
    validateValue: UIUtils.raw_js("function (value) {return #{show_release_signature ? 'value && this.checked' : 'true'};}")),
  #{xtype:'label', html:'<br><b>Test Performed By:</b>'},
  #field_row([
  #  udf('Test Performer First Name', subj, required:true, fieldLabel:"First Name",
  #      defaultValue:system_setting.get_value('Lab Performer First Name')),
  #  udf('Test Performer Last Name', subj, required:true, fieldLabel:"Last Name",
  #      defaultValue:system_setting.get_value('Lab Performer Last Name'))
  #]),
  #require_lab_npi ? udf('Test Performer NPI Number', subj, required:true, fieldLabel:"NPI Number", defaultValue:lab_npi) : nil
])