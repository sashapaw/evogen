extend ScriptRunner::UI
params[:required] = {
  'Full Name'=>true,
  'EMail'=>true
}

params[:custom_fields] = encode_fields([
  udf('Address', nil, height:40, validateAddress:true, anchor:'100%'),
  udf('Comments', nil, anchor:'100%')
])