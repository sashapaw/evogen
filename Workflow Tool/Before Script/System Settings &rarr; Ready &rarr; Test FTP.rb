extend UI
def test_ftp_layout(subj)
  UIUtils.encode_fields([	  
    udf('Hostname', subj, labelWidth:160,anchor:'100%'),
    udf('Port', subj, labelWidth:160,anchor:'100%'),
    udf('Username', subj, labelWidth:160,anchor:'100%'),
    udf('Password', subj, labelWidth:160,anchor:'100%'),
    udf('Server Type', subj, info:'Choices are: FTP, FTPS, SFTP', anchor:'100%', allowCreate:false, labelWidth:160),
    udf('Remote File Path', subj, defaultValue:"/", labelWidth:160, anchor:'100%')
  ])
end
params[:custom_fields] = test_ftp_layout(nil)