msg = "Please specify the date range for the QC Report."
params[:background_execution] = true
params[:show_progress] = true

params[:tool_message] = msg.html_safe

params[:required] = {
  'Start Date'=> true,
  'End Date'=>true
}

params[:allowCreate] = {'Analytes'=>false, 'Instruments'=>false}
params[:udf_config] = {
  'Analytes' => { filter: 'terminated is false' }
}
