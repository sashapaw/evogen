
# Get the name of the requisition layout from system settings
# Different labs have different requisition layouts
params[:custom_fields] = Requisition::LayoutPortal.requisition_layout_portal(subj)
params[:submit_disabled_as_empty] = true