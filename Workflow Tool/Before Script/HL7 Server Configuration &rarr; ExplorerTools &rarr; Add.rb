extend UI
params[:custom_fields] = encode_fields([
    udf('Name', nil, required: true, anchor: '100%')
  ] + udfs_for_subject_type('HL7 Server Configuration', nil, {
    'Facility Name' => {fieldLabel: 'Lab Identifier'},
    'Facility Number' => {fieldLabel: 'Lab Account Number'},
    'HL7 Message Type' => {disabled: true, react: {
        change: <<-EOJS,
          this.setDisabled(!value);
          this.setValue(null);
          if(value) {
            var store = this.getStore();
            form.setValuesAsync({id: value, udfs: "HL7 Message Type"}, function(result) {
                    store.loadData(Ext.Array.map(result["HL7 Message Type"], function(el) {return [el];}));
                  });
          }
EOJS
        only: 'HL7 Provider'
      }}
  }))
