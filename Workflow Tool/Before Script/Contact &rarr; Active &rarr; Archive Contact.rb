raise "Select at least one Contact" unless subjects.length>0

msg = "<b>ATTENTION</b>: Contact's data will be archived. Contact will be removed from all Facilities."
params[:tool_message] = msg.html_safe