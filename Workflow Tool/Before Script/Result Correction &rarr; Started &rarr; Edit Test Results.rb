raise "Nothing to edit" if subjects.blank?

params[:background_execution] = true
payloads = {}
editors = []
subjects.each{|s|
  analyte = s.get_value('Analyte')
  qualitative = analyte.get_value('Qualitative')
  payloads[s.id] = {'Value'=>{editor: editors.size}}
  editors << if qualitative
    values = analyte.result_choices.map{|v| {v:v, t:UIUtils.h(v)}}
    {xtype:'select',
      store:UIUtils.raw_js("Ext.create('Ext.data.Store', {fields:['v','t'], data:#{values.to_json}})"),
      queryMode:'local', displayField:'t', valueField:'v'}
  else
    {xtype:'numberfield', decimalPrecision:analyte.get_value('Decimal Places') || 6}
  end
}
params[:record_payloads] = payloads
params[:keep_selection] = true
params[:column_configs] = {'Value'=> {editor: {xtype:'fieldswitcher', items:editors}}}
params[:std_fields] = []