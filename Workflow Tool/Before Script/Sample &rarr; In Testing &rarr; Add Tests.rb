extend UI
req = subj.get_value('Requisition')
facility = req.get_value('Facility')

system_settings = LIS.system_settings
any_specimen_type = system_settings.get_value('Allow Any Specimen Type on Accessioning')

specimen_types = any_specimen_type ? find_subjects(query: 'FROM "Specimen Type" WHERE terminated is false') : req.get_value('Specimen Types')
tests_by_specimen = {}
specimen_types.each{|specimen|
  tests = find_subjects(query:"FROM Test WHERE <-Requisition::\"Tests\" = #{req.id} AND (\"Specimen Types\" = #{specimen.id} OR \"Specimen Types\" = null)")
  if tests
    tests_by_specimen[specimen.id] = tests.map{|test| {id: test.id, name: test.name}}
  end
}

specimen = subj.get_value('Specimen Type') || req.get_value('Specimen Types')[0]

params[:custom_fields] = encode_fields([
  custom_radio_group(name:'custom[user-action]', udfName:'rb-auto', margin: '0 0 20 0',
    values: ['Regular Tests', 'Non-Orderable Tests'], value: 'Regular Tests'),

  udf('Tests', nil, anchor:'100%', required:true, 
    filter: "<-Requisition::Tests = #{req.id} AND \"Non-Orderable\" = null AND (\"Specimen Types\" = #{specimen ? specimen.id : 0} OR \"Specimen Types\" = null) AND NOT (<-Sample::Tests = #{subj.id})",
    react: {shown_when: "value == 'Regular Tests'", only: 'rb-auto'}, allowCreate:false
  ),
  
  udf('Non-Orderable Tests', nil, anchor:'100%', required:true, filter:"terminated=false AND \"Non-Orderable\" != null AND (Facility is null OR Facility = #{facility.id}) AND NOT (<-Sample::Tests = #{subj.id})",
    react: {shown_when: "value == 'Non-Orderable Tests'", only: 'rb-auto'}, allowCreate:false
  )
])