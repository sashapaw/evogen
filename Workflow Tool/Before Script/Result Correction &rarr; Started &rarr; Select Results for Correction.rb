extend UI

params[:custom_fields] = UIUtils.encode_fields([
  udf('Test', subj, required: true, anchor:'100%',
    filter:"id in (SELECT \"Requisition\"->\"Tests\"->id FROM Subject WHERE id = #{subj.id})"),
  udf('Analyte', nil, required: true, 
    react: {
      shown_when: 'value',
      change: "this.setFilter('<-Test::Analytes='+value);",
      only:'Test'
  })
])

