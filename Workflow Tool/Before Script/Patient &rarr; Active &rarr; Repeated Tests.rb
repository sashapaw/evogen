extend UI
facility = subj.get_value('Facility')
facility_id = facility && facility.id || 0
params[:custom_fields] = encode_fields([
  udf('Test Panels', subj, anchor:'100%', labelWidth:160, allowCreate:false,
    filter:"terminated is false and (Facility = #{facility_id} or Facility is null)"),
  udf('Tests', subj, labelWidth:160, allowCreate:false, anchor:'100%',
    filter:"terminated is false and (Facility = #{facility_id} or Facility is null) and \"Non-Orderable\" = null",
    react:{
      change: '
            var me = this;
            if (Ext.isArray(value) && value.length) {
              var newPanels = me.panels ? Ext.Array.difference(value, me.panels) : me.panels;
              if (newPanels && newPanels.length) {
                RURO.runHelperScript("tests_in_panels", newPanels, function(result) {
                  var resultTests = Ext.decode(result);
                  var tests = data.Tests;
                  var newTests = Ext.Array.filter(resultTests, function(it) {return Ext.Array.indexOf(tests, it.id) < 0;});
                  form.setValue("Tests", tests.concat(newTests));
                });
              }
            }
            me.panels = value;',
      only: 'Test Panels'
  })
])