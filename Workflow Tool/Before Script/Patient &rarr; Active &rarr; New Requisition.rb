
# Get the name of the requisition layout from system settings
# Different labs have different requisition layouts

params[:defaults]={
  'Facility'=>subj.get_value('Facility'),
  'Patient'=>subj,
  'Tests'=>subj.get_value('Tests'),
  'Medications'=>subj.get_value('Medications')
}

params[:custom_fields] = Requisition::LayoutPortal.requisition_layout_portal(subj)
params[:submit_disabled_as_empty] = true