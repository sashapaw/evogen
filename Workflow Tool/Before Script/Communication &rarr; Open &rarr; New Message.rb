extend UI
params[:custom_fields] = UIUtils.encode_fields([
  udf('Facility', subj, filter:'terminated is false', allowCreate:false, required:true),
  field_container([
    udf('Requisition', subj, allowCreate:false, required:false, react: {
      change: 'this.setFilter("Facility="+value)',
      only: 'Facility'
    }),
    udf('External Source ID', subj, react: {
      change: '
        if (value)
          form.setValuesAsync({id: value, udfs: "External Source ID"});
        else
          this.setValue("");',
      only: 'Requisition'
    })
  ], react: {
    shown_when: 'value',
    only: 'Facility'
  }),
  udf('Message Template', subj, allowCreate:true,
    hidden: !SubjectType.find_by_name('Message Template').subjects.present?),
  udf('Message', subj, anchor:'100%', height:160),
  udf('File', subj, labelWidth:200),
  udf('Also Send Email to the Support Representative', subj)
])