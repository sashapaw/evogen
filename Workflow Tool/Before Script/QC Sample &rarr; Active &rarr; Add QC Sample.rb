params[:required] = {
  'Analytes'=> true,
  'Control Type'=>true
}
params[:allowCreate] = {'Analytes'=>false}

params[:udf_config] = {
  'Analytes' => { filter: 'terminated is false'}
}