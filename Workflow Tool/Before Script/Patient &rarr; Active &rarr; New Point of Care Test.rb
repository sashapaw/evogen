extend UI

facility = subj.get_value('Facility')

defaultSpecimenTypes = LIS.system_settings.get_value('Default Specimen Types') || []
params[:custom_fields] = encode_fields([
  {xtype:'label', anchor:'100%', html:"<b>Patient: #{subj}</b>"}, 
  udf('Requesting Physician', subj, addCustomFields: Physician::Layout.physician_layout(facility, 2), required:true,
    filter:"Facility = #{facility.id} and terminated is false",
    allowCreate:facility.get_value('Allow Physicians Management')),
  udf('Testing Date', subj, defaultValue:Date.today, required:true),
  udf('Specimen Types', subj, value: defaultSpecimenTypes, required:true),
  {xtype:'hidden', name:'custom[poc_grid_results]', itemId:'poc_grid_results_id'}, 
  Requisition.poct_results_tab(nil, 500)
])