raise "Please select exactly 1 Reflex Rule" if subjects.size != 1
subj = subjects[0]

params[:udf_config] = {
  'Primary POC Test'=> {
      value:subj.get_value('Primary POC Test'), allowCreate:false
  },
  'Operator'=> {
      value:subj.get_value('Operator')
  },
  'Value'=> {
      value:subj.get_value('Value')
  },
  'Reflex Packaged Tests'=> {
      value:subj.get_value('Reflex Packaged Tests'), allowCreate:false
  }
}