params[:required] = %w(File)
params[:background_execution] = true
params[:custom_fields] = {xtype:'uploader',
  						  name:'custom[files]',
  						  fieldLabel:'Attachment Files',
  						  labelWidth: 'auto',
                          required:true}.to_json
  

