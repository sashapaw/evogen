params[:required]={
  'Code'=>true,
  'Tests'=>true,
  'Description'=>true
}

params[:udf_config] = {
  'Facility'=>{filter:"terminated != true", allowCreate:false},
  'Tests'=>ScriptRunner::UI.prepare_react({filter:"terminated is false AND \"Non-Orderable\" is null AND Facility is null", allowCreate:false, react:{
          change: '
            this.setFilter(value ? "terminated is false AND \"Non-Orderable\" is null AND (Facility = "+value+" OR Facility is null)" : "terminated is false AND \"Non-Orderable\" is null AND Facility is null");',
          only: 'Facility'
        }})
}
