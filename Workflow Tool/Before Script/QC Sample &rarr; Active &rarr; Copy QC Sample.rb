raise "Please select exactly 1 QC Sample to copy" if subjects.size != 1

subj = subjects.first
params[:defaults] = {
  #'Name' => subj.name,
  'Control Type' => subj.get_value('Control Type'),
  'Analytes' => subj.get_value('Analytes')
}

params[:required] = {
  'Control Type' => true,
  'Analytes' => true
}

params[:udf_config] = {
  'Analytes' => { filter: 'terminated is false'}
}