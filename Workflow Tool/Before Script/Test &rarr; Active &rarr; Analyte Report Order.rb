subj = Subject.find(params[:subj_id])

ro = subjects
an = subj.get_value('Analytes') || []

cur_an = []
rm_ro = ro.select do |r| 
  a = r.get_value('Analyte')
  found = an.include?(a)
  cur_an << a if found
  !found
end

ro -= rm_ro
rm_ro.each {|r| r.destroy }

an_add = an.select {|a| !cur_an.include?(a) }

an_add.each do |a| 
  ro << create_subject('Report Order') do |s| 
    s.set_value('Analyte', a)
    s.set_value('Parent Test', subj)
  end
end

subj.set_value('Report Orders',ro)

params[:reload_subject] = true # needed to reload the Editor
params[:std_fields] = [{name: 'UID', width:0}]
params[:sort_field] = find_prop("Report Order").id
params[:sort_direction] = "ASC"