raise 'Select Exactly One Requisition' if subjects.size != 1

req = subjects[0]

params[:keep_selection] = true
raise 'No report file exists to preview' if req.get_value('Preliminary Report File').nil?

prop = Property.find_by_display_name('Preliminary Report File')

params[:custom_fields] = {
  xtype:'panel',
  height: 500,
  anchor:'100%',
  #overflowY: 'auto',
  html:"<iframe width=100% height=100%  frameborder=0 src=\"/subjects/file/?id=#{req.id}&prop_id=#{prop.id}&inline=on\"></iframe>"
}.to_json
