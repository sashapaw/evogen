raise "Please select exactly 1 server configuration" if subjects.size != 1

extend ScriptRunner::UI
subj = subjects.first
params[:custom_fields] = encode_fields([
    udf('Name', nil, value: subj.name, anchor: '100%')
  ] + udfs_for_subject_type('HL7 Server Configuration', subj, {
    'HL7 Provider' => {readOnly: true},
    'Facility Name' => {fieldLabel: 'Lab Identifier'},
    'Facility Number' => {fieldLabel: 'Lab Account Number'},
    'HL7 Message Type' => {react: {
      init: <<-EOJS,
        if(value) {
          var store = this.getStore();
          form.setValuesAsync({id: value, udfs: "HL7 Message Type"}, function(result) {
            store.loadData(Ext.Array.map(result["HL7 Message Type"], function(el) {return [el];}));
            });
          }
          EOJS
          only: 'HL7 Provider'
      }}

  })
)
