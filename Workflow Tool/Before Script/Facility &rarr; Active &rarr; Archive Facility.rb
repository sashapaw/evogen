raise "Select exactly one Facility" unless subjects.length==1
msg = "<b>ATTENTION</b>: Facility's data will be archived, all access for Facility's users will be suspended."
params[:tool_message] = msg.html_safe

