params[:defaults] = {
  'Name' => subj.name,
  'Instrument Type' => subj.get_value('Instrument Type')
}

params[:required] = {
  'Name' => true,
  'Instrument Type' => true
}
