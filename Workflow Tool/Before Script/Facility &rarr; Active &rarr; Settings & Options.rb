extend ScriptRunner::UI
params[:custom_fields] = encode_fields([
  udf('Report Template', subj, fieldLabel:'Outcome Report Template', xtype:'choicefield', list: Requisition::Reports.test_results_report_templates, anchor:'100%')
])
if LIS.system_settings.get_value('Patient Testing Scheduler Enabled')
  params[:custom_fields] = encode_fields([
    udf('Use Patient Testing Scheduler', subj, fieldLabel:'Use Patient Testing Scheduler', xtype:'checkboxfield', anchor:'100%')
    #udf('Scheduler Physician Approval', subj, fieldLabel:'Scheduler Physician Approval', xtype:'checkboxfield', anchor:'100%')
  ])
end

