extend UI

params[:custom_fields] = UIUtils.encode_fields([
  udf('Select Requisition', subj, required: true, anchor:'100%', allowCreate:false,
    filter:"Facility = #{ subj.get_value('Facility').id})"),
  udf('Message', subj, required:true, anchor:'100%', height:150)
])