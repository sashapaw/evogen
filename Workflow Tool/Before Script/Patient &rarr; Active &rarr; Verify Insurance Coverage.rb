msg = "<b style='font-size:120%;'>Select a Test to verify Insurance Coverage.</b>"
params[:tool_message] = msg.html_safe


params[:required] = {
  'Primary Test'=>true
}

params[:allowCreate] = {
  'Primary Test'=>false
}