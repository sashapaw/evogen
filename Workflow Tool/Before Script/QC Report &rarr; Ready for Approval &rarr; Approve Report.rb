raise 'Select Exactly One QC Report' if subjects.size != 1

# prefill to current user/date
params[:defaults] = {
  'Approved On' => Date.today,
  'Approved By' => User.curr_user
}

params[:required] = {
  'Approved By'=>true,
  'Approved On'=> true
}

