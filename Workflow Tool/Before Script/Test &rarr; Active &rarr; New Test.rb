params[:required] = {
  'Code'=>true
}

params[:allowCreate] = {
  'Facility'=>false,
  'Analytes'=>false
}

params[:udf_config] = {
  'Analytes'=> {
      filter: "terminated is false"
  }
}