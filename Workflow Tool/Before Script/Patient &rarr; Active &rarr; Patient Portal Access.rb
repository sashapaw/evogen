unless LIS.system_settings.get_value('Patient Portal Enabled')
  raise "This module is not available under your current LimitLIS license. Please contact your account representative or email sales@ruro.com."
end

extend UI

ops = ActionMailer::Base.default_url_options
raise "Email service is not configured." unless ops and ops[:protocol] and ops[:host]

msg = "<b>ATTENTION</b>: Welcome email will be send to Patient with Portal link and access instructions:"
params[:tool_message] = msg.html_safe

portal = subj.my_users.present?
portal_user = portal ? subj.my_users[0] : nil
portal = false if portal_user and portal_user.disabled

params[:custom_fields] = encode_fields([
  udf('Enable Access to Patient Portal', subj, anchor:'100%', checked:portal),
  field_container([
      udf('EMail', subj, required: true, value:portal_user ? portal_user.username : ''),
      #udf('Password', nil, required: true),
      #udf('Verify Password', nil, required: true)
    ], defaults:{anchor:'100%'}, react: {
      shown_when: 'value',
      only: 'Enable Access to Patient Portal'
  })
])


