raise "Select exactly one Test Class" unless subjects.length==1
extend ScriptRunner::UI
subj = subjects.first
params[:custom_fields] = encode_fields([
  udf('Name', nil, value:subj.name, anchor:'100%', required: true),
  udf('CPT Codes', subj, anchor:'100%')
])