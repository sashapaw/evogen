raise "Select at least one Test" if subjects.blank?
if subjects.length==1
  msg = "<b>ATTENTION</b>: Selected Test will be archived, it will not be possible to Select the test <b>#{subjects[0].name}</b> in the Requisition workflow"
  params[:tool_message] = msg.html_safe
else
  msg = "<b>ATTENTION</b>: The selected Tests will be archived, it will not be possible to Select these <b>#{subjects.length}</b> tests in the Requisition workflow"
  params[:tool_message] = msg.html_safe
end