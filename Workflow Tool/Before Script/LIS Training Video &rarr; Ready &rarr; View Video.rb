raise "Select exactly one Video" unless subjects.length==1

subj = subjects[0]
rec = create_subject('User Training Record') do |r|
  r.set_value('LIS Training Video', subj)
  r.set_value('User', User.curr_user)
  #r.set_value('View Count', 1)
  r.set_value('Date / Time Watched', Time.now)
end

msg = '<p><iframe src="https://www.youtube.com/embed/'+subj.get_value('Video ID')+'?rel=0&autoplay=1&amp;vq=hd1080" frameborder="0" allowfullscreen width="853" height="480"></iframe></p>'
params[:tool_message] = msg.html_safe

