raise "You must select exactly 1 Test Panel" if subjects.size != 1
subj = subjects[0]
facility = subj.get_value('Facility')

params[:udf_config] = {
  'Code'=>{value:subj.get_value('Code'), required:true},
  'Description'=>{value:subj.get_value('Description'), required:true},
  'Facility'=>{filter:"terminated != true", readOnly: true, allowCreate:false, value: facility},
  'Tests'=>{filter:"terminated != true" + (facility ? " and Facility = #{facility.id} or Facility is null" : "Facility is null"), allowCreate:false, value: subj.get_value('Tests'), required:true}
}

