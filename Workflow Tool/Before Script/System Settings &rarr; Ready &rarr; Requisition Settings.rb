extend ScriptRunner::UI
params[:custom_fields] = encode_fields([
  udf('Report Template', subj, xtype:'choicefield', list: Requisition::Reports.test_results_report_templates, anchor:'100%')
])
