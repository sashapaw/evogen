raise "Select one Flag" if subjects.size != 1
subj = subjects[0]

params[:custom_fields] = Flag::Layout.flag_layout(subj)