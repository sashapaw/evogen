params[:background_execution] = true

msg = '<span style="font-size:120%;"><b>NOTE:</b> Completed tests will not be affected. Scheduling changes will only apply to future tests.</span>'
params[:tool_message] = msg.html_safe

extend UI

system_settings = LIS.system_settings
facility = subj.get_value('Facility')  
#facility = User.curr_user.get_value('Facility')
fields = [
  field_row([
    udf('Start Date', subj, required:true),
    udf('End Date', subj, required:true)
  ]),
  {xtype:'label', html:'<br><b>Point of Care Tests:</b>'},
  udf('Schedule Type (PoC)', subj),
  udf('Defined Schedule (PoC)', subj, react: {
    shown_when: 'value == "Defined"',
    only: 'Schedule Type (PoC)'
  }),
  udf('Defined from Start Date (PoC)', subj, react: {
    shown_when: 'value == "Defined from Start Date"',
    only: 'Schedule Type (PoC)'
  }),
  udf('Randomized Schedule (PoC)', subj, react: {
    shown_when: 'value == "Randomized"',
    only: 'Schedule Type (PoC)'
  }),
  {xtype:'label', html:'<br><b>Laboratory Tests:</b>'},
  udf('Scheduled Test Panels', subj, allowCreate:false,
    filter:"terminated is false and (Facility = #{facility.id} or Facility is null)"),
  udf('Scheduled Tests', subj, allowCreate:false,
    filter:"terminated is false and (Facility = #{facility.id} or Facility is null)",
    react:{
      change: 'form.setValuesAsync({id: value, udfs:{"Tests":"Scheduled Tests"}})',
      only: 'Scheduled Test Panel'
  }),
  udf('Schedule Type (Lab)', subj),
  udf('Defined Schedule (Lab)', subj, react: {
    shown_when: 'value == "Defined"',
    only: 'Schedule Type (Lab)'
  }),
  udf('Defined from Start Date (Lab)', subj, react: {
    shown_when: 'value == "Defined from Start Date"',
    only: 'Schedule Type (Lab)'
  }),
  udf('Randomized Schedule (Lab)', subj, react: {
    shown_when: 'value == "Randomized"',
    only: 'Schedule Type (Lab)'
  })
]
acknowledgement = system_settings.get_value('Provider Practitioner Authorization Statement')
show_statement = system_settings.get_value('Use Provider Practitioner Authorization Statement')

params[:custom_fields] = encode_fields([
  udf('Use Patient Testing Scheduler', subj, anchor:'100%'),
  field_set(title:'Schedule Information', defaults:{anchor:'100%'}, items:fields, react: {
    shown_when: 'value',
    only: 'Use Patient Testing Scheduler'
  }),
  if show_statement.present?
    field_set(title:'Subscriber Information', items:[{
        xtype: 'displayfield',      
        fieldLabel: 'Provider Practitioner Authorization Statement',
        anchor:'100%', 
        labelWidth:160,
        value: acknowledgement
      }], react: {
        shown_when: 'value',
        only: 'Use Patient Testing Scheduler'
    })
  end
])
