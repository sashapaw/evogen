trc = count_subjects(query: search_query(from: 'Test Result') {|qb|
  qb.prop('QC Sample').eq(subj)
})

params[:udf_config] = {
  'Analytes' => { readOnly: trc > 0 }
}

params[:required] = {
  'Analytes' => true,
  'Control Type' => true
}

params[:udf_config] = {
  'Analytes' => { filter: 'terminated is false'}
}