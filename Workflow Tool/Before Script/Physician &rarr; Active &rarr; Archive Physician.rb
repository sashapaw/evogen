raise "Please select at least 1 Physician to archive" if subjects.blank?
msg = "<b>ATTENTION</b>: Physician's data will be archived, access to Physician Portal will be suspended."
params[:tool_message] = msg.html_safe