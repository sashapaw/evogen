extend UI
params[:custom_fields] = encode_fields([
    custom_radio_group(name:'custom[user-action]', udfName:'rb-auto', margin:'0 0 20 0', items: [
        {boxLabel:'Add User', inputValue:'add', checked:true},
        {boxLabel:'Edit User', inputValue:'edit'},
        {boxLabel:'Archive User', inputValue:'archive'},
        {boxLabel:'Password Reset', inputValue:'reset'}
    ]),
    
    udf('User', nil, anchor:'100%', required:true, margin:'0 0 20 0', filter:"Facility = #{subj.id}",
      react: {
        shown_when: 'data["rb-auto"] != "add"',
        change: '
          if (name == "User" && value) {
            RURO.runHelperScript("user_info_lookup_helper", {id:value}, function(result) {
              if (!result.result) {
                form.setValue("Full Name", result.fullname);
                form.setValue("EMail", result.email);
                form.setValue("Login", result.username);
                form.setValue("User Role", result.user_role);
              }
            });
          }',
        only: ['rb-auto', 'User']
      }
    ),
  
    field_container([
        udf('Login', nil, anchor:'100%', required:true),
        udf('Full Name', nil, anchor:'100%', required:true),
        udf('EMail', nil, anchor:'100%', vtype:'email', required:true),
        udf('User Role', nil, anchor:'100%', required:true)
      ], react: {
        shown_when: 'value == "add" || value == "edit"',
        only: 'rb-auto'
    })
])