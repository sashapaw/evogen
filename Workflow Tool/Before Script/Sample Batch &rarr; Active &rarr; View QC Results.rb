raise 'Select Exactly One Batch' if subjects.size != 1
raise "There are no QC Results to View." if subjects[0].get_value('Sample Import QC Test Results').size == 0