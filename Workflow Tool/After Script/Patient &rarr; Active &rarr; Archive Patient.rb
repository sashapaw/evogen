subjects.each do |s|
  s.my_users.each do |u|
  	u.update_attributes!(:disabled=>true)
  end
  
  advance_workflow('Patient', 'Archived', s)
  s.terminated=true
  s.save
  SystemHooks.patient_archived(s)
end