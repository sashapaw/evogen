ro = subj.get_value('Report Orders') || []
an = subj.get_value('Analytes') || []

ro.each do |r| 
  a = r.get_value('Analyte')
  r.destroy if not an.include?(a)
end