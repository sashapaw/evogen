confirmation_report = ''

patient = subj.get_value('Patient')
unless patient.get_value('Sent to Billing')
  LISHL7::Billing.create_and_send_adt(patient)
  patient.set_value('Sent to Billing',true)
end

dft_report = LISHL7::Billing.create_and_send_dft(subj)
Rails.logger.info("dft_report:|#{dft_report}|")
confirmation_report.concat(dft_report)

if confirmation_report != ""
  Rails.logger.info("confirmation_report:|#{confirmation_report}|")
  show_message(confirmation_report.concat('<br/><hr>'))
end