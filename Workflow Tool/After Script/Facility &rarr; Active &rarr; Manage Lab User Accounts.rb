if params['user-action'] == 'add'  
  facility_ug = Facility.find_or_create_group(subj)

  groups = ['Everyone', params['User Role']+'s', facility_ug]

  u = create_user(
    username:params['Login'], password:SecureRandom.uuid,
    fullname:params["Full Name"], email:params['EMail'], roles:[ params['User Role'] ], groups:groups
  )
  u.extend(ScriptRunner::Helper)
  u.set_value('Facility', subj)
  show_alert('User was successfully created')
elsif params['user-action'] == 'edit'
  rlu = Role.find_by_name('Lab User')
  rlm = Role.find_by_name('Lab Manager')
  rla = Role.find_by_name('Lab Admin')
  
  u = params['User']
  u.fullname = params['Full Name']
  u.email = params['EMail']
  u.username = params['Login']
  u.roles.delete([rlu, rlm, rla])
  u.roles << Role.find_by_name( params['User Role'] )
  u.save

  
  lu = UserGroup.find_by_name('Lab Users')
  lm = UserGroup.find_by_name('Lab Managers')
  la = UserGroup.find_by_name('Lab Admins')

  lu.users.delete(u)
  lm.users.delete(u)
  la.users.delete(u)
  
  lu.users.push(u) if params['User Role'] == "Lab User"
  lm.users.push(u) if params['User Role'] == "Lab Manager"
  la.users.push(u) if params['User Role'] == "Lab Admin"

  lu.save
  lm.save
  la.save

  #ops = ActionMailer::Base.default_url_options
  #raise "Email service is not configured." unless ops and ops[:protocol] and ops[:host]
  #params['base_url'] = base_url
  #send_email([params['EMail']] , find_email_template('Client Portal Information Updated'), subj, params)

  show_alert('User was successfully updated')
elsif params['user-action'] == 'archive'
  params['User'].update_attributes!(:disabled=>true)
  show_alert('User was successfully updated.')
elsif params['user-action'] == 'reset'
  force_user_reset_password(params['User'])
  show_alert('Password was successfully reset. User was notified.')
end