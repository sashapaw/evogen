sample = subjects.first
# Create a QC sample, associate it to the Run Plan
# Users can name their own QC samples if they choose
s = create_subject('QC Sample') do |v|
  v.copy_properties_from(sample)
  v.set_value('Analytes', params['Analytes'])
  v.set_value('Control Type', params['Control Type'])
  v.set_value('Lot Number', params['Lot Number'])
  v.set_value('Lot Expiration Date', params['Lot Expiration Date'])
  v.advance_workflow('QC Sample', 'Active')
end

if params['Name'].present?
  # Make sure thare no other samples with the same name
  raise "QC Sample with name \"#{params['Name']}\" already exists" if find_subject(subject_type: 'QC Sample', name: params['Name']).present?

  s.name = params['Name']
  s.barcode_tag = s.name
  s.save!
end

open_subject s
params[:keep_selection] = true
