subjects.each do |s|
  s.get_value('Used In Facilities').each do |f|
    f.remove_value('Additional Contacts', s)
  end
  
  advance_workflow('Contact', 'Archived', s)
  s.terminated=true
  s.save
end