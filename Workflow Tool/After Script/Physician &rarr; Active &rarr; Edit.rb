params.each_pair{|udf, value|
  if value.present?
  	subj.set_value(udf, value)
  else
    subj.remove_prop(udf)
  end
}

open_subject(subj)