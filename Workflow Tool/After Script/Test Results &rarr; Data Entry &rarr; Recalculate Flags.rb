reqs = []
subjects.each do |s|
  s.calculate_flags
  #Determine if we should generate PDF 
  req = s.get_value('Sample').get_value('Requisition')
  reqs.push(req) unless reqs.include?(req)
  s.save!
end

init_progress(reqs.size)

reqs.each do |req|
  Requisition::Reports.generate_test_results_report(req,{})
  advance_progress
end

params[:keep_selection] = true