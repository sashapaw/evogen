params[:tab_name] = 'Pre-Registered Samples'
params[:subject_type] = 'Sample'
params[:explorer_states] = 'SamplePortal/Pre-Registered'
params[:query] = "Requisition = #{subj} AND #state = 'Pre-Registered'"
params[:search_guid] = "PRESAMPLES"

params[:need_search_panel] = true

params[:qb_filters] = [
    UIUtils.search_filter('name', op: 'contains', label:'Sample ID'),
    UIUtils.search_filter('barcode_tag', op: 'contains', label: 'Sample Barcode'),
    UIUtils.search_filter('"Patient"->name', op: 'contains', label:'Patient Name'),
    UIUtils.search_filter('"Patient"->DOB', op: 'eq', label:'DOB'),
].to_json