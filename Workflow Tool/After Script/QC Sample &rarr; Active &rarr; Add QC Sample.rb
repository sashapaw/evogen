
# Find if there is an existing control sample with that type/analyte/lot already
query = search_query(subject_type: 'QC Sample') do |qb|
  q = qb.and(qb.compare('"Analytes"', :eq, params['Analytes']))
  q << qb.compare('"Control Type"', :eq, params['Control Type'])
  q << qb.compare('"Lot Expiration Date"', :eq, params['Lot Expiration Date'])
  q << qb.compare('"Lot Number"', :eq, params['Number'])
  q
end

qc_sample = find_subjects({query: query})

raise "QC Sample with the same Analytes, Control Type and Lot already exists" if qc_sample.present?
  
# Create a QC sample, associate it to the Run Plan
# Users can name their own QC samples if they choose
s = create_subject('QC Sample') do |v|
  v.set_value('Analytes', params['Analytes'])
  v.set_value('Control Type', params['Control Type'])
  v.set_value('Lot Number', params['Lot Number'])
  v.set_value('Lot Expiration Date', params['Lot Expiration Date'])
  v.advance_workflow('QC Sample', 'Active')
end
if params['Name'].present?
  # Make sure thare no other samples with the same name
  raise "QC Sample with name \"#{params['Name']}\" already exists" if find_subject(subject_type: 'QC Sample', name: params['Name']).present?

  s.name = params['Name']
  s.barcode_tag = s.name
  s.save!
end

open_subject s