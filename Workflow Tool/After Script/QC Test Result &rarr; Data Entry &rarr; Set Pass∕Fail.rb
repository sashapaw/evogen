=begin
def check_control_type(dictionary, qct)
  return dictionary.get_value('Control Type') == qct.get_value('QC Sample').get_value('Control Type')
end

def check_lot_number(dictionary, qct)
   dictionary.get_value('Lot Number') == qct.get_value('QC Sample').get_value('Lot Number')
end

subjects.each do |qct|
  analyte = qct.get_value('Analyte')
  # Attach the dictionary record to the QC Test Result
  # get all dictionaries
  dicts = analyte.get_value('QC Ranges')

  my_control_type = nil
  dicts.each do |dictionary|
    my_control_type = dictionary if 
    if dictionary.get_value('Lot Number').present?
      check_lot_number(dictionary, qct) && check_control_type(dictionary, qct)
    else
      check_control_type(dictionary, qct)
    end
  end
  #raise "This analyte does not have a dictionary record associated with it" unless my_control_type
  qct.set_value('QC Range', my_control_type)
  qct.save!
end
=end

subjects.each do |s|
  s.calculate_flags
end
TestResult.group_validation(subjects)

params[:keep_selection] = true