selected_sheets = []
SpreadsheetImporter.sheets.each{|sheet|
  selected_sheets << sheet if params[sheet]
}
raise "Please select at least one item" if selected_sheets.empty?
file = SpreadsheetImporter.export(selected_sheets)
p = {rep: File.split(file.path).last}.to_param
show_message("<a href=\"reports/get_report?#{p}\">Download</a>")