params[:need_search_panel] = true
params[:tab_name]='All Analytes'
params[:explorer_states] = 'Active'
params[:subject_type] = "Analyte"
params[:query] = 'terminated is false and #state = Active'
params[:search_guid] = "ANALYTES"
params[:item_viewer] = <<-EOS
{
xtype: 'gridsubjectviewer',
baseParams: {tree_only: true}
}
EOS


params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label: 'Name'),
  UIUtils.search_filter('Instrument Code', op: 'contains', label: 'Instrument Code')
].to_json

