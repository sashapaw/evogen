#samples = subj.get_value('Imported Samples')
reqs = []

requisitions = subj.get_value('Requisitions')
requisitions.each do |req|
  #if !req.current_states.map(&:name).include?('Released')
     reqs.push(req.id)
  #end
end

filter = []
reqs.each do |req|
  filter.push("(id = #{req} and \"Queued for Release\" = null and \"Process Lock\" = null and #state = Received)")
end
params[:subject_type] = 'Requisition'
query = "#state = Received"
query = query + " and #{filter.join(' or ')}" if filter.length > 0
params[:query] = query
params[:search_guid] = "Release_Reqs"
params[:tab_name] = "Release Requisitions"
params[:explorer_states] = 'Batch'