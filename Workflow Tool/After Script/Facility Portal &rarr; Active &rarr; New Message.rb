facility_to = params['Select Requisition'].get_value('Facility')

s = create_subject('Communication') do |v|
  v.set_value('Requisition', params['Select Requisition'])
  v.set_value('Facility', facility_to )
  v.set_value('Message', params['Message'])
  v.set_value('Date Opened', Time.now)
end
advance_workflow('Communication', 'Open', s)
SystemHooks.communication_received(s)

#p = Property.find_by_display_name('Facility')
facility_from = User.curr_user.get_value('Facility')

# Parse the Contact Email field for semicolons, send emails to multiple recipients
recipients = []
recipients.concat(LIS.parse_email_field(facility_to.get_value('Contact Email')))
recipients.concat(LIS.parse_email_field(facility_from.get_value('Contact Email')))


#--REDMINE 8379
email_recipients = facility_to.get_value('Allow New Communication Emailing')
if email_recipients.present?
  LIS.as_admin {
    send_email(recipients, 'New Communication', s, params) {|ep|
    lab_logo = LIS.lab_logo
    ep.add_inline_attachment(lab_logo, 'lab_logo') unless lab_logo.nil?
      }
    }
end
#--END


show_message('Message successfully sent')