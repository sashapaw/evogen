params[:subject_type] = 'Physician'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:search_guid] = "PHYSICIANS"

params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label:'Physician Name or NPI Number')
].to_json

params[:query] = search_query do |qb|
  q = qb.and(qb.state("Active"))
  q << qb.compare("Facility", :eq, User.curr_user.get_value('Facility'))
  q
end