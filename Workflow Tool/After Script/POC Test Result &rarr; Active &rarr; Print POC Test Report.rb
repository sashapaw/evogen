pocr = subj.get_value('Point of Care Test Result Entries', :order=>'name ASC') || []
data = pocr.map{|u| [u.id, u.get_value('Point of Care Test').name, u.get_value('PoC Result')=="Positive" ? "X" : "", 
  u.get_value('PoC Result')=="Negative" ? "X" : "", 
  u.get_value('PoC Result')=="Not Tested" ? "X" : ""]
  }

run_custom_report('Point of Care Test Result Report', subj, 'Point of Care Test Result Report') do |extra_data|
  extra_data['poc_test_results'] = pocr
  extra_data['result'] = data
  extra_data['Now'] = Time.now
end

prop = Property.find_by_display_name('Point of Care Test Result Report')

show_message("Click to download <a target='_blank' href='/subjects/attachment/?id=#{subj.id}&pid=#{prop.id}'>Point of Care Test Result Report</a>".html_safe )