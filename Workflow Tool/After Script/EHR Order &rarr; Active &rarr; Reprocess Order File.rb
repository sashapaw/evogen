ehr_order = subjects[0]
hl7_processor = LISHL7::EHR::Importer::Processor.new

provider_configuration = ehr_order.get_value('EHR Server Configuration')

provider_settings = {
  create_patient: provider_configuration.get_value('Create Patient'),
  update_patient: provider_configuration.get_value('Update Patient From Order'),
  create_insurance: provider_configuration.get_value('Create Insurance From Order'),
  use_poc_test: LIS.system_settings.get_value('Use Point of Care Tests')
  }


filename = File.absolute_path(ehr_order.get_value('File').path)
data = File.open(filename, 'rb') { |f| f.read }

data = hl7_processor.cleanse(data)
msg = hl7_processor.data_parse(data)
message = ''


if msg[:MSH][:message_type] == "ORM^O01"
  begin
    req = LISHL7::EHR::Importer.create_req(hl7_processor,msg,provider_settings)
    if provider_settings[:use_poc_test]
      LISHL7::EHR::Importer.create_poc_tests(hl7_processor,msg,req[:req])
    end
    ehr_order.set_value('Requisition',req[:req]) if !req[:req].nil?  
    
    message = "#{ehr_order.get_value('Comments')} \n -----Reprocessed File-----\n"

    message.concat(req[:errors]) if !req[:errors].nil?
    message.concat(req[:warnings]) if !req[:warnings].nil?
    if message != ''
      show_message(message.gsub(/[\n\r]/,"<br/>\n")) if !message.nil?
    end
  rescue Exception=>e
    if !req.present?
      req = {req:nil}
    end

    if message == '' 
      message = "There was a problem processing the order. #{e}"
    end
  end

  ehr_order.set_value('Comments',message)
end
