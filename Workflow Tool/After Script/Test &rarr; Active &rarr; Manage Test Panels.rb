params[:need_search_panel] = true
params[:tab_name]='All Test Panels'
params[:explorer_states] = 'Active'
params[:subject_type] = "Test Panel"
params[:query] = '#state = Active'
params[:search_guid] = "TESTPANELS"


params[:qb_filters] = [
  UIUtils.search_filter('Code', op: 'contains', label: 'Code'),
  UIUtils.search_filter('Description', op: 'contains', label: 'Description')
].to_json