s = create_subject('Portal Document') do |v|
    v.set_value('File', params['File'])
    v.set_value('Published Date', params['Published Date'])
    v.name = params['Name']
end
advance_workflow('Document', 'Active', s)