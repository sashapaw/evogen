params[:subject_type] = 'LIS Training Video'
params[:search_guid] = "TrainingVideos"
params[:need_search_panel] = true
params[:explorer_states] = 'Ready'

params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label:'Video Name')
].to_json


params[:query] = search_query(subject_type: 'LIS Training Video')

