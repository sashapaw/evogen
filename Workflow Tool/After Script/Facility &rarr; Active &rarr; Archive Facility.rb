subjects.each do |s|
  raise "Can not Archive default Facility: #{s.name}" if s.get_value('RURO_CLIENT').present?
  s.my_users.each do |u|
  	u.update_attributes!(:disabled=>true)
  end
  
  advance_workflow('Facility', 'Archived', s)
  s.terminated=true
  s.save
  SystemHooks.facility_archived(s)
end