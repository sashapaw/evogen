state = params['Sample Import State']
batch = params['Sample Import Batch']
if state && batch
  advance_workflow('Sample Batch', state, batch)
  SystemHooks.sample_batch_completed(batch) if state == 'Complete'
end