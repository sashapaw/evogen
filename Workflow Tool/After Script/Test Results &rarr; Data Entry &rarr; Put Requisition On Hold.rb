requisitions = subjects.map{|s| s.get_value('Requisition')}.compact.uniq
requisitions.each do |r|
  advance_workflow('Requisition','On Hold',r)
  SystemHooks.requisition_on_hold(r)
end