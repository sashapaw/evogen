req = subjects.first
Requisition::Reports.generate_test_results_report(req,{})
prop = find_prop('Preliminary Report File')
show_dialog(
  title: 'Regenerated Report',
  width: 900,
  height: 600,
  layout: 'fit',
  items: [{
    xtype: 'box',
    html: "<iframe width=100% height=100% frameborder=0 src=\"/subjects/file/?id=#{req.id}&prop_id=#{prop.id}&inline=on\"></iframe>"
  }]
)

params[:keep_selection]=true