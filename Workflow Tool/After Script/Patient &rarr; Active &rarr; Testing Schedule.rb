# first, delete all scheduled tests
find_subjects( query: search_query(from: "Testing Schedule", 
               where: QE::Builder.prop('Patient').eq(subj)) ).each {|s| s.destroy}

# different logic if Physician Approval is needed
# for approvals, do not create tests yet. That will be done by physicial.
need_approval = subj.get_value('Facility').get_value('Scheduler Physician Approval').present?


if params['Use Patient Testing Scheduler'].present?
  TestingScheduler.verify_and_save_schedule(subj, params)
  subj.set_value('Schedule Defined By', User.curr_user)
  subj.remove_prop('Schedule Approved By')
  subj.remove_prop('Schedule Rejected By')
  subj.remove_prop('Schedule Approval Date')
  subj.remove_prop('Testing Schedule Comments')
  
  if need_approval # create just the schedule
  	subj.set_value('Awaiting Physician Approval', true)
    
  	TestingScheduler.notify_physicians(subj, params)
    show_message("Test Scheduler has been defined for #{subj}. Now awaiting Physician approval.")
  else # create schedule and tests

  	TestingScheduler.create_schedule_tests(subj, params)
    show_message("Test Scheduler has been created for #{subj}.")
  end
else
  TestingScheduler.clear_schedule(subj, params)
  show_message("Test Scheduler has been disabled for #{subj}")
end

