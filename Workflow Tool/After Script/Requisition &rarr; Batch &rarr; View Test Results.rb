params[:subject_type] = 'Test Result'
params[:query] = "Sample->Requisition = (#{subjects.map(&:id).join(',')})"
params[:sort_direction] = "DESC"
params[:sort_field] = "Flag"
params[:search_guid] = "Test_Results"
params[:tab_name] = "Review Test Results"
params[:explorer_states] = 'Data Entry'