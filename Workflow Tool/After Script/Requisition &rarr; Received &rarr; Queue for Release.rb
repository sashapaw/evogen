subjects.each do |subj|
  params.each_pair{|udf, value|
    if value.present?
      subj.set_value(udf, value)
    else
      subj.remove_prop(udf)
    end
  }

  #Check for all values before allowing queue for release.
  raise "Requisition #{subj.name} is already being processed by the batch processor. Cannot Queue Requisition." if subj.get_value('Process Lock')
  #subj.get_value('Test Results').each do |s|
  #   if !s.get_value('Value').present?
  #      raise "#{s.get_value('Analyte').name} does not have a value. Please enter a value in order to release the Requisition."
  #   end
  #end #test_Results do
  subj.set_value("Queued for Release", true)
end

# TODO: mark batch as Complete when the last requisition in batch is released

=begin
# Not sure what was done here... some meaningless code...

#-- REDMINE 7441
subjects.each do |s|
  sb = s.get_value('Sample Batch')
  if sb.nil?
    s.set_value("Queued for Release", true)
  else
    reqs = sb.get_value('Requisitions')
    reqs.each do |state|
      if !state.current_states.map(&:name).include?('Received') && state.get_value('Queued for Release') == true
        advance_workflow('Sample Batch', 'Complete', sb)
        SystemHooks.sample_batch_completed(sb)
      elsif !state.current_states.map(&:name).include?('Complete')
        advance_workflow('Sample Batch', 'Complete', sb)
        SystemHooks.sample_batch_completed(sb)
      end
    end
  end
end
#--END

=end