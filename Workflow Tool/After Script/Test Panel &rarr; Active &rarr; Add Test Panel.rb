s = create_subject('Test Panel') do |v|
  params.each_pair{|udf, value|     
  	 v.set_value(udf, value)  if value.present?
  }
end
