confirmation_report = ''
system_settings = LIS.system_settings

subjects.each do |s|
  params.each_pair{|udf, value|
    if value.present?
      s.set_value(udf, value)
    else
      s.remove_prop(udf)
    end
  }
  raise "Requisition #{s.name} is being processed by the batch processor. Cannot Release Requisition." if s.get_value('Process Lock')
  
  release_report = Requisition.release(s)
  confirmation_report << release_report if release_report.present?
end

if confirmation_report != ""
  show_message(confirmation_report.concat('<br/><hr>'))
end

=begin
# Not sure what was done here... some meaningless code...

#-- REDMINE 7441
subjects.each do |s|
  sb = s.get_value('Sample Batch')
  if sb.nil?
    advance_workflow('Requisition','Released',s)
    SystemHooks.requisition_released(s)
  else
    reqs = sb.get_value('Requisitions')
    reqs.each do |state|
      if !state.current_states.map(&:name).include?('Complete')
      	advance_workflow('Sample Batch', 'Complete', sb)
        SystemHooks.sample_batch_completed(sb)
      end
    end
  end
end
#-- END
=end