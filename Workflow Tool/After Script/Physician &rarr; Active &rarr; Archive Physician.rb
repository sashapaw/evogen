subjects.each do |s|
  advance_workflow('Physician', 'Archived', s)
  s.terminated=true
  s.save
  SystemHooks.physician_archived(s)
end