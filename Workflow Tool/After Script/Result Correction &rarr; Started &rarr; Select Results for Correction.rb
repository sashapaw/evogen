h = find_subjects(:advanced=>true) do |qb|
    qb.add_subject_type('Test Result')
  

  qb.add('Test Result', 'Requisition', :eq, subj.get_value('Requisition'))
    qb.add('Test Result', 'Invoice Option', :eq, inv_option)
end



cr = subj.get_value('Corrected Results') || []

cr << test_res

subj.set_value('Corrected Results', cr)