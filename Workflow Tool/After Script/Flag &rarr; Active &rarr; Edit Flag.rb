subj = subjects[0]

params.each_pair{|udf, value|
  next if udf == 'Name'
  if value.nil?
    subj.remove_prop(udf)
  else
    subj.set_value(udf, value)
  end  
}
subj.name = params['Name']
subj.save!
params[:keep_selection] = true