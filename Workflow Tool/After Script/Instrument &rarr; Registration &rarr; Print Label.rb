itype = subj.get_value('Instrument Type')
iname = nil
if itype.blank?
  iname = subj.name
  subj.name = nil
end
print_subject_barcode(subj, 1, 'Instrument Label Printer')
if !iname.nil?
  subj.name = iname
end