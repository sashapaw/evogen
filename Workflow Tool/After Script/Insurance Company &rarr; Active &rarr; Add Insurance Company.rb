s = create_subject('Insurance Company', {:name=>params['Name']}) do |v|
  params.each_pair{|udf, value|
    next if udf == "Name"
    v.set_value(udf, value) if value.present?
  }
end

open_subject(s)