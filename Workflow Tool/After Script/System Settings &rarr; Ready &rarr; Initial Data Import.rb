sheets = SpreadsheetImporter.sheets
modes = {}
sheets.each{|sheet|
  mode = case params[sheet]
    when 'Skip'
      :skip
    when 'Update'
      :update
    when 'Delete'
      :delete
  end
  modes[sheet] = mode if mode
}
msg = SpreadsheetImporter.process(params['File'], modes)
show_message(msg)