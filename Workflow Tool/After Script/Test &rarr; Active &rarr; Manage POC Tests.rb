params[:need_search_panel] = true
params[:tab_name]='All POC Tests'
params[:explorer_states] = 'Active'
params[:subject_type] = "Point of Care Test"
params[:query] = '#state = Active'
params[:search_guid] = "POCTS"


params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label: 'Name'),
  UIUtils.search_filter('Code', op: 'contains', label: 'Code')
].to_json
