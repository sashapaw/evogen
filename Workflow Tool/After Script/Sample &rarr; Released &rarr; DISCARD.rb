subjects.each do |s|
  s.set_value('Date Discarded', params['Date Discarded'])
  s.set_value('Comments', params['Comments'])
  advance_workflow('Sample', 'Discarded', s)
  SystemHooks.sample_discarded(s)
end

show_alert("#{subjects.length} Samples were successfully discarded" )