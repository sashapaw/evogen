params[:need_search_panel] = true
params[:tab_name]='All Test Classes'
params[:explorer_states] = 'Tools'
params[:subject_type] = "Test Class"
params[:query] = 'terminated is false'
params[:search_guid] = "TESTCLASSES"


params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label: 'Name'),
  UIUtils.search_filter('CPT Codes', op: 'contains', label: 'CPT Code')
].to_json