def handle_result_entry(results)
  results.each do |s|
     if s.get_value('Value').present?
       s.calculate_flags
       SystemHooks.on_result_change(s)
     #else
        #raise "#{s.get_value('Analyte').name} does not have a value. Please enter a value in order to release the Requisition."
     end
  end #subjects do
end

handle_result_entry(subjects)

subjects.each do |s|
  params[:source_subj].add_value('Corrected Results', s) if s.updated_at > 5.seconds.ago
end
