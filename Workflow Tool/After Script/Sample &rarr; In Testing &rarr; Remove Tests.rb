del_tests = params[:selection]
subj.delete_tests_and_test_results(del_tests)

if params['Remove Tests From Requisition'].present?
  req = subj.get_value('Requisition')
  req.remove_value('Tests', del_tests)
end