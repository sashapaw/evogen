subjects.each do |s|
  s.set_value('Freezer', params['Freezer'])
  s.set_value('Rack', params['Rack'])
  s.set_value('Shelf', params['Shelf'])
  s.set_value('Box', params['Box'])
  s.set_value('Slot', params['Slot'])
  s.set_value('Date Stored', params['Date Stored'])

  advance_workflow('Sample', 'Stored', s)
  SystemHooks.sample_stored(s)
end

show_alert("#{subjects.length} Samples successfully stored to #{params['Freezer'].name}" )