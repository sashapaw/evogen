def change_batch_state(subj)
  if !subj.get_value('Sample Batch').current_states.map(&:name).include?('Complete')
     requisitions = subj.get_value('Sample Batch').get_value('Requisitions')
     released = 0
     requisitions.each do |req|
       #Check if released.
       if req.current_states.map(&:name).include?('Released')
         released = released + 1
       end
     end
     #If released count = total count then put batch in complete state.
     if requisitions.size == released
       subj.get_value('Sample Batch').set_value('All Test Results Finalized',true)
     end
     if subj.get_value('Sample Batch').get_value('All Test Results Finalized') && subj.get_value('Sample Batch').get_value('All QC Results Finalized')
       sb = subj.get_value('Sample Batch')
       advance_workflow('Sample Batch','Complete',sb)
       SystemHooks.sample_batch_completed(sb)
     end
  end
end

subjects.each do |subj|
  params.each_pair{|udf, value|
    if value.present?
      subj.set_value(udf, value)
    else
      subj.remove_prop(udf)
    end
  }

  #Check for all values before allowing queue for release.
  raise "Requisition #{subj.name} is already being processed by the batch processor. Cannot Queue Requisition." if subj.get_value('Process Lock')
  subj.get_value('Test Results').each do |s|
     if !s.get_value('Value').present?
        raise "#{s.get_value('Analyte').name} does not have a value. Please enter a value in order to release the Requisition."
     end
  end #test_Results do
  subj.set_value("Queued for Release", true)
  change_batch_state(subj)
end
