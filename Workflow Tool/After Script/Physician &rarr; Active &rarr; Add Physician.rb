params['Facility'] = User.curr_user.get_value('Facility') unless params['Facility'].present?

s = create_subject('Physician') do |v|
  params.each_pair{|udf, value|
  	 v.set_value(udf, value)  if value.present?
  }
end
