reply = create_subject('Communication Reply') do |v|
  v.set_value('Communication', subj)
  v.set_value('Reply', params['Reply']) 
  v.set_value('Reply By', User.curr_user.get_value('Facility'))
  v.set_value('User', User.curr_user) 
end
SystemHooks.communication_replied(reply)

facility_to = subj.get_value('Facility')
facility_from = User.curr_user.get_value('Facility')

#recepients = [facility_to.get_value('Contact Email'), facility_from.get_value('Contact Email')]
# Parse the Contact Email field for semicolons, send emails to multiple recipients
recipients = []
recipients.concat(LIS.parse_email_field(facility_to.get_value('Contact Email')))
recipients.concat(LIS.parse_email_field(facility_from.get_value('Contact Email')))

params['base_url'] = base_url

#--REDMINE 8379
email_recipients = facility_to.get_value('Allow New Communication Emailing')
if email_recipients.present?
  LIS.as_admin {
    send_email(recipients, 'Communication Reply', subj, params) {|ep|
      #lab_logo = LIS.lab_logo
      #ep.add_inline_attachment(lab_logo, 'lab_logo') unless lab_logo.nil?
      }
    }
end
#-- END

