subj = subjects[0]

params.each_pair{|udf, value|
  if value.nil?
    subj.remove_prop(udf)
  else
    subj.set_value(udf, value)
  end  
}