samples = subj.get_value('Sample Import QC Test Results')
sampleIDs = []
samples.each do |sample|
  sampleIDs.push(sample.id)
end

params[:tab_name] = "QC Test Results for Batch #{subj.name}"

# Open the QC Test Results search page
params[:subject_type] = 'QC Test Result'
params[:need_search_panel] = true
params[:explorer_states] = 'Data Entry'
params[:search_guid] = "QCTESTRESULTS"
# Want only results from the QC Samples from the batch
params[:query] = search_query do |qb|
  qb.and(
    qb.state('Data Entry'),
    qb.compare('id', :in, sampleIDs)
    )
end



