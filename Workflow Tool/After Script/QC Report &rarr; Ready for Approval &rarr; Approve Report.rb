subjects.each do |s|
    # Approve report
    s.set_value('Approved On', params['Approved On'])
    s.set_value('Approved By', params['Approved By'])
    s.set_value('Lab Manager Signature', params['Lab Manager Signature'])
    # concatenate the old and new comments to make "blog-roll" comments
    comments = s.get_value('Comments') 
    new_comments = params['Comments']
    user = User.curr_user
    today = Date.today.strftime('%m/%d/%Y')
    if !comments.present?
      comments = ""
    end
    if new_comments.present?  
      comments = comments + "\n"  + "#{today} (#{user}) - " + new_comments
      s.set_value('Comments', comments)
    end
  advance_workflow('QC Report', 'Approved', s)
end