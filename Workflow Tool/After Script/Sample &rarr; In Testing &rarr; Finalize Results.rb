raise 'Select at least one Test Result in the list' if params[:selection].size == 0

reqs = []
params[:selection].each do |s|
  s.set_value('Finalized', true)
  # Determine if we should generate PDF 
  #req = s.get_value('Sample').get_value('Requisition')
  #reqs.push(req) unless reqs.include?(req)
  s.save!
end

#init_progress(reqs.size)

Requisition::Reports.generate_test_results_report(subj.get_value('Requisition'), {})
