fax_number = params["Fax Number"]
subj_type = find_subject_type('Requisition')  
req = subj_type.subjects.first

coversheet = run_custom_report('Fax Cover Sheet', req) do |extra_data|
  extra_data['Lab Name'] = "LIS Test Fax"
  extra_data['Lab Address'] = "123 test street<br/> Test, TS 12345"
  extra_data['Lab Logo'] = ""
  extra_data['to'] = "Test Person"
  extra_data['to_fax'] = fax_number
  extra_data['to_phone'] = ""
  extra_data['to_date'] = Time.now
  extra_data['regarding'] = "Testing Fax"
  extra_data['Comments'] = "We are testing if the fax has been received."
end

send_fax(fax_number, coversheet,{batch:true,batch_delay:10})