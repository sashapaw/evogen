s = create_subject('Flag', {:name=>params['Name']}) do |v|
  params.each_pair{|udf, value|     
     next if udf == 'Name'
  	 v.set_value(udf, value)  if value.present?
  }
end
start_workflow('Flag',s)