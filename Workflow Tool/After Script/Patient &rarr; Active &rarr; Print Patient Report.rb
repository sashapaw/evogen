require 'base64'
require 'combine_pdf'

def file_to_base64(file_path)
    encoded_string = Base64.strict_encode64(File.open(file_path, "rb").read) 
end

system_settings = LIS.system_settings
patient_photo_file = file_to_base64(File.absolute_path(subj.get_value('Patient Photo File').path)) if subj.get_value('Patient Photo File')

run_custom_report('Patient Report', subj, 'File') do |extra_data|     
  extra_data['Lab Name'] = system_settings.get_value('Lab Name')      
  extra_data['Lab Logo'] = file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path))
  extra_data['System Settings'] = system_settings
  extra_data['Patient Picture'] = patient_photo_file
  extra_data['Test Results'] = count_subjects(query: search_query(from: 'Test Result') {|qb|
    qb.prop('Sample->Requisition->Patient').eq(subj)
  })
end # PDF

additional_pdfs = [File.absolute_path(subj.get_value('File').path)]

subj.get_value('Requisitions').each do |req|
  if req.get_value('Requisition Report')
    additional_pdfs.push(File.absolute_path(req.get_value('Requisition Report').path))
  end
  if req.get_value('Report File')
    additional_pdfs.push(File.absolute_path(req.get_value('Report File').path))
  end
end

#Compile all PDF's into 1 Super PDF.
init_progress ( additional_pdfs.size )

pdf = CombinePDF.new
additional_pdfs.each do |pdf_file|
  begin
  pdf << CombinePDF.load(pdf_file) # one way to combine, very fast.
  rescue Exception=>e
    Rails.logger.info("File Not Found while generating Patient Report")
  end
  sleep(1)
  advance_progress
end
file_path = "#{Rails.root}/tmp/#{subj.get_value('File')}"
pdf.save file_path
combined_pdf = File.open(file_path, "r")
subj.set_value('File',combined_pdf)
File.delete(combined_pdf)

prop = Property.find_by_display_name('File')

show_message("Click to download <a target='_blank' href='/subjects/attachment/?id=#{subj.id}&pid=#{prop.id}'>Patient Report</a>".html_safe )