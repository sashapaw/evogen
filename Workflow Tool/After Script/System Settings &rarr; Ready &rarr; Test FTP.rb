require 'net/ftp'
require 'net/sftp'

require_script 'DoubleBagFTPS'

host = params['Hostname']
port = params['Port']
username = params['Username']
password = params['Password']
path = params['Remote File Path'] || '/'
server_type = params['Server Type']
files = [];
if server_type == "FTP"
  Net::FTP.open(host, username, password) do |ftp|
      ftp.binary=true		
      ftp.passive=true
      ftp.chdir(path)
      ftp.nlst.each do |entry|
        files.push(entry.to_s)
      end
  end
  
#  ftp = Net::FTP.new
#  ftp.passive = true
#  ftp.connect(host, 990)
#  ftp.login(username, password)
#  ftp.chdir(path)
#  ftp.nlst.each do |entry|
#    files.push(entry.to_s)
#  end
#  ftp.close
elsif server_type == "FTPS"
  port = 990 if port.nil?
  ftps = DoubleBagFTPS.new(nil, nil, nil, nil, DoubleBagFTPS::IMPLICIT)
  ftps.ssl_context = DoubleBagFTPS.create_ssl_context(:verify_mode => OpenSSL::SSL::VERIFY_NONE)
  ftps.connect(host, port)
  ftps.login(username, password)
  ftps.chdir(path)
  ftps.nlst.each do |entry|
    files.push(entry.to_s)
  end
  ftps.close  
elsif server_type == "SFTP"
  Net::SFTP.start(host, username, {:password => password}) do |sftp|
    sftp.dir.foreach(path) do |entry|
      files.push(entry.to_s)
    end
  end
else
  raise "Server Type has not been configured"
end

show_message('Files of ' + path + '<br/>' + files.join('<br/>'))