API_KEY = LIS.system_settings.get_value('Availity API Key')

User.curr_user = User.admin
ctx = ScriptRunner.context

url = 'https://api.availity.com'
req_url = "/demo/v1/coverages?"

pars = {
	'payerId' => 'BCBSF',
    'providerNpi' => '1234567893',
    'memberId' => 'PBHR123456',
    'patientLastName' => subj.get_value('Last Name'),
    'patientFirstName' => subj.get_value('First Name'),
    'serviceType' => '98',
    'patientBirthDate' => subj.get_value('DOB').strftime('%Y-%m-%d'),
    'providerTaxId' => '123456789'
}

resp = ctx.call_external_service(url + req_url + pars.to_query, nil, nil, :get) do |req, http|
  req["X-Api-Key"] = API_KEY
end

show_message(resp.inspect)