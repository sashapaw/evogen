subjects.each do |s|
  advance_workflow('Document', 'Archived', s)
  s.terminated=true
  s.save
end