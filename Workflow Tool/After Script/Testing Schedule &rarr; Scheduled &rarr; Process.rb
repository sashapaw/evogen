def are_test_scheduled(patient)
  tests = find_subjects(query: search_query(from:'Testing Schedule') {|qb|
    qb.and(
      qb.compare('Patient', :eq, patient),
      qb.compare('Testing Date', :gt, Time.now),
      qb.state('Scheduled')
    )
  })
  
  if tests.size > 0
    return true
  else
    return false
  end
end

def email_send(email,patient)
  unless email.nil?
    emailArray = email.split(';')
    begin
      send_email(emailArray,find_email_template("Last Schedule Testing"),patient) 
    rescue
      raise "Could Not Send Email. There was an error attempting to send an Email."
    end
  end
end

def send_communication(facility, patient)  
  facility_email = facility.get_value('Contact Email')
  email_send(facility_email, patient)
end

lab = subj.get_value('Lab Test')
patient = subj.get_value('Patient')
p_test_codes = patient.get_value('Tests')
facility = patient.get_value('Facility')
poc_grid_results = params.delete('poc_grid_results')
confirmation_results = params.delete('confirmation')
s = nil
if lab  # full requisition
  #require_script "requisition_support_helper"
  Requisition.error_check(params)

  s = create_subject('Requisition') do |v|
    params.each_pair{|udf, value|
      v.set_value(udf, value)  if value.present?
    }
  end
  
  if poc_grid_results.present?
    poc = Requisition.create_poc_tests(poc_grid_results, patient, s, params['Testing Date'])
    Requisition::ReflexRules.maybe_run_reflex_rules(s)
  end
  conf = Requisition.create_confirmation(confirmation_results, s) if confirmation_results.present?
  advance_workflow('Requisition', 'Draft', s)  
  
  SystemHooks.requisition_created(s)
  open_subject(s)  
else
  
  s,poc = Requisition.process_just_poc_tests(patient, poc_grid_results, params['Requesting Physician'],
                             params['Specimen Types'], params['Testing Date'])
  if s.present?
    open_subject(s)
    show_message("Requisition #{s.name} was created because a reflex rule was triggered. If you do not want this Requisition, please cancel the requisition.")  
  end

#  s = create_subject('Requisition') do |v|
#    v.set_value('Facility',facility)
#    v.set_value('Patient',patient)
#    ['Diagnoses', 'Medications', 'Bill To', 'Relation To Insured',
#      'Insurance Company 1', 'Policy #1', 'Group #1','Insurance Company 2', 'Policy #2', 'Group #2','Insurance Company 3', 'Policy #3', 'Group #3',
#      'Primary Physician NPI', 'Subscriber Name', 'Subscriber DOB', 'Subscriber Sex', 'Subscriber Address', 'Billing Facility', 'Billing Facility Address'].each{|udf|
#        v.set_value(udf, patient[udf]) if patient[udf].present?
#    }
#    v.set_value('Requesting Physician', params['Requesting Physician'])
#    v.set_value('Collection Date', Time.now)
#    v.set_value('Specimen Types', params['Specimen Types'])
#    v.set_value('Tests',p_test_codes) if !p_test_codes.nil?
#  end

#  poc = Requisition.create_poc_tests(poc_grid_results, patient, s, params['Testing Date'])
  #change this method to return if a reflex rule fired
#  reflex_fired = Requisition::ReflexRules.maybe_run_reflex_rules(s)
  
#  if reflex_fired
#    advance_workflow('Requisition', 'Draft', s)
#    SystemHooks.requisition_created(s)
#    open_subject(s)
#    show_message("Requisition #{s.name} was created because a reflex rule was triggered. If you do not want this Requisition, please cancel the requisition.")  
#  else
#    #We do not need the requisition so lets remove it.
#    s.destroy
#  end  
end

advance_workflow('Testing Schedule', 'Completed', subj)
show_alert("Patient <b>#{patient}</b> successfully processed".html_safe)  

if !are_test_scheduled(patient)
  #send email
  send_communication(facility, patient)
  show_message('This is the last scheduled test for this Patient')
end