params[:need_search_panel] = true
params[:tab_name]='All Confirmation Tests'
params[:subject_type] = 'Confirmation Test'
params[:query] = 'id != null'
params[:search_guid] = 'CONFIRM'

params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label: 'Name')
].to_json
