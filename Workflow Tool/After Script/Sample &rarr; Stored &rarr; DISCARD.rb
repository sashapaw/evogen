subjects.each do |s|
  s.remove_prop('Freezer')
  s.remove_prop('Rack')
  s.remove_prop('Shelf')
  s.remove_prop('Box')
  s.remove_prop('Slot')
  s.remove_prop('Date Stored')
  s.set_value('Date Discarded', params['Date Discarded'])
  s.set_value('Comments', params['Comments'])

  advance_workflow('Sample', 'Discarded', s)
  SystemHooks.sample_discarded(s)
end

show_alert("#{subjects.length} Samples were successfully discarded" )