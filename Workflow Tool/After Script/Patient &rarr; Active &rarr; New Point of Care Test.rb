
req,poc = Requisition.process_just_poc_tests(subj, params['poc_grid_results'],  params['Requesting Physician'],
                             params['Specimen Types'], params['Testing Date'])
if req.present?
  show_message("Requisition #{req.name} was created because a reflex rule was triggered. If you do not want this Requisition, please cancel the requisition.")  
end

s = req || poc
#poc = Requisition.create_poc_tests(params['poc_grid_results'], subj, nil, params['Testing Date'])
#Requisition::ReflexRules.maybe_run_reflex_rules(subj)

show_alert("Patient <b>#{subj}</b> successfully processed".html_safe)
advance_workflow('POC Test Result', 'Active', poc)
open_subject(s)