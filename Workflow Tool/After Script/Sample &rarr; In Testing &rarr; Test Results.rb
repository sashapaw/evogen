params[:subject_type] = 'Test Result'
params[:need_search_panel] = true

params[:query] = "Finalized = null and Sample = #{subj.id}"
params[:search_guid] = "TEST_RESULTS"
params[:tab_name] = "Test Results"
params[:explorer_states] = 'Data Entry'

params[:qb_filters] = [
  UIUtils.search_filter('"Sample"->name', op: 'contains', label:'Sample'),
  UIUtils.search_filter('Analyte<-Test::Analytes->name', op: 'contains', label:'Test'),
  UIUtils.search_filter('"Analyte"->name', op: 'contains', label:'Analyte'),
  UIUtils.search_filter('"Sample"->"Requisition"->"Patient"->name', op: 'contains', label:'Patient'),
  UIUtils.search_filter('"Sample"->"Requisition"->name', op: 'contains', label:'Requisition')
].to_json