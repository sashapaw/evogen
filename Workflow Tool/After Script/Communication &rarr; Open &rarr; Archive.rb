subjects.each do |s|
  s.set_value('Date Closed', Time.now)
  advance_workflow('Communication', 'Closed', s)
  s.terminated = true
  s.save
  SystemHooks.communication_archived(s)
end