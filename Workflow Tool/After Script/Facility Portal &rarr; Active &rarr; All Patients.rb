params[:subject_type] = 'Patient'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:search_guid] = "PATIENTS"


params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label:'Patient Name or MRN'),
  UIUtils.search_filter('Sex', op: 'eq', label:'Sex'),
  UIUtils.search_filter('DOB', op: 'eq', label:'DOB')
].to_json

params[:query] = search_query do |qb|
  q = qb.and(qb.state("Active"))
  q << qb.compare("Facility", :eq, User.curr_user.get_value('Facility'))
  q
end