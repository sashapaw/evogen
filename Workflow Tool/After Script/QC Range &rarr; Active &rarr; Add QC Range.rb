s = create_subject('QC Range') do |v|
  params.each_pair{|udf, value|
  	 v.set_value(udf, value)  if value.present?
  }
end
advance_workflow('QC Range', 'Active', s)
