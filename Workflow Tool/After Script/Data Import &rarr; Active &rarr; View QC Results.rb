params[:subject_type] = 'QC Test Result'
params[:query] = "<-\"Associated Test Results\" = #{subj.id}"
params[:search_guid] = "Received_QC_Results"
params[:tab_name] = "Imported QC Results"
params[:explorer_states] = 'Data Entry'

params[:sort_field] = Property.find_by_display_name('Analyte').id
params[:sort_direction] = "ASC"

