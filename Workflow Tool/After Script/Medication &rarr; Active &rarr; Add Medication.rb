s = create_subject('Medication') do |v|
  params.each_pair{|udf, value|
  	 v.set_value(udf, value)  if value.present?
  }
end

open_subject(s)