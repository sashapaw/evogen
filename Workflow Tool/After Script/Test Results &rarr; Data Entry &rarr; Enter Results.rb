changed_subjs = subjects.select{|s| s.changed?}
init_progress(changed_subjs.size)
changed_subjs.each do |s|
  s.calculate_flags
  SystemHooks.on_result_change(s)
  advance_progress
end #subjects do

params[:keep_selection] = true