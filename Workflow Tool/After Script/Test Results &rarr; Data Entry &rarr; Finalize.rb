reqs = []
subjects.each do |s|
  s.set_value('Finalized', true)
  #Determine if we should generate PDF 
  req = s.get_value('Sample').get_value('Requisition')
  reqs.push(req) unless reqs.include?(req)

  #
  # req.get_value('Samples').each do |sample|
  #   siblingTests = sample.get_value('Test Results')
  #   siblingTests.each do |siblingTest|
  #     flag = siblingTest.get_value('Flag')
  #     if flag == "CH" || flag == "CL"
  #       if siblingTest.get_value('Comments').nil?
  #         raise siblingTest.get_value("Analyte").to_s + " has a flag of " + flag.to_s + ". You must report the result and enter comments before finalizing"
  #       end
  #     end
  #   end
  # end
  #
  s.save!
end

init_progress(reqs.size)

reqs.each do |req|
  Requisition::Reports.generate_test_results_report(req,{})

#  system_settings = SubjectType.find_by_name('System Settings').subjects.first
#  interpretation_script_name = system_settings.get_value('Pass or Fail Interpretation Script')
#  if interpretation_script_name = 'Toxicology'
#    set_toxicology_pass_or_fail(req)
#  end
#  if interpretation_script_name = 'Molecular Diagnostics'
#    set_molecular_diagnostics_pass_or_fail(req)
#  end
   advance_progress
end