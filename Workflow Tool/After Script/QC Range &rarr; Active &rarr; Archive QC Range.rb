subjects.each do |qcr|
  advance_workflow('QC Range', 'Archived', qcr)
  qcr.update_attributes!(terminated: true)
end
