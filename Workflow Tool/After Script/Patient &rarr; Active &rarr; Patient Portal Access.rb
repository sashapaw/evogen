user = subj.my_users.present? ? subj.my_users[0] : nil

if params['Enable Access to Patient Portal'].present?
  if user
    user.update_attributes({:disabled=>false, 
      username:params['EMail'], 
      email:params['EMail']
    })
    params['Password'] = SecureRandom.urlsafe_base64(12/2+1)[0...8]
    user.set_password(params['Password'])
    
    force_user_reset_password(user)
    #params['base_url'] = base_url
    #send_email([params['EMail']] , find_email_template('Welcome to Patient Portal'), subj, params) 
  else
    user = create_user(
      username:params['EMail'],
      password:SecureRandom.urlsafe_base64(12/2+1)[0...8],
      fullname:subj.name,
      email:params['EMail'],
      roles:['Patient Portal User'], 
      groups:['Patients', 'Everyone']
    )
    user.extend(ScriptRunner::Helper)
    user.set_value('Patient', subj)
    user.set_value('Facility', subj.get_value('Facility'))
  end
elsif user.present?
  user.update_attributes!(disabled: true)
end