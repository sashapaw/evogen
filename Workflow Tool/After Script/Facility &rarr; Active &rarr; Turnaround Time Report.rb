subj = subjects.first
rows = []
specimenType = params['Specimen Type']
startDate = params['Start Date']
endDate = params['End Date']
context = ScriptRunner::Context.new(User.curr_user)

query = context.search_query(subject_type: 'Sample') do |qb|
    q = qb.and(qb.state('Released'))
  	q << qb.and(qb.compare('"created_at"', :gte, startDate)) if startDate
    q << qb.and(qb.compare('"created_at"', :lte, endDate)) if endDate
    q << qb.and(qb.compare('"Requisition"->"Facility"', :eq, subj)) 
    q << qb.and(qb.compare('"Specimen Type"', :in, specimenType)) if specimenType
    q
  end

samples = context.find_subjects({query: query})
file = run_report('TAT Report', samples)
subj.set_value('TAT Report', file)

prop = Property.find_by_display_name('TAT Report')

show_message("Click to download <a target='_blank' href='/subjects/attachment/?id=#{subj.id}&pid=#{prop.id}'>TAT Report</a>".html_safe )


