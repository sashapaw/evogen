params['Facility'] = User.curr_user.get_value('Facility') unless params['Facility'].present?
Requisition.error_check(params)
poc_grid_results = params.delete('poc_grid_results')
confirmation = params.delete('confirmation')

s = ScriptRunner::UI.add_subject('Requisition', params)

if poc_grid_results.present? # Point of Care Test Results
  poc = Requisition.create_poc_tests(poc_grid_results, params['Patient'], s, params['Collection Date'])
  Requisition::ReflexRules.maybe_run_reflex_rules(s)
end

Requisition.create_confirmation(confirmation, s) if confirmation.present?

advance_workflow('Requisition', 'Draft', s)
SystemHooks.requisition_created(s)
open_subject(s)
