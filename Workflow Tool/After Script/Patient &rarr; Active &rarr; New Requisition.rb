params['Facility'] = User.curr_user.get_value('Facility') unless params['Facility'].present?
params['Patient'] = subj unless params['Patient'].present? and subj.subject_type.name=="Patient"

Requisition.error_check(params)
poc_grid_results = params.delete('poc_grid_results')
confirmation = params.delete('confirmation')
params['Facility'] = User.curr_user.get_value('Facility') unless params['Facility'].present?
s = create_subject('Requisition') do |v|
  params.each_pair{|udf, value|
    v.set_value(udf, value) if value.present?
  }
end

if poc_grid_results.present? # Point of Care Test Results
  poc = Requisition.create_poc_tests(poc_grid_results, params['Patient'], s, params['Collection Date'])
  Requisition::ReflexRules.maybe_run_reflex_rules(s)
end

Requisition.create_confirmation(confirmation, s) if confirmation.present?

advance_workflow('Requisition', 'Draft', s)

bt = s.get_value('Bill To')
if bt == 'Facility' || bt == 'Patient/Self Pay' 
  #if insurance company 1 is filled in
  s.remove_prop('Insurance Company 1')
  s.remove_prop('Policy #1')
  s.remove_prop('Group #1')
  #if insurance company 2 is filled in
  s.remove_prop('Insurance Company 2')
  s.remove_prop('Policy #2')
  s.remove_prop('Group #2')
  #if insurance company 3 is filled in
  s.remove_prop('Insurance Company 3')
  s.remove_prop('Policy #3')
  s.remove_prop('Group #3')
elsif bt == 'Medicare'
  #if insurance company 2 is filled in
  s.remove_prop('Insurance Company 2')
  s.remove_prop('Policy #2')
  s.remove_prop('Group #2')
  #if insurance company 3 is filled in
  s.remove_prop('Insurance Company 3')
  s.remove_prop('Policy #3')
  s.remove_prop('Group #3')
end

SystemHooks.requisition_created(s)
open_subject(s)