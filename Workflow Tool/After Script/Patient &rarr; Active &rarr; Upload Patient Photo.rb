file = params['Patient Photo File']
if file
  subj.set_value('Patient Photo File', file)
else
  subj.remove_prop('Patient Photo File')
end