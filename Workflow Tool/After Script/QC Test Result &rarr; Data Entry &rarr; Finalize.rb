batch = ""
subjects.each do |s|
  advance_workflow('QC Test Result', 'Finalized',  s)
  if batch == ""
    batch = s.get_value('Sample Import Batch')
  end
end

def change_batch_state(batch)
  finalized = true
  if !batch.current_states.map(&:name).include?('Complete')
    batch.get_value('Sample Import QC Test Results').each do |item|
      if !item.current_states.map(&:name).include?('Finalized')
        finalized = false
      end
    end
    if finalized
      batch.set_value('All QC Results Finalized',true)
    end
    if batch.get_value('All Test Results Finalized') && batch.get_value('All QC Results Finalized')
      advance_workflow('Sample Batch','Complete', batch)
      SystemHooks.sample_batch_completed(batch)
    end
  end
end
  
if batch
  change_batch_state(batch)
end