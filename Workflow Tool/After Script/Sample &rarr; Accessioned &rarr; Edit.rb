tests = params.delete('Tests')
non_orderable_tests = params.delete('Non-Orderable Tests')
subj.set_value('Tests', tests + (non_orderable_tests || []))
UI.save_subject(subj, params)

params.each_pair{|udf, value|
  if value.present?
  	subj.set_value(udf, value)
  else
    subj.remove_prop(udf)
  end
}