files = params.reject{|udf, value| !value.is_a?(FileProxy)}
s = create_subject('Sample') do |obj|
  obj.set_value('Requisition', subj)
  
  params.each_pair{|udf, value|
     next if udf == 'Tests' or udf == 'Non-Orderable Tests'
  	 obj.set_value(udf, value) if value.present?
  }
  
  obj.set_value('Tests', params['Tests'] + (params['Non-Orderable Tests'] || []))
  obj.advance_workflow('Sample', 'Accessioned')
end
files.each_pair{|udf, value|
  s.set_value(udf, value)
}
s.save!

SystemHooks.sample_accessioned(s)
open_subject(s)