if params['user-action'] == 'add'  
  facility_ug = Facility.find_or_create_group(subj)
  groups = ['Everyone', 'Clients', facility_ug]
  groups << 'Clients Report Access'  if params['Allow to View Test Results'].present?

  u = create_user(
    username:params['Login'], password:SecureRandom.uuid,
    fullname:params["Full Name"], email:params['EMail'], roles:['Facility Portal User'], groups:groups
  )
  u.extend(ScriptRunner::Helper)
  u.set_value('Facility', subj)
  show_message('User was successfully created')
elsif params['user-action'] == 'edit'
  u = params['User']
  u.fullname = params['Full Name']
  u.email = params['EMail']
  u.username = params['Login']
  u.save

  cra = UserGroup.find_by_name('Clients Report Access')
  if params['Allow to View Test Results']
    if !cra.users.include?(params['User'])
      cra.users.push(params['User'])
      cra.save
    end
  else
    if cra.users.include?(params['User'])
      cra.users.delete(params['User'])
      cra.save
    end
  end

  ops = ActionMailer::Base.default_url_options
  raise "Email service is not configured." unless ops and ops[:protocol] and ops[:host]
  params['base_url'] = base_url
  send_email([params['EMail']] , find_email_template('Client Portal Information Updated'), subj, params)

  show_message('User was successfully updated')
elsif params['user-action'] == 'archive'
  u = params['User']
  u.update_attributes!(:disabled=>true)
  Session.where(user:u).to_a.each {|s| s.destroy }
  show_message('User was successfully updated.')
elsif params['user-action'] == 'reset'
  force_user_reset_password(params['User'])
  show_message('Password was successfully reset. User was notified.')
end