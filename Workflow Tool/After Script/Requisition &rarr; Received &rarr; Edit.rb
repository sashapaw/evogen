raise "The Requisition #{subj.name} is currently being processed in a batch. Cannot Edit Requisition." if subj.get_value('Process Lock') || subj.get_value('Queued for Release')

poc_grid_results = params.delete('poc_grid_results')
confirmation = params.delete('confirmation')

ScriptRunner::UI.save_subject(subj, params)

if poc_grid_results.present? # Point of Care Test Results
  poc = Requisition.edit_poc_tests(poc_grid_results, params['Patient'], subj, params['Collection Date'])
  #maybe_run_reflex_rules(s)
end

Requisition.edit_confirmation(confirmation, subj) if confirmation.present?
