startDate = params['Start Date']
endDate = params['End Date']
analytes = params['Analytes']
instruments = params['Instruments']

raise "Start Date: #{startDate} cannot be after End Date: #{endDate}" if startDate > endDate

require_script('lj_chart_report_helper')

context = ScriptRunner::Context.new(User.curr_user)

init_progress ( 10 )
generate_qc_report(context, startDate, endDate, analytes, instruments)
(1..10).each { |i|   
  sleep(1)
  advance_progress
}