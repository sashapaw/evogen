s = create_subject('Reflex Rule') do |v|
  params.each_pair{|udf, value|     
  	 v.set_value(udf, value)  if value.present?
  }
end
start_workflow('Reflex Rule',s)