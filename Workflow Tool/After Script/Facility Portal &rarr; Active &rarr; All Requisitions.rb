params[:subject_type] = 'Requisition'
params[:need_search_panel] = true
#params[:explorer_states] = 'Active'
params[:search_guid] = "Requsitions"


params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label:'Requsition ID'),
  UIUtils.search_filter('"Patient"->name', op: 'contains', label:'Patient Name'),
  UIUtils.search_filter('created_at', op: 'gt', label:'Date From'),
  UIUtils.search_filter('created_at', op: 'lt', label:'Date To'),
].to_json

params[:query] = search_query do |qb|
  q = qb.compare("Facility", :eq, User.curr_user.get_value('Facility'))
  q
end