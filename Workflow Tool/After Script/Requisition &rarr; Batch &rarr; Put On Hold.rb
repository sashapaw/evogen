subjects.each do |subj|
  raise "The Requisition #{subj.name} is currently being processed in a batch. Cannot place on hold." if subj.get_value('Process Lock') || subj.get_value('Queued for Release')
  advance_workflow('Requisition','On Hold',subj)
  SystemHooks.requisition_on_hold(subj)
end