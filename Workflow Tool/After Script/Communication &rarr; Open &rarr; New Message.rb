raise "Either choose a Template or enter a Message text" unless params['Message'].present? or params['Message Template'].present?

raise "Either a Template or a Message, but not both" if params['Message'].present? and params['Message Template'].present?

params['Message'] = params['Message'].present? ? params['Message'] : params['Message Template']

s = create_subject('Communication') do |v|
  v.set_value('Requisition', params['Requisition'])
  v.set_value('Facility', params['Facility'])
  v.set_value('External Source ID', params['External Source ID'])
  v.set_value('Also Send Email to the Support Representative', params['Also Send Email to the Support Representative'])
  v.set_value('Message', params['Message'])
  v.set_value('File', params['File'])
  v.set_value('Date Opened', Time.now)
end
advance_workflow('Communication', 'Open', s)
SystemHooks.communication_received(s)

facility_to = params['Facility']
facility_from = User.curr_user.get_value('Facility')

#params['Requisition'] = "" unless params['Requisition'].present?

#recipients = [facility_to.get_value('Contact Email'), facility_from.get_value('Contact Email')]

# Parse the Contact Email field for semicolons, send emails to multiple recipients
recipients = []
recipients.concat(LIS.parse_email_field(facility_to.get_value('Contact Email')))
recipients.concat(LIS.parse_email_field(facility_from.get_value('Contact Email')))

# If the send email to sales rep option is present, also send email to facility sales rep
if params['Also Send Email to the Support Representative']
  support_rep = facility_to.get_value('Support Representative')
  if support_rep.present?
    recipients << support_rep.get_value('EMail')
  end
end

params['base_url'] = s.get_value('url') || base_url

#--REDMINE 8379
email_recipients = facility_to.get_value('Allow New Communication Emailing')
if email_recipients.present?
  send_email(recipients, 'New Communication', subj, params) {|ep|
    #lab_logo = LIS.lab_logo
    #ep.add_inline_attachment(lab_logo, 'lab_logo') unless lab_logo.nil?
    }
end
#--END