require 'date'
require 'csv'
require_script "quality_control_helper"

def time_diff_milli(start, finish)
   (finish - start) * 1000.0
end

def find_by_attr(sub_type,attr,value)
  begin
    query = search_query(subject_type: sub_type) do |qb|
      q = qb.compare(attr, :eq, value)
      q
    end
    return find_subjects(query: query).first
  rescue Exception=>e
    Rails.logger.info "Bad query: #{query.inspect} Error: #{e}"
  end
end

def find_by_attrs(sub_type,attrs,values)
  begin
    query = search_query(subject_type: sub_type) do |qb|
      q = qb.and
      attrs.each_with_index { |attr,index|       
       q << qb.compare(attr, :eq, values[index])
      }
      q
    end
    return find_subjects(query: query).first
  rescue Exception=>e
    Rails.logger.info "Bad query: #{query.inspect} Error: #{e}"
  end

end

def get_control(control)
  if control.eql? "LC1"
    return "Low"
  elsif control.eql? "LC2"
    return "Medium"
  elsif control.eql? "LC3"
    return "High"
  end
end

def set_error_msg(error_str, error_msgs)
   if !error_msgs.include? error_str
      error_msgs.push(error_str)
   end
end

def insert_results_in_sample(results,batch)
  test_results = []
  reqs = []
  time_start = Time.now
  results.each do | result |
    if result[:sample_id].blank?
      # TODO: raise exception or report in a summary message, maybe?
      Rails.logger.info "Skipping result #{result.inspect} because of blank Sample ID"
      next
    end
    ts = Time.now
    tr = find_by_attrs('Test Result',['Sample','Analyte'],[result[:sample_id],"#{result[:analyte]}"])
    if tr
      req = tr.get_value('Sample').get_value('Requisition')
      if !reqs.include?(req)
      	reqs.push(req)
      end 

      tr.set_value('Value',result[:value])
      tr.calculate_flags
      if result[:finalize] == "Yes"
        tr.set_value('Finalized',true)
      end
      test_results.push(tr)
    end
  end
  Rails.logger.info "Test Result Process Took: #{time_diff_milli(time_start, Time.now)}ms"
  return  { reqs:reqs, results:test_results }
end

def insert_qc_results(qc_results, instrument_sn, acquisition_date, batch)
  today_date = Date.parse(Time.now.to_s)
  results = { results:[], errors:"", instrument:nil, samples:[] }
  error_msgs = []
  instrument = find_by_attr('Instrument', 'Serial Number', instrument_sn)

  results[:instrument] = instrument
  qc_run = create_subject('QC Run') do |qcr|
    qcr.set_value('Instrument',instrument)  if instrument.present?
    qcr.set_value('Run Date/Time',acquisition_date)  if acquisition_date.present?
  end

  #Add Test Results
  qc_results.each do | result |    
    sample = find_by_attr('QC Sample','name',result[:sample_id]) #If find Sample
    if sample     
      if sample.get_value('Lot Expiration Date') >= today_date       
        analyte = find_by_attr('Analyte', 'name', result[:analyte])
        if analyte
          if !results[:samples].include? (sample)
            results[:samples].push(sample)
          end
          qc_group = result[:group]
          dictionary = find_by_attrs('QC Range',['Analyte','Control Type'],["#{result[:analyte]}",get_control("#{result[:sample_name]}")])          
          qc_test_result = create_subject('QC Test Result') do |qctr|
            qctr.set_value('QC Sample',sample) if sample.present?
            qctr.set_value('Analyte',analyte) if analyte.present?
            qctr.set_value('Value',result[:value])
            qctr.set_value('Unit',analyte.get_value('Unit'))
            qctr.set_value('QC Run',qc_run) if qc_run.present?
            qctr.set_value('QC Set Number',qc_group) if qc_group.present?
            qctr.set_value('QC Range',dictionary) if dictionary.present?
          end
          #Set the Batch Associated with it
          qc_test_result.set_value('Sample Import Batch',batch);

          qc_test_result.set_targets
          advance_workflow('QC Test Result', 'Data Entry', qc_test_result)
          results[:results].push(qc_test_result)
        else
          set_error_msg("ERROR: Sample #{result[:sample_id]}: Analyte #{result[:analyte]} was not found", error_msgs)  
        end
      else
        set_error_msg("ERROR: Sample #{result[:sample_id]} is expired.", error_msgs)      
      end
    else
      set_error_msg("ERROR: Sample #{result[:sample_id]} was not found", error_msgs)      
    end
  end    
  #Check and set which QC Groups are Valid.
  group_validation(results[:results])
  results[:errors] = error_msgs.join("\n")
  return results
end

class ResultTestImport
	INSTRUMENT = [1,0] #Row 1, Col 0
	AQUIRED_DATE = [1,1] #Row 1, Col 1
	DATA_START = 3 #Row 3
	def initialize(filename = "")
    	@@filename = filename
  	end

  	def self.filename
    	@@filename
  	end
  	def set_filename(file_name)
    	@@filename = file_name
    end
	def create_map(row,group)
		{
		   sample_name:row[0],
		   sample_id:row[1],
		   analyte:row[2],
		   value:row[3],
		   within_10:row[4] || "",
		   within_20:row[5] || "",
		   passed:row[6] || "",
		   comments:row[7] || "",
           group:group || "",
		   finalize: row[3] == "N/A" ? "Yes" : "No"
	 	}	
	end

	def get_csv_results
		data = {
			instrument_sn:"",
			acquisition_date:"",
			qc_samples:[],
			test_samples:[]
		}
		row_counter = 0
        group_count_qc = 1
        group_store = ''
        #group_map = {}
		begin
			CSV.foreach(@@filename, :headers => false, :skip_blanks => true) do |row|
				#purge empty rows
				if !row.all? &:nil? 
					if row_counter == INSTRUMENT[0]
						data[:instrument_sn] = row[INSTRUMENT[1]]
						data[:acquisition_date] = row[AQUIRED_DATE[1]].strip.sub(" ",", ")
					elsif row_counter >= DATA_START
						if row[0].include?("LC")
							if group_store == "LC3" && row[0] == "LC1"
                               group_count_qc = group_count_qc + 1                               
                          	end
                          	group_store = row[0]
							data[:qc_samples].push(create_map(row, "Group #{group_count_qc}"))
						else
							data[:test_samples].push(create_map(row,''))
						end	
					end
				end
				row_counter += 1
			end
		return data
		rescue Exception => e
			return nil
		end
	end
end

def process_file(file, comments,system_settings)    
    s = create_subject('Sample Import') do |v|
      v.set_value('Sample Import File', file)  if file.present?
      v.set_value('Sample Import Comments', comments)  if comments.present?
    end
  
    batch = s.get_value('Sample Import File').to_s.split('_')
    import_batch = find_by_attr("Sample Import Batch","name","#{batch[0]}_#{batch[1]}")
    if !import_batch
      import_batch = create_subject('Sample Import Batch') do |v|
        v.set_value('Sample Import Batch ID', "#{batch[0]}_#{batch[1]}") 
      end
      start_workflow('Sample Batch',import_batch)
    end
    
    results = ResultTestImport.new(s.get_value('Sample Import File').path)
    data = results.get_csv_results()
    #QC Test Results 
    qc_test_results = insert_qc_results(data[:qc_samples],data[:instrument_sn],data[:acquisition_date],import_batch)
    qc_results_imported = qc_test_results[:results].length
    s.set_value("QC Test Results Imported",qc_results_imported)
        if import_batch.get_value('Sample Import QC Test Results').length > 0
      qc_test_results[:results].concat(import_batch.get_value('Sample Import QC Test Results'))
    end
    import_batch.set_value('Sample Import QC Test Results',qc_test_results[:results])

    import_batch.set_value("QC Samples", qc_test_results[:samples])
    
    qc_tr_imported = import_batch.get_value("QC Test Results Imported").to_i
    qc_tr_imported += qc_results_imported
    import_batch.set_value("QC Test Results Imported", qc_tr_imported)

    if import_batch.get_value('QC Test Results Imported').to_i == 0
      import_batch.set_value('All QC Results Finalized',true)
    else
      import_batch.set_value('All QC Results Finalized',false)
    end
    
    #Test Results
    test_results = insert_results_in_sample(data[:test_samples],import_batch)
    test_results_imported = test_results[:results].length
    s.set_value("Test Results Imported",test_results_imported)
    
    tr_imported = import_batch.get_value("Test Results Imported").to_i
    tr_imported += test_results_imported
    import_batch.set_value("Test Results Imported", tr_imported)

    if import_batch.get_value('Test Results Imported').to_i == 0
      import_batch.set_value('All Test Results Finalized',true)
    else
      import_batch.set_value('All Test Results Finalized',false)
    end
    SystemHooks.sample_batch_created(import_batch)

    reqs = test_results[:reqs]
    #Link batches, set pass or fail of requisition
    reqs.each do |req|
      req.set_value('Sample Batch',import_batch)
      set_pass_or_fail(req) #test_results_helper
    end
   
    s.set_value('Sample Import Errors',qc_test_results[:errors])
    s.set_value('Sample Import Batch',import_batch)
    
    complete_msg = "<strong>Import Complete</strong> - #{s.get_value('Sample Import File')} <br/> #{qc_results_imported} QC Test Results Imported<br/> #{test_results_imported} Test Results Imported"
    if s.get_value('Sample Import Errors').present?
      complete_msg.concat("<br/> <span style='color:red;font-weight:bold;'>Import file contained errors - please review.</span>")
    end
    
    return {subject:s,msg:complete_msg}
end