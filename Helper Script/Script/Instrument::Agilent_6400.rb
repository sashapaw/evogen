module Instrument
  class Agilent_6400 < Common

    def import_file(file)
      context.open_spreadsheet_file_simple(file.path, csv_options: {encoding: 'bom|utf-8'}, headers: ('A'..'Z').to_a) {|csv|
        
        state = :header
        context.init_progress csv.length
        csv.each_with_index {|row, n, row_raw|
          Rails.logger.info("-->> state: #{state} n: #{n} data: #{row_raw.inspect}")
          context.advance_progress

          case state
            when :header
              if row_raw[0] == 'Sample' && row_raw.size > 10 && n == 0
                # raw file header line 1
              elsif row_raw[0] == 'Name' && n == 1
                state = :table_raw
              elsif row_raw[0] == 'Sample' && row_raw.size == 3
                state = :table_postprocessed
              end
            
            when :table_raw
              yield({sample: row_raw[0], analyte: 'ETG', value: row_raw[9], completed_at: row_raw[2], row: n})
              yield({sample: row_raw[0], analyte: 'ETS', value: row_raw[19], completed_at: row_raw[2], row: n})
            
            when :table_postprocessed
              yield({sample: row_raw[0], analyte: row_raw[1], value: row_raw[2], row: n})
          end
        }
      }
    end
    
    def preprocess_record(result_data)
      result_data[:completed_at] = DateTime.strptime(result_data[:completed_at], '%m/%d/%Y %H:%M') if result_data[:completed_at].present?
    end

  end
end