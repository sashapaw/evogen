# Define you custom Liquid Filters here. Visit www.liquidmarkup.org for more information

def test_sort(tests)
  tests.sort!{|t1,t2| t1.id <=> t2.id }
  tests
end
def get_valid_samples(samples)
  result = samples.select do |sample|    
    !sample.unwrap.current_states.map(&:name).include?('Cancelled') && !sample.unwrap.current_states.map(&:name).include?('Accessioned')
  end
  result
end
def test_sort_report_order(tests)
#  tests.sort!{|t1,t2| (t1.unwrap.get_value("Analyte").get_value("Report Order") || 1_000_000) <=> (t2.unwrap.get_value("Analyte").get_value("Report Order") || 1_000_00 )}
#  tests
  tests.sort!{|t1,t2| (t1.unwrap.get_value("Test").get_value("Report Order") || 1_000_000) <=> (t2.unwrap.get_value("Test").get_value("Report Order") || 1_000_00 )}
  tests
end

def test_result_sort(test_results)
  test_results.sort!{|t1,t2| t1.id <=> t2.id }
  test_results.sort!{|t1,t2| (t1.unwrap.get_value("Report Order") || 1_000_000) <=> (t2.unwrap.get_value("Report Order") || 1_000_00 )}  
  test_results
end

def test_result_sort_abc(test_results)
  test_results.sort!{|t1,t2| t1.id <=> t2.id }
  test_results.sort!{|t1,t2|
    test1 = "#{t1.unwrap.get_value("Analyte")}"
    test2 = "#{t2.unwrap.get_value("Analyte")}"
   	test1 <=> test2
  }
  test_results.sort!{|t1,t2|
    test1 = "#{t1.unwrap.get_value("Test").get_value("Description")}"
    test2 = "#{t2.unwrap.get_value("Test").get_value("Description")}"
   	test1 <=> test2
  }  
  test_results
end

def poct_result_sort_abc(poct_results)
  poct_results.sort!{|t1,t2|    
    test1 = "#{t1.get_value("Point of Care Test").name}"
    test2 = "#{t2.get_value("Point of Care Test").name}"
   	test1 <=> test2
  }  
  poct_results
end

def analyte_type_sort(test_results)
  # this sort will sort analytes alphabetically, and then put them into analyte type categories
  test_results.sort!{|t1,t2|
    test1 = "#{t1.unwrap.get_value("Analyte").name}"
    test2 = "#{t2.unwrap.get_value("Analyte").name}"
   	test1 <=> test2
  }
  test_results.sort!{|t1,t2|
    test1 = "#{t1.unwrap.get_value("Analyte").get_value("Analyte Type")}"
    test2 = "#{t2.unwrap.get_value("Analyte").get_value("Analyte Type")}"
   	test1 <=> test2
  }  
  test_results
end

def limited_result(test_result)
  return '' if test_result.nil?
  tr = test_result.unwrap
  an = tr.get_value('Analyte')
  return '' if an.get_value('Hide Value')
  v = tr.get_value('Interpretation')
  return v if v
  v = tr.get_value('Value')
  return v if an.get_value('Qualitative')
  return v unless /^[+-]?\d+(\.\d+)?$/.match(v)
  dp = an.get_value('Decimal Places')
  return v unless dp
  num = v.to_f
  "%.#{dp.to_i}f" % num
end

def sample_results_by_test(sample, reportable = nil)
  sample.unwrap.test_results_by_test(reportable)
end

def multiline(s, sep='<br>')
  return '' if s.nil?
  s.split("\n").map{|line| UIUtils.h(line)}.join(sep).html_safe
end

def initials(user)
  return '' if user.nil?
  user.unwrap.fullname.split(' ').map{|s| s[/[[:upper:]]/]}.compact.join('.')+'.'
end