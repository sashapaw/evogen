module Instrument
  class Horiba_ABX_Pentra_80 < ASTM

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @result_status_map = get_result_status_map
      @analyte_map = get_analyte_map
      @specimen_type_map = get_specimen_type_map
      @unit_map = get_unit_map
    end

    def get_unit_map
      {
      }
    end

    def get_analyte_map
      {
        'WBC' => 'WBC'
      }
    end

    def get_result_status_map
      {
        'F' => 'Final result'
      }
    end

    def get_specimen_type_map
      {
        'Plasma' => '1',
        'Serum' => '1',
        'Urine (Random)' => '2',
        'Urine' => '2'
      }
    end

    def get_specimen_type(sample)
      st_name = sample.get_value('Specimen Type').name rescue nil
      @specimen_type_map.fetch(st_name) rescue '3'
    end

    def get_status(test_result, field)
      res_stat = field['value']
      @result_status_map.fetch(res_stat) rescue res_stat
    end

    def get_test_ids(test_results)
      test_results.keys.map {|aname| @analyte_map.key(aname)}.
        select {|aname| aname.present?}.
        map {|aname| "^^^#{aname}"}.join("\\")
    end

    def get_patient(requisition)
      pdata = get_patient_data(requisition)
      if pdata
        {
          'type': 'P',
          'fields': [
            {'idx': 2, 'value': 1, 'name': :sequence_number},
            {'idx': 3, 'value': pdata[:id], 'name': :practice_assigned_patient_id},
            {'idx': 4, 'value': '', 'name': :laboratory_assigned_patient_id},
            {'idx': 5, 'value': '', 'name': :patient_id},
            {'idx': 6, 'value': "#{pdata[:last_name]}^#{pdata[:first_name]}", 'name': :name},
            {'idx': 7, 'value': ''},
            {'idx': 8, 'value': "#{pdata[:birth_date]}", 'name': :birthdate},
            {'idx': 9, 'value': pdata[:sex], 'name': :sex},
            {'idx': 10, 'value': ''},
            {'idx': 11, 'value': ''},
            {'idx': 12, 'value': ''},
            {'idx': 13, 'value': ''},
            {'idx': 14, 'value': "#{pdata[:physician_last_name]}^#{pdata[:physician_first_name]}", 'name': :attending_physician}
          ]
        }
      else
        nil
      end
    end

    def get_orders(sample, test_results)
      specimen_type = get_specimen_type(sample)
      test_ids = get_test_ids(test_results)
      requested_at = get_time_requested_at

      [{
        'order': {
            'type': 'O',
            'fields': [
                {'idx': 2, 'value': 1, 'name': :sequence_number},
                {'idx': 3, 'value': "#{sample.barcode_tag}", 'name': :specimen_id},
                {'idx': 4, 'value': ''},
                {'idx': 5, 'value': test_ids, 'name': :universal_test_id_internal},
                {'idx': 6, 'value': 'R', 'name': :priority},
                {'idx': 7, 'value': requested_at, 'name': :requested_at},
                {'idx': 8, 'value': sample.get_value('Collection Date').strftime('%Y%m%d%H%M%S'), 'name': :collected_at},
                {'idx': 9, 'value': ''},
                {'idx': 10, 'value': ''},
                {'idx': 11, 'value': ''},
                {'idx': 12, 'value': 'N'},
                {'idx': 13, 'value': ''},
                {'idx': 14, 'value': ''},
                {'idx': 15, 'value': ''},
                {'idx': 16, 'value': specimen_type}
            ]
        }}]
    end


  end
end