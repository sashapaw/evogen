module LISHL7
  module Providers
    
    def self.generate_config_accessors(target_instance)
      unless target_instance.respond_to?(:config)
        target_instance.instance_eval { define_singleton_method(:config, parent.method(:config).to_proc) }
      end
      target_instance.config.each {|k,v|
        method_name = k.to_s
        method_name += '?' if v.is_a?(TrueClass) || v.is_a?(FalseClass)
        target_instance.class_eval(<<-EOS)	# This will evaluate to a singleton class of target instance
          def #{method_name}
            #{target_instance.name}.config[:#{k}]
          end
        
        def config
          superconfig = defined?(super) ? super : nil
          selfconfig = #{target_instance.name}.config
          superconfig.present? ? superconfig.merge(selfconfig) : selfconfig
        end
        EOS
      }
    end

  end
end
