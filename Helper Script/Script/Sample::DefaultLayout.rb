module Sample
  
  module DefaultLayout
    include ScriptRunner::UI
    extend self
  
    def edit_fields(subj)
      system_settings = LIS.system_settings
      req = subj.subject_type.name == 'Requisition' ? subj : subj.get_value('Requisition')
      sample = subj.subject_type.name == 'Sample' ? subj : nil
      ctx = ScriptRunner.context
      any_specimen_type = system_settings.get_value('Allow Any Specimen Type on Accessioning')
      specimen_types = any_specimen_type ? ctx.find_subjects(query: 'FROM "Specimen Type" WHERE terminated is false') : req.get_value('Specimen Types')
      tests_by_specimen = {}
      specimen_types.each{|specimen|
        tests = ctx.find_subjects(query:"FROM Test WHERE <-Requisition::\"Tests\" = #{req.id} AND (\"Specimen Types\" = #{specimen.id} OR \"Specimen Types\" = null)")
        if tests
          tests_by_specimen[specimen.id] = tests.map{|test| {id: test.id, name: test.name}}
        end
      }
      specimen = sample ? sample.get_value('Specimen Type') : req.get_value('Specimen Types')[0]
      if sample
        tests = []
        non_orderable_tests = []
        sample.get_value('Tests').each{|t| (t.get_value('Non-Orderable') ? non_orderable_tests : tests) << t}
      else
        tests = req.get_value('Tests')
        non_orderable_tests = SystemHooks.non_orderable_tests_for_accession_sample_form(req)
      end
      [
        udf('Specimen Type', nil, value:specimen, required:true, allowCreate:false,
          filter: any_specimen_type ? nil : "<-Requisition::\"Specimen Types\" = #{req.id}"),
        udf('Tests', nil, value:tests, required:true, allowCreate:false,
          filter: "<-Requisition::\"Tests\" = #{req.id} AND (\"Specimen Types\" = #{specimen ? specimen.id : 0} OR \"Specimen Types\" = null)",
          react: {
            change: "
              this.setFilter('<-Requisition::\"Tests\" = #{req.id} AND (\"Specimen Type\" = '+(value || 0)+' OR \"Specimen Types\" = null)');
              this.setValue(#{tests_by_specimen.to_json}[value]);",
            only: 'Specimen Type'
        }),
        udf('Non-Orderable Tests', nil, value:non_orderable_tests, filter: 'terminated = false AND "Non-Orderable" = true', allowCreate:false),
        sample ? nil : udf('Collection Date', subj, anchor:'60%'),
        #sample ? nil : 
        udf('Received Date', subj, anchor:'60%'),
        field_row([
          udf('Volume', subj),
          udf('Unit', subj, allowCreate:false, labelWidth: 60)
        ]),
        system_settings.get_value('Use Concentration on Sample') ? field_row([
          udf('Concentration', subj),
          udf('Concentration Unit', subj, fieldLabel: 'Unit', labelWidth: 60, allowCreate:false)
        ]) : nil,
        udf('Condition', subj, anchor: '100%'),
        udf('Comments', subj, anchor: '100%')
      ]
    end


    def edit_fields_portal(subj)
      system_settings = LIS.system_settings
      req = subj.subject_type.name == 'Requisition' ? subj : subj.get_value('Requisition')
      sample = subj.subject_type.name == 'Sample' ? subj : nil
      ctx = ScriptRunner.context
      any_specimen_type = system_settings.get_value('Allow Any Specimen Type on Accessioning')
      specimen_types = any_specimen_type ? ctx.find_subjects(query: 'FROM "Specimen Type" WHERE terminated is false') : req.get_value('Specimen Types')
      tests_by_specimen = {}
      specimen_types.each{|specimen|
        tests = ctx.find_subjects(query:"FROM Test WHERE <-Requisition::\"Tests\" = #{req.id} AND (\"Specimen Types\" = #{specimen.id} OR \"Specimen Types\" = null)")
        if tests
          tests_by_specimen[specimen.id] = tests.map{|test| {id: test.id, name: test.name}}
        end
      }
      specimen = sample ? sample.get_value('Specimen Type') : req.get_value('Specimen Types')[0]
      tests = req.get_value('Tests')

      [
        udf('Specimen Type', nil, value:specimen, required:true, allowCreate:false,
          filter: any_specimen_type ? nil : "<-Requisition::\"Specimen Types\" = #{req.id}"),
        udf('Tests', nil, value:tests, required:true, allowCreate:false,
          filter: "<-Requisition::\"Tests\" = #{req.id} AND (\"Specimen Types\" = #{specimen ? specimen.id : 0} OR \"Specimen Types\" = null)",
          react: {
            change: "
              this.setFilter('<-Requisition::\"Tests\" = #{req.id} AND (\"Specimen Type\" = '+(value || 0)+' OR \"Specimen Types\" = null)');
              this.setValue(#{tests_by_specimen.to_json}[value]);",
            only: 'Specimen Type'
        }),
        sample ? nil : udf('Collection Date', subj, anchor:'60%'),
        field_row([
          udf('Volume', subj),
          udf('Unit', subj, allowCreate:false, labelWidth: 60)
        ]),
        system_settings.get_value('Use Concentration on Sample') ? field_row([
          udf('Concentration', subj),
          udf('Concentration Unit', subj, fieldLabel: 'Unit', labelWidth: 60, allowCreate:false)
        ]) : nil,
        udf('Comments', subj, anchor: '100%')
      ]
    end

    
    def edit_layout(subj)
      encode_fields(edit_fields(subj))
    end
    
    def edit_layout_portal(subj)
      encode_fields(edit_fields_portal(subj))
    end
    
  end
  
end