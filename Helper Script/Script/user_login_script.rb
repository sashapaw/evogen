subjects = []
dashboards = []
user = User.curr_user

facility = user.get_value('Facility') # Client Portal
patient = user.get_value('Patient') # Patient Portal

if patient
  subjects << { subject: patient.id, glyph: 0xf015, title:'Patient Portal', closable: false }
elsif facility
  fp = facility.get_value('Facility Portal')
  if fp.present? # clients
	subjects << { subject: fp.first.id, glyph: 0xf015, title:'Client Portal', closable: false } 
  else # lab
    dashboards << { name:'LabHomePage', glyph:0xf015, title:'', closable:false } 
  end
end

user.initial_views = {home_page: false, subjects: subjects} if subjects.present?

user.initial_views = {home_page: false, dashboards: dashboards} if dashboards.present?
