require 'ruby-hl7'

module LISHL7
  
  module EHR
    module Importer

      class Processor
        def context
          @context ||= ScriptRunner.context
        end

        def split_segment(str)
          if str
            str.split('^')
          else
            [""]
          end
        end

        def data_parse(data)
          ::HL7::Message.new(data)
=begin
          message = {}
          message[:IN1] = []
          message[:DG1] = []
          message[:OBR] = []
          obr_count = -1
          begin
            data.each_line do | line |
              line = line.strip
              msg = ::HL7::Message.new( line )
              if msg[:MSH]
                message[:MSH] = {
                  :enc_chars => msg[:MSH].enc_chars,
                  :sending_app => msg[:MSH].sending_app,
                  :sending_facility => msg[:MSH].sending_facility,
                  :recv_app => msg[:MSH].recv_app,
                  :recv_facility => msg[:MSH].recv_facility,
                  :time => msg[:MSH].time,
                  :message_type => msg[:MSH].message_type,
                  :message_control_id => msg[:MSH].message_control_id,
                  :processing_id => msg[:MSH].processing_id,
                  :version_id => msg[:MSH].version_id
                  }
              end
              if msg[:PID]
                message[:PID] = {
                  :set_id => msg[:PID].set_id,
                  :patient_id => msg[:PID].patient_id,
                  :patient_id_list => msg[:PID].patient_id_list,
                  :alt_patient_id => msg[:PID].alt_patient_id,
                  :patient_name => split_segment(msg[:PID].patient_name),
                  :patient_dob => msg[:PID].patient_dob,
                  :admin_sex => msg[:PID].admin_sex,
                  :race => msg[:PID].race,
                  :address => split_segment(msg[:PID].address),
                  :phone_home => split_segment(msg[:PID].phone_home),
                  :phone_business => split_segment(msg[:PID].phone_business),
                  :marital_status => msg[:PID].marital_status,
                  :account_number => msg[:PID].account_number,
                  :social_security_num => msg[:PID].social_security_num
                  }
              end
              if msg[:ORC]
                message[:ORC] = {
                  :order_control => msg[:ORC].order_control,
                  :placer_order_number => msg[:ORC].placer_order_number,
                  :parent => msg[:ORC].parent,
                  :date_time_of_transaction => msg[:ORC].date_time_of_transaction,
                  :ordering_provider => split_segment(msg[:ORC].ordering_provider)
                  }
              end
              if msg[:IN1]
                message[:IN1].push({
                  :set_id => msg[:IN1].set_id,
                  :insurance_company_id => msg[:IN1].insurance_company_id,
                  :insurance_company_name => msg[:IN1].insurance_company_name,
                  :insurance_company_address => split_segment(msg[:IN1].insurance_company_address),
                  :insurance_company_phone_number => split_segment(msg[:IN1].insurance_company_phone_number),
                  :group_number => msg[:IN1].group_number,
                  :name_of_insured => split_segment(msg[:IN1].name_of_insured),
                  :insureds_relationship_to_patient => msg[:IN1].insureds_relationship_to_patient,
                  :insureds_date_of_birth => msg[:IN1].insureds_date_of_birth,
                  :policy_number => msg[:IN1].policy_number,
                  :insureds_sex => msg[:IN1].insureds_sex,
                  })
              end
              if msg[:DG1]
                message[:DG1].push({
                  :set_id => msg[:DG1].set_id,
                  :diagnosis_code => split_segment(msg[:DG1].diagnosis_code)
                  })
              end
              ## OBR's hold the OBX, NTE
              if msg[:OBR]
                obr_count = obr_count + 1
                message[:OBR].push({
                  :set_id => msg[:OBR].set_id,
                  :placer_order_number => msg[:OBR].placer_order_number,
                  :universal_service_id => split_segment(msg[:OBR].universal_service_id),
                  :priority => msg[:OBR].priority,
                  :observation_date => msg[:OBR].observation_date,
                  :specimen_source => msg[:OBR].specimen_source,
                  :ordering_provider => split_segment(msg[:OBR].ordering_provider),
                  :OBX => [],
                  :NTE => []
                  })
              end
              if msg[:OBX]
                #Added this fix to what appears to be bad parsing.
                if msg[:OBX].set_id.nil?
                  obx = msg[:OBX].to_s.split("|")
                  if obx.size > 1
                    message[:OBR][obr_count][:OBX].push({
                      :set_id => obx[1],
                      :value_type => obx[2],
                      :observation_id => split_segment(obx[3]),
                      :observation_value => obx[5]
                      })
                  else
                    Rails.logger.info "[EMR Importer]::Invalid OBX Message"
                  end
                else
                  message[:OBR][obr_count][:OBX].push({
                    :set_id => msg[:OBX].set_id,
                    :value_type => msg[:OBX].value_type,
                    :observation_id => split_segment(msg[:OBX].observation_id),
                    :observation_value => msg[:OBX].observation_value
                    })
                end
              end
              if msg[:NTE]
                message[:OBR][obr_count][:NTE].push({
                  :set_id => msg[:NTE].set_id,
                  :source => msg[:NTE].source,
                  :comment => msg[:NTE].comment
                  })
              end
            end
          # Let it fly through...
          #rescue Exception => e
          #  Rails.logger.error("Error parsing HL7: #{e.message}\n#{e.backtrace.join("\n")}")
          end
          return message
=end
        end

        def cleanse(data)
          cleaned = data.gsub(/\r\n?/,"\r")
          begin
            cleaned = cleaned.encode("UTF-8")
          rescue Encoding::UndefinedConversionError
            # couldn't encode
          end
          return cleaned
        end

        def generate_diagnosis_codes(diagnosis_array)
          codes = []
          diagnosis_array.each do | diagnosis |
            codes.push(diagnosis[:diagnosis_code][0])
          end
          return codes
        end

        def generate_speciment_source(obr_array)
          specimens = Set[]
          obr_array.each do | obr |
            specimen = obr[:specimen_source]
            if specimen
              specimens.add(specimen)
            else
              specimens.add('UR')
            end
          end
          return specimens.to_a
        end

        def get_medications(message)
          #This is geared only for use with KIPU
          medications = []
          message[:OBR].each do | obr |
            obr[:NTE].each do | nte |
              begin
                #Only if medications is first word.
                if nte[:comment].split.first.include? "Medications"
                  med_str = nte[:comment].split(' ', 2)
                  medications = med_str[1].split(",")
                end
              rescue
                Rails.logger.info "There was a problem parsing comment for Medications."
              end
            end
          end

          return medications
        end

        def generate_test_codes(obr_array)
          tests = []
          obr_array.each do | obr |
            test = obr[:universal_service_id][0]
            if test
              tests.push(test)
            end
          end
          return tests
        end

        def get_comments(message)
          comments = ''
          message[:OBX].each do | obx |
            test = obx[:observation_id][0]
            if test == "Comments"
              comments.concat(obx[:observation_value] + "\n")
            end
          end

          message[:NTE].each do | nte |
            comments.concat(nte[:comment] + "\n")
          end

          return comments
        end

        def get_bill_to(message)
          if message[:IN1].present?
            return 'Insurance'
          end
          #return 'Patient/Self Pay'
        end

        def clean_diagnosis_code(code)
          code = code.gsub(".","")
          return code
        end

        def get_insurance_companies(message)
          insurance = {}
          message[:IN1].each_with_index do |item, index|
            insurance[index.to_s] = {
              id:item[:insurance_company_id],
              name:item[:insurance_company_name].strip,
              address:item[:insurance_company_address],
              phone_number:item[:insurance_company_phone_number],
              policy_number:item[:policy_number],
              group_number:item[:group_number],
              relation:item[:insureds_relationship_to_patient],
              insured_name:item[:name_of_insured]
              }
          end
          return insurance
        end

        def find_patient_id(patient_msg)
          id = nil
          if patient_msg[:patient_id].present?
            id = patient_msg[:patient_id]
          elsif patient_msg[:alt_patient_id].present?
            id = patient_msg[:alt_patient_id]
          end

          return id
        end

        def create_file(ehr_record_name, message)
          filename = "#{Rails.root}/tmp/#{ehr_record_name}.hl7"
          file = "#{ehr_record_name}.hl7"
          begin
            f = File.open(filename, "w")
            f.write(message)
          rescue IOError => e
            #some error occur, dir not writable etc.
            ensure
            f.close unless f.nil?
          end
          return f
        end
      end

    end
  end
end
