require 'base64'
def file_to_base64(file_path)
    encoded_string = Base64.strict_encode64(File.open(file_path, "rb").read) 
end
# Find QC Test Results for a given date range
def findQCData(context, startDate, endDate, analytes,instruments, report)
  query = context.search_query(subject_type: 'QC Test Result') do |qb|
    q = qb.and(qb.compare('"QC Run"->"Run Date/Time"', :gte, startDate))
    q << qb.and(qb.compare('"QC Run"->"Run Date/Time"', :lte, endDate))
    q << qb.and(qb.state('Finalized'))
    q << qb.and(qb.compare('"QC Run"->"Instrument"', :in, instruments)) if instruments
    q <<  qb.and(qb.compare('"QC Sample"->"Analytes"', :in, analytes)) if analytes
    q
  end

  qc_test_results = context.find_subjects({query: query})
  qc_test_results = removeNonNumeric(qc_test_results)  
  
  qc_samples = qc_test_results.collect{|qct| qct.get_value('QC Sample')}.uniq
  qc_instruments = qc_test_results.collect{ |qct| qct.get_value('QC Run').get_value('Instrument')}.uniq
  charts = []

  qc_instruments.each do |instrument|
    qc_samples.each do |qcs|
      control_type = qcs.get_value('Control Type')

      qc_analytes = analytes ? analytes : qcs.get_value('Analytes')
      qc_analytes.each do |analyte|

        # find all the test results for that control type, analyte, instrument and within the date range
        # Don't pull in results that have no value
        query_for_graph = context.search_query(subject_type: 'QC Test Result') do |qb|
          qb.and(
            qb.compare('"QC Run"->"Run Date/Time"', :gte, startDate),
            qb.compare('"QC Run"->"Run Date/Time"', :lte, endDate),
            qb.compare('"Analyte"', :eq, analyte),
            (qb.state('Finalized')),
            qb.compare('"QC Sample"->"Control Type"', :eq, control_type),
            qb.compare('"QC Sample"->"Lot Number"', :eq, qcs.get_value('Lot Number')),
            qb.compare('"QC Run"->"Instrument"', :eq, instrument),
            qb.prop('Value').neq(nil)
          )
        end
        qc_test_results_for_LJchart = context.find_subjects({query: query_for_graph})
        qc_test_results_for_LJchart =  removeNonNumeric(qc_test_results_for_LJchart)
        
        if( qc_test_results_for_LJchart.size > 0) # don't create an empty chart
          # Get the dictionary record for the first QC test result (doesn't matter which), just to plot the target line 
          # on the chart as well as maximum/minimum ranges
          dictionary = qc_test_results_for_LJchart[0].get_value('QC Range')
          # Create a chart object to hold all the data so it can be graphed with header and footer
          chart = create_subject('LJ Chart') do |chart|
            chart.set_value('Instrument', instrument)
            chart.set_value('QC Test Results', qc_test_results_for_LJchart)
            chart.set_value('Facility', User.curr_user.get_value('Facility'))
            chart.set_value('QC Sample', qcs)
            chart.set_value('Start Date', startDate)
            chart.set_value('End Date', endDate)
            chart.set_value('QC Range', dictionary)
            chart.set_value('Analyte', analyte)
            #qc_values = qc_test_results_for_LJchart.collect{|qct| qct.get_value('Value').to_f}
          end

          charts << chart
        end
      end
    end
  end
  # sort to group the same analyte + control together
 # charts = charts.sort_by { |chart| [ chart.get_value('QC Sample').get_value('Control Type').downcase]}
  charts = charts.sort{|a1,a2|      
      a1.get_value('Analyte').name <=> a2.get_value('Analyte').name
    }.sort{|a1,a2|a1.get_value('Instrument').name <=> a2.get_value('Instrument').name}
  report.set_value('LJ Charts', charts)
  file = createReport(context, charts, report)
  # Comment this out when PDF generation works....right now this is generating an HTML file
  report.set_value('File', file)
end

def createReport (context, charts, report)

  # Uncomment this when PDF generation for javascript charts works..................
  #file = run_custom_report('Levey Jennings Report', report, 'File') do |extra_data|
  system_settings = SubjectType.find_by_name('System Settings').subjects.first
  address = ''
  if system_settings.get_value('Lab Address')
    address_array = system_settings.get_value('Lab Address').split("\n")
    address = address_array.join('<br/>')
  end
  file = run_custom_report('Levey Jennings Report', report, nil, :html) do |extra_data|
    extra_data['public_path'] = File.absolute_path('public')
    extra_data['Lab Name'] = system_settings.get_value('Lab Name')
    extra_data['Lab Logo'] = file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path))
    extra_data['Lab Logo Vertical'] = system_settings.get_value('Lab Logo Vertical')
    extra_data['Lab Address'] = address
    extra_data['LJ Charts'] = charts
  end 
  #File.delete(header_path, footer_path, file.path)
  return file
end


def createQCReport(context, startDate, endDate, analytes, instruments)
  report = create_subject('QC Report') do |report|
    report.set_value('Start Date', startDate)
    report.set_value('End Date', endDate)
    if analytes.present?
      report.set_value('Analytes', analytes)
    end
    if instruments.present?
      report.set_value('Instruments', instruments)
    end
  end
  advance_workflow('QC Report', 'Ready for Approval',  report)
  return report
end

def generate_qc_report(context, startDate, endDate, analytes, instruments)
  report  = createQCReport(context, startDate, endDate, analytes, instruments)
  findQCData(context, startDate, endDate, analytes, instruments, report)
end

def mean(array)
  (average = array.sum.to_f/array.size.to_f).round(2) 
end

def sd(array)
  m = mean(array) 
  variance = array.inject(0) { |variance, x| variance += (x - m) ** 2 } 
  return (Math.sqrt(variance/(array.size-1))).round(2)
end

def is_number? string
  true if Float(string) rescue false
end

def removeNonNumeric(array)
  resultArray = []
  array.each do |value|
    # delete non-numeric results, do not plot them
    if is_number?( value.get_value('Value'))
      resultArray << value
    end
  end
  return resultArray
end