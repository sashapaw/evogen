module TestingScheduler
  extend self

  def notify_physicians(subj, params)
    ctx = ScriptRunner.context
    ops = ActionMailer::Base.default_url_options
    raise "Email service is not configured." unless ops and ops[:protocol] and ops[:host]

    facility = subj.get_value('Facility')
    params['base_url'] = base_url
    qb = QE::Builder.new
    q = qb.and(qb.prop('Facility').eq(facility), qb.condition('Physician is NOT null'))
    q = QE::QOM::Query.new(nil,User,q)

    qom2arel = QE::QOMToArel.new(User)
    arel = q.accept(qom2arel)
    users = qom2arel.target_class.find_by_sql(arel)
    raise "No Physicians for Facility #{facility} to approve the schedule. Enable Physician Portal access for at least one Physician." if users.size==0
  
    users.each do |u| 
      ctx.send_email([u.email] , ctx.find_email_template('Testing Schedule Awaiting Approval'), subj, params)
    end
  end


  def schedule_dates(dates_range, type, opts)
    dates = case type
    when 'Defined'
      dates_range.select{|d| wd = d.strftime('%A'); opts.include?(wd) }
    when 'Defined from Start Date'
      dev = {'Every Second Day'=>2, 'Every Third Day'=>3}[opts]
      dates = []
      dates_range.each_with_index{|d,i| dates << d if (i % dev) == 0} if dev
      dates
    when 'Randomized'
      dev = {'Once Every 7 Days'=>1, 'Twice Every 7 Days'=>2, 'Three Times Every 7 Days'=>3}[opts]
      dates = []
      dates_range.to_a.to_a.in_groups_of(7,false).each {|g| dates += g.sample(dev)} if dev
      dates
    else
      []
    end
    dates
  end

  def extract_conditions(params, type)
    type_key = ['Schedule Type (PoC)','Schedule Type (Lab)'].find{|s| s.index(type)}
    hopts = {'Defined'=>'Defined Schedule', 'Defined from Start Date' => 'Defined from Start Date', 'Randomized'=> 'Randomized Schedule'}
    type_value = params[type_key]
    opt_key = "#{hopts[type_value]} #{type}"
    opt_value = params[opt_key]
    [type_value, opt_value]
  end


  def verify_and_save_schedule(subj, params)
    raise "Start Date can not be in the past" if params['Start Date'] < Date.today
    raise "End Date can not be earlier than Start Date" if params['End Date'] < params['Start Date']
    raise "End Date can not be less that 7 days from Start Date" if params['End Date'] - params['Start Date'] < 7
    raise "End Date can not be more that 90 days from Start Date" if params['End Date'] - params['Start Date'] > 90
    raise "You must fill out Schedule Type (PoC) and Schedule Type (Lab) in order for the patient to display on the schedule" if !params['Schedule Type (PoC)'].present? && !params['Schedule Type (Lab)'].present?
    raise "You must fill out Defined Schedule (Lab)" if params['Schedule Type (Lab)']=="Defined" && !params['Defined Schedule (Lab)'].present?
  
    # clear old values
    subj.remove_prop('Defined Schedule (Lab)')
    subj.remove_prop('Defined Schedule (PoC)')
    subj.remove_prop('Defined from Start Date (Lab)')
    subj.remove_prop('Defined from Start Date (PoC)')
    subj.remove_prop('Randomized Schedule (Lab)')
    subj.remove_prop('Randomized Schedule (PoC)')
  
    # remove extra values from parameters
    params.delete('Defined Schedule (Lab)') unless params['Schedule Type (Lab)'] == 'Defined'
    params.delete('Defined Schedule (PoC)') unless params['Schedule Type (PoC)'] == 'Defined'
    params.delete('Defined from Start Date (Lab)') unless params['Schedule Type (Lab)'] == 'Defined from Start Date'
    params.delete('Defined from Start Date (PoC)') unless params['Schedule Type (PoC)'] == 'Defined from Start Date'
    params.delete('Randomized Schedule (Lab)') unless params['Schedule Type (Lab)'] == 'Randomized'
    params.delete('Randomized Schedule (PoC)') unless params['Schedule Type (PoC)'] == 'Randomized'
  
    # set new
    params.each_pair{|udf, value|
      if value.present?
        subj.set_value(udf, value)
      else
        subj.remove_prop(udf)
      end
    }
  end


  def clear_schedule(subj, params)
    params.each_pair{|udf, value|
  	  subj.remove_prop(udf)
    }
    subj.remove_prop('Awaiting Physician Approval') # AND ADD others
    subj.remove_prop('Schedule Defined By')
    subj.remove_prop('Schedule Approved By')
    subj.remove_prop('Schedule Rejected By')
    subj.remove_prop('Schedule Approval Date')
    subj.remove_prop('Testing Schedule Comments')

    subj.remove_prop('Defined Schedule (Lab)')
    subj.remove_prop('Defined Schedule (PoC)')
    subj.remove_prop('Defined from Start Date (Lab)')
    subj.remove_prop('Defined from Start Date (PoC)')
    subj.remove_prop('Randomized Schedule (Lab)')
    subj.remove_prop('Randomized Schedule (PoC)')
  end


  def create_schedule_tests(subj, params)
    dates_range = (params['Start Date']..params['End Date'])
    type, opts = extract_conditions(params, '(Lab)')
    lab_dates = schedule_dates(dates_range, type, opts)

    schedule = lab_dates.inject({}) {|h, a| h.merge!({a=>{:lab=>true}})}
    type, opts = extract_conditions(params, '(PoC)')
    poc_dates = schedule_dates(dates_range, type, opts)
    poc_dates.each do |d|
      schedule[d] ||= {}
      schedule[d].merge!({:poc=>true})
    end
  
    ctx = ScriptRunner.context
    ctx.init_progress(schedule.size)
    schedule.each do |d, opts|	
      s = ctx.create_subject('Testing Schedule') do |ts|
        ts.set_value('Patient', subj)
        ts.set_value('Testing Date', d)
        ts.set_value('PoC Test', true) if opts[:poc]
        ts.set_value('Lab Test', true) if opts[:lab]
        ts.set_value('Tests', params['Scheduled Tests']) if params['Scheduled Tests'].present?
        ts.set_value('Test Panels', params['Scheduled Test Panels']) if params['Scheduled Test Panels'].present?
      end
      ctx.start_workflow('Testing Schedule',s)
      ctx.advance_progress
    end
  end
end