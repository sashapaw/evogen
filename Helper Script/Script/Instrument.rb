module Instrument
  extend self
  
  class Submission

    attr_accessor :result_entry, :instrument, :implementation

    def initialize(data)
      @ctx = ScriptRunner::Context.new(User.curr_user)
      @result_entry = ResultEntry.new(@ctx, data)
      @instrument = get_instrument(data)
      @implementation = get_implementation(data)
    end

    def done
      @result_entry.flush
    end

    def get_instrument(data)
      instrument = Instrument.find_instrument(data)
      @result_entry.new_message("Instrument", "Instrument: #{data.except('data').inspect} not found") if instrument.nil?
      instrument
    end

    def get_implementation(data)
      if @instrument.present?
        @result_entry.set_instrument(@instrument)
        instrument_type = @instrument.get_value('Instrument Type')
        instrument_type.integration_class.new(@instrument, @result_entry, @ctx)
      else
        @result_entry.new_message("Instrument", "Instrument could not be found")
        nil
      end
    end
  end

  class ResultEntry
    attr_accessor :result_entry, :samples, :instrument

    def initialize(ctx, data)
      @ctx = ctx
      @result_entry = create_entry(data)
      @samples = []
      @test_results = []
      @qc_runs = []
    end

    def create_entry(data)
      @ctx.create_subject('Result Entry') do |re|
        re.set_value('Instrument Result Data', data)
      end
    end

    def set_instrument(instrument)
      @instrument = instrument
      @result_entry.set_value('Instrument', instrument)
    end

    def add_sample(sample)
      #@result_entry.set_value('Sample', sample)
      #@result_entry.add_value('Associated Samples', sample)
      @samples << sample unless @samples.include?(sample)
    end

    def set_requisition(requisition)
      @result_entry.set_value('Requisition', requisition)
    end

    def add_qc_run(qc_run)
      @qc_runs << qc_run
    end
    
    def add_qc_runs(qc_runs)
      @qc_runs += qc_runs if qc_runs.present?
    end

    def new_message(msg_type, msg)
      Rails.logger.info("creating result entry message: #{msg_type}: #{msg}")
      @ctx.create_subject('Result Entry Message') do |rem|
        rem.set_value('Instrument Result Entry', @result_entry)
        rem.set_value('Message Type', msg_type)
        rem.set_value('Message', msg)
      end
    end
    
    def add_test_result(test_result)
      @test_results << test_result
    end

    def link_test_results(test_results)
      @test_results += test_results
    end

    def flush
      @result_entry.set_value('Associated Samples', @samples) if @samples.present?
      @result_entry.set_value('Associated Test Results', @test_results) if @test_results.present?
      @result_entry.set_value('Associated QC Runs', @qc_runs) if @qc_runs.present?
    end
  end

  def find_instrument(d)
    q = if d['uid'].present?
      QE::Builder.prop(:id).eq(d['uid'].to_i)
    elsif d['serial_number'].present?
      QE::Builder.prop('Serial Number').eq(d['serial_number'])
    elsif d['mac_address'].present?
      QE::Builder.prop('MAC Address').eq(d['mac_address'])
    else
      raise "No instrument identifier in request"
    end
    ctx = context
    instruments = ctx.find_subjects( query: ctx.search_query(from: "Instrument", where: q) )
    instruments.first
  end

  def new_instrument(d)
    ctx = context
    ist = ctx.find_subject_type('Instrument')
    iname = "#{ist.name} #{d['serial_number'] || d['mac_address'] || ist.get_next_name_number}"
    instrument = ctx.create_subject(ist, name:iname) do |sp|
      sp.set_value('MAC Address', d['mac_address'])
      sp.set_value('Serial Number', d['serial_number'])
      #sp.set_value('Last Seen', Time.zone.now)
    end
    ctx.start_workflow('Instrument', instrument)
    instrument
  end

end
