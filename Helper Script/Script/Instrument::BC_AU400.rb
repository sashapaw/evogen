module Instrument
  class BC_AU400 < BC_AU480

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @analyte_map = get_analyte_map
    end

    def get_analyte_map
      {
        '001' => 'Creatinine',
        '002' => 'pH',
        '003' => 'Oxidants',
        '004' => 'Specific Gravity'
      }
    end

  end
end
