module Facility
  extend self
  
  def create(params)
    c = context
    s = c.create_subject('Facility', {:name=>params['Name']}) do |v|
      v.set_value('Contact Name', params['Contact Name'])
      v.set_value('Contact Email', params['Contact Email'])
      v.set_value('Phone Number', params['Phone Number'])
      v.set_value('Fax Number', params['Fax Number'])
      v.set_value('Address', params['Address'])
      v.set_value('Comments', params['Comments'])
      v.set_value('Additional Contacts', params['Additional Contacts'])
      v.set_value('Facility Type', params['Facility Type'])
    end
    c.advance_workflow('Facility', 'Active', s)
    find_or_create_group(s)  
    s
  end

  def find_or_create_group(facility)
    q = context.search_query(from: UserGroup) {|qb|
      qb.and qb.prop('Facility').eq(facility),
             qb.condition("name LIKE '\\_%'")
      }
    ugs = context.execute_query q

    return ugs.first if ugs.present?

    ug = UserGroup.create! name: "_#{facility.subject_type.name} #{facility.name}", acts_as_configuration: false
    ug.extend ScriptRunner::Helper
    ug.set_value 'Facility', facility

    Acl.set_permission facility, ug, Acl::READWRITE
    ug
  end

end