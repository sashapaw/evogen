module Sample
  # Evogen custom layout for sample
  module CustomLayout
    include Sample::DefaultLayout
    extend self

    def edit_fields(subj)
      req = subj.subject_type.name == 'Requisition' ? subj : subj.get_value('Requisition')
      fields = super(subj)
      i = fields.index{|f| f && f[:udfName] == 'Collection Date'}
      fields[i] = udf('Collection Date', subj, anchor:'60%', required:true) if i
      fields.unshift(
        udf('DNA Extraction', subj, fieldLabel:'Use Sample for DNA Extraction'),
        udf('Parent Sample', subj, allowCreate:false, filter:"Requisition=#{req.id} AND \"DNA Extraction\"=true", react: {
          shown_when: '!value',
          only:'DNA Extraction'
        })
      )
      #fields << udf('Collection Date', subj, anchor:'60%', required:true)
      fields << udf('Sample Status', subj, allowCreate:true, anchor:'100%', addCustomFields:encode_fields(set_level(2, udfs_for_subject_type('Sample Status', nil)))  )
      fields << udf('Home Drawn', subj)
      fields << udf('Sample Image', subj)
      fields
    end
    
  end
  
end