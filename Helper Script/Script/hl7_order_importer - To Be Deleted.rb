require 'set'
require 'ruby-hl7'
require_script "maybe_reflex_tests"

class Hl7_Order_Processor
  def initialize(context = "")
    @@context = context
  end

  def self.context
      @@context
    end

    def set_filename(context)
      @@context = context
    end

  def split_segment(str)
    if str
      str.split('^')
    else
      [""]
    end
  end

  def data_parse(data)
    message = {}
    message[:IN1] = []
    message[:DG1] = []
    message[:OBR] = []
      obr_count = -1
    begin
      data.each_line do | line |
        line = line.strip
        msg = HL7::Message.new( line )
        if msg[:MSH]
          message[:MSH] = {
            :enc_chars => msg[:MSH].enc_chars,
            :sending_app => msg[:MSH].sending_app,
            :sending_facility => msg[:MSH].sending_facility,
            :recv_app => msg[:MSH].recv_app,
            :recv_facility => msg[:MSH].recv_facility,
            :time => msg[:MSH].time,
            :message_type => msg[:MSH].message_type,
            :message_control_id => msg[:MSH].message_control_id,
            :processing_id => msg[:MSH].processing_id,
            :version_id => msg[:MSH].version_id
          }
        end
        if msg[:PID]
          message[:PID] = {
            :set_id => msg[:PID].set_id,
            :patient_id => msg[:PID].patient_id,
            :patient_id_list => msg[:PID].patient_id_list,
            :alt_patient_id => msg[:PID].alt_patient_id,
            :patient_name => split_segment(msg[:PID].patient_name),
            :patient_dob => msg[:PID].patient_dob,
            :admin_sex => msg[:PID].admin_sex,
            :race => msg[:PID].race,
            :address => split_segment(msg[:PID].address),
            :phone_home => split_segment(msg[:PID].phone_home),
            :phone_business => split_segment(msg[:PID].phone_business),
            :marital_status => msg[:PID].marital_status,
            :account_number => msg[:PID].account_number,
            :social_security_num => msg[:PID].social_security_num
          }
        end
        if msg[:ORC]
            message[:ORC] = {
              :order_control => msg[:ORC].order_control,
              :placer_order_number => msg[:ORC].placer_order_number,
              :parent => msg[:ORC].parent,
              :date_time_of_transaction => msg[:ORC].date_time_of_transaction,
              :ordering_provider => split_segment(msg[:ORC].ordering_provider)
            }
        end
        if msg[:IN1]
            message[:IN1].push({
              :set_id => msg[:IN1].set_id,
              :insurance_company_id => msg[:IN1].insurance_company_id,
              :insurance_company_name => msg[:IN1].insurance_company_name,
              :insurance_company_address => split_segment(msg[:IN1].insurance_company_address),
              :insurance_company_phone_number => split_segment(msg[:IN1].insurance_company_phone_number),
              :group_number => msg[:IN1].group_number,
              :name_of_insured => split_segment(msg[:IN1].name_of_insured),
              :insureds_relationship_to_patient => msg[:IN1].insureds_relationship_to_patient,
              :insureds_date_of_birth => msg[:IN1].insureds_date_of_birth,
              :policy_number => msg[:IN1].policy_number,
              :insureds_sex => msg[:IN1].insureds_sex,
            })
        end
        if msg[:DG1]
            message[:DG1].push({
              :set_id => msg[:DG1].set_id,
              :diagnosis_code => split_segment(msg[:DG1].diagnosis_code)
            })
        end
        ## OBR's hold the OBX, NTE
        if msg[:OBR]
          obr_count = obr_count + 1
          message[:OBR].push({
            :set_id => msg[:OBR].set_id,
            :placer_order_number => msg[:OBR].placer_order_number,
            :universal_service_id => split_segment(msg[:OBR].universal_service_id),
            :priority => msg[:OBR].priority,
            :observation_date => msg[:OBR].observation_date,
            :specimen_source => msg[:OBR].specimen_source,
            :ordering_provider => split_segment(msg[:OBR].ordering_provider),
            :OBX => [],
            :NTE => []
          })
        end
        if msg[:OBX]
          #Added this fix to what appears to be bad parsing.
          if msg[:OBX].set_id.nil?
            obx = msg[:OBX].to_s.split("|")
            if obx.size > 1
              message[:OBR][obr_count][:OBX].push({
                :set_id => obx[1],
                :value_type => obx[2],
                :observation_id => split_segment(obx[3]),
                :observation_value => obx[5]
              })
            else
              Rails.logger.info "[EMR Importer]::Invalid OBX Message"
            end
          else
            message[:OBR][obr_count][:OBX].push({
              :set_id => msg[:OBX].set_id,
              :value_type => msg[:OBX].value_type,
              :observation_id => split_segment(msg[:OBX].observation_id),
              :observation_value => msg[:OBX].observation_value
            })
          end
        end
        if msg[:NTE]
          message[:OBR][obr_count][:NTE].push({
            :set_id => msg[:NTE].set_id,
            :source => msg[:NTE].source,
            :comment => msg[:NTE].comment
          })
        end
      end
    rescue Exception => e
      #puts "Parsing Failure: #{e}"
    end
    return message
  end

  def cleanse(data)
    cleaned = data.gsub(/\r\n?/,"\n")
    begin
      cleaned = cleaned.encode("UTF-8")
    rescue Encoding::UndefinedConversionError
        # couldn't encode
    end
    return cleaned
  end

  def generate_diagnosis_codes(diagnosis_array)
    codes = []
    diagnosis_array.each do | diagnosis |
      codes.push(diagnosis[:diagnosis_code][0])
    end
    return codes
  end

  def generate_speciment_source(obr_array)
    specimens = Set[]
    obr_array.each do | obr |
      specimen = obr[:specimen_source]
      if specimen
        specimens.add(specimen)
      else
        specimens.add('UR')
      end
    end
    return specimens.to_a
  end

  def get_medications(message)
  #This is geared only for use with KIPU
    medications = []
    message[:OBR].each do | obr |
     obr[:NTE].each do | nte |
       begin
        #Only if medications is first word.
        if nte[:comment].split.first.include? "Medications"
          med_str = nte[:comment].split(' ', 2)
          medications = med_str[1].split(",")
        end
       rescue
         Rails.logger.info "There was a problem parsing comment for Medications."
       end
     end
    end

    return medications
  end

  def generate_test_codes(obr_array)
    tests = []
    obr_array.each do | obr |
      test = obr[:universal_service_id][0]
      if test
        tests.push(test)
      end
    end
    return tests
  end

  def get_comments(message)
    comments = ''
    message[:OBX].each do | obx |
      test = obx[:observation_id][0]
      if test == "Comments"
        comments.concat(obx[:observation_value] + "\n")
      end
    end

    message[:NTE].each do | nte |
      comments.concat(nte[:comment] + "\n")
    end

    return comments
  end

  def get_bill_to(message)
    if !message[:IN1].nil? && message[:IN1].size > 0
      return 'Insurance'
    end
    #return 'Patient/Self Pay'
  end

  def clean_diagnosis_code(code)
    code = code.gsub(".","")
    return code
  end

  def get_insurance_companies(message)
    insurance = {}
    message[:IN1].each_with_index do |item, index|
      insurance[index.to_s] = {
        id:item[:insurance_company_id],
        name:item[:insurance_company_name],
        address:item[:insurance_company_address],
        phone_number:item[:insurance_company_phone_number],
        policy_number:item[:policy_number],
        group_number:item[:group_number],
        relation:item[:insureds_relationship_to_patient]
     }
    end
    return insurance
  end

  def find_patient_id(patient_msg)
    id = nil
    if patient_msg[:patient_id].present?
      id = patient_msg[:patient_id]
    elsif patient_msg[:alt_patient_id].present?
      id = patient_msg[:alt_patient_id]
    end

    return id
  end

  def create_file(ehr_record_name, message)
    filename = "#{Rails.root}/tmp/#{ehr_record_name}.hl7"
    file = "#{ehr_record_name}.hl7"
    begin
      f = File.open(filename, "w")
      f.write(message)
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      f.close unless f.nil?
    end
    return f
  end
end

def find_by_attr(sub_type,attr,value,like = false)
  query = search_query(subject_type: sub_type) do |qb|
    q = qb.and
    if like
      q << qb.compare(attr, :like, "%#{value}%")
    else
      q << qb.compare(attr, :eq, value)
    end
    q
  end
  return find_subjects(query: query).first
end

def find_by_attrs(sub_type,attrs,values)
  begin
    query = search_query(subject_type: sub_type) do |qb|
      q = qb.and
      attrs.each_with_index { |attr,index|
       q << qb.compare(attr, :eq, values[index])
      }
      q
    end
    return find_subjects(query: query).first
  rescue Exception=>e
    Rails.logger.info "Bad query: #{query.inspect} Error: #{e}"
  end
end


def lookup_requisition(alt_req_id)
  r = find_by_attr('Requisition', 'External Source ID', alt_req_id)
  return r
end

def lookup_facility(facility)
  f = find_by_attr('Facility', 'name', facility)
  return f
end

def lookup_test_panel(test_code)
  t = find_by_attr('Test Panel', 'Code', test_code)
  return t
end

def lookup_test(test_code)
  t = find_by_attr('Test', 'Code', test_code)
  return t
end

def lookup_patient(patient_id,facility)
  if facility
    p = find_by_attrs('Patient',['MRN','Facility'],[patient_id,facility])
    if !p
      p = find_by_attrs('Patient',['External Source ID','Facility'],[patient_id,facility])
    end
  end
  return p
end

def lookup_physician(physician_npi, facility)
  p = if facility.present?
    find_by_attrs('Physician', ['NPI Number','Facility'], [physician_npi,facility])
  else
    find_by_attr('Physician', 'NPI Number', physician_npi)
  end
  return p
end

def lookup_diagnoses(diagnosis_code)
  d = find_by_attr('Diagnosis', 'Code', diagnosis_code)
  return d
end

def lookup_specimen_types(specimen_type_code)
  if !specimen_type_code.present?
    specimen_type_code = "UR" #Default to Urine per spec sheet.
  end
  s = find_by_attr('Specimen Type', 'name', specimen_type_code, true)
  return s
end

def lookup_insurance_company(insurance_company)
  i = find_by_attr('Insurance Company', 'name', insurance_company, true)
  return i
end

def lookup_poc_test(test)
  t = find_by_attr('Point of Care Test', 'Code', test, true)
  return t
end

def lookup_medication(medication)
  t = find_by_attr('Medication', 'Description', medication)
  return t
end

def comment_check(str)
  begin
    if str.include? "Medical Necessity"
      return true
    elsif str.split.first.include? "Medications"
      return false
    end
    return true
  rescue NoMethodError=>e
    Rails.logger.info "Comment was Nil."
  end
end

def get_comments(hl7_processor,msg,requisition = nil)
  comments = []
  msg[:OBR].each do |obr|
    obr[:NTE].each do |nte|
      #KIPU sends medications via comments, pull those out.
      if comment_check(nte[:comment])
         comments.push(nte[:comment])
      end
    end
  end
  if !requisition.nil?
    requisition.set_value('Comments',comments.join(','))
  end
end

def create_insurance_company(insurance)
  ins_name = insurance[:name]
  code = insurance[:id]
  address = ''
  address.concat("#{insurance[:address][0]}").concat("\n")
  address.concat("#{insurance[:address][1]} ").concat("#{insurance[:address][2]}, ")
  address.concat("#{insurance[:address][3]} ").concat("#{insurance[:address][4]}")
  phone = insurance[:phone_number][0]

  ins = create_subject('Insurance Company') do |v|
    v.set_value('Code', code)
    v.set_value('Address', address)
    v.set_value('Phone Number', phone)
  end
  ins.name = ins_name

  start_workflow('Insurance Company', ins)
  return ins
end

def create_patient(hl7_processor,facility,msg)
  patient_msg = msg[:PID]
  #Facility*
  #Let the System Auto-Generate MRN
  #if !patient_msg[:patient_id]
  #  mrn = hl7_processor.find_patient_id(patient_msg) #MRN* - patient_id/alt_patient_id
  #else
  #  mrn = patient_msg[:patient_id]
  #end
  alt_mrn = hl7_processor.find_patient_id(patient_msg) #External Source ID - alt_patient_id
  firstname = patient_msg[:patient_name][1] #First Name* - patient_name[1]
  middlename = patient_msg[:patient_name][2] #Middle Name - patient_name[2]
  lastname = patient_msg[:patient_name][0] #Last Name* - patient_name[0]
  dob = patient_msg[:patient_dob] #DOB* - patient_dob
  sex = patient_msg[:admin_sex] #Sex* - admin_sex
  phone_number = patient_msg[:phone_home][0] #Phone Number* - phone_home
  address = ''
  address.concat("#{patient_msg[:address][0]}").concat("\n")
  address.concat("#{patient_msg[:address][1]} ").concat("#{patient_msg[:address][2]}, ")
  address.concat("#{patient_msg[:address][3]} ").concat("#{patient_msg[:address][4]}")

  patient = create_subject('Patient') do |p|
    p.set_value('First Name',firstname)
    p.set_value('Middle Name',middlename)
    p.set_value('Last Name',lastname)
  end

  patient.set_value('External Source ID',alt_mrn)
  patient.set_value('Facility',facility)
  patient.set_value('DOB',dob)
  patient.set_value('Sex',sex)
  patient.set_value('Phone Number',phone_number)
  patient.set_value('Address',address)

  bill_to = hl7_processor.get_bill_to(msg)
  bill_to = "Patient/Self Pay" if bill_to != "Insurance"

  if bill_to == "Insurance"
    patient.set_value('Bill To',bill_to)
    insurance = hl7_processor.get_insurance_companies(msg)
    begin
      i = lookup_insurance_company("#{insurance["0"][:name]}")
    rescue Exception=>e
      i = nil
      Rails.logger.info "[HL7 Order Import] There was an error looking up insurance company. #{e}"
    end
    begin
      if i
        patient.set_value('Insurance Company 1',i)
        patient.set_value('Policy #1',"#{insurance["0"][:policy_number]}")
        patient.set_value('Group #1',"#{insurance["0"][:group_number]}")
        patient.set_value('Relation To Insured',"#{insurance["0"][:relation]}")
      end
    rescue Exception=>e
      Rails.logger.info "[HL7 Order Import] There was an error processing patient insurance information. #{e}"
      if !patient.get_value("Relation To Insured").present?
        patient.set_value("Relation To Insured","Other")
      end
    end
  end
  return patient
end

def update_patient(patient,hl7_processor,facility,msg)
  patient_msg = msg[:PID]
  #Facility*
  firstname = patient_msg[:patient_name][1] #First Name* - patient_name[1]
  middlename = patient_msg[:patient_name][2] #Middle Name - patient_name[2]
  lastname = patient_msg[:patient_name][0] #Last Name* - patient_name[0]
  dob = patient_msg[:patient_dob] #DOB* - patient_dob
  sex = patient_msg[:admin_sex] #Sex* - admin_sex
  phone_number = patient_msg[:phone_home][0] #Phone Number* - phone_home
  address = ''
  address.concat("#{patient_msg[:address][0]}").concat("\n")
  address.concat("#{patient_msg[:address][1]} ").concat("#{patient_msg[:address][2]}, ")
  address.concat("#{patient_msg[:address][3]} ").concat("#{patient_msg[:address][4]}")

  begin
    patient.set_value('First Name',firstname)
    patient.set_value('Middle Name',middlename)
    patient.set_value('Last Name',lastname)
    patient.set_value('Facility',facility)
    patient.set_value('DOB',dob)
    patient.set_value('Sex',sex)
    patient.set_value('Phone Number',phone_number)
    patient.set_value('Address',address)
  rescue Exception=>e
	Rails.logger.info "There was a failure updating the patient. #{e}"
  end
  bill_to = hl7_processor.get_bill_to(msg)
  bill_to = "Patient/Self Pay" if bill_to != "Insurance"
  if bill_to == "Insurance"
    patient.set_value('Bill To',bill_to)
    insurance = hl7_processor.get_insurance_companies(msg)
    begin
      i = lookup_insurance_company("#{insurance["0"][:name]}")
      if i
        patient.set_value('Insurance Company 1',i)
        patient.set_value('Policy #1',"#{insurance["0"][:policy_number]}")
        patient.set_value('Group #1',"#{insurance["0"][:group_number]}")
        patient.set_value('Relation To Insured',"#{insurance["0"][:relation]}")
      end
    rescue Exception=>e
      Rails.logger.info "[HL7 Order Import] There was an error, #{e}"
      if !patient.get_value("Relation To Insured").present?
        patient.set_value("Relation To Insured","Other")
      end
    end
  end
  return patient
end

def get_collection_date(msg)
  collection_date = ''
  tz = Time.zone
  msg[:OBR].each do |obr|
    collection_date = DateTime.strptime(obr[:observation_date],'%Y%m%d%H%M')
    collection_date = ActiveSupport::TimeWithZone.new(nil, tz, collection_date)
    break;
  end
  collection_date
end

def get_alternate_req_id(msg)
  #alt_id = "#{msg[:MSH][:message_control_id]}"
  #KIPU uses placer_order_number
  alt_id = "#{msg[:ORC][:placer_order_number]}"
  alt_id
end

def create_req(hl7_processor,msg,system_settings)
  create_pat = system_settings[:create_patient]
  update_pat = system_settings[:update_patient]
  create_ins = system_settings[:create_insurance]
  errors = []
  warnings = []
  collection_date = get_collection_date(msg)
  #if msg[:ORC][:date_time_of_transaction]
  #   collection_date = DateTime.strptime(msg[:ORC][:date_time_of_transaction],'%Y%m%d%H%M')
  #end
  
  f = lookup_facility("#{msg[:MSH][:sending_facility]}")
  if !f
    errors.push("Could Not Find Facility #{msg[:MSH][:sending_facility]}\n")
  end
  
  req_lookup_id = get_alternate_req_id(msg)
  requisition = lookup_requisition(req_lookup_id)
  if !requisition
    requisition = create_subject('Requisition') do |req|
      #Req ID msg[:MSH][:message_control_id]/msg[:ORC][:placer_order_number]
      #Alternate Requisition ID msg[:MSH][:message_control_id]
      #req.set_value('name',"#{msg[:MSH][:message_control_id]}")
      req.set_value('Alternate Requisition ID',get_alternate_req_id(msg))
      req.set_value('Collection Date', collection_date)
      requisition.set_value('Facility',f) if f
      #req.set_value('Received Date', Time.now)
    end
  else
    warnings.push("Requisition #{msg[:MSH][:message_control_id]} already exists in the system. Updating Requisition information with import. \n")
  end

  npi_number = msg[:ORC][:ordering_provider][0]
  if npi_number
    ph = lookup_physician("#{npi_number}", f)
    if ph
      requisition.set_value('Requesting Physician',ph)
    else
      errors.push("Could Not Find Requesting Physician by NPI #{npi_number}\n")
    end
  else
    errors.push("Could Not Find Find Requesting Physician no NPI Provided.\n")
  end

  patient_id = hl7_processor.find_patient_id(msg[:PID])
  p = lookup_patient("#{patient_id}",f)
  if p
    patient_dob = Date.parse msg[:PID][:patient_dob]
    if patient_dob == p.get_value('DOB')
       if update_pat
         update_patient(p,hl7_processor,f,msg)
       end
       requisition.set_value('Patient',p)
    else
      errors.push("Could Not Find Patient with id #{patient_id} and Date of Birth of #{patient_dob}\n")
    end
  else
    if create_pat
      if errors.size == 0
        p = create_patient(hl7_processor,f,msg)
        requisition.set_value('Patient',p)
        warnings.push("Created Patient #{patient_id}")
      end
    else
      errors.push("Could Not Find Patient by id #{patient_id}\n")
    end
  end

  diagnoses = []
  hl7_processor.generate_diagnosis_codes(msg[:DG1]).each do |code|
    code = hl7_processor.clean_diagnosis_code(code)
    d = lookup_diagnoses(code)
    if d
      diagnoses.push(d)
    else
      warnings.push("Could Not Find Diagnosis Code #{code}")
    end
  end
  if diagnoses.size > 0
    requisition.set_value('Diagnoses',diagnoses)
  end

  specimen = []
  hl7_processor.generate_speciment_source(msg[:OBR]).each do |spec|
    s = lookup_specimen_types(spec)
    if s
      specimen.push(s)
    else
      warnings.push("Could Not Find Specimen Type #{spec}")
    end
  end
  if specimen.size > 0
    requisition.set_value('Specimen Types',specimen)
  end

  meds = []
  medications = hl7_processor.get_medications(msg)
  medications.each do |med|
    m = lookup_medication(med)
    if m
      meds.push(m)
    else
      warnings.push("Could Not Find Medication #{med}")
    end
  end
  if meds.size > 0
    requisition.set_value('Medications',meds)
  end

  tests = []
  test_panel_count = 0
  test_panel_names = []
  hl7_processor.generate_test_codes(msg[:OBR]).each do |test|
    tp = lookup_test_panel(test)
    if tp
      requisition.set_value('Test Panel',tp)
      tp.get_value('Tests').each do |tc|
        tests.push(tc)
      end
      test_panel_count = test_panel_count + 1
      test_panel_names.push("#{tp.name}")
    else
      t = lookup_test(test)
      if t
        tests.push(t)
      else
        errors.push("Could Not Find Test Code #{test}\n")
      end
    end
  end

  if tests.size > 0
    requisition.set_value('Tests', tests)
  end

  begin
    get_comments(hl7_processor,msg,requisition)
  rescue Exception=>e
    Rails.logger.info "There was a problem Retreiving comments"
  end

  bill_to = hl7_processor.get_bill_to(msg)
  if bill_to == "Insurance"
    requisition.set_value('Bill To',bill_to)
    insurance = hl7_processor.get_insurance_companies(msg)
    begin
      i = lookup_insurance_company("#{insurance["0"][:name]}")
      if i
        requisition.set_value('Insurance Company 1',i)
        requisition.set_value('Policy #1',"#{insurance["0"][:policy_number]}")
        requisition.set_value('Group #1',"#{insurance["0"][:group_number]}")
        requisition.set_value('Relation To Insured',"#{insurance["0"][:relation]}")
      else
        if create_ins
          if errors.size == 0
            new_ins = create_insurance_company(insurance["0"])
            warnings.push("Created Insurance Company #{insurance["0"][:name]} as it was not found.")
            requisition.set_value('Insurance Company 1',new_ins)
            requisition.set_value('Policy #1',"#{insurance["0"][:policy_number]}")
            requisition.set_value('Group #1',"#{insurance["0"][:group_number]}")
            requisition.set_value('Relation To Insured',"#{insurance["0"][:relation]}")
          end
        else
          warnings.push("Could Not Find Insurance Company #{insurance["0"][:name]}")
        end
      end
    rescue Exception=>e
      warnings.push("An error occured while setting insurance information. #{e}")
      if !requisition.get_value("Relation To Insured").present?
        requisition.set_value("Relation To Insured","Other")
      end
    end
  end

  if errors.size > 0
    requisition.destroy
    requisition = nil
  else
    #start_workflow('Requisition',requisition)
    #submission code
    require_script "requisition_support_helper"
    submit_requisition(requisition)
    if test_panel_count > 1
      requisition.set_value('Comments',"The following Test Panels were selected #{test_panel_names.join(', ')}. Only 1 Test Panel is allowed.")
      advance_workflow('Requisition','On Hold',requisition)
    else
      advance_workflow('Requisition','Submitted',requisition)
    end
    open_subject(requisition)
  end

  return {errors:errors.join("\n"), warnings:warnings.join("\n"), req:requisition, patient:p}
end

#POC Tests
def lookup_poc_result(observation_value)
  result = 'Not Tested'
  if !observation_value.nil?
    if observation_value.strip == "N"
      result = 'Negative'
    elsif observation_value.strip == "P"
      result = 'Positive'
    end
  end
  return result
end

def create_poc_tests(hl7_processor,msg,requisition = nil)
  tz = Time.zone
  poc_test_result = nil
  if !requisition.nil?
    patient = lookup_patient("#{msg[:PID][:patient_id]}",requisition.get_value('Facility'))
    if !patient
      patient = lookup_patient("#{msg[:PID][:alt_patient_id]}",requisition.get_value('Facility'))
    end
    if patient
      poc_test_results = requisition.get_value('Point of Care Test Results')

      msg[:OBR].each do |obr|
        testing_date = DateTime.strptime(obr[:observation_date],'%Y%m%d%H%M')
        testing_date = ActiveSupport::TimeWithZone.new(nil, tz, testing_date)

        if !poc_test_results.nil? && poc_test_results.size > 0
          poc_test_result = poc_test_results[0]
        end
        if poc_test_result.nil?
          poc_test_result = create_subject('Point of Care Test Result') do |v|
            v.set_value('Patient', patient)
            v.set_value('Testing Date', testing_date)
            v.set_value('Requisition', requisition)
          end
        end

        if poc_test_result.get_value("Point of Care Test Result Entries").size == 0
          poc_tests = SubjectType.find_by_name('Point of Care Test')
          poc_tests.subjects.each do |s|
            res = create_subject('Point of Care Test Result Entry') do |v|
              v.set_value('Point of Care Test Result', poc_test_result)
              v.set_value('Point of Care Test', s)
              v.set_value('PoC Result', 'Not Tested')
            end
          end #Poc Tests
        end
        #optimizing happened here
        obr[:OBX].each do |obx|
           test_lookup = lookup_poc_test(obx[:observation_id][0])
           test_result = lookup_poc_result(obx[:observation_value])
           if test_lookup
             query = search_query(subject_type: 'Point of Care Test Result Entry') do |qb|
                q = qb.and
                q << qb.compare('Point of Care Test', :eq, test_lookup)
                q << qb.compare('Point of Care Test Result', :eq, poc_test_result)
                q
             end
             result = find_subjects(query: query).first
             if result
               result.set_value('PoC Result', test_result)
             end
           end
        end
      end #MSG
      maybe_run_reflex_rules(requisition)
    else
      Rails.logger.info "[EMR Order Processor]::Could not find Patient Ids #{msg[:PID][:patient_id]}, #{msg[:PID][:alt_patient_id]}. POC Test Results where not processed."
    end #patient
  else
    Rails.logger.info "[EMR Order Processor]::No requisition was specified, POC Test Results where not processed."
  end #requisition
end #def

def importHL7(host,username,password,path,provider, system_settings)
  if !system_settings.key?(:create_patient)
    system_settings[:create_patient] = false
  end
  if !system_settings.key?(:update_patient)
    system_settings[:update_patient] = false
  end
  if !system_settings.key?(:create_insurance)
    system_settings[:create_insurance] = false
  end
  if !system_settings.key?(:use_poc_test)
    system_settings[:use_poc_test] = false
  end

  hl7_processor = Hl7_Order_Processor.new(self)
  hl7Data = get_hl7_files(host,username,password,path)
  hl7Data[:data].each do | data |
    #Need to fix those nasty formatted Windows Files.
    data = hl7_processor.cleanse(data)
    msg = hl7_processor.data_parse(data)
    if msg[:MSH][:message_type] == "ORM^O01"
      begin
        req = create_req(hl7_processor,msg,system_settings)
        if system_settings[:use_poc_test]
          create_poc_tests(hl7_processor,msg,req[:req])
        end

        if hl7Data[:response].present?
          message = hl7Data[:response].concat("\n\n")
        else
          message = ''
        end
        message.concat("Errors:" + req[:errors] + "\n")
        if(req[:errors].present?)
          message.concat("Requisition creation aborted due to Errors.\n")
        end
        message.concat("Warnings:" + req[:warnings])
        if message != ''
          show_message(message)
        end
      rescue Exception=>e
        if !req.present?
          req = {req:nil}
        end
        if message == ''
          message = "There was a problem processing the order. #{e}"
        end
      end

      ehr_order = create_subject('EHR Order') do |order|
        order.set_value('EHR System',provider)
        order.set_value('Comments',message)

        alt_req_id = get_alternate_req_id(msg) rescue nil
        order.set_value('External Source ID', alt_req_id) if alt_req_id.present?
      end
      ehr_order.set_value('Requisition',req[:req]) if !req[:req].nil?
      ehr_order.set_value('Patient',req[:patient]) if !req[:patient].nil?

      f = hl7_processor.create_file(ehr_order.name, data)
      ehr_order.set_value('File',f)
      File.delete(f)
    end
  end
end