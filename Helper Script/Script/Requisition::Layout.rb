module Requisition
  
  module Layout
    include Physician
    include Requisition.const_find('CustomLayout', 'DefaultLayout')
    extend self
  end
  
end