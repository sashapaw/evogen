def get_tunnel_settings
  system_settings = find_subject_type('System Settings').subjects.first
  if system_settings.present?
    ssh_tunnel_settings = system_settings.get_value('Instrument Gateway')
    if ssh_tunnel_settings.present?
      Rails.logger.info ssh_tunnel_settings.inspect
      ssh_tunnel_settings.gsub!("\n","")
      Rails.logger.info ssh_tunnel_settings.inspect
      ActiveSupport::JSON.decode(ssh_tunnel_settings)
    end
  end
end

data = params[:data]
instrument = Instrument::find_instrument(data)
instrument = Instrument::new_instrument(data) if instrument.blank?

instrument.set_value('Last Seen', Time.zone.now)

ssh_tunnel = nil
ssh_tunnel_port = instrument.get_value('SSH Tunnel')
if ssh_tunnel_port.present?
  ssh_tunnel_settings = get_tunnel_settings
  if ssh_tunnel_settings.present?
    ssh_tunnel = { tunnel_port: ssh_tunnel_port.to_i }.merge(ssh_tunnel_settings)
  end
end

ret = { uid: instrument.id, name: instrument.name, ssh_tunnel: ssh_tunnel }
instrument_type = instrument.get_value('Instrument Type')
if instrument_type.present?
  ret[:type] = instrument_type.name
  ret[:integration_class] = instrument_type.get_value('Protocol Class Name') || instrument_type.get_value('Integration Class Name')
end
ret