module Instrument
  class Siemens_VTwin < ASTM

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @result_status_map = get_result_status_map
      @unit_map = get_unit_map
    end

    def get_unit_map
      {
        '1' => 'Ref',
        '2' => 'mol/L',
        '3' => 'mol/dL',
        '4' => 'mmol/L',
        '5' => 'mmol/dL',
        '6' => 'μmol/L',
        '7' => 'μmol/dL',
        '8' => 'nmol/L',
        '9' => 'nmol/dL',
        '10' => 'pmol/L',
        '11' => 'pmol/dL',
        '12' => 'g/L',
        '13' => 'g/dL',
        '14' => 'mg/L',
        '15' => 'mg/dL',
        '16' => 'μg/L',
        '17' => 'μg/dL',
        '18' => 'ng/L',
        '19' => 'ng/dL',
        '20' => 'mg/mL',
        '21' => 'μg/mL',
        '22' => 'ng/mL',
        '23' => 'pg/mL',
        '24' => 'μkat/L',
        '25' => 'nkat/L',
        '26' => 'U/L',
        '27' => 'U/dL',
        '28' => 'mU/L',
        '29' => 'mU/dL',
        '30' => 'U/mL',
        '31' => 'mU/mL',
        '32' => 'IU/L',
        '33' => 'IU/dL',
        '34' => 'mIU/L',
        '35' => 'mIU/dL',
        '36' => 'mIU/mL',
        '37' => 'mval/L',
        '38' => 'mEq/L',
        '39' => '%',
        '40' => 's',
        '41' => 'KU/L',
        '42' => 'kIU/L',
        '43' => 'g/mol',
        '44' => 'mg/g',
        '45' => 'Δ A',
        '46' => 'Δ A/min',
        '47' => 'Δ %',
        '48' => 'IU/mL'
      }
    end

    def get_result_status_map
      {
        'F' => 'Final result'
      }
    end

    def get_status(field)
      res_stat = field['value']
      @result_status_map.fetch(res_stat) rescue res_stat
    end

    def get_universal_test_id(test_results)
      get_test_ids(test_results).map {|aname| "^^^#{aname}"}.join("\\")
    end

    def get_patient(requisition)
      pdata = get_patient_data(requisition)
      if pdata
        {
          'type': 'P',
          'fields': [
            {'idx': 2, 'value': 1, 'name': :sequence_number},
            {'idx': 3, 'value': '', 'name': :practice_assigned_patient_id},
            {'idx': 4, 'value': '', 'name': :laboratory_assigned_patient_id},
            {'idx': 5, 'value': '', 'name': :patient_id},
            {'idx': 6, 'value': "#{pdata[:first_name]} #{pdata[:last_name]}", 'name': :name},
            {'idx': 7, 'value': ''},
            {'idx': 8, 'value': "#{pdata[:birth_date]}", 'name': :birthdate},
            {'idx': 9, 'value': pdata[:sex], 'name': :sex}
          ]
        }
      else
        nil
      end
    end

    def get_qc_orders(sample)
      analytes = sample.get_value('Analytes')
      test_ids = get_universal_test_id(analytes)

      [{
        'order': {
            'type': 'O',
            'fields': [
                {'idx': 2, 'value': 1, 'name': :sequence_number},
                {'idx': 3, 'value': "#{sample.barcode_tag}", 'name': :specimen_id},
                {'idx': 4, 'value': ''},
                {'idx': 5, 'value': test_ids, 'name': :universal_test_id_internal},
                {'idx': 6, 'value': 'R', 'name': :priority},
                {'idx': 7, 'value': '', 'name': :requested_at},
                {'idx': 8, 'value': '', 'name': :collected_at},
                {'idx': 9, 'value': ''},
                {'idx': 10, 'value': ''},
                {'idx': 11, 'value': ''},
                {'idx': 12, 'value': 'N'},
                {'idx': 13, 'value': ''},
                {'idx': 14, 'value': ''},
                {'idx': 15, 'value': ''},
                {'idx': 16, 'value': ''},
                {'idx': 17, 'value': ''},
                {'idx': 18, 'value': ''},
                {'idx': 19, 'value': ''},
                {'idx': 20, 'value': ''},
                {'idx': 21, 'value': ''},
                {'idx': 22, 'value': ''},
                {'idx': 23, 'value': ''},
                {'idx': 24, 'value': ''},
                {'idx': 25, 'value': ''},
                {'idx': 26, 'value': ''}
            ]
        }}]
    end

    def get_orders(sample, test_results)
      specimen_type = get_specimen_type(sample)
      test_ids = get_universal_test_id(test_results)
      requisition = sample.get_value('Requisition')
      physician = requisition.get_value('Requesting Physician')

      [{
        'order': {
            'type': 'O',
            'fields': [
                {'idx': 2, 'value': 1, 'name': :sequence_number},
                {'idx': 3, 'value': "#{sample.barcode_tag}", 'name': :specimen_id},
                {'idx': 4, 'value': ''},
                {'idx': 5, 'value': test_ids, 'name': :universal_test_id_internal},
                {'idx': 6, 'value': 'R', 'name': :priority},
                {'idx': 7, 'value': '', 'name': :requested_at},
                {'idx': 8, 'value': '', 'name': :collected_at},
                {'idx': 9, 'value': ''},
                {'idx': 10, 'value': ''},
                {'idx': 11, 'value': ''},
                {'idx': 12, 'value': 'N'},
                {'idx': 13, 'value': ''},
                {'idx': 14, 'value': ''},
                {'idx': 15, 'value': ''},
                {'idx': 16, 'value': specimen_type},
                {'idx': 17, 'value': "#{physician.get_value('First Name')} #{physician.get_value('Last Name')}", 'name': :attending_physician},
                {'idx': 18, 'value': ''},
                {'idx': 19, 'value': ''},
                {'idx': 20, 'value': ''},
                {'idx': 21, 'value': ''},
                {'idx': 22, 'value': ''},
                {'idx': 23, 'value': ''},
                {'idx': 24, 'value': ''},
                {'idx': 25, 'value': ''},
                {'idx': 26, 'value': ''}
            ]
        }}]
    end


  end
end