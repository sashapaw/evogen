module Patient
  
  def insurance_check(params)
    bill_to = params['Bill To']
    raise '"Bill To" field is required' unless bill_to
    case bill_to
    when 'Insurance', 'Medicare'
      insurances = [
        {ins:params['Insurance Company 1'], policy:params['Policy #1'], pos:1},
        {ins:params['Insurance Company 2'], policy:params['Policy #2'], pos:2},
        {ins:params['Insurance Company 3'], policy:params['Policy #3'], pos:3}
      ]
      raise "\"Insurance Company 1\" is required when \"Bill To\" is #{bill_to}" unless insurances[0][:ins]
      
      check_ins_policy = ->(ins,policy,pos) do
        if ins && !policy.present?
          raise "You have Insurance Company #{pos} selected. You must also provide Policy ##{pos}."
        end
        if !ins.present? && policy
          raise "You have provided Policy ##{pos}. You must also provide Insurance Company #{pos}."
        end
      end

      insurances.each do |insurance|
        check_ins_policy.call(insurance[:ins], insurance[:policy], insurance[:pos])
      end
      
      rel = params['Relation To Insured']
      raise "\"Relation To Insured\" is required when \"Bill To\" is #{bill_to}" if rel.blank?
      if rel != 'Self'
        raise "#{rel} Name is required" if params['Subscriber Name'].blank?
        #raise "#{rel} DOB is required" if params['Subscriber DOB'].blank?
        #raise "#{rel} Sex is required" if params['Subscriber Sex'].blank?
        #raise "#{rel} Address is required" if params['Subscriber Address'].blank?
        LIS.validate_address(params['Subscriber Address'])
      end
    when 'Facility'
      raise "\"Billing Facility\" is required when \"Bill To\" is #{bill_to}" if params['Billing Facility'].blank?
      raise "\"Billing Facility Address\" is required when \"Bill To\" is #{bill_to}" if params['Billing Facility Address'].blank?
      LIS.validate_address(params['Billing Facility Address'])
    end
  end

  extend self
end