module Instrument
  class BC_Access2 < ASTM

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @flags_map = get_flag_map
      @result_status_map = get_result_status_map
      @analyte_map = get_analyte_map
      @specimen_type_map = get_specimen_type_map
    end

    def get_flag_map
      {
        'H' => 'H',
        'L' => 'L',
        'LL' => 'CL',
        'HH' => 'CH',
        '<' => 'CL',
        '>' => 'CH',
        'N' => ''
      }
    end

    def get_analyte_map
      {
        'AFP' => 'Alpha-Fetoprotein',
        'CEA2' => 'Carcinoembryonic Antigen (CEA)',
        'CK-MB' => 'Creatine Kinase, Total',
        'CMV-G' => 'Cytomegalovirus Ab, IgG',
        'CMV-M' => 'Cytomegalovirus Ab, IgG',
        'Cortisol' => 'Cortisol, Total',
        'DHE-S' => 'DHEA-S',
        'Dig' => 'Digoxin',
        'E2' => 'Estradiol',
        'Ferritin' => 'Ferritin',
        'FOLW' => 'Folate',
        'freePSA' => 'Prostate Specific Antigen (PSA), Free',
        'FRT4' => 'Free T4 (Thyroxine)',
        'FT3' => 'Free T3 (Triiodothyronine)',
        'fTSH2' => 'Thyroid Stimulating Hormone',
        'GToxo' => 'Toxoplasma Antibody IgG',
        'HAV-IgM' => 'Hepatitis A Ab, IgM',
        'HBc-IgM' => 'Hepatitis B Core Antibody, IgM',
        'HBsAgV3' => 'Hepatitis B Surface Antigen',
        'HCVPLUS' => 'Hepatitis C Antibody',
        'hFSH' => 'Follicle Stimulating Hormone',
        'HIVco' => 'HIV-1/HIV-2 Ab',
        'IgM-Toxo' => 'Toxoplasma Antibody IgM',
        'InhibinA' => 'Inhibin A',
        'Insulin' => 'Insulin',
        'P4DE' => 'Progesterone',
        'PRL' => 'Prolactin',
        'Prog' => 'Progesterone',
        'PSA-Hyb' => 'Prostate Specific Antigen (PSA)',
        'PTH' => 'Parathyroid Hormone',
        'Rub-IgG' => 'Rubella IgG',
        'TBhCG2' => 'Beta hCG, Quantitative',
        'Testo' => 'Testosterone',
        'Tg' => 'Thyroglobulin IgG',
        'TotIgE' => 'Immunoglobulin E (IgE), Total Allergen',
        'TotT4' => 'Total T4',
        'TotT3' => 'Total T3',
        'TPOAb' => 'Thyroid Peroxidase IgG',
        'TSH' => 'Thyroid Stimulating Hormone',
        'VitB12' => 'Vitamin B12',
        'VitdA' => '25-OH Vitamin D, Total',
      }
    end

    def get_specimen_type_map
      {
        'Amniotic' => 'Amniotic',
        'Blood' => 'Blood',
        'Cerebrospinal Fluid' => 'CSF',
        'Cervical' => 'Cervical',
        'Other' => 'Other',
        'Plasma' => 'Plasma',
        'Ratio' => 'Ratio',
        'Saliva' => 'Saliva',
        'Serum' => 'Serum',
        'Synovial' => 'Synovial',
        'Urethral' => 'Urethral',
        'Urine' => 'Urine'
      }
    end

    def get_result_status_map
      {
        'F' => 'Final result',
        'X' => 'Results cannot be generated. Request is not honored, or result is fatally flagged.'
      }
    end

    def get_patient(requisition)
      pdata = get_patient_data(requisition)
      if pdata
        {
          'type': 'P',
          'fields': [
            {'idx': 2, 'value': 1, 'name': :sequence_number},
            {'idx': 3, 'value': pdata[:id], 'name': :practice_assigned_patient_id},
            {'idx': 4, 'value': '', 'name': :laboratory_assigned_patient_id},
            {'idx': 5, 'value': '', 'name': :patient_id},
            {'idx': 6, 'value': "#{pdata[:last_name]}^#{pdata[:first_name]}^^^", 'name': :name},
            {'idx': 7, 'value': ''},
            {'idx': 8, 'value': "#{pdata[:birth_date]}", 'name': :birthdate},
            {'idx': 9, 'value': pdata[:sex], 'name': :sex},
            {'idx': 10, 'value': '', 'name': :attending_physician},
          ]
        }
      else
        nil
      end
    end

    def get_orders(sample, test_results)
      specimen_type = get_specimen_type(sample)
      test_ids = get_test_ids(test_results)
      requested_at = get_time_requested_at

      [{
        'order': {
            'type': 'O',
            'fields': [
                {'idx': 2, 'value': 1, 'name': :sequence_number},
                {'idx': 3, 'value': "#{sample.barcode_tag}", 'name': :specimen_id},
                {'idx': 4, 'value': ''},
                {'idx': 5, 'value': test_ids, 'name': :universal_test_id_internal},
                {'idx': 6, 'value': 'R', 'name': :priority},
                {'idx': 7, 'value': ''},
                {'idx': 8, 'value': ''},
                {'idx': 9, 'value': ''},
                {'idx': 10, 'value': ''},
                {'idx': 11, 'value': ''},
                {'idx': 12, 'value': 'A'},
                {'idx': 13, 'value': ''},
                {'idx': 14, 'value': ''},
                {'idx': 14, 'value': ''},
                {'idx': 14, 'value': specimen_type}
            ]
        }}]
    end

    def get_analyte_code(field)
      field['value'].match(/\^\^\^(.*?)\^\d*/)[1]
    end

    def get_result_value(test_result, field)
      val = field['value']
      test_result.set_value('Value', val) unless val.blank?
    end

    def get_range(test_result, field)
      range = field['value']
      test_result.set_value('Range', range) unless range.blank?
    end

    def get_flags(test_result, field)
      flag = field['value']
      if flag.present?
        flag = @flags_map.fetch(flag_name) rescue nil
        test_result.set_value('Flag', flag) if flag.present?
      end
    end

    def get_status(test_result, field)
      res_stat = field['value']
      result_status = @result_status_map.fetch(res_stat) rescue nil
      test_result.set_value('Instrument Result Status', result_status) unless result_status.blank?
    end

    def get_comments(test_result, res)
      comms = res['comments']
      if comms.present?
        comments_fields = comms.first['fields']
        comments = comments_fields[2]['value'].split('^').join("\n")
        #test_result.set_value('Comments', comments) unless comments.blank?
      end
    end
  end
end