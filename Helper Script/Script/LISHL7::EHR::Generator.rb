require 'ruby-hl7'

module LISHL7
  
  module EHR
    class Generator
      include LISHL7
      
      attr_accessor :context

      def initialize(context = ScriptRunner.context)
        @context = context
      end

      def get_flag(provider_type,flag)
        flag.present? ? flag.name : ''
=begin
# Cannot use flag_map anymore because we now have configurable flags
        f = flag
        if !flag.nil?
          flags = provider_type.flag_map
          if flags.present?
            if flags.key?(flag.to_sym)
              f = flags[flag.to_sym]
            else
              Rails.logger.info ("HL7 GENERATION(44)::: Could not map the Flag #{flag} during HL7 Generation")
            end
          else
            Rails.logger.info ("HL7 GENERATION(47)::: Could not find flag map for provider #{provider_type.name} during HL7 Generation")
          end
        end
        return f
=end
      end

      ## END Add to own hl7 utils class
      ## Segment Creation
      def generate_msh_segment(req, configuration)
        msh = configuration.provider_type.generate_ehr_msh(configuration, req.get_value('Facility'))
        msh.message_control_id = req.name
        msh.message_type = 'ORU^R01'
        msh
      end

      def generate_ehr_zps_map(facility, app_map)
        return [{ set_id:"1",
          "facility_footnote_code":"",
          "facility_name":"#{facility}",
          "facility_address":"",
          "facility_phone_number":"",
          "facility_contact":"",
          "facility_director":"",
          "facility_lab_code":"",
          "facility_clia_number":""}]
      end

      def generate_ehr_pid_map(patient)
        #Default to alternate id which is provided by EMR/EHR
        patient_id = patient.get_value('External Source ID').to_s
        #If one not provided we use ours
        patient_id = patient.get_value('MRN').to_s if !patient_id.present?

        patient_address = get_address_xad_field(patient.get_value('Address'))
        patient_phone = get_phone(patient.get_value('Phone Number'))

        return [{"set_id":1,
          "patient_id":patient_id,
          "patient_id_list":patient_id,
          "alt_patient_id":patient.get_value('MRN').to_s,
          "patient_name":createSegmentString([patient.get_value('Last Name').to_s,patient.get_value('First Name').to_s,patient.get_value('Middle Name').to_s]),
          "mother_maiden_name":"",
          "patient_dob":remove_dash(patient.get_value('DOB').to_s),
          "admin_sex":patient.get_value('Sex').to_s,
          "patient_alias":"",
          "race":"",
          "address":patient_address,
          "country_code":"",
          "phone_home":patient_phone.sanitized,
          "phone_business":"",
          "primary_language":"",
          "marital_status":"",
          "religion":"",
          "account_number":patient.get_value('MRN').to_s,
          "social_security_num":"",
          "driver_license_num":"",
          "mothers_id":"",
          "ethnic_group":""}]
      end

      def generate_ehr_orc_map(req,configuration)
        begin
          transaction_time = req.get_value("Released Date").strftime("%Y%m%d%H%M%S")
        rescue
          raise "Released Date is missing on the Requisition. Please correct and try again."
        end

        facility = req.get_value("Facility")
        orc_fields = {"order_control":"RE",
          "placer_order_number": req.get_value('External Source ID') || req.name,
          "filler_order_number": req.name,
          "placer_group_number":"",
          "order_status":"",
          "response_flag":"D",
          "quantity_timing":"",
          "parent":"",
          "date_time_of_transaction":"#{transaction_time}",
          "entered_by":"",
          "verified_by":"",
          "ordering_provider":createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")]),
          "enterers_location":"",
          "call_back_phone_number":"",
          "order_effective_date_time":req.get_value("Received Date"),
          "order_control_reason":"",
          "entering_organization":"",
          "entering_device":"",
          "action_by":"",
          "advanced_beneficiary_notice_code":"",
          "ordering_facility_name": get_xon_field(facility.name, facility.get_value("EHR Account Number"), identifier_type: 'AN')
          }
        #if configuration.provider_type.hl7_version >= '2.7'
        #  orc_fields["alternate_placer_order_number"] = req.name
        #end
        return [orc_fields]
      end

      def generate_obr_string(req, sample, provider_type, test, index, correction)
        test_code = test.get_value("Code")

        obr = Segments.obr(index || 1, req.name, req.get_value('External Source ID') || req.name)
        obr.universal_service_id = createSegmentString([test_code,test.get_value("Description"),"L"])
        obr.priority = "R"
        obr.requested_date = req.get_value("Received Date")
        obr.observation_date = req.get_value("Collection Date")
        obr.specimen_received_date = sample.get_value("Received Date")
        obr.specimen_source = sample.get_value("Specimen Type").to_s
        obr.ordering_provider = createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")])
        obr.results_status_change_date = req.get_value("Report Date")
        obr.result_status = correction || "F"
        #obr.quantity_timing = "^^^^^R"
        obr.alternate_placer_order_number = sample.get_value('External Source ID') || sample.name if provider_type.hl7_version >= '2.7'

        return obr.to_hl7
      end

      #Used for Toxicology labs
      def select_value(test_result)
        value = test_result.get_value("Value")
        if test_result.get_value("Interpretation").present?
          value = test_result.get_value("Interpretation")
        end
        value
      end

      def generate_obx_string(req, result, index, provider_type, send_only_failed)
        #Producer Id
        system_settings = LIS.system_settings
        producer_id = createSegmentString([system_settings.get_value('CLIA Number'), system_settings.get_value('Lab Name')])
        
        analyte = result.get_value("Analyte")
        non_reportable = analyte.get_value('Non-Reportable')
        qualitative = analyte.get_value('Qualitative')

        if !non_reportable && (!send_only_failed || result.get_value("Flag"))
          value = select_value(result)
          value_type = "ST"
          if !qualitative && is_number?(value)
            value_type = "NM"
          elsif value.to_s.length > 32
            value_type = "TX"
          else
            value_type = "ST"
          end
          
          analyte_code = analyte.name #analyte.get_value("Code")

          obx = Segments.obx(index, producer_id)
          obx.value_type=value_type
          obx.observation_id=createSegmentString([analyte_code,analyte.name,"L","","",""])
          obx.observation_sub_id=index
          obx.observation_value=value
          obx.units=result.get_value("Unit")
          obx.references_range=result.get_value("Range")
          obx.abnormal_flags=get_flag(provider_type, result.get_value("Flag"))
          obx.observation_date=req.get_value("Report Date")
          obx.performing_organization_medical_director=createSegmentString([req.get_value('Test Performer NPI Number'),req.get_value('Test Performer Last Name'),req.get_value('Test Performer First Name')])

          return obx.to_hl7
        else
          return ""
        end
      end

      def generate_obr_poc(req,correction,index)
        obr = ::HL7::Message::Segment::OBR.new
        obr.set_id = index.to_s || "1"
        obr.placer_order_number = req.get_value('External Source ID') || req.name
        obr.filler_order_number = req.name
        obr.universal_service_id = createSegmentString(['POC','POC Results'])
        obr.priority = "R"
        obr.requested_date = req.get_value("Received Date")
        obr.observation_date = req.get_value("Collection Date")
        obr.observation_end_date = ""
        obr.collection_volume = ""
        obr.collector_identifier = ""
        obr.specimen_action_code = ""
        obr.danger_code = ""
        obr.relevant_clinical_info = ""
        obr.specimen_received_date = req.get_value("Received Date")
        obr.specimen_source = "#{req.get_value("Specimen Types").join(',')}"
        obr.ordering_provider = createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")])
        obr.order_callback_phone_number = ""
        obr.placer_field_1 = ""
        obr.placer_field_2 = ""
        obr.filler_field_1 = ""
        obr.filler_field_2 = ""
        obr.results_status_change_date = req.get_value("Report Date")
        obr.charge_to_practice = ""
        obr.diagnostic_serv_sect_id = ""
        obr.result_status = correction || "F"
        obr.parent_result = ""

        return obr.to_hl7
      end

      def generate_obx_poc(req,poc_tre,index)
        producer_id = createSegmentString(["POC","Point of Care"])

        poc_test = poc_tre.get_value('Point of Care Test')
        value = poc_tre.get_value('PoC Result')

        flag = "N"
        flag = "A" if value == "Positive"

        obx = ::HL7::Message::Segment::OBX.new
        obx.set_id = index.to_s || "1"
        obx.value_type='ST'
        obx.observation_id=createSegmentString([poc_test.name,poc_test.name,"L"])
        obx.observation_sub_id=''
        obx.observation_value=value
        obx.units="ng/mL"
        obx.references_range=""
        obx.abnormal_flags=flag
        obx.probability=""
        obx.nature_of_abnormal_test=""
        obx.observation_result_status="F"
        obx.effective_date_of_reference_range=""
        obx.user_defined_access_checks=""
        obx.observation_date=req.get_value("Report Date")
        obx.producer_id=producer_id
        obx.responsible_observer=""
        obx.observation_method=""
        obx.equipment_instance_id=""
        #obx.analysis_date=""
        #obx.observation_site=""
        #obx.observation_instance_id=""
        #obx.mood_code=""
        #obx.performing_organization_name=""
        #obx.performing_organization_address=""
        #obx.performing_organization_medical_director=createSegmentString([req.get_value('Test Performer NPI Number'),req.get_value('Test Performer Last Name'),req.get_value('Test Performer First Name')])

        return obx.to_hl7
      end

      def generate_poc_string(index,req,correction)
        messages = []
        req.get_value('Point of Care Test Results').each do |poc_tr|
          poc_tr_obr = generate_obr_poc(req,correction,index)
          messages.push(poc_tr_obr) if poc_tr_obr
          poc_tr.get_value('Point of Care Test Result Entries').each_with_index do |poc_tre,i|
            poc_tr_obx = generate_obx_poc(req,poc_tre,(i + 1))
            messages.push(poc_tr_obx) if poc_tr_obx
          end
        end
        messages.join('/n')
        return messages
      end

      def generate_obr(req,configuration,correction,send_only_failed)
        provider_type = configuration.provider_type
        messages = []
        o = 1
        #if poc tests enabled, lets add them to the fun.
        poc = ""
        if req.get_value('Point of Care Test Results').size > 0
          poc = generate_poc_string(o, req, correction)
          if poc != ""
            messages.push(poc)
          end
          o = o + 1
        end

        #loop through each sample and test
        req.get_value('Samples').each do |sample|
          sample.get_value("Tests").each do |test|
            test_analytes = test.get_value('Analytes')
            #loop through each result
            obx_messages = []
            obx_index = 1
            sample.get_value("Test Results").each do |result|
              nte_index = 1
              if test_analytes.include?(result.get_value("Analyte"))
                lab = generate_obx_string(req,result,obx_index,provider_type,send_only_failed)
                if lab != ""
                  obx_messages.push(lab)
                  obx_index += 1
                  comments = result.get_value('Comments')
                  if comments.present?
                    obx_messages.push(Segments.nte(nte_index, 'O', comments).to_hl7)
                    nte_index += 1
                  end
                  outcome = result.get_value('Outcome')
                  if outcome.present?
                    obx_messages.push(Segments.nte(nte_index, 'L', "Outcome: #{outcome}", 'RE').to_hl7)
                    nte_index += 1
                  end
                end
              end
            end

            if obx_messages.size > 0
              obr = generate_obr_string(req, sample, provider_type, test, o, correction)
              if obr != ""
                messages.push(obr)
                messages.push(*obx_messages)
                o += 1
              end
            end
          end # Tests
        end # Samples
        
        ##Only PDF supported providers
        if configuration.provider_type.embed_report_in_oru_as_obx? && (file_path = req.get_value("Report File"))
          file_path = file_path.path
          if file_path
            obr_pdf = generate_obr_pdf_string(file_path, o, req, correction)
            pdf = generate_obx_pdf_string(provider_type,file_path,1)
            if pdf != ""
              messages.push(obr_pdf)
              messages.push(pdf)
            end
          end
        end
        return messages.join("\n")
      end

      ## Add to its own class
      def create_hl7_message(req, configuration)
        patient = req.get_value('Patient')
        message = ::HL7::Message.new
        message << generate_msh_segment(req, configuration)
        msg = {}
        #msg["MSH"] = generate_ehr_msh_map(req, app_map)
        msg["PID"] = generate_ehr_pid_map(patient)
        msg["ORC"] = generate_ehr_orc_map(req,configuration)
        return Segments.generate_message(msg, message)
      end

      def generate_obr_pdf_string(file_path, index, req, correction)
        obr = ::HL7::Message::Segment::OBR.new
        obr.set_id = index.to_s || "1"
        obr.placer_order_number = req.get_value('External Source ID') || req.name
        obr.filler_order_number = req.name
        obr.universal_service_id = createSegmentString(["Base64","Embedded PDF"])
        obr.priority = "R"
        obr.requested_date = ""
        obr.observation_date = req.get_value("Collection Date")
        obr.observation_end_date = ""
        obr.collection_volume = ""
        obr.collector_identifier = ""
        obr.specimen_action_code = ""
        obr.danger_code = ""
        obr.relevant_clinical_info = ""
        obr.specimen_received_date = req.get_value("Received Date")
        obr.specimen_source = "#{req.get_value("Specimen Types").join(',')}"
        obr.ordering_provider = createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")])
        obr.order_callback_phone_number = ""
        obr.placer_field_1 = ""
        obr.placer_field_2 = ""
        obr.filler_field_1 = ""
        obr.filler_field_2 = ""
        obr.results_status_change_date = req.get_value("Report Date")
        obr.charge_to_practice = ""
        obr.diagnostic_serv_sect_id = ""
        obr.result_status = correction || "F"
        obr.parent_result = ""

        return obr.to_hl7
      end

      def generate_zps(system_settings)
        zps = ::HL7::Message::Segment::ZPS.new
        zps.set_id = "1"
        zps.facility_footnote_code = ""
        zps.facility_name = "#{system_settings.get_value('Lab Name')}"
        zps.facility_address = get_address_xad_field(system_settings.get_value('Lab Address'))
        zps.facility_phone_number = "#{system_settings.get_value('Lab Phone')}"
        zps.facility_contact = ""
        #	PhysCode^Fname^Lname^Degree^License^^^
        zps.facility_director = createSegmentString([system_settings.get_value('Lab Director Physicians Code'),system_settings.get_value('Lab Director First Name'),system_settings.get_value('Lab Director Last Name'),system_settings.get_value('Lab Director Degree'),"CLIA ID# #{system_settings.get_value('CLIA Number')}","","",""])
        zps.facility_lab_code = ""
        zps.facility_clia_number = "#{system_settings.get_value('CLIA Number')}"

        return zps.to_hl7
      end

      def generate_obx_pdf_string(provider_type, file_path, index)
        base64_file = LIS.file_to_base64(file_path)
        profile = provider_type.pdf_profile

        obx5 = profile[:obx5]
        if obx5.size > 1
          obx5.push(base64_file)
        else
          obx5[0] = base64_file
        end

        obx = ::HL7::Message::Segment::OBX.new
        obx.set_id=index.to_s
        obx.value_type="ED"
        obx.observation_id=createSegmentString(profile[:obx3])
        obx.observation_sub_id=profile[:obx4]
        obx.observation_value=createSegmentString(obx5)

        return obx.to_hl7
      end

      def create_file(ehr_record_name, message)
        filename = "#{Rails.root}/tmp/#{ehr_record_name}.hl7"

        message = message.gsub("\n","\r")
        begin
          message = message.encode("UTF-8")
        rescue Encoding::UndefinedConversionError
          # couldn't encode
        end

        begin
          f = File.open(filename, "wb")
          f.write(message)
        rescue IOError => e
          #some error occur, dir not writable etc.
          Rails.logger.error("Error while writing file #{filename}: #{e.to_s}")
        ensure
          f.close unless f.nil?
        end
        f
      end

      def create_record(configuration,req,flag)
        ehr_record = @context.create_subject('EHR Record') do |er|
          er.set_value('Requisition', req)
          er.set_value('EHR Server Configuration', configuration)
        end

        message = create_hl7_message(req,configuration).to_hl7 + "\n"
        message = message + generate_obr(req,configuration,flag,configuration.get_value('Only Send Failed Results'))
        if configuration.provider_type.generate_zps_in_oru?
          message += "\n" + generate_zps(LIS.system_settings)
        end
        f = create_file(ehr_record.name, message)
        ehr_record.set_value('File',f)
        File.delete(f)
        ehr_record
      end
            
    end
  end
end
