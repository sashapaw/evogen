module Instrument
  class ClinitekAdvantus < Common

    def initialize(instrument, result_entry = nil, ctx = context)
      @instrument = instrument
      @ctx = ctx
      @result_entry = result_entry
      @analyte_map = get_analyte_map
    end

    def get_analyte_map # bacteria units unclear
      {
        '1' => 'Glucose, Urine',
        '2' => 'Bilirubin, Urine',
        '3' => 'Ketones',
        '4' => 'Specific Gravity, Blood',
        '5' => 'pH',
        '6' => 'Protein, Urine',
        '7' => 'Urobilinogen',
        '8' => 'Nitrite',
        '9' => 'Occult Blood',
        '10' => 'Leukocyte esterase',
        '11' => 'Color',
        '12' => 'Clarity',
        '13' => 'Creatinine',
        '14' => 'Protein to Creatinine Ratio',
        '15' => 'Albumin',
        '16' => 'Albumin to Creatinine Ratio',
        '100' => 'Red Blood Cell Count',
        '101' => 'RBC, Urine',
        '102' => '',
        '103' => 'White Blood Cell Count',
        '104' => 'WBC, Urine',
        '105' => '',
        '106' => '',
        '107' => '',
        '108' => 'Epithelial Cells',
        '109' => 'Bacteria',
        '110' => 'Bacteria',
        '111' => 'Bacteria',
        '112' => '',
        '113' => '',
        '114' => 'Casts',
        '115' => '',
        '116' => '',
        '117' => '',
        '118' => '',
        '119' => '',
        '120' => 'Crystals',
        '121' => '',
        '122' => '',
        '123' => '',
        '124' => '',
        '125' => '',
        '126' => '',
        '127' => ''
      }
    end

    def get_patient_data(data)
      data['patient'].first rescue nil
    end

    def get_results_data(data)
      data['results'] rescue nil
    end

    def get_sample_barcode_from_result(field)
      field['value'] rescue nil
    end

    def get_sample_from_result(data)
      patient_data = get_patient_data(data)
      patient_fields = patient_data['fields'] rescue nil if patient_data
      sample = nil
      if patient_fields
        patient_fields.each do |field|
          if field['idx'] == 5
            barcode = get_sample_barcode_from_result(field)
            sample = get_sample(barcode)
          end
        end
      else
        no_data_found
      end
      sample
    end

    def get_results(data, sample, test_results)
      results_data = get_results_data(data)
      imported_results = []
      results_data.each do |res|
        test_result, range = nil
        result_fields = res['result']['fields']
        result_fields.each do |field|
          idx = field['idx']
          case idx
            when 3
              range = get_range(field)
            when 5
              test_result = get_test_result(field, sample, test_results)
              break if test_result.blank?
              imported_results << test_result
              test_result.set_value('Range', range) unless range.blank?
            when 6
              get_result_value(test_result, field)
          end
        end
      end
      @result_entry.link_test_results(imported_results)
    end

    def get_analyte_code(field)
      field['value']
    end

    def get_result_value(test_result, field)
      val = field['value']
      if val.present? && test_result.get_value('Analyte').name == 'Urobilinogen'
        val = val.split('^').first
        unit = @ctx.find_subject({subject_type: 'Unit', name: 'EU/dL'})
        test_result.set_value('Unit', unit)
      end
      test_result.set_value('Value', val) unless val.blank?
    end

    def get_range(field)
      r = field['value'].to_sym rescue false
      case r
        when :N
          return "Within normal range"
        when :A
          return "Outside normal range"
      end
      nil
    end
  end
end