module QCFlagCalculator

  def ctx
    ScriptRunner.context
  end
    
  def analyte
    @__analyte__ ||= self.get_value('Analyte')
  end

  def sample
    @__sample__ ||= self.get_value('QC Sample')
  end

  def calculate_flags
    control_type = sample.get_value('Control Type')
    range = ctx.find_subjects(query: ctx.search_query(from: 'QC Range') {|qb|
      sample_lot = sample.get_value('Lot Number')
      lot_cond = if sample_lot.present?
        qb.or qb.condition('"Lot Number" is null'), qb.prop('"Lot Number"').eq(sample_lot)
      else
        qb.condition('"Lot Number" is null')
      end
      qb.and qb.prop('Analyte').eq(analyte),
             qb.prop('Control Type').eq(control_type),
             lot_cond
    }).first

    Rails.logger.info(" >>> QCFlagCalculator#calculate_flags : #{subject_type.name} #{name}: found range #{range}")
    if range.blank?
      Rails.logger.error " >>> QCFlagCalculator#calculate_flags : No QC Ranges found for #{analyte.name}:#{control_type}"
      return nil
    end
    self.set_value('QC Range', range)

    sd = range.get_value('SD') || 0.0
=begin
    if sd.blank?
      Rails.logger.error " >>> QCFlagCalculator#calculate_flags : Standard Deviation not specified in range #{range.name} for #{analyte.name}:#{control_type}"
      return nil
    end
=end
  
    mean = range.get_value('Target Value')
    mean = Float(mean) rescue nil
    if mean.blank?
      Rails.logger.error " >>> QCFlagCalculator#calculate_flags: Target Value not specified in range #{range.name} for #{analyte.name}:#{control_type}"
      return nil
    end

    _1sd_range = (mean-sd)..(mean+sd)
    _2sd_range = (mean-2*sd)..(mean+2*sd)

    rvalue = self.get_value('Value')
    value = begin
      Float(rvalue)
    rescue Exception=>e
      instrument = self.get_value('Instrument')
      instrument_impl = instrument.type.integration_class.new(instrument) if instrument
      value = instrument_impl.parse_value(rvalue) if instrument_impl
      Rails.logger.error(" >>> QCFlagCalculator#calculate_flags : QC Result value: #{rvalue} cannot be parsed as number: #{e}") if value.nil?
      value
    end
  
    if value.is_a?(Numeric)
      within_1sd = _1sd_range.include?(value)
      within_2sd = _2sd_range.include?(value)

      self.set_value('Within 20% Target?', within_2sd ? 'Yes' : 'No')
      self.set_value('Within 10% Target?', within_1sd ? 'Yes' : 'No')

      Rails.logger.info(" >>> QCFlagCalculator#calculate_flags : QC Result flags evaluation - analyte \"#{analyte.to_s}\":#{control_type}, 1sd_range: #{_1sd_range}, value: #{value}, within_1sd: #{within_1sd}, within_2sd: #{within_2sd}")
    end
  
  end
  
end

