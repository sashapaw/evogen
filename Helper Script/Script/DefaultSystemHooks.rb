module DefaultSystemHooks
  # Patient
  
  def validate_patient(subject)
  end
  
  def patient_created(subject)
  end

  def patient_archived(subject)
  end

  # Physician
  
  def physician_created(subject)
  end

  def physician_archived(subject)
  end

  # Facility
  
  def facility_created(subject)
  end

  def facility_archived(subject)
  end

  # Sample
  
  def sample_preinit(subject)	# called immediately before new Sample subject is created. UDFs are not yet available.
    #subject.name = subject.barcode_tag = "#{Time.now.strftime('%y%m%d')}%06d" % subject.serial_number
  end
  
  def sample_init(subject)		# called immediately after new Sample subject is created. UDFs are available.
    #run_rules("Barcode Generation", "onCreate", "Sample", self, context)
    #run_rules("Name Generation", "onCreate", "Sample", self, context)

    #subject.save!
  end
  
  def sample_accessioned(subject)
  end

  def sample_accessioned_portal(subject)
  end

  def sample_received_portal(subject)
  end



  # to hide 'Approve for Testing' button return false
  def sample_testing_enabled?(subj: )
    true
  end

  def sample_approved(subject)
  end
  
  def sample_stored(subject)
  end
  
  def sample_discarded(subject)
  end
  
  def sample_released(subject)
  end
  
  # Sample Batch
  
  def sample_batch_created(subject)
  end
  
  def sample_batch_completed(subject)
  end

  # Requisition
  
  def requisition_preinit(subject)	# called immediately before new Requisition subject is created. UDFs are not yet available.
    #subject.name = subject.barcode_tag = 'R%08i' % subject.serial_number
  end
  
  def requisition_init(subject)		# called immediately after new Requisition subject is created. UDFs are available.
    #run_rules("Name Generation", "onCreate", "Requisition", self, context)
    
    #subject.save!
  end
  
  def requisition_created(requisition, subj=nil)
  end
  
  def requisition_received(subject)
=begin
    test_codes = subject.get_value('Tests')
    sample_types = subject.get_value('Specimen Types')
    samples = sample_types.map do |sample_type|
      s = context.create_subject('Sample') do |sample|
        sample.set_value('Requisition', subject)
        sample.set_value('Received Date', subject.get_value('Received Date'))
        sample.set_value('Collection Date', subject.get_value('Collection Date'))
        sample.set_value('Tests', test_codes)
        sample.set_value('Specimen Type', sample_type)
      end
      s.name = s.barcode_tag
      context.start_workflow('Sample',s)
      SystemHooks.sample_accessioned(s)
      context.open_subject(s)
      s
    end
    context.print_subject_barcode(samples, 1, 'Sample Label Printer')
=end
  end
  
  def requisition_released(subject)
  end
  
  def requisition_accepted(subject)
  end
  
  def requisition_corrected(subject)
  end
  
  def requisition_on_hold(subject)
  end
  
  def requisition_off_hold(subject)
  end
  
  def requisition_submitted(subject)
  end

  def validate_requisition_submission(subject)
    true
  end
  
  def non_orderable_tests_for_accession_sample_form(req)
    nil
  end
  
  # Communications
  
  def communication_received(subject)
  end
  
  def communication_archived(subject)
  end
  
  def communication_replied(subject)
  end
  
  # Test Results
  
  def on_result_change(subject)
  end
  
  
end