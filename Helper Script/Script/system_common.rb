=begin
def as_admin
  cu = User.curr_user
  srcu = @user
  User.curr_user = @user = User.admin
  yield
ensure
  @user = srcu
  User.curr_user = cu
end

def find_facility_groups(facility)
  qb = QE::Builder.new
  q = qb.and(qb.prop('Facility').eq(facility), qb.condition("name LIKE '_%'"))
  q = QE::QOM::Query.new(nil,UserGroup,q)

  qom2arel = QE::QOMToArel.new(UserGroup)
  aq = q.accept qom2arel
  qom2arel.target_class.find_by_sql(aq)
end
=end

require_class 'LIS'
def get_system_settings_subject
  LIS.system_settings
end

=begin
def get_lab_logo
  ss = get_system_settings_subject
  ss && ss.get_value('Lab Logo')
end

def parse_email_field(email)
  unless email.nil?
    emailArray = email.split(%r{;\s*})
  end
end
=end