require 'ruby-hl7'

module LISHL7
  
  module Billing
    include LISHL7
    extend self
    
    #Sends HL7 Message
    def create_and_send_dft(req)
      result = ''
      ss = LIS.system_settings
      configuration = ss.get_value('HL7 Billing Provider')
	  return nil unless configuration.present?

      msg_types = configuration.get_value('HL7 Message Type')
      return nil unless msg_types.include?('DFT')

      message = create_dft_message(req,configuration)
      message = message.gsub("\n","\r\n")
      
      ctx = context
      billingRecord = ctx.create_subject('Billing Record') do |br|
        br.set_value('Requisition', req)
      end
      filename = "#{Rails.root}/tmp/#{billingRecord.name}.hl7"
      file = "#{billingRecord.name}.hl7"

      begin
        message = message.encode("UTF-8")
      rescue Encoding::UndefinedConversionError
        # couldn't encode
      end

      begin
        f = File.open(filename, "wb")
        f.write(message)
      rescue IOError => e
        Rails.logger.error( "Error creating HL7 file #{filename}: #{e.to_s}")
        return e.to_s
      ensure
        f.close unless f.nil?
      end
      billingRecord.set_value('File',f)
      ctx.advance_workflow('Billing Record', 'Active', billingRecord)

      send_billing_file(billingRecord, f, configuration)

      File.delete(f)
      if billingRecord.get_value('Comments').to_s.include?("Failed Attempt")
        result = "An error occurred attempting to contact the Billing Server. Please attempt to resubmit the billing file from the requisition."
      else
        result = result + generate_confirmation_report(req)
      end

      return result
    end
    
    #Sends ADT Message
    def create_and_send_adt(patient, event_type = "A04")
      #return nil if patient.get_value('Sent to Billing')
      
      ss = LIS.system_settings
      configuration = ss.get_value('HL7 Billing Provider')
      return nil unless configuration.present?
      
      msg_types = configuration.get_value('HL7 Message Type')
      return nil unless msg_types.include?('ADT')

      message = create_adt_message(patient,configuration)
      message = message.gsub("\n","\r\n")

      ctx = context
      billingRecord = ctx.create_subject('Patient Demographic Record') do |br|
        br.set_value('Patient', patient)
      end

      begin
        message = message.encode("UTF-8")
      rescue Encoding::UndefinedConversionError
        # couldn't encode
      end

      filename = "#{Rails.root}/tmp/#{billingRecord.name}.hl7"
      file = "#{billingRecord.name}.hl7"
      begin
        f = File.open(filename, "wb")
        f.write(message)
      rescue IOError => e
        #some error occur, dir not writable etc.
        ensure
        f.close unless f.nil?
      end

      billingRecord.set_value('File',f)
      
      send_billing_file(billingRecord, f, configuration)
      
      File.delete(f)
    end
    
    def send_billing_file(billingRecord, f = nil, configuration = nil)
      configuration = LIS.system_settings.get_value('HL7 Billing Provider') if configuration.nil?
      raise "Billing Provider is not configured" if configuration.nil?
      
      if f.nil?
        file_proxy = billingRecord.get_value('File')
        raise "No HL7 File is attached to billing record #{billingRecord.name}" if file_proxy.nil?
        f = file_proxy.file
      end
      
      enable_config = configuration.get_value('Enabled')
      if enable_config
        remote_file_path = billingRecord.subject_type.name.include?('Demographic') ? configuration.get_value('ADT Remote File Path') : nil
        remote_file_path = configuration.get_value('Remote File Path') if remote_file_path.nil?
        remote_file_path = '' if remote_file_path.nil?
        response = Transmission::upload_hl7_file(configuration, f, remote_file_path)
        billingRecord.set_value('Comments',response)
      else
        billingRecord.set_value('Comments',"#{configuration} #{configuration.get_value('HL7 Server Type')} is currently disabled.")
      end
    end
    
    def generate_confirmation_report(req)
      main = "The following information was transmitted to billing:<br/> <strong>Requisition:</strong> #{req.name} </br><strong> Patient Information</strong><br/>&nbsp;Name: #{req.get_value('Patient').get_value('Last Name')}, #{req.get_value('Patient').get_value('First Name')}<br/>&nbsp;DOB: #{req.get_value('Patient').get_value('DOB')}<br/> &nbsp;Physician: #{req.get_value('Requesting Physician').get_value('First Name')} #{req.get_value('Requesting Physician').get_value('Last Name')} <br/> &nbsp;Physician NPI: #{req.get_value('Requesting Physician').get_value('NPI Number')} <br/>"

      tests = "<strong>Test Information</strong><br/>"
      diagnosis = get_diagnoses(req)
      tests = tests + "&nbsp;Diagnosis(ICD-10): #{diagnosis} <br/>"
      req.get_value('Samples').each do |sample|
        if !sample.current_states.map(&:name).include?('Cancelled') && !sample.current_states.map(&:name).include?('Accessioned')
          test_array = []
          cpt_array = []
          sample.get_value('Tests').each do |result|
            unless test_array.include?(result.name.to_s)
              test_array.push(result.name.to_s)
            end
            unless cpt_array.include?(result.get_value("CPT Codes"))
              cpt_array.push(result.get_value("CPT Codes"))
            end
          end

          test_string = test_array.join(';')
          cpt_string = cpt_array.join(';')
          tests = tests + "&nbsp;Sample: #{sample.name} <br/>&nbsp;&nbsp;Tests: #{test_string} <br/> &nbsp;&nbsp;CPT Codes: #{cpt_string}<br/> State: #{sample.current_states.map(&:name).to_s}<br/>"
        end
      end

      insurance = ""
      for i in 1..3
        if req.get_value('Insurance Company ' + i.to_s)
          insurance_company = req.get_value('Insurance Company ' + i.to_s)
          insurance = insurance + "<strong>Insurance Information #{i}</strong><br/>"
          insurance = insurance + "&nbsp;Company: #{insurance_company} <br/>&nbsp;Address: #{insurance_company.get_value('Address')} <br/>&nbsp;Phone: #{insurance_company.get_value('Phone Number')} <br/>
    &nbsp;Group Number: #{req.get_value('Group #' + i.to_s)} <br/>&nbsp;Policy Number: #{req.get_value('Policy #' + i.to_s)}<br/>"
        end
      end

      billling_facility = ""

      if req.get_value('Billing Facility')
        billling_facility = "<strong>Billing Facility</strong><br/>"
        billling_facility = billling_facility + "&nbsp;Facility: #{req.get_value('Billing Facility')} <br/> &nbsp;Address: #{req.get_value('Billing Facility Address')}"
      end

      template = main + tests + insurance + billling_facility
      return template
    end

    #Create HL7 Message
    def create_dft_message(req, configuration, event_type = 'P03')
      patient = req.get_value('Patient')
      msg = ::HL7::Message.new
      process_msh(msg,req,configuration,"DFT",event_type)
      process_pid(msg,patient)
      process_pv1(msg,req)
      provider_config = configuration.provider_type.config
      
      class_based_billing = LIS.system_settings['Test Class Billing']
      bill_type = req.get_value("Bill To")
      hcpcs_gcode_billing = if class_based_billing && (bill_type == "Insurance" || bill_type == "Medicare")
        insurance_company1 = req.get_value('Insurance Company 1')
        insurance_company1.present? ? insurance_company1.get_value('HCPCS Code G048X Billing') : false
      else
        false
      end
      
      process_ft1(msg,req, add_diag: provider_config[:dg1_under_each_ft1], split_cpt_codes: provider_config[:split_cpt_codes], emit_dot_in_icd10: provider_config[:emit_dot_in_icd10],
        		  class_based_billing: class_based_billing, hcpcs_gcode_billing: hcpcs_gcode_billing)
      process_dg1(msg,req, provider_config[:emit_dot_in_icd10]) if !provider_config[:dg1_under_each_ft1]
      process_gt1(msg,patient,req)
      process_in1(msg,patient,req)
      return msg.to_hl7
    end

    #Creates ADT Message
    def create_adt_message(patient, configuration, event_type = 'A04')
      msg = ::HL7::Message.new
      process_msh(msg,patient,configuration,"ADT",event_type)
      process_pid(msg,patient)
      provider_config = configuration.provider_type.config
      process_pd1(msg,patient) if provider_config[:generate_pd1_in_adt]
      process_nk1(msg,patient) if provider_config[:generate_nk1_in_adt]
      process_pv1(msg,patient)
      process_gt1(msg,patient,patient)
      process_in1(msg,patient,patient)
      return msg.to_hl7
    end

    #Data collecion and insertion
    def process_msh(hl7_msg,req,configuration,messagetype,triggerevent)
      msh = configuration.provider_type.generate_billing_msh(configuration)
      facility = req.get_value('Facility')
      msh.recv_facility = createSegmentString([facility.name,facility.get_value('Billing Identifier')])
      msh.message_control_id = req.name
      msh.message_type = createSegmentString([messagetype,triggerevent])

      hl7_msg << msh
    end

    def process_pv1(hl7_msg, patient, send_account_manager: true)
      referring_doctor = ""
      bill_type = ""
      billing_identifier = ""
      other_healthcare_provider = ""
      if patient.present?
        facility = patient.get_value('Facility')
        billing_identifier = facility.get_value('Billing Identifier')
        physician = patient.get_value("Requesting Physician")
        if physician
          npi_number = physician.get_value("NPI Number").to_s
          referring_doctor = createSegmentString([npi_number,physician.get_value("Last Name"),physician.get_value("First Name")])
        end
        if send_account_manager
          account_manager = facility.get_value('Account Manager')
          other_healthcare_provider = account_manager.present? ? LISHL7.get_xcn_field('', account_manager.get_value('Full Name')) : ''
        end
        bill_type = patient.get_value('Bill To')
      end
        hl7_msg << Segments.pv1(referring_doctor, bill_type, billing_identifier, other_healthcare_provider)
    end

    def process_dg1(hl7_msg,req, icd10_with_dot = false)
      dg1 = []
      di = 0
      req.get_value('Diagnoses').each do |diagnosis|
        di = di + 1
        diagnosis_code = format_icd10_code(diagnosis.get_value('Code'), icd10_with_dot)
        diagnosis_description = diagnosis.get_value('Description')
        hl7_msg << Segments.dg1(di,diagnosis_code,diagnosis_description)
      end
    end
    
    def get_tests(sample, aggregate_to_classes: false, use_hcpcs_gcodes: false)
      test_results = sample.get_value('Test Results')
      tests = []
      test_classes = []
      sample.get_value('Tests').each do |test|
        if test.get_value('Non-Reportable')
          test_analytes = test.get_value('Analytes')
          test_results.each do |r|
            if test_analytes.include?(r.get_value('Analyte'))
              test = {"test":test.name, "procedure_code":test.get_value('Code'), "cptcodes":format_cpt_codes(test.get_value("CPT Codes")),"reported":false,"value":r.get_value('Value'),"unit":r.get_value('Unit')}
              unless tests.include?(test)
                tests.push(test)
              end
            end
          end
        else
          test_class = test.get_value('Test Class')
          if aggregate_to_classes && test_class.present?
            test_classes << test_class unless test_classes.include?(test_class)
          else
            test = {"test":test.name,"procedure_code":test.get_value('Code'),"cptcodes":format_cpt_codes(test.get_value("CPT Codes")),"reported":true}
            tests << test unless tests.include?(test)
          end
        end
      end
      
      if use_hcpcs_gcodes
        hcpcs_code = if test_classes.count >= 1 && test_classes.count <= 7
            'G0480'
          elsif test_classes.count >= 8 && test_classes.count <= 14
            'G0481'
          elsif test_classes.count >= 15 && test_classes.count <= 21
            'G0482'
          elsif test_classes.count >= 22
            'G0483'
        end
        tests << {"test":'', "cptcodes":LISHL7.createSegmentString([hcpcs_code,'','HCPCS']), "reported":true}
      else
        test_classes.each do |test_class|
          test = {"test":test_class.name, "cptcodes":format_cpt_codes(test_class.get_value("CPT Codes")), "reported":true}
          tests << test unless tests.include?(test)
        end
      end
      
      tests
    end

    def format_cpt_codes(str)
      rm_decimal = ''
      if !str.nil?
         rm_decimal = str.gsub(".0","")
      end
      return rm_decimal
    end

    def get_diagnoses(req)
      code = []
      req.get_value('Diagnoses').each do |diagnosis|
        code.push(diagnosis.get_value('Code'))
      end
      return code.join(", ")
    end

    def process_ft1(hl7_msg,req, add_diag: false, split_cpt_codes: true, emit_dot_in_icd10: true, class_based_billing: false, hcpcs_gcode_billing: false)
      test_total = "1"
      department_code = ""
      transaction_code = ""
      unit_cost = ""
      assigned_patient_location = ''#req.get_value('Patient').get_value('Billing Facility') || req.get_value('Facility')

      physician = req.get_value("Requesting Physician")
      physician_npi_number = physician.get_value("NPI Number").to_s
      physician_last_name = physician.get_value("Last Name")
      physician_first_name = physician.get_value("First Name")

      test_performer_first_name = req.get_value("Test Performer First Name")
      test_performer_last_name = req.get_value("Test Performer Last Name")
      test_performer_npi = req.get_value("Test Performer NPI Number")

      if test_performer_first_name == "."
        performed_by = createSegmentString([test_performer_npi.to_s,test_performer_last_name])
      else
        performed_by = createSegmentString([test_performer_npi.to_s,test_performer_last_name,test_performer_first_name])
      end

      tc = 1
      #Bill for each test on each sample.
      req.get_value('Samples').each do |sample|
        sample_states = sample.current_states.map(&:name)
        Rails.logger.info("===> generating FT1 for sample #{sample.name} in states: #{sample_states.inspect}")
        if !sample_states.include?('Cancelled') && !sample_states.include?('Accessioned')
          collection_date = sample.get_value("Collection Date")

          gen_ft1 = ->(cptcode, test_total, transaction_code, test_name) {
            cptcode = cptcode.gsub(" ","")	#Remove extra space from string
            code = cptcode.split('x')		#check if its a quantity type of code
            if code.size > 1				#check if it is set quantity
              test_total = code[1]
            end

            hl7_msg << Segments.ft1(tc, collection_date, transaction_code, test_name, test_total, department_code, assigned_patient_location, get_diagnoses(req),
              						performed_by, physician_npi_number, physician_last_name, physician_first_name,
              						unit_cost, code[0], '', sample.barcode_tag)
            if add_diag
              process_dg1(hl7_msg, req, emit_dot_in_icd10)  # Telcor, PractiSource
            end
            tc += 1
            }

          tests = get_tests(sample, aggregate_to_classes: class_based_billing, use_hcpcs_gcodes: hcpcs_gcode_billing)
          Rails.logger.info("===> got following tests: #{tests.inspect}")

          #Go through each of the collected tests
          tests.each_with_index do |test,index|
            #If the test is not reported get the quanity for the test from entered value
            if !test[:reported]
              test_total = test[:value]
            else
              test_total = 1;
            end

            if split_cpt_codes
              test[:cptcodes].split(',').each {|cptcode| gen_ft1.call(cptcode, test_total, test[:procedure_code], test[:test])}
            elsif test[:cptcodes].present?
              gen_ft1.call(test[:cptcodes], test_total, test[:procedure_code], test[:test])
            end
          end
        end
      end
    end

    def process_gt1(hl7_msg,patient,req)
      bill_type = req.get_value("Bill To")

      patient_phone = patient.get_value('Phone Number')
      guarantor_id = patient.get_value('MRN')
      guarantor_spouse_name = ""
      set_gt1 = false

      if ["Insurance","Medicare"].include?(bill_type)
        relation_to_insured = req.get_value('Relation To Insured')
        guarantor_type = "Y"
        if relation_to_insured != "Self"
          subscriber_name = req.get_value('Subscriber Name')
          guarantor_name = LISHL7.get_xpn_field(subscriber_name)
          guarantor_address = get_address_xad_field(req.get_value('Subscriber Address'))
          guarantor_phone_home = createSegmentString([patient_phone,"","","","","",""])
          guarantor_phone_business = guarantor_phone_home
          guarantor_dob = req.get_value("Subscriber DOB")
          guarantor_sex = req.get_value("Subscriber Sex")
          guarantor_relationship = relation_to_insured
          guarantor_relationship += ' adult' if relation_to_insured == 'Other'
          set_gt1 = true
        end
      elsif bill_type == "Facility"
        billing_facility = req.get_value('Billing Facility')
      
        guarantor_type = "N"
        guarantor_id = billing_facility.get_value('Billing Identifier')
        guarantor_name = createSegmentString([billing_facility.name])
        guarantor_address = get_address_xad_field(req.get_value('Billing Facility Address'))
        guarantor_phone_home = createSegmentString([billing_facility.get_value('Phone Number'),"","","","","",""])
        guarantor_phone_business = guarantor_phone_home
        guarantor_dob = ""
        guarantor_sex = ""
        guarantor_relationship = 'Other'
        set_gt1 = true
      end
    
      #If guarantor is not set above use self.
      Rails.logger.info("===> GT1: guarantor_address: #{guarantor_address}")
      if !set_gt1
        guarantor_name = createSegmentString([patient.get_value('Last Name'),patient.get_value('First Name'),patient.get_value('Middle Name')])
        guarantor_address = get_address_xad_field(patient.get_value('Address'))
        guarantor_phone_home = createSegmentString([patient_phone,"","","","","",""])
        guarantor_phone_business = guarantor_phone_home
        guarantor_dob = patient.get_value("DOB")
        guarantor_sex = patient.get_value("Sex")
        guarantor_relationship = 'Self'
      end

      guarantor_relationship = get_relationship_cwe_field(guarantor_relationship)
      hl7_msg << Segments.gt1(guarantor_id,guarantor_name,guarantor_spouse_name,guarantor_address,guarantor_phone_home,guarantor_phone_business,guarantor_dob,guarantor_sex,guarantor_type,guarantor_relationship)
    end

    def process_in1(hl7_msg,patient,insurance)
      insurance_array = [1,2,3]

      bill_type = insurance.get_value("Bill To")

      if bill_type == "Insurance" || bill_type == "Medicare"
        patient_last_name = patient.get_value('Last Name')
        patient_first_name = patient.get_value('First Name')
        patient_middle_name = patient.get_value('Middle Name')
        patient_dob = patient.get_value("DOB")
        patient_sex = patient.get_value("Sex")
        patient_address = get_address_xad_field(patient.get_value('Address'))

        subscriber_name = insurance.get_value('Subscriber Name')
        subscriber_dob = insurance.get_value("Subscriber DOB")
        subscriber_sex = insurance.get_value("Subscriber Sex")
        insured_id = ""

        relation_to_insured = insurance.get_value('Relation To Insured')
        if relation_to_insured == "Self"
          insured_name = createSegmentString([patient_last_name,patient_first_name,patient_middle_name])
          insured_dob = patient_dob
          insured_sex = patient_sex
          insured_address = patient_address
          insured_relationship = 'Self'
        else
          insured_name = LISHL7.get_xpn_field(subscriber_name)
          insured_dob = subscriber_dob
          insured_sex = subscriber_sex
          subscriber_address = insurance.get_value('Subscriber Address') # || patient.get_value('Subscriber Address')
          insured_address = get_address_xad_field(subscriber_address)
          insured_relationship = relation_to_insured
          insured_relationship += ' adult' if insured_relationship == 'Other'
        end

        insurance_array.each do | count |
          insurance_company = insurance.get_value('Insurance Company ' + count.to_s)
          if insurance_company
            insurance_company_name = insurance_company
            insurance_company_id = insurance_company.get_value('Payer ID').to_s
            insurance_address = get_address_xad_field(insurance_company.get_value('Address'))
            insurance_company_phone = insurance_company.get_value('Phone Number').to_s
            insurance_policy_num = insurance.get_value('Policy #' + count.to_s)
            insurance_group_num = insurance.get_value('Group #' + count.to_s)

            insured_relationship = get_relationship_cwe_field(insured_relationship)
            hl7_msg << Segments.in1(count,insurance_company_id,insurance_company_name,insurance_address,insurance_company_phone,insurance_group_num,insured_name,insured_relationship,insured_dob,insured_address,insurance_policy_num,insured_sex,insured_id)
          end
        end
      end
    end

    def process_pid(hl7_msg,patient)
      patient_last_name = patient.get_value('Last Name').to_s
      patient_first_name = patient.get_value('First Name').to_s
      patient_middle_name = patient.get_value('Middle Name').to_s
      patient_sex = patient.get_value('Sex').to_s
      patient_mrn = patient.get_value('MRN').to_s
      patient_address = get_address_xad_field(patient.get_value('Address'))
      patient_phone_number = patient.get_value('Phone Number') || "717-555-1212"
      patient_phone = get_phone(patient_phone_number)
      patient_id = patient.get_value('External Source ID').to_s
      patient_id = patient.get_value('MRN').to_s if patient_id.nil? || patient_id.empty?
      patient_dob = patient.get_value('DOB')

      hl7_msg << Segments.pid(patient_id,patient_last_name,patient_first_name,patient_middle_name,patient_dob,patient_sex,patient_address,patient_phone_number,patient_phone.area_code,patient_phone.local_number,patient_mrn)
    end

    def process_pd1(hl7_msg, requisition)
      primary_care_provider = requisition.get_value('Primary Physician NPI')
      hl7_msg << Segments.pd1(primary_care_provider)
    end

    def process_nk1(hl7_msg, patient)
      hl7_msg << Segments.nk1(1)
    end

  end
end