module Instrument
  class Shimadzu_8040 < Common
    
    def import_file(file)
      context.open_spreadsheet_file_simple(file.path, extension: :csv, csv_options: {col_sep: "\t", encoding: 'bom|utf-8'}, headers: ('A'..'O').to_a) {|csv|
        analyte = nil
        
        block = nil
        line = nil
        context.init_progress csv.length
        csv.each {|row, row_raw|
          Rails.logger.info("-->> #{row_raw.inspect}")
          context.advance_progress
          
          col0 = row_raw[0]
          if col0 == 'ID#'
            block = row_raw[1]
            line = nil
            analyte = nil
          elsif col0 == 'Name'
            analyte = row_raw[1]
          elsif col0.blank?
            # (header or separator) => skip
          else
            sample_type = row_raw[3]
            next if sample_type.nil? || !sample_type.start_with?('Unknown')	# 'Unknown' are actually regular and QC samples, everything else is garbage
            line = col0
            value = row_raw[8]
            value = 0.0 if value == '-----' || value.blank?	# per Defect #10002, value should be set to 0.0 if concentration was not reported by the instrument
            yield({sample: row_raw[2], analyte: analyte, value: value, row: "#{block}:#{line}"})
          end
        }
      }
    end

  end
end