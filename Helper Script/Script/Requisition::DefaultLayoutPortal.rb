module Requisition
  
  module DefaultLayoutPortal
    include Requisition
    include Patient::Layout
    include Physician::Layout
    include ScriptRunner::UI
    extend self
    
    
    def general_tab(subj,bill_facil,system_settings,facility)
      defaultSpecimenTypes = system_settings.get_value('Default Specimen Types') || []
      # if physician management is disabled for that facility, don't let them create new ones  
      allowCreatePhysician = facility.get_value('Allow Physicians Management')

      field_set(title:'General Information', border:'1 0 0 0', defaults:{anchor:'100%'}, items:[
        udf('Facility', subj, filter:'terminated is false and RURO_CLIENT is null', defaultValue:facility, labelWidth:160, allowCreate:false, hidden:true),
        udf('Requesting Physician', subj, addCustomFields: physician_layout(subj, 2), labelWidth:160, required:true,
          filter:"Facility = #{facility.id} and terminated is false",
          allowCreate:facility.get_value('Allow Physicians Management')),
        #Name of Collector (auto populate with current user, but editable; required) -Needs System Setting Control.
        udf('Name of Collector', subj, defaultValue: "#{User.curr_user}", labelWidth:160, anchor:'50%', required:true),
        udf('Collection Date', subj, labelWidth:160, anchor:'50%', required:true),
        udf('Specimen Types', subj, defaultValue: defaultSpecimenTypes, required:true, labelWidth:160),
        udf('External Source ID', subj, info:'This field can be used for the Sample Barcode Entry', labelWidth:160),
        udf('Comments', subj, labelWidth:160)
        # Portal users should not be allowed to choose recepients,
        #udf('Additional Report Recipients', subj, filter:"terminated is false", labelWidth:160, allowCreate:false)
      ])
    end

    def patient_tab(subj,system_settings,facility,sch_page,sch_test_codes = nil)
      patient = nil
      if subj
        if subj.get_value('Patient')
        	patient = subj.get_value('Patient')
        elsif subj.subject_type.name == "Patient"
        	patient = subj
        end
      end

      patient_field = if sch_page
        udf('Patient', subj, readOnly:true, filter:'terminated is false', labelWidth:160, allowCreate:false, required:true,
          value:patient)
      elsif subj.subject_type.name == 'Patient'
        udf('Patient', nil, labelWidth:160, allowCreate:false, required:true, readOnly:true, value:subj)
      else
        udf('Patient', subj, labelWidth:160, required:true, addCustomFields: patient_layout(patient, 2),
          filter:"Facility = #{facility.id} and terminated is false")
      end
  
      test_codes = []
      unless sch_test_codes.nil?
        test_codes.concat sch_test_codes
      end
  
      if subj
        test_codes.concat subj.get_value('Tests')
        diagnoses = subj.get_value('Diagnoses')
        medications = subj.get_value('Medications')
      else
        test_codes = validity_test
      end
  
      if !patient.nil?
        if subj.get_value('Tests').size == 0
          #Added Concat Here. 
          test_codes.concat patient.get_value('Tests')
        end
        diagnoses = patient.get_value('Diagnoses') if diagnoses.blank?
        medications = patient.get_value('Medications') if medications.blank?
      end
      field_set(title:'Patient Information', border:'1 0 0 0', items: [
          patient_field,
          patient_dependent_fields(subj, patient, test_codes)
        ],
        react: {
          shown_when: 'value',
          only: 'Facility'
      })
    end

    def physician_acknowledgement_tab(subj,system_settings)
      #Medical Necessity Statement
      medical_necessity_statement = if subj.get_value('Patient')
        subj.get_value('Patient').get_value('Medical Necessity Statement')
      elsif subj.get_value('Medical Necessity Statement') && subj.get_value('Use Medical Necessity On Requisition')
        subj.get_value('Medical Necessity Statement')
      end
      if system_settings.get_value('Ask for Medical Necessity')
        field_set(title:'Physician Acknowledgments', border:'1 0 0 0', items:[
          udf('Medical Necessity Statement', subj, anchor:'100%',
            defaultValue: medical_necessity_statement, labelWidth:160, required:true)
        ])
      end
    end

    def patient_acknowledgement_tab(subj, system_settings)
      #Patient Signature -‐ see requirement REQ-‐14 below MISSING
      #Physician Signature -- attached to Physician ST
      if system_settings.get_value('Require Patient Signature in Portal')
        field_set(title:'Patient Acknowledgments', border:'1 0 0 0', items:[
          {
            xtype: 'displayfield',  
            fieldLabel: 'Signature Authorization Statement',
            #udfName:'Signature Authorization Statement',
            anchor:'60%',
            labelWidth: 160,
            #name: 'signatureAuthorizationStatement',
            value: system_settings.get_value('Signature Authorization Statement')
          },
          udf('Patient Signature', subj, required:true, anchor:'100%', labelWidth:160)
        ])
      end
    end

    def requisition_layout_portal(subj,sch_test_codes = nil)
      # four options here:
      # 1. Created from Client Portal home page
      # 2. edit
      system_settings_type = SubjectType.find_by_name('System Settings')  
      system_settings = system_settings_type.subjects.first
  
      sch_page = (subj and subj.subject_type.name=="Testing Schedule")
      portal_page = (subj and subj.subject_type.name=="Facility Portal")

      if sch_page
        facility = User.curr_user.get_value('Facility')
      else
        if subj && subj.get_value('Facility')
          facility = subj.get_value('Facility')
        else
          facility = User.curr_user.get_value('Facility')
        end
      end
  
      bt = subj && subj.get_value('Bill To')
      bill_insur = bt == 'Insurance'
      bill_facil = bt == 'Facility'
      bill_medicare = bt == 'Medicare'
      rel = subj && subj.get_value('Relation To Insured') || 'Self'
  
      tabs = [{
        title:'General', defaults: {width: '100%'}, defaultType: 'textfield', autoScroll:true,
        items: [
          general_tab(subj,bill_facil,system_settings, facility),
          patient_tab(subj,system_settings,facility,sch_page,sch_test_codes),
          physician_acknowledgement_tab(subj, system_settings),
          patient_acknowledgement_tab(subj, system_settings)
        ]
      }]
      
      if system_settings.get_value("Use Point of Care Tests")
        tabs << {
          title:'PoC Tests', defaults: { width: '100%'}, defaultType:'textfield', layout:'fit', items: [
            poct_results_tab(subj),
            {xtype:'hidden', name:'custom[poc_grid_results]', itemId:'poc_grid_results_id'}
          ]}
      end
  
      if system_settings.get_value("Use Confirmation Tests")
        tabs << {
          title:'Confirmation Tests', defaults:{width:'100%'}, layout:'fit', items: [
            confirmation_tab(subj),
            {xtype:'hidden', name:'custom[confirmation]', itemId:'confirmation_id'}
          ]
        }
      end
  
      encode_fields({
        xtype:'tabpanel', plain:true, activeTab: 0, height:500, defaults:{bodyPadding: 10},
        items:tabs
      })
    end
        
  end
  
end