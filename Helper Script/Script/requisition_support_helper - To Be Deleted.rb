require 'base64'
def file_to_base64(file_path)
  begin
    encoded_string = Base64.strict_encode64(File.open(file_path, "rb").read) 
  rescue Exception=>e
    Rails.logger.info "Could not convert to Base64. #{e}"
  end
end

def find_by_attrs(sub_type,attrs,values)
  query = search_query(subject_type: sub_type) do |qb|
    q = qb.and
    attrs.each_with_index { |attr,index|       
     q << qb.compare(attr, :eq, values[index])
    }
    q
  end
  return find_subjects(query: query).first
end

def is_insurance_selected(params)
  if params['Bill To'] == "Insurance"
    unless params["Insurance Company 1"].present? || params["Insurance Company 2"].present? || params["Insurance Company 3"].present?
      return false
    end  
  end
  return true
end

def is_insurance_required(use,company,field)
  str = ''
  if use.present? && !company.present?
    str = "#{field} is empty, please make a selection."
  end
  return str
end

def post_process_requisition(requistion)
end

def error_check(params)
  raise "Collection Date can not be in the future" if params['Collection Date'].present? && params['Collection Date'] > Time.now
  raise "Received Date can not be before Collection Date." if !params['Collection Date'].nil? && !params['Received Date'].nil? && params['Collection Date'] > params['Received Date']
end

def generate_requisition_report(subj,lab_name, logo, address, system_settings, poc_test_results, physician_file,patient_file)
  run_custom_report('Requisition Report', subj, 'Requisition Report') do |extra_data|     
    extra_data['Lab Name'] = lab_name      
    extra_data['Lab Logo'] = logo
    extra_data['Lab Address'] = address || ""
    extra_data['System Settings'] = system_settings
    extra_data['poc_test_results'] = poc_test_results
    extra_data['Physician Signature'] = physician_file
    extra_data['Patient Signature'] = patient_file
    extra_data['Now'] = Time.now
  end # PDF
end

def _submit_requisition(subj)
  message = ""
  system_settings_type = SubjectType.find_by_name('System Settings')  
  system_settings = system_settings_type.subjects.first

  #format the address since \n is not honored by HTML
  if system_settings.get_value('Lab Address')
    address_array = system_settings.get_value('Lab Address').split("\n")
    address = address_array.join('<br/>')
  end

  begin
    physician_file = ''
    physician_file = file_to_base64(File.absolute_path(subj.get_value('Requesting Physician').get_value('Physician Signature').path)) if subj.get_value('Requesting Physician').get_value('Physician Signature').present?

    patient_file = ''
    patient_file = file_to_base64(File.absolute_path(subj.get_value('Patient Signature').path)) if subj.get_value('Patient Signature').present?
  rescue Exception => e
    Rails.logger.error("There was an error getting one or both of the signatures. #{e}")
  end
  
  if subj.get_value('Point of Care Test Results').size > 0
    poct_results = subj.get_value('Point of Care Test Results')[0].get_value('Point of Care Test Result Entries')
  end

  generate_requisition_report(subj,system_settings.get_value('Lab Name'), file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path)), address, system_settings, poct_results, physician_file,patient_file)
  
  close_tab_ids(subj.id)

  prop = Property.find_by_display_name('Requisition Report')
  message.concat("<p>Requisition was sucessfully submitted. Requisition ID:  <strong>#{subj.name}</strong><br><br>Click to download <a target='_blank' href='/subjects/attachment/?id=#{subj.id}&pid=#{prop.id}'>Requisition Report</a></p>")

  show_message(message.html_safe)
  
  SystemHooks.requisition_submitted(subj)
end