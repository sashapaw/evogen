module Instrument
  class BC_AU480 < Common

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @analyte_map = get_analyte_map
    end

    def get_analyte_map
      {
        '001' => 'Creatinine',
        '002' => 'pH',
        '003' => 'Oxidants',
        '004' => 'Specific Gravity'
      }
    end

    def get_query_response(data)
      Rails.logger.info("=== get_query_response: #{data}")
      queries = data['query']
      queries = [queries] unless queries.is_a?(Array)

      orders = []
      queries.each do |query|
        order = process_query(query)
        orders << order if order.present?
      end

      { orders: orders }
    end

    def process_query(query)
      Rails.logger.info("=== Processing query: #{query}")
      sample_request = query['sample_request']

      res = {'sample': sample_request}
      sample = get_sample(sample_request['sample_id'].strip)
      if sample.present?
        # Add patient information for real samples
        requisition = get_requisition(sample)
        if requisition.present?
          patient = get_patient(requisition)
          if patient.present?
            res.merge!({'patient': patient})
          end
        end

        # Get orders
        orders = if sample.subject_type.name == 'Sample'
          test_results = get_test_results(sample)
          if test_results.present?
            Rails.logger.info("=== found test_results: #{test_results}")
            get_orders(sample, test_results)
          end
        elsif sample.subject_type.name == 'QC Sample'
          get_qc_orders(sample)
        else
          @result_entry.new_message("Sample", "Sample: #{sample.to_s} is of unsupported type #{sample.subject_type.name}")
        end

        if orders.present?
          Rails.logger.info("=== test_results mapped to device tsts: #{orders}")
          res.merge!({'tests': orders})
        end
      end
      res
    end

    def get_patient_data(requisition)
      patient = requisition.get_value('Patient')
      if patient.blank?
        rname = requisition.name rescue 'nil'
        @result_entry.new_message("Patient", "Requisition: #{rname} has no Patient")
        nil
      else
        id = patient.id rescue ''
        fname = patient.get_value('First Name')
        lname = patient.get_value('Last Name')
        birthdate = patient.get_value('DOB').to_s.gsub!("-", "")
        age = Date.today.year.to_i - patient.get_value('DOB').year.to_i
        sex = patient.get_value('Sex')
        {
          id: id,
          first_name: fname,
          last_name: lname,
          birth_date: birthdate,
          age: age,
          sex: sex
        }
      end
    end

    def get_patient(requisition)
      pdata = get_patient_data(requisition)
      if pdata.present?
        { 'patient_info_1': pdata[:first_name], 'patient_info_2': pdata[:last_name], sex: pdata[:sex], age_years: pdata[:age], age_months: nil }
      else
        nil
      end
    end

    def get_orders(sample, test_results)
      tests = []
      test_results.keys.each do |aname|
        test_id = get_analyte_map.key(aname)
        tests << test_id if test_id.present?
      end
      tests
    end

    def get_qc_orders(sample)
      #control_type = sample.get_value('Control Type')
      analytes = sample.get_value('Analytes')

      tests = []
      analytes.each do |analyte|
        test_id = get_analyte_map.key(analyte.name)
        tests << test_id if test_id.present?
      end
      tests
    end

    #=====

    def import_data(data)
      data_results = data['results']
      if data_results.present?
        data_results.each do |data_result|
          import_result(data_result)
        end
      end
      nil
    end

    def import_result(data_result)
      sample_information = data_result['sample_information']
      Rails.logger.info("=== importing test_result: #{data_result}")
      return unless sample_information.present?

      sample = get_sample(sample_information['sample_id'].strip)
      return unless sample.present?

      test_results = get_test_results(sample)
      imported_results = get_results(data_result, sample, test_results)
      Rails.logger.info("=== calculating flags for #{imported_results.size} test results")
      imported_results.each do |r|
        r.calculate_flags
      end
    end

    def get_results(data, sample, test_results)
      Rails.logger.info("=== processing results: #{data}")
      imported_results = []
      data['analyses'].each do |result|
        field = result['online_test_no']
        val = result['analysis_data']
        flag = result['data_flag']
        test_result = get_test_result(field, sample, test_results)
        if test_result
          analyte = test_result.get_value('Analyte')
          val = if analyte.get_value('Qualitative')
            map_qualitative_value(val, analyte)
          else
            begin
              Float(val)
            rescue
              val
            end
          end
          test_result.set_value('Value', val)
          test_result.set_value('Instrument Comments', flag) if flag.present?
          imported_results << test_result
        end
      end
      @result_entry.link_test_results(imported_results)
      get_comments(data, sample)
      imported_results
    end

    def map_qualitative_value(val, analyte)
      # AU-480/680 report values as quantitative for qualitative tests
      # We need to make sure the value is within acceptable range to prevent Abnormal fflag triggering  (Westox request)

      range = analyte.get_value('Applicable Ranges', limit: 1).first  # Fixme: pick range based on patient's data
      if range.present?
        begin
          val = Float(val)
          range_low = range.get_value('Low Limit')
          #range_high = range.get_value('High Limit')
          val = range_low if val < range_low
          #val = range_high if val > range_high
        rescue
        end
      end
      val
    end

    def get_comments(data, sample)
      comments = ""
      data['analyses'].each do |section|
        section.each do |result|
          val = result['data_flag'] rescue nil
          comment = "#{val}" if val
          comments += comment + "\n" if comment
        end
      end
      sample.set_value('Instrument Comments', comments) unless comments.blank?
    end

    def get_analyte_code(field)
      field
    end
  end
end