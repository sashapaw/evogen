module LISHL7
  
  module Providers
    
    module Billing
      
      mattr_accessor :config, instance_accessor: false do
        {
          generate_nk1_in_adt: true,
          generate_pd1_in_adt: true,
          dg1_under_each_ft1: false,
          split_cpt_codes: true,
          emit_dot_in_icd10: true
        }
      end

      module PractiSource
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.merge({
            dg1_under_each_ft1: true,
            split_cpt_codes: false
          })
        end

        Providers.generate_config_accessors self
      end

      module Telcor
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.merge({
            generate_nk1_in_adt: false,
            #generate_pd1_in_adt: false,
            dg1_under_each_ft1: true,
            split_cpt_codes: false
          })
        end
		
        Providers.generate_config_accessors self
      end
      
      module HealthFusion
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.merge({
            emit_dot_in_icd10: false
          })
        end

        Providers.generate_config_accessors self
      end
      
      module Quadax
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.merge({
          })
        end
        
        Providers.generate_config_accessors self
      end
      
      module Generic
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.merge({
          })
        end
        
        Providers.generate_config_accessors self
      end
      

      def generate_billing_msh(configuration)
        LISHL7::Segments.msh(
          sending_facility: LISHL7.createSegmentString([configuration.get_value('Facility Name'), configuration.get_value('Facility Number')]),
          recv_app: self.name,
          processing_id: configuration.get_value('HL7 Mode') || 'P',
          version_id: self.get_value('HL7 Version')
          )
      end
      
    end
    
  end
  
end