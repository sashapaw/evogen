module Analyte
  module Submit
    extend self
    
    def process_range_data(subj, range_data)
      context = ScriptRunner.context
      map = ['Low', 'High', 'Critical Low', 'Critical High', 'Age From', 'Age To', 'Age Unit', 'Gender']
      
      arr = range_data.nil? ? [] : range_data.split(',')
      subj.get_value('Applicable Ranges').each do |s|
        s.destroy
      end
      
      ranges = arr.map do |a|
        ll = a.split('|')
        context.create_subject('Range') do |v|
          map.each_with_index do |m, idx|
	        v.set_value(m, ll[idx])
          end
          v.set_value('Analyte', subj)
        end
      end
      
      validate_ranges(ranges)
    end
    
    def validate_ranges(ranges)
      m_ranges = []
      f_ranges = []
      b_ranges = []
      ranges.each{|range|
        case range.get_value('Gender')
        when 'Male'
          check_range_intersection(m_ranges, range)
        when 'Female'
          check_range_intersection(f_ranges, range)
        else
          check_range_intersection(b_ranges, range)
        end
      }
    end
      
    def check_range_intersection(ranges, range)
      m = range_in_months(range)
      if m
        ranges.each{|r|
          raise "Overlapping ranges: #{format_age(m[2])} and #{format_age(r[2], true)}" if m[0] <= r[0] && m[1] > r[0] || m[0] < r[1] && m[1] >= r[1] || m[0] >= r[0] && m[1] <= r[1]
        }
        ranges << m
      end
    end
    
    def range_in_months(range)
      age_unit = range.get_value('Age Unit')
      months = age_unit == 'Years' ? 12 : 1
      age_from = range.get_value('Age From')
      age_from = age_from ? age_from * months : -Float::INFINITY
      age_to = range.get_value('Age To')
      age_to = age_to ? (age_to + 1) * months : Float::INFINITY
      [age_from, age_to, range]
    end
    
    def format_age(range, show_gender=false)
      age_from = range.get_value('Age From')
      age_to = range.get_value('Age To')
      suffix = show_gender ? " for #{range.get_value('Gender') || 'Both'}" : ''
      age_to_s = age_to ? age_to.to_i.to_s : 'Infinity'
      "#{(age_from || 0).to_i} ... #{age_to_s} #{range.get_value('Age Unit')}#{suffix}"
    end
    
    def save_analyte(params)
      result_type = params.delete('Result Type')
      params['Qualitative'] = result_type == 'Qualitative'
      name = params.delete('Description')
      range_data = params.delete('range_data')
      subj = UI.add_subject('Analyte', params, {name: name})
      process_range_data(subj, result_type == 'Numeric' ? range_data : nil)
      ScriptRunner.context.open_subject(subj)
    end

    def update_analyte(subj, params)
      result_type = params.delete('Result Type')
      params['Qualitative'] = result_type == 'Qualitative'
      name = params.delete('Description')
      (subj.get_value('Applicable Ranges') || []).each{|s| s.destroy}
      range_data = params.delete('range_data')
      UI.save_subject(subj, params, name != subj.name ? {name: name} : {})
      process_range_data(subj, result_type == 'Numeric' ? range_data : nil)
    end
  end
  
  module Layout
    extend ScriptRunner::UI
        
    def analyte_layout(subj)
      range_data_str = []
      if subj.present?
        trs = (subj.get_value('Applicable Ranges') || []).sort{|r1,r2|r1.id <=> r2.id}
        range_data = []
        trs.each do |a|
          the_line = [a.get_value('Low') || '', a.get_value('High') || '', a.get_value('Critical Low') || '', a.get_value('Critical High') || '', 
            a.get_value('Age From') || '', a.get_value('Age To') || '', a.get_value('Age Unit') || '', a.get_value('Gender') || '']
          range_data << the_line
          range_data_str << the_line.join('|')
        end
        (0..4-trs.length).each do; range_data << ([''] * 8); end
      else
        range_data = [[''] * 8] * 5
      end

      encode_fields([
        {xtype:'hidden', value:range_data_str.join(','), name:'custom[range_data]', id:'range_data_id'},
        {xtype:'tabpanel', anchor:'100%', height:480, items:[
          {xtype:'panel', title:'General', layout:'form', glyph:0xf170, overflowY:'auto', items:[

            #udf('Test', subj, anchor:'100%', allowCreate:false, hidden:subj.present?),

            udf('Description', subj, value:subj.present? ? subj.name : '', fieldLabel: 'Analyte Name', anchor:'100%', required:true),
            udf('Analyte Type', subj, required: false, anchor:'100%', allowCreate:true),
            udf('Specimen Type', subj),

            field_row([
              udf('Instrument Code', subj, anchor:'100%'),
              udf('Instrument Type', subj, labelWidth:140, anchor:'100%', allowCreate:false)
            ]),

            udf('Result Type', subj, anchor:'100%', value:((subj and subj.get_value('Qualitative').present?) ? 'Qualitative' : 'Numeric'), required:true, udfName:'sn'),
            udf('Result Choices', subj, height:290, anchor:'100%', react: {
              shown_when:"value == 'Qualitative'", only: 'sn'}),

            field_container([
                field_row([
                  udf('Decimal Places', subj),
                  udf('Unit', subj, labelWidth:60)
                ]),
                udf('Non-Reportable', subj),
                {xtype:'label', marginTop:5, html:'<b>Applicable Ranges:</b><br>'},
                {
                    xtype:'grid', clicksToEdit:1, id:'range_data_grid', stripeRows:true, height:170,
                    store:UIUtils.raw_js("new Ext.data.SimpleStore({
                        fields: ['low', 'high', 'clow', 'chigh', 'age_from' , 'age_to', 'age_unit', 'gender'], 
                        data:#{range_data}
                    })"),
                    enableColumnMove:false, enableColumnResize:false,
                    plugins:[ UIUtils.raw_js("Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit:1, listeners:{
                        'edit':function(editor, e) {
                          var the_store = Ext.getCmp('range_data_grid').getStore();
                          var all_data = [];
                          for (var i=0; i!=the_store.getCount(); i++) {
                            var rec = the_store.getAt(i);
                            if (rec==null) rec 
                            var the_str = rec.get('low') + '|' +rec.get('high') + '|' +rec.get('clow') + '|' +rec.get('chigh') + '|' +rec.get('age_from') + '|' +rec.get('age_to') + '|' +rec.get('age_unit') + '|' +rec.get('gender');
                            the_str = the_str.replace(/null/g, '')
                            if (the_str != '|||||||') all_data.push( the_str );
                          }  
                          Ext.getCmp('range_data_id').setValue(all_data.join(','));
                        }
                      }} )") 
                    ],
                    columns:[
                      {sortable:false, menuDisabled:true, header:'Critical Low', width:'12.5%', dataIndex:'clow', editor:UIUtils.raw_js("new Ext.form.NumberField({decimalPrecision: 6})")},
                      {sortable:false, menuDisabled:true, header:'Low*', width:'12.5%', dataIndex:'low', editor:UIUtils.raw_js("new Ext.form.NumberField({required:true, decimalPrecision: 6})")},
                      {sortable:false, menuDisabled:true, header:'High*', width:'12.5%', dataIndex:'high', editor:UIUtils.raw_js("new Ext.form.NumberField({decimalPrecision: 6})")},
                      {sortable:false, menuDisabled:true, header:'Critical High', width:'12.5%', dataIndex:'chigh', editor:UIUtils.raw_js("new Ext.form.NumberField({decimalPrecision: 6})")},
                      {sortable:false, menuDisabled:true, header:'Age From', width:'12.5%', dataIndex:'age_from', editor:UIUtils.raw_js("new Ext.form.NumberField({decimalPrecision: 6})")},
                      {sortable:false, menuDisabled:true, header:'Age To', width:'12.5%', dataIndex:'age_to', editor:UIUtils.raw_js("new Ext.form.NumberField({decimalPrecision: 6})")},
                      {sortable:false, menuDisabled:true, header:'Age Unit', width:'12.5%', dataIndex:'age_unit', editor:UIUtils.raw_js("new Ext.form.ComboBox({valueField:'val', displayField:'val', queryMode:'local', forceSelection:true, store:new Ext.data.SimpleStore({fields:['val'], data :[[''],['Years'], ['Months']]})})")},
                      {sortable:false, menuDisabled:true, header:'Gender', width:'12.0%', dataIndex:'gender', editor:UIUtils.raw_js("new Ext.form.ComboBox({valueField:'val', displayField:'val', queryMode:'local', forceSelection:true, selectOnFocus:true, store:new Ext.data.SimpleStore({fields:['val'], data :[[''],['Male'], ['Female'], ['Both']]})})")},
                    ],
                    viewConfig:{ forceFit:true}
                },
                {xtype: 'container', layout: 'hbox', items: [
                  {xtype:'label', html:'Limits of Detection:', width: 220},
                  udf('Low Limit', subj, labelWidth: 80, flex: 1, decimalPrecision: 6),
                  {xtype:'box', width: 100},
                  udf('High Limit', subj, labelWidth: 80, flex: 1, decimalPrecision: 6)
                ]}
              ], react: {
                shown_when:"value == 'Numeric'",
                only: 'sn'
            })
        ]},
        {xtype:'panel', glyph:0xf18c, title:'Medications', layout:'form', items:[
          {xtype:'label', text:'Medications that influence outcome:'},
          udf('Medications', subj, fieldLabel:'', labelSeparator:'', labelWidth:0, anchor:'100%', filter:'terminated is false')
        ]},
        {xtype:'panel', title:'Report Notes', layout:'form', glyph:0xf0f6, items:[
          {xtype:'label', text:'Notes to show on the report for this analyte:'},
          udf('Report Notes', subj,  fieldLabel:'', labelSeparator:'', labelWidth:0, anchor:'100%', height:430)
        ]},
        {xtype:'panel', title:'More Settings', layout:'form', glyph:0xf085, items:[
          udf('Trigger Normalization', subj),
          udf('Hide Value', subj),
          udf('Propagate Outcome To', subj)
        ]},
        {xtype:'panel', title:'Formula', layout:'form', glyph:0xf12b, items:[
          {xtype:'label', text:'Advanced Users Only:  Ruby code formula to compute flags and ranges:'},
          {xtype:'hidden', name:'custom[Formula Code]', id:'formula_code'},
          {xtype:'textarea', fieldLabel:'', plugins: {ptype:'initcodemirror', hiddenField:'formula_code'}, labelSeparator:'', labelWidth:0, anchor:'100%', height:430}
        ]}
      ]}

      ])
    end
    
    extend self
  end
end