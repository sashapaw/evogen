module Instrument
  class Shimadzu_8050 < Common
    
    def next_not_blank(row_raw, start_col)
      (start_col...row_raw.length).find {|col| row_raw[col].present?}
    end
    
    def import_file(file)
      context.open_spreadsheet_file_simple(file.path, extension: :csv, csv_options: {col_sep: "\t", encoding: 'bom|utf-8'}, headers: ('A'..'O').to_a) {|csv|
        analyte = nil
        
        block = nil
        line = nil
        intable = false
        context.init_progress csv.length
        csv.each {|row, row_raw|
          Rails.logger.info("-->> #{row_raw.inspect}")
          context.advance_progress
          
          if intable
            if row_raw.blank?
              intable = false
            else
              value_index = next_not_blank(row_raw, 2)
              if value_index.present?
                value = row_raw[value_index]
                value = 0.0 if value == '-----' || value.blank?	# per Defect #10002, value should be set to 0.0 if concentration was not reported by the instrument
                
                sample_index = next_not_blank(row_raw, value_index+1)
                if sample_index.present?
                  sample_index = next_not_blank(row_raw, sample_index+1)
                  if sample_index.present?
                    Rails.logger.info("-->> value: #{value}, sample_index: #{sample_index}, analyte: #{row_raw[1]}")
                    yield({sample: row_raw[sample_index], analyte: row_raw[1], value: value, row: "#{block}:#{row_raw[0]}"})
                  end
                end
              end
            end
          else
            if row_raw[0] == '#' && row_raw[1] == 'Compound Name'
              intable = true
            end
          end

        }
      }
    end

  end
end