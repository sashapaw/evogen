module Instrument
  class Common

    def initialize(instrument, result_entry = nil, ctx = context)
      @instrument = instrument
      @ctx = ctx
      @result_entry = result_entry
    end

    # This implementation will work for single sample per result data report instruments
    def import_data(data)
      sample_barcode = get_sample_from_result(data)
      if sample_barcode
        importer = TestResult::Importer.new(@instrument)
        imported_results = []
        test_results_data = get_results(data, sample_barcode)
        importer.import_test_results(test_results_data) {|result_data, sample, test_result, error|
          @result_entry.add_test_result(test_result) if test_result.present?
          @result_entry.add_sample(sample) if sample.present?
          if error.present?
            error_type = error.is_a?(TestResult::ImportError) ? error.type : ''
            @result_entry.new_message(error_type, error.message)
          end
        }
        @result_entry.add_qc_runs(importer.qc_runs)
      else
        Rails.logger.info("=== Was unable to extract any sample IDs from result data: #{data.inspect}")
        @result_entry.new_message("Sample", "Unable to extract any Sample IDs")
      end
      nil
    end

    def no_data_found
      instrument_type = @instrument.get_value('Instrument Type')
      @result_entry.new_message("#{instrument_type.name}", "No data found")
    end

    # The methods below are used to process instrument's queries
    def get_requisition(sample)
      return nil unless sample.subject_type.name == 'Sample'
      requisition = sample.get_value('Requisition')
      if requisition.blank?
        @result_entry.new_message("Requisition", "Sample: #{sample.barcode_tag} has no Requisition")
      else
        @result_entry.set_requisition(requisition)
      end
      requisition
    end

    def get_sample(barcode)
      sample = Subject.find_by_barcode_tag(barcode) unless barcode.blank?
      if sample.blank?
        @result_entry.new_message("Sample", "Sample: #{barcode} could not be found")
      else
        if !['Sample','QC Sample'].include?(sample.subject_type.name)
          @result_entry.new_message("Barcode", "Barcode: #{sample.barcode_tag} not of Sample type")
          sample = nil
        else
          if sample.subject_type.name == 'Sample' && sample.current_states[0].name != 'In Testing'
            @result_entry.new_message("Sample", "Sample: #{sample.barcode_tag} not in 'In Testing' state")
          end
          @result_entry.add_sample(sample)
        end
      end
      Rails.logger.info("=== For '#{barcode}' found #{sample.present? ? sample.subject_type.name : 'nil'}: #{sample}")
      sample
    end

    def get_test_results(sample)
      query = @ctx.search_query(subject_type: 'Test Result') do |qb|
        qb.and qb.compare('Sample', :eq, sample),
               qb.compare('Analyte->"Instrument Type"', :eq, @instrument.type)
      end
      qresults = @ctx.find_subjects(query: query)
      if qresults.blank?
        @result_entry.new_message("Test Result", "Sample: #{sample.barcode_tag} has no Test Results for instrument #{@instrument}")
        nil
      else
        qresults.group_by{|tr| tr.get_value('Analyte')}.to_h
      end
    end

    def get_tests(requisition)
      tests = requisition.get_value('Tests')
      if tests.blank?
        @result_entry.new_message("Tests", "Requisition: #{requisition.name} has no Tests")
      end
      tests
    end

    def parse_value(s)
      return nil if s.nil?
      s = s.strip
      return Float(s) if /^[+-]?\d+(\.\d*)?([eE][+-]?\d+)?$/.match(s)
      return Float::INFINITY if s.start_with?('>')
      return -Float::INFINITY if s.start_with?('<')
      nil
    end

  end
end