# Evogen custom hooks
module CustomSystemHooks
  include DefaultSystemHooks
  extend self
  
  def validate_patient(pat)
    raise "Parent/Guardian Full Name is required if patient is less than 18 years old" if pat.get_value('Age') < 18 && pat.get_value('Parent/Guardian Full Name').blank?
  end
  
  def sample_preinit(subject)
    s = subject.serial_number.to_s(18).upcase
    s.prepend('0'*(5 - s.length)) if s.length < 5
    subject.name = subject.barcode_tag = 'S'+s
  end

  def sample_accessioned(subject)
    #raise 'Only one sample per requisition is allowed' if subject.get_value('Requisition').get_value('Samples').size > 1
  end
  
  def sample_testing_enabled?(subj: )
    !subj.get_value('DNA Extraction')
  end
  
end