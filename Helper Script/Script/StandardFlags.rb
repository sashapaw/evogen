module StandardFlags

  extend self
  
  def flags
    fv = ScriptRunner.cache_store['StandardFlags'] # will keep them in local thread storage
    unless fv
      ctx = ScriptRunner.context
      fv = ctx.find_subjects(query:ctx.search_query(from:'Flag') do |qb| 
        qb.compare('Analytes', :eq, nil)
      end)
=begin
      u = ctx.find_udf('Triggered When Value')
      uv = u.dictionary.values.map(&:value).reject{|v| v == 'Other'}
      fv = uv.map do |v,i|
        f = ctx.find_subjects(query:ctx.search_query(subject_type:'Flag') do |qb| 
          qb.and(qb.compare('Triggered When Value', :eq, v),
                 qb.compare('Analyte', :eq, nil)
          )
        end).first  ## TODO handle the error if there are more the 1 flags
      end.compact
=end
      #Rails.logger.info(" >>> StandardFlags::flags\n#{@__std_flags__.inspect}")
      ScriptRunner.cache_store['StandardFlags'] = fv if fv.present?
    end
    fv
  end
  
end