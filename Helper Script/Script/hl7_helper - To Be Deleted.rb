require 'ruby-hl7'
require 'indirizzo'
require 'phonelib'

def set_hl7_property(obj, prop_name, prop_value)
    obj.send("#{prop_name}=",prop_value)
    rescue NoMethodError # you can also add this
      puts "#{prop_name} is undefined."
end

def add_to_hl7_message(segment,values)
	msg = HL7::Message.new
	values.each do |key, val|
		set_hl7_property(segment, key, val)
	end
	msg << segment
	msg.to_hl7
end

def createSegmentString(array)
  return array.join("^")
end

def getDiagnoses(req)
  code = []
  req.get_value('Diagnoses').each do |diagnosis|
    code.push(diagnosis.get_value('Code'))
  end
  return code.join(", ")
end

def get_address(address_text)
  return nil if address_text.blank?
  Indirizzo::Address.new(address_text)
end

def get_address_xad_field(address)
  address = get_address(address) unless address.is_a?(Indirizzo::Address)
  return '' if address.blank?
  createSegmentString([address.line1,'',address.city.join(' '),address.state,address.zip])
end

def get_phone(phone)
  #phone.area_code, phone.local_number
  phone = Phonelib.parse(phone,'us')
  if phone.nil?
    phone = Phonelib::Phone.new("")
  end
  return phone
end

def generateMessage(msg)
	message = ""
	segment = ""
	msg.each do |key, val|
		case key.downcase
		when "msh" #Message Header - DFT(1), SIU(1), ADT(1)
			segment = HL7::Message::Segment::MSH.new
		when "sch" #Schedule Activity Information - SIU(2) *
			segment = HL7::Message::Segment::SCH.new
		when "pid" #Patient Demographic - DFT(2), SIU(3), ADT(2)
			segment = HL7::Message::Segment::PID.new
		when "pd1" #Patient Additional Demographics - ADT(3) *
			segment = HL7::Message::Segment::PD1.new
		when "nk1" #Next of Kin - ADT(4)
			segment = HL7::Message::Segment::NK1.new
		when "rgs" #Resource Group - SIU(4) *
			segment = HL7::Message::Segment::RGS.new
		when "ail" #Location Resource - SIU(5) *
			segment = HL7::Message::Segment::AIL.new
		when "aip" #Personnel Resource - SIU(6) *
			segment = HL7::Message::Segment::AIP.new
		when "pv1" #Patient Visit - DFT(3), ADT(5) *
			segment = HL7::Message::Segment::PV1.new
		when "ft1" #Financial Transaction - DFT(4)
			segment = HL7::Message::Segment::FT1.new
		when "dg1" #Diagnosis - DFT(5)
			segment = HL7::Message::Segment::DG1.new
		when "gt1" #Guarantor - DFT(6), ADT(6)
			segment = HL7::Message::Segment::GT1.new
		when "in1" #Insurance - DFT(7), ADT(7)
			segment = HL7::Message::Segment::IN1.new
		when "acc" #Accident - DFT(8)
			segment = HL7::Message::Segment::ACC.new
		when "zdi" #Date of Current Illness - DFT(9)
			segment = HL7::Message::Segment::ZDI.new
		when "zdt" #Additional Date - DFT(10)
			segment = HL7::Message::Segment::ZDI.new
		when "orc" #EMR
			segment = HL7::Message::Segment::ORC.new
    	when "obr" #EMR
			segment = HL7::Message::Segment::OBR.new
    	when "obx" #EMR
			segment = HL7::Message::Segment::OBX.new
        when "nte" #EMR
			segment = HL7::Message::Segment::NTE.new
		end

		val.each do | values |
			message.concat(add_to_hl7_message(segment,values)+"\n")
		end
		#Clear value
		segment = ""
	end
	return message
end

def findInArrayByMapProp(array, id, val)
  index = -1
  count = -1
  array.each do | item |
  	count = count + 1
  	if item.has_key?(id.to_sym)
  		if item[id.to_sym] == val
  			index = count
  		end
  	end
  end
  index
end

def generate_confirmation_report(req)
  main = "The following information was transmitted to billing:<br/> <strong>Requisition:</strong> #{req.name} </br><strong> Patient Information</strong><br/>&nbsp;Name: #{req.get_value('Patient').get_value('Last Name')}, #{req.get_value('Patient').get_value('First Name')}<br/>&nbsp;DOB: #{req.get_value('Patient').get_value('DOB')}<br/> &nbsp;Physician: #{req.get_value('Requesting Physician').get_value('First Name')} #{req.get_value('Requesting Physician').get_value('Last Name')} <br/> &nbsp;Physician NPI: #{req.get_value('Requesting Physician').get_value('NPI Number')} <br/>"

  tests = "<strong>Test Information</strong><br/>"
  diagnosis = getDiagnoses(req)
  tests = tests + "&nbsp;Diagnosis(ICD-10): #{diagnosis} <br/>"
  req.get_value('Samples').each do |sample|
    if !sample.current_states.map(&:name).include?('Cancelled') && !sample.current_states.map(&:name).include?('Accessioned')
      test_array = []
      cpt_array = []
      sample.get_value('Test Codes').each do |result|
        unless test_array.include?(result.name.to_s)
          test_array.push(result.name.to_s)
        end
        unless cpt_array.include?(result.get_value("CPT Codes"))
          cpt_array.push(result.get_value("CPT Codes"))
        end
      end
      test_string = test_array.join(';')
      cpt_string = cpt_array.join(';')
      tests = tests + "&nbsp;Sample: #{sample.name} <br/>&nbsp;&nbsp;Tests: #{test_string} <br/> &nbsp;&nbsp;CPT Codes: #{cpt_string}<br/> State: #{sample.current_states.map(&:name).to_s}<br/>"
    end
  end

  insurance = ""
  for i in 1..3
    if req.get_value('Insurance Company ' + i.to_s)
      insurance_company = req.get_value('Insurance Company ' + i.to_s)
      insurance = insurance + "<strong>Insurance Information #{i}</strong><br/>"
      insurance = insurance + "&nbsp;Company: #{insurance_company} <br/>&nbsp;Address: #{insurance_company.get_value('Address')} <br/>&nbsp;Phone: #{insurance_company.get_value('Phone Number')} <br/>
&nbsp;Group Number: #{req.get_value('Group #' + i.to_s)} <br/>&nbsp;Policy Number: #{req.get_value('Policy #' + i.to_s)}<br/>"
    end
  end

  billling_facility = ""

  if req.get_value('Billing Facility')
    billling_facility = "<strong>Billing Facility</strong><br/>"
    billling_facility = billling_facility + "&nbsp;Facility: #{req.get_value('Billing Facility')} <br/> &nbsp;Address: #{req.get_value('Billing Facility Address')}"
  end

  template = main + tests + insurance + billling_facility
  return template
end