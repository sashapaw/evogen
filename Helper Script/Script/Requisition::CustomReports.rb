module Requisition
  
  # Evogen Requisition Custom Reports
  module CustomReports
    include Requisition::DefaultReports
    extend self
    
    def generate_requisition_report(subj)
      system_settings = LIS.system_settings
      poct_results = subj.get_value('Point of Care Test Results')
      conf_results = subj.get_value('Confirmation Test Results')
      physician_signature = subj.get_value('Requesting Physician').get_value('Physician Signature')
      patient_signature = subj.get_value('Patient Signature')

      context.run_custom_report('Requisition Report - Evogen', subj, 'Requisition Report') do |extra_data|     
        extra_data['Lab Logo'] = LIS.file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path))
        extra_data['system_settings'] = system_settings
        extra_data['poc_test_results'] = poct_results[0].get_value('Point of Care Test Result Entries') if poct_results.present?
        extra_data['conf_test_results'] = conf_results if conf_results.present?
        extra_data['Now'] = Time.now
        extra_data['Physician Signature'] = LIS.file_to_base64(File.absolute_path(physician_signature.path)) if physician_signature.present?
        extra_data['Patient Signature'] = LIS.file_to_base64(File.absolute_path(patient_signature.path)) if patient_signature.present?
      end
    end
    
    def customize_test_results_report_data(req, extra_data)
      sample = ScriptRunner.context.find_subjects(query:"FROM Sample WHERE Requisition=#{req.id} AND \"Test Results\"!=null AND terminated=false", limit:1).first
      if sample
        if sample["Parent Sample"] != nil
		  extra_data['sample'] = sample["Parent Sample"]
		else
		  extra_data['sample'] = sample
		end
        copy_numbers = []
        methylations = []
        pcr_result = nil
        pcr_comment = nil
        meth_result = nil
        meth_comment = nil
        outcome = '???'
        interpretation = nil
        max_copy = 0
        max_copy_allele = 0
        
        results = sample.get_value('Test Results')
        results.each{|tr|
          v = tr.get_value('Value')
          if v
            an = tr.get_value('Analyte').name
            m = /Repeat Number Allele (\d)/.match(an)
            if m
              allele = m[1].to_i
              iv = v.split('-').map(&:to_i).max
              copy_numbers[allele - 1] = v
              if iv > max_copy
                max_copy = iv
                max_copy_allele = allele
              end
            else
              m = /Methylation Allele (\d)/.match(an)
              if m
                allele = m[1].to_i
                v = v.to_i
                methylations[allele - 1] = v
              else
                case an
                  when 'PCR Interpretation'
                    pcr_result = v
                    pcr_comment = tr.get_value('Comments')
                  when 'Methylation Interpretation'
                    meth_result = v
                    meth_comment = tr.get_value('Comments')
                  when 'Test Outcome'
                    outcome = v == 'Custom' ? tr.get_value('Comments') : v
                  when 'Test Interpretation'
                    interpretation = case v
                      when 'Full mutation' # max_copy >= 200
                        #"These results indicate the presence of the fragile X chromosomal allele. In males, this confirms the diagnosis of fragile X syndrome. Females with full mutations are carriers for the fragile X allele, and may exhibit a range of phenotype from affected to mild or undetectable. The severity of the clinical phenotype cannot be determined by these studies. Fragile X DNA testing is recommended for all at-risk family members. Genetic counseling is recommended."
						"These results indicate the presence of the fragile X mutation in the <i>FMR1</i> chromosomal allele. In males, this confirms the diagnosis of fragile X syndrome. Females with full mutations of the <i>FMR1</i> allele are carriers and may exhibit a range of phenotype from affected to mild or undetectable. The severity of the clinical phenotype cannot be determined by these studies. Fragile X DNA testing is available for all at-risk family members. Genetic counseling is recommended."
                      when 'Premutation' # max_copy >= 55
                        #"These results indicate that this patient is a carrier of a premutation <i>FMR1</i> allele. Males with a fragile X premutation are at increased risk of developing the fragile X-associated tremor/ataxia syndrome (FXTAS). Females who carry a fragile X premutation are also at risk for FXTAS or fragile X-associated premature ovarian insufficiency (FXPOI). Fragile X DNA testing is recommended for other at-risk family members. Prenatal fragile X testing is recommended for future pregnancies, since expansion to a full mutation can occur. Genetic counseling is recommended."
						"These results indicate that this patient is a carrier of a premutation <i>FMR1</i> allele. Males with a premutation allele are at increased risk of developing the fragile X-associated tremor/ataxia syndrome (FXTAS). Females who carry a premutation allele are also at risk for FXTAS or fragile X-associated premature ovarian insufficiency (FXPOI). Fragile X DNA testing is available for other at-risk family members. Prenatal fragile X testing is available for future pregnancies, since expansion to a full mutation can occur. Genetic counseling is recommended."
                      when 'Intermediate' # max_copy >= 45
                        #"These results indicate normal or unaffected <i>FMR1</i> allele(s). This patient is not a carrier of a premutation or full mutation of the fragile X allele. If the family history is suggestive of fragile X syndrome, DNA studies and clinical evaluation of potential affected family members should be considered. Although CGG repeats of 45-54 are considered normal, the stability of this number of repeats over multiple generations has not been clearly established. Studies of other family members could help determine the stability of the <i>FMR1</i> allele within this family."
						"These results indicate normal or unaffected <i>FMR1</i> allele(s). This patient is not a carrier of a premutation or full mutation <i>FMR1</i> allele. If the family history is suggestive of fragile X syndrome, DNA studies and clinical evaluation of potential affected family members should be considered. Although CGG repeats of 45-54 are considered normal, the stability of this number of repeats over multiple generations has not been clearly established. Studies of other family members could help determine the stability of the <i>FMR1</i> allele within this family."
                      when 'Normal'
                        #"These results indicate normal or unaffected <i>FMR1</i> allele(s). This patient is not a carrier of a premutation or full mutation of the fragile X allele. These results do not indicate evidence of the common trinucleotide repeat expansion observed in almost all patients with fragile X syndrome, indicating that a diagnosis or carrier status of fragile X syndrome is highly unlikely in this patient."
						"These results indicate normal or unaffected <i>FMR1</i> allele(s). This patient is not a carrier of a premutation or full mutation of the <i>FMR1</i> allele. These results do not indicate evidence of the common trinucleotide repeat expansion observed in almost all patients with fragile X syndrome, indicating that a diagnosis or carrier status of fragile X syndrome is highly unlikely in this patient."
                      when 'Custom'
                        tr.get_value('Comments')
                    end
                end
              end
            end
          end
        }
        
        extra_data['copy_numbers'] = copy_numbers
        extra_data['methylations'] = methylations if methylations.present?
        extra_data['outcome'] = outcome
        
        meth_statements = methylations.map{|v| v <= 20 ? 'Unmethylated' : v < 80 ? 'Partially methylated' : 'Fully Methylated'}
        extra_data['meth_statements'] = meth_statements if meth_statements.present?
        patient = req.get_value('Patient')
        patient_name = patient.get_value('First Name')+' '+patient.get_value('Last Name')
        extra_data['pcr'] = case pcr_result
          when 'Heterozygous'
            #"PCR analysis performed on DNA extracted from #{patient_name} indicates that the proband possesses #{copy_numbers[0]} CGG repeats in one <i>FMR1</i> allele and #{copy_numbers[1]} CGG repeats on the other allele."
			"PCR analysis identified #{copy_numbers[0]} CGG repeats on one <i>FMR1</i> allele and #{copy_numbers[1]} CGG repeats on the other."
          when 'Homozygous'
            #"PCR analysis performed on DNA extracted from #{patient_name} indicates that the proband possesses #{copy_numbers[0]} CGG repeats in both <i>FMR1</i> alleles."
			"PCR analysis identified #{copy_numbers[0]} CGG repeats on both <i>FMR1</i> alleles."
          when 'Hemizygous'
            #"PCR analysis performed on DNA extracted from #{patient_name} indicates that the proband possesses #{copy_numbers[0]} CGG repeats in the <i>FMR1</i> allele."
			"PCR analysis identified #{copy_numbers[0]} CGG repeats on the <i>FMR1</i> allele."
          when 'Mosaic'
            #"PCR analysis performed on DNA extracted from #{patient_name} indicates that the proband possesses mosaicism with #{copy_numbers.join(', ')} across alleles and cell lines."
			"PCR analysis identified repeat number mosaicism with #{copy_numbers.join(', ')} across alleles and cell populations."
          else
            pcr_comment
        end
        if methylations.present?
          extra_data['meth'] = case meth_result
            when 'Homozygous/Hemizygous'
              #"Methylation studies were consistent with the presence of a #{phrase(methylations[0], copy_numbers[0])}."
			  "Methylation studies were consistent with the presence of a #{phrase(methylations[0], copy_numbers[0])} allele(s)."
            when 'Heterozygous'
              #"Methylation studies were consistent with the presence of a #{phrase(methylations[0], copy_numbers[0])} and #{phrase(methylations[1], copy_numbers[1])}."
			  "Methylation studies were consistent with the presence of #{phrase(methylations[0], copy_numbers[0])} and #{phrase(methylations[1], copy_numbers[1])} alleles."
            when 'Mosaic'
              #"Methylation studies were consistent with the presence of #{copy_numbers.map.with_index{|cn,i| phrase(methylations[i], cn)}.join(', ')}."
			  "Methylation studies were consistent with the presence of #{copy_numbers.map.with_index{|cn,i| phrase(methylations[i], cn)}.join(', ')} alleles."
            else
              meth_comment
          end
        end
        extra_data['interpretation'] = interpretation || '???'
      end
    end

    def phrase(meth, copy)
      if copy.to_s == ">200" || copy.to_s == "> 200"
	    cnum = 201
	  else
	    cnum = copy.to_i
	  end #end if
	  #copy.to_i
      m = meth <= 20 ? 'unmethylated' : meth < 80 ? 'partially methylated' : 'fully methylated'
      c = cnum < 45 ? 'normal' : cnum < 55 ? 'intermediate' : cnum < 200 ? 'premutation' : 'full mutation'
      "#{m} #{c}"
    end #end phrase
  
    def samples_for_test_results_report(req)
      nil
    end
    
  end
end