module LISHL7
  
  module EHR
    extend self
    
    def send_results(req, correction)
      flag = correction ? 'C' : 'F'
      emr_generator = Generator.new(context)

      facility = req.get_value('Facility')
      configuration = facility.get_value('EHR Server Configuration')
      return nil if configuration.blank?
      
      account_number = facility.get_value('EHR Account Number')
      raise "Both \"EHR Server Configuration\" and \"EHR Account Number\" fields must be set for a facility #{facility.name}" if account_number.blank?

      msg_type = configuration.get_value('HL7 Message Type')
      raise "Selected EHR Server Configuration #{configuration} does not support ORU messages" unless msg_type.include?('ORU')

      if !configuration.get_value('Only Send Failed Results') || req.get_value('Result') == 'Fail'
        ehr_record = emr_generator.create_record(configuration,req,flag)
        send_ehr_file(ehr_record, nil, configuration)
      end

    end
    
    def send_ehr_file(ehr_record, f = nil, configuration = nil)
      configuration = ehr_record.get_value('EHr Provider') if configuration.nil?

      if f.nil?
        file_proxy = ehr_record.get_value('File')
        raise "No HL7 File is attached to EHR record #{ehr_record.name}" if file_proxy.nil?
        f = file_proxy.file
      end

      enable_config = configuration.get_value('Enabled')
      if enable_config
        remote_file_path = configuration.get_value('Remote File Path')
        #If not use the Global Path, then use path attached to facility
        if !configuration.get_value("Use Global File Path")
          req = ehr_record.get_value('Requisition')
          facility_file_path = req.get_value('Facility').get_value('Remote File Path')
          remote_file_path = facility_file_path if facility_file_path.present?
        end

        response = Transmission::upload_hl7_file(configuration, f, remote_file_path)
        ehr_record.set_value('Comments', response)
      else
        ehr_record.set_value('Comments', "#{configuration} #{configuration.get_value('HL7 Server Type')} is currently disabled.")
      end
    end
    
  end
  
end