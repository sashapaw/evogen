module Instrument
  class Diatron_Pictus700 < ASTM

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @result_status_map = get_result_status_map
      @analyte_map = get_analyte_map
      @specimen_type_map = get_specimen_type_map
    end

    def get_analyte_map
      {
        'CHOL' => 'Cholesterol',
        'SPG' => 'Specific Gravity',
        '736-9' => 'Lymphocytes',
        '742-7' => 'Absolute Monocytes',
        '744-3' => 'Monocytes',
        '751-8' => 'Absolute Neutrophils',
        '770-8' => 'Neutrophils',
        '711-2' => 'Absolute Eosinophils',
        '713-8' => 'Eosinophils',
        '704-7' => 'Absolute Basophils',
        '706-2' => 'Basophils',
        '733-6' => 'Absolute Atypical Lymphocytes',
        '735-1' => 'Atypical Lymphocytes',
        'X-LIC' => 'Absolute Immature Plasma Cells',
        '11117-9' => 'Immature Plasma Cells',
        '789-9' => 'Red Blood Cell Count',
        '717-9' => 'Hemoglobin',
        '4544-3' => 'Hematocrit',
        '787-2' => 'Mean Corpuscular Volume',
        '785-6' => 'Mean Corpuscular HGB',
        '786-4' => 'Mean Corpuscular HGB Conc.',
        '788-0' => 'Red Cell Dist. Width',
        '777-3' => 'Platelet Count',
        '776-5' => 'Mean Platelet Volume',
        'X-PCT' => 'Plateletcrit',
        'X-PDW' => 'Platelet Distribution Width'
        }
    end

    def get_result_status_map
      {
        'F' => 'Final result'
        }
    end

    def get_specimen_type_map
      {
        'Cerebrospinal Fluid' => 'CSF',
        'Plasma' => 'Plasma',
        'Serum' => 'Serum',
        'Dialysis' => 'Dialysis',
        'Urine (Random)' => 'Urine',
        'Urine' => 'Urine'
        }
    end

    def get_universal_test_id(test_results)
      get_test_ids(test_results).map {|aname| "^^^#{aname}"}.join("\\")
    end

    def get_status(field)
      res_stat = field['value']
      @result_status_map.fetch(res_stat) rescue res_stat
    end

    def get_patient(requisition)
      pdata = get_patient_data(requisition)
      if pdata
        {
          'type': 'P',
          'fields': [
            {'idx': 2, 'value': 1, 'name': :sequence_number},
            {'idx': 3, 'value': pdata[:id], 'name': :practice_assigned_patient_id},
            {'idx': 4, 'value': '', 'name': :laboratory_assigned_patient_id},
            {'idx': 5, 'value': '', 'name': :patient_id},
            {'idx': 6, 'value': "#{pdata[:last_name]}^#{pdata[:first_name]}", 'name': :name},
            {'idx': 7, 'value': ''},
            {'idx': 8, 'value': "#{pdata[:birth_date]}", 'name': :birthdate},
            {'idx': 9, 'value': pdata[:sex], 'name': :sex},
            {'idx': 10, 'value': ''},
            {'idx': 11, 'value': ''},
            {'idx': 12, 'value': ''},
            {'idx': 13, 'value': ''},
            {'idx': 14, 'value': "#{pdata[:physician_last_name]}^#{pdata[:physician_first_name]}", 'name': :attending_physician}
            ]
          }
      else
        nil
      end
    end

    def get_orders(sample, test_results)
      specimen_type = get_specimen_type(sample)
      test_ids = get_universal_test_id(test_results)
      requested_at = get_time_requested_at

      [{
        'order': {
          'type': 'O',
          'fields': [
            {'idx': 2, 'value': 1, 'name': :sequence_number},
            {'idx': 3, 'value': "#{sample.barcode_tag}", 'name': :specimen_id},
            {'idx': 4, 'value': ''},
            {'idx': 5, 'value': test_ids, 'name': :universal_test_id_internal},
            {'idx': 6, 'value': '', 'name': :priority},
            {'idx': 7, 'value': ''},
            {'idx': 8, 'value': sample.get_value('Collection Date').strftime('%Y%m%d%H%M%S'), 'name': :collected_at},
            {'idx': 9, 'value': ''},
            {'idx': 10, 'value': ''},
            {'idx': 11, 'value': ''},
            {'idx': 12, 'value': ''},
            {'idx': 13, 'value': ''},
            {'idx': 14, 'value': ''},
            {'idx': 15, 'value': ''},
            {'idx': 16, 'value': specimen_type}
            ]
          }}]
    end

  end
end