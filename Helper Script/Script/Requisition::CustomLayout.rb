module Requisition
  
  module CustomLayout
    include Requisition::DefaultLayout
    extend self

    def general_tab(subj, facility, patient_subject, system_settings)
      defaultSpecimenTypes = system_settings.get_value('Default Specimen Types') || []
  
      field_set(title:'General Information', border:'1 0 0 0', defaults:{anchor:'100%'}, items:[      
        udf('Facility', subj, filter:'terminated is false and ("Facility Type" is null OR "Facility Type" != \'Reference Lab\') and RURO_CLIENT is null', labelWidth:160, allowCreate:true, required:true, disabled:patient_subject),
        udf('Requesting Physician', subj, addCustomFields: physician_layout(subj, 2), labelWidth:160, required:true,
          filter:"Facility = #{facility ? facility.id : 0} and terminated is false",
          react: {
            shown_when: 'value',
            change: "this.setFilter('terminated is false and Facility='+(value || 0));",
            only:'Facility'
        }),
        #udf('Collection Date', subj, labelWidth:160, anchor:'50%', required:true),
        # the only difference from default method is value: Time.zone.now
        udf('Received Date', subj, fieldLabel:'Requisition Received Date', labelWidth:160, anchor:'50%', required:true, hidden:patient_subject, value: Time.zone.now),
        #udf('Specimen Types', subj, defaultValue: defaultSpecimenTypes, required:true, labelWidth:160),
        udf('Paper Requisition File', subj, labelWidth:160, anchor:'100%'),
        udf('External Source ID', subj, info:'This field can be used for the Sample Barcode Entry', labelWidth:160),
        udf('Reference Lab', subj, labelWidth:160, allowCreate:false, hidden:!system_settings.get_value('Use Reference Lab'), filter:'terminated is false and "Facility Type" = \'Reference Lab\''),
        udf('Comments', subj, labelWidth:160),
        udf('Additional Report Recipients', subj, filter:"terminated is false", labelWidth:160,
          allowCreate:true, addCustomFields:encode_fields(set_level(2, udfs_for_subject_type('Contact', nil))))
      ])
    end

    def patient_tab(subj, facility, patient_subject, system_settings)
      patient = subj && subj.get_value('Patient')
      test_codes = subj.get_value('Tests') if subj
      test_codes = patient.get_value('Tests') if test_codes.blank? && patient
      if subj
        test_codes = subj.get_value('Tests')
        diagnoses = subj.get_value('Diagnoses')
        medications = subj.get_value('Medications')
      end
      facility = subj ? subj.get_value('Facility') : User.curr_user.get_value('Facility')
      facility = nil if facility.get_value('RURO_CLIENT') # do not use Lab as facility
      facility_filter = facility.present? ? " and (Facility = #{facility.id} or Facility is null)" : ''

      patient_subject = !subj.nil? && subj.subject_type.name=="Patient"
      if patient
        if subj.get_value('Tests').blank?
          test_codes = patient.get_value('Tests')
        end
        if subj.get_value('Diagnoses').blank?
          diagnoses = patient.get_value('Diagnoses')
        end
        if subj.get_value('Medications').blank?
          medications = patient.get_value('Medications')
        end
      end
      field_container([
        field_set(title:'Patient Information', border:'1 0 0 0', defaults:{anchor:'100%'}, items: [
          udf('Patient', subj, itemId:'patient', defaultValue:subj, labelWidth:160, required:true, addCustomFields: patient_layout(patient, 2),
            filter:"Facility = #{facility ? facility.id : 0} and terminated is false", disabled:patient_subject,
            react: patient_subject ? nil : {
              shown_when: 'value',
              change: "this.setFilter('terminated is false and Facility='+(value || 0));",
              only: 'Facility'
          }),
          field_container([
            udf('Address', patient, disabled:true, labelWidth:160, height:40),
            field_row([
              udf('DOB', patient, fieldLabel: 'Date of Birth', disabled:true, labelWidth:160, flex:1.75),
              udf('Phone Number', patient, fieldLabel: 'Phone', disabled:true, labelWidth:110)
            ]),
            field_container(Patient::CustomLayout.patient_custom_fields(patient), disabled:true),
            udf('Tests', subj, defaultValue: test_codes, labelWidth:160, required:true,
              filter:'terminated is false and "Non-Orderable" = null'),
            udf('Medications', subj, defaultValue: medications, labelWidth:160),
            field_row([
              udf('Consent Signed', subj, labelWidth:160),
              prepare_react(xtype:'fieldcontainer', layout:'fit', flex:2.5, items:[
                udf('Consent Form', subj, labelWidth:90)
              ], react: {
                shown_when:'value', only:'Consent Signed'
              })
            ]),
            udf('Consent for future use', subj, labelWidth:160),
            udf('Prior Genetic Testing', subj, labelWidth:160),
            udf('If yes, was it done at Evogen?', subj, labelWidth:160, react: {
              shown_when:'value', only:'Prior Genetic Testing'
            }),
            udf('What test?', subj, labelWidth:160, react: {
              shown_when:'value', only:'Prior Genetic Testing'
            })
          ], defaults:{anchor:'100%'}, disabled:!patient, react:{
            change: 'this.setDisabled(!value)',
            only: 'Patient'
          })
        ]),
        field_set(title:'Reason for Testing and Clinical Indications', border:'1 0 0 0', defaults:{anchor:'100%'}, items:[      
          udf('Reason for Testing', subj, labelWidth:160, allowCreate:true, required:true, addCustomFields:encode_fields(set_level(2, udfs_for_subject_type('Reason for Testing', nil)))),
          udf('Clinical Indications for Testing', subj, labelWidth:160, required:true,  allowCreate:true, addCustomFields:encode_fields(set_level(2, udfs_for_subject_type('Clinical Indications for Testing', nil)))),
          udf('Clinical Indications Comment', subj, labelWidth:160),
          udf('Diagnoses', subj, fieldLabel:'ICD-10 Code(s)', defaultValue: diagnoses, filter:'#state=Active', labelWidth:160, required:true),
          udf('Clinical Diagnosis', subj, required:true, labelWidth:160),
          udf('Copy of Medical Records', subj, labelWidth:160),
          udf('Additional Clinical Information', subj, labelWidth:160),
          udf('Parent/carrier testing unaffected and/or no symptoms', subj, labelWidth:160, fieldLabel:'', boxLabel:'Parent/carrier testing unaffected and/or no symptoms')
        ]),
        field_set(title:'Variant-specific Testing/Family Test', border:'1 0 0 0', defaults:{anchor:'100%'}, items:[
          field_row([
            udf('Variant-specific testing', subj, labelWidth:160, fieldLabel:'', boxLabel:'Variant-specific testing'),
            udf('WES-Family Test', subj, padding:'0 10 0 0', labelWidth:160, fieldLabel:'', boxLabel:'WES-Family Test')
          ]),
          field_container([
            udf('Name of Index Case', subj, labelWidth:160, anchor:'50%'),
            udf('Index Case Patient ID', subj, labelWidth:160, anchor:'100%', info:'The index case patient ID is generated for each index case. This ID is located in the index case’s report.'),
            udf('Relationship to Index Case', subj, anchor:'100%', labelWidth:160),
            udf('Variant(s)', subj, labelWidth:160, anchor:'100%', info:'Please specify variant(s) to be tested, including gene name. If blank, all recommended variants in index report will be tested for a fee per variant.'),
            {xtype:'box', html:'If previous testing was performed at a different lab:'},
            udf('Positive Control Sample Included', subj, padding:'0 0 0 20', labelWidth:160, fieldLabel:'', boxLabel:'Positive Control Sample Included', afterBoxLabelTextTpl:'<br><span style="color:#999;">A sample from a previously tested individual known to carry the familial variant is recommended. Please contact the lab if you would like a separate report issued for the positive control sample.</span>'),
            field_container([
              udf('Positive Family Member Test Report', subj, labelWidth:220)
            ], padding:'0 0 0 20'),
          ], react:{
            shown_when:'data["Variant-specific testing"] || data["WES-Family Test"]',
            only:['Variant-specific testing', 'WES-Family Test']
          })
        ]),
        field_set(title:'Shipping Information', border:'1 0 0 0', items:[
          field_row([
            udf('Carrier', subj, required:true, labelWidth:160, columnWidth:0.4),
            udf('Tracking Number', subj, required:true, labelWidth:160, columnWidth:0.6)
          ], layout:'column'),
          udf('Shipping Label', subj, labelWidth:160, anchor:'100%')
        ]),
        field_set(title:'Billing Information', border:'1 0 0 0', defaults:{anchor:'100%'}, items:[      
          billing_layout(subj, true, false)
        ], react:{
          init: 'this.setDisabled(!value)',
          change: '
            var me = this;
            me.setDisabled(!value);
            Ext.each(["Tests", "Diagnoses", "Medications", "Bill To", "Address", "Phone Number", "DOB"], function(udf) {
              form.setValue(udf, null);
            });
            if (Ext.isNumber(value)) {
              form.setValuesAsync({id: value, suspendListening: "Billing Facility", udfs: [
                "Address", "Phone Number", "EMail", "Evening Phone",
                "Tests", "Diagnoses", "Medications", "Bill To",
                "Insurance Company 1", "Policy #1", "Group #1",
                "Insurance Company 2", "Policy #2", "Group #2",
                "Insurance Company 3", "Policy #3", "Group #3",
                "Primary Physician NPI", "Billing Facility", "Billing Facility Address", "Relation To Insured",
                "Subscriber Name", "Subscriber DOB", "Subscriber Sex", "Subscriber Address", "DOB",
                "Patient Status", "Ethnicities", "Parent/Guardian Full Name", "Date of Death"]});
            }',
          only: 'Patient'
        })
      ], react: {
        shown_when: 'value',
        only: 'Facility'
      })
    end

  end
  
end