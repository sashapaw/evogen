jargs = JSON.parse(params[:data])
user_id = jargs['id']

user = User.find(user_id)
user_role = "Lab User"

cra = UserGroup.find_by_name('Clients Report Access')
view_test_results = cra.users.include?(user) 
lm = UserGroup.find_by_name('Lab Managers')
la = UserGroup.find_by_name('Lab Admins')

if lm.users.include?(user) 
  user_role = 'Lab Manager'
elsif la.users.include?(user)
  user_role = 'Lab Admin'
end

if user  
  {
    fullname:user.fullname,
    email:user.email,
    username:user.username,
    viewTestResult: view_test_results,
    user_role:user_role
  }
else 
  {result:"Not FOUND"}
end