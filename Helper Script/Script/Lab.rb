module Lab
  extend self
  
  def dashboard(range)
    where = case range
    when 'today'
      "created_at = '#{Date.today}' AND terminated is false"
    when 'week'
      "created_at >= '#{Date.today - 6.days}' AND terminated is false"
    when 'month'
      "created_at >= '#{Date.today - 30.days}' AND terminated is false"
    when 'all'
      "terminated is false"
    else
      raise "Unsupported time range: #{range.inspect}"
    end
    {requisitions: requisitions_html(where), samples: samples_dashboard(where), specimen_types: samples_by_specimen_types(where),
      facilities: top_facilities(where), turnaround: turnaround(range)}
  end
  
  def subjects_link(subj_type_name, query, tab_name, count=nil)
    opts = count ? {count: count} : {}
    ScriptRunner::UI::search_tab_link(subj_type_name, query, "style=\"color:white; text-decoration:none;\" onmouseover=\"this.style.color='black'\"  onmouseout=\"this.style.color='white'\"", tab_name, opts)
  end
  
  def requisitions_html(where)
   req_open_q = "FROM \"Requisition\" WHERE (#state = Submitted OR #state = Received) AND #{where}"
   req_hold_q = "FROM \"Requisition\" WHERE #state = 'On Hold' AND #{where}"
   req_released_q = "FROM \"Requisition\" WHERE #state = (Released, Accepted) AND #{where}"
    <<EOS
      <div style="display:inline-block;white-space:nowrap;margin-left:5px;">
         <div class="dash-client-status-title" style="margin-top:6px">REQUISITIONS</div>
         <div class="dash-client-status-good">
           <span>Open</span>
           <div>#{subjects_link('Requisition', req_open_q, 'Open Requisitions')}</div>
         </div><div class="dash-client-status-warn">
           <span>On Hold</span>
           <div>#{subjects_link('Requisition', req_hold_q, 'Requisitions On Hold')}</div>
         </div><div class="dash-client-status-bad">
           <span>Released</span>
           <div>#{subjects_link('Requisition', req_released_q, 'Released Requisitions')}</div>
         </div>
      </div>
EOS
  end

  def samples_dashboard(where)
    ctx = ScriptRunner.context
    sample_all = ctx.count_subjects(query:"FROM \"Sample\" WHERE #{where}")

    sample_accessioned_q = "FROM \"Sample\" WHERE #state = 'Accessioned' AND #{where}"
    sample_accessioned = ctx.count_subjects(query:sample_accessioned_q)
    sample_testing_q = "FROM \"Sample\" WHERE #state = 'In Testing' AND #{where}"
    sample_testing = ctx.count_subjects(query:sample_testing_q)
    sample_complete_q = "FROM \"Sample\" WHERE #state != 'In Testing' AND #state != 'Accessioned' AND #state != 'Cancelled' AND #{where}"
    sample_complete = ctx.count_subjects(query:sample_complete_q)
    sample_hold_q = "FROM \"Sample\" WHERE #state = 'On Hold' AND #{where}"
    sample_hold = ctx.count_subjects(query:sample_hold_q)

    s_a_percent = sample_all>0 ? (sample_accessioned * 100 / sample_all).to_i : 0
    s_t_percent = sample_all>0 ? (sample_testing * 100 / sample_all).to_i : 0
    s_c_percent = sample_all>0 ? (sample_complete * 100 / sample_all).to_i : 0
    s_h_percent = sample_all>0 ? (sample_hold * 100 / sample_all).to_i : 0
    
    html = <<EOS
       <div style="display:inline-block;white-space:nowrap;margin-left:5px;">
         <div class="dash-client-status-title">SAMPLES</div>
         <div class="dash-client-status-good">
           <span>Accessioned</span>
           <div>#{subjects_link('Sample', sample_accessioned_q, 'Accessioned Samples', sample_accessioned)}</div>
         </div><div class="dash-client-status-warn">
           <span>In Testing</span>
           <div>#{subjects_link('Sample', sample_testing_q, 'Samples In Testing', sample_testing)}</div>
         </div><div class="dash-client-status-bad">
           <span>Completed</span>
           <div>#{subjects_link('Sample', sample_complete_q, 'Completed Samples', sample_complete)}</div>
         </div>
       </div>
EOS
    {html: html, all: sample_all, accessioned: sample_accessioned, testing: sample_testing, complete: sample_complete, hold: sample_hold}
  end
  
  def samples_by_specimen_types(where, limit=3)
    ctx = ScriptRunner.context
    group_query = "SELECT COUNT(*) FROM Sample WHERE #{where}"
    result = ctx.execute_query(group_query, group: '"Specimen Type"')
    result.delete(nil)
    a = result.to_a.sort{|r1, r2| -(r1[1] <=> r2[1])}
    a = a[0...limit] if a.size > limit
    
    body = if a.blank?
      default_types = LIS.system_settings.get_value('Default Specimen Types') || []
      default_types.take(3).map{|specimen_type|
        "<div data-qtip=\"#{UIUtils.h(specimen_type.name)}\" class=\"dash-client-status dash-client-status-good\"><span>#{UIUtils.h(specimen_type.name.truncate(15))}</span><div>0</div></div>"
      }.join
    else
      a.map{|r|
        specimen_type = ctx.find_subject(uid: r[0].to_i)
        sample_count = r[1]
        q = "#{where} AND \"Specimen Type\"=#{specimen_type.id}"
        link = subjects_link('Sample', q, specimen_type.name, sample_count)
        "<div data-qtip=\"#{UIUtils.h(specimen_type.name)}\" class=\"dash-client-status dash-client-status-good\"><span>#{UIUtils.h(specimen_type.name.truncate(15))}</span><div>#{link}</div></div>"
      }.join
    end
    <<EOS
    <div style="margin-left:5px;">
      <div class="dash-client-status-title">SPECIMEN TYPES</div>
      #{body}
    </div>
EOS
  end
  
  def top_facilities(where, limit=10)
    ctx = ScriptRunner.context
    group_query = "SELECT COUNT(*) FROM Requisition WHERE #{where}"
    result = ctx.execute_query(group_query, group: 'Facility')
    result.delete(nil)
    a = result.to_a.sort{|r1, r2| -(r1[1] <=> r2[1])}
    a.take(limit).map{|r|
      facility = context.find_subject(uid: r[0].to_i)
      req_count = r[1]
      facility_link = "<a href=\"javascript:void(0)\" onclick=\"RURO.viewSubject(#{facility.id})\">#{UIUtils.h(facility.name)}</a>"
      req_link = ScriptRunner::UI::search_tab_link('Requisition',
        "#{where} AND Facility = #{facility.id}", '', "Requisitions at #{facility.name}", count: req_count)
      sample_link = ScriptRunner::UI::search_tab_link('Sample',
        "#{where} AND Requisition->Facility = #{facility.id}", '', "Samples at #{facility.name}")
      [facility.id, facility_link, req_link, sample_link]
    }
  end
  
  def turnaround(range)
    st = SubjectType.find_by_name('Requisition')
    released_date_udf = Property.find_by_display_name('Released Date')
    received_date_udf = Property.find_by_display_name('Received Date')
    now = Time.now
    mn = Time.new(now.year, now.month, now.day)
    dates = case range
    when 'today'
      (0..now.hour).map{|h| [mn + h.hours, "#{h} h"]}
    when 'week'
      (-6..0).map{|d| t = mn + d.days; [t, t.strftime('%A')]}
    when 'month'
      this_week = mn - mn.wday.days
      (-4..0).map{|w|
        t1 = this_week + (w*7).days
        t2 = [this_week + (w*7+6).days, mn].min
        [t1, "#{t1.strftime('%m/%d')}...#{t2.strftime('%m/%d')}"]
      }
    when 'all'
      this_month = Time.new(now.year, now.month, 1)
      (-5..0).map{|m| t = this_month + m.months; [t, t.strftime('%B')]}
    else
      raise "Unsupported time range: #{range.inspect}"
    end
    dates.map.with_index{|a,i|
      t1 = a[0]
      t2 = i < dates.size - 1 ? dates[i+1][0] : now
      sql = <<EOS
        SELECT COUNT(s."id") AS count, EXTRACT(epoch FROM MIN(d1."value"-d2."value"))/3600 AS min, EXTRACT(epoch FROM AVG(d1."value"-d2."value"))/3600 AS avg, EXTRACT(epoch FROM MAX(d1."value"-d2."value"))/3600 AS max
          FROM "subjects" AS s
          INNER JOIN "obj_property_links" AS o1 on o1."obj_id" = s."id"
          INNER JOIN "date_values" AS d1 on o1."value_id" = d1."id"
          INNER JOIN "obj_property_links" AS o2 on o2."obj_id" = s."id"
          INNER JOIN "date_values" AS d2 on o2."value_id" = d2."id"
          WHERE s."subject_type_id" = #{st.id} AND s."terminated" = 'f'
            AND o1."obj_type" = 'Subject' AND o1."property_id" = #{released_date_udf.id}
            AND d1."value" BETWEEN '#{t1.utc.iso8601}' AND '#{t2.utc.iso8601}'
            AND o2."obj_type" = 'Subject' AND o2."property_id" = #{received_date_udf.id}
EOS
      results = ActiveRecord::Base.connection.select_all(sql)
      Rails.logger.warn("%%%%%%%%%%%%% #{results.inspect}")
      result = results.first
      count = result['count'].to_i
      if count > 0
        {
          count: count,
          min: result['min'] && [result['min'].to_f.round(1), 0].max,
          avg: result['avg'] && [result['avg'].to_f.round(1), 0].max,
          max: result['max'] && [result['max'].to_f.round(1), 0].max,
          label: a[1]
        }
      else
        nil
      end
    }.compact
  end
  
end