module Physician
  
  module Layout
    include ScriptRunner::UI
    
    def physician_layout(subj, level=nil)
      phys_subj = (subj and subj.subject_type.name=="Physician")
      bill_insur = subj ? subj.get_value('Bill To') == 'Insurance' : false
      rel = subj && subj.get_value('Relation To Insured') || 'Self'
      facility = (subj and subj.subject_type.name=="Facility") ? subj : phys_subj ? subj['Facility'] : nil
      can_facility = LIS.is_lab_user?(User.curr_user)

      fields = [
        #udf('Code', subj, xtype:phys_subj ? 'displayfield' : 'textfield', labelWidth:160, required:false),
        udf('Facility', subj, value:facility, labelWidth:160, allowCreate:false, required:can_facility, hidden:!can_facility, filter:"terminated is false and RURO_CLIENT is null"),
        field_row([
          udf('First Name', subj, labelWidth:160, required:true),
          udf('Middle Name', subj, labelWidth: 100, flex:0.75),
          udf('Last Name', subj, labelWidth:110, required:true)
        ]),
        udf('Address', subj, anchor:'100%', labelWidth:160),
        field_row([
          udf("Phone Number", subj, labelWidth:160),
          udf("Fax Number", subj, labelWidth:100, flex:0.75),
          udf("Contact Email", subj, labelWidth:110)
        ]),
        udf('PHYSICIAN IDS', subj, anchor:'100%'),
        udf("License", subj, anchor:'100%', labelWidth:160),
        udf("NPI Number", subj, minLength:10, maxLength:10, required:true, anchor:'100%', labelWidth:160),
        udf("UPIN Number", subj, anchor:'100%', labelWidth:160)
      ]
 
      system_settings = SubjectType.find_by_name('System Settings').subjects.first
      show_authorization_statement = system_settings.get_value('Use Provider Practitioner Authorization Statement')
      authorization_statement_text = system_settings.get_value('Provider Practitioner Authorization Statement')
  
      if show_authorization_statement.present?
        fields << {
          xtype: 'displayfield',      
          fieldLabel: 'Provider Practitioner Authorization Statement',
          udfName:'Provider Practitioner Authorization Statement',
          anchor:'100%', 
          labelWidth:160,
          value: authorization_statement_text
        }
      end

      if system_settings.get_value('Require Physician Signatures')
        fields << udf("Physician Signature", subj, anchor:'100%', required:true, labelWidth:160)
      end
  
      encode_fields(set_level(level, fields))
    end
 
    extend self
  end
  
end