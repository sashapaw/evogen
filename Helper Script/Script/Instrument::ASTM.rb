module Instrument

  # Should be used as a base class only. Derive concrete classes for ASTM-compliant instruments from this class
  class ASTM < Common

    def get_samples_from_query(data)
      samples = nil
      query_fields = data['query']['fields']
      query_fields.each do |field|
        idx = field['idx']
        case idx
          when 3
            barcodes = get_sample_barcodes_from_query(field)
            samples = barcodes.map{|barcode| get_sample(barcode)}.compact
        end
      end
      samples
    end

    def get_query_response(data)
      res = {}
      samples = get_samples_from_query(data).reverse
      if samples
        Rails.logger.info("=== Found samples #{samples.map(&:name)}")
        samples.each do |sample|
          requisition = get_requisition(sample)
          patient = requisition ? get_patient(requisition) : nil
          
          orders = if sample.subject_type.name == 'Sample'
            test_results = get_test_results(sample)
            if test_results.present?
              Rails.logger.info("=== found test_results: #{test_results}")
              get_orders(sample, test_results)
            end
          elsif sample.subject_type.name == 'QC Sample'
            get_qc_orders(sample)
          else
            @result_entry.new_message("Sample", "Sample: #{sample.to_s} is of unsupported type #{sample.subject_type.name}")
          end
          
          if orders.present?
            cpy = res.dup unless res.blank?
            res.clear.merge!({'patient': patient, 'orders': orders})
            res['next_order'] = cpy unless cpy.blank?
          end
        
        end
      end
      res
    end

    def get_test_ids(analytes)
      instrument_type = @instrument.type
      analytes = analytes.keys if analytes.is_a?(Hash)
      analytes.select {|analyte|
        analyte.get_value('Instrument Type') == instrument_type
      }.map {|analyte| 
        analyte.get_value('Instrument Code')
      }.select {|aname|
        aname.present?
      }
    end
    
    def get_universal_test_id(test_results)
      get_test_ids(test_results).map {|aname| "^^^#{aname}^1"}.join("\\")
    end

    def get_specimen_type(sample)
      st_name = sample.get_value('Specimen Type').name rescue nil
      @specimen_type_map.fetch(st_name) rescue 'Other'
    end

    def get_analyte_code(field)
      field['value'].split('^')[3]
    end

    def get_time_requested_at
      Time.zone.now.strftime('%Y%m%d%H%M%S')
    end

    def get_patient_data(requisition)
      patient = requisition.get_value('Patient')
      if patient.blank?
        rname = requisition.name rescue 'nil'
        @result_entry.new_message("Patient", "Requisition: #{rname} has no Patient")
        nil
      else
        id = patient.id rescue ''
        fname = patient.get_value('First Name')
        lname = patient.get_value('Last Name')
        birthdate = patient.get_value('DOB').to_s.gsub!("-", "")
        age = Date.today.year.to_i - patient.get_value('DOB').year.to_i
        sex = patient.get_value('Sex')
        if sex.present? && !['M','F','U'].include?(sex)
          sex = 'U'
        end
        physician = requisition.get_value('Requesting Physician')
        {
          id: id,
          first_name: fname,
          last_name: lname,
          birth_date: birthdate,
          age: age,
          sex: sex,
          physician_last_name: physician.get_value('Last Name'),
          physician_first_name: physician.get_value('First Name')
        }
      end
    end

    def get_orders_data(data)
      data['orders'].first rescue nil
    end

    def get_sample_barcodes_from_query(field)
      field['value'].split("\\").map{|bcode| bcode.split('^')[1]} rescue nil
    end

    def get_sample_barcode_from_result(field)
      field['value'].split('^').first rescue nil
    end

    def get_sample_from_result(data)
      orders_data = get_orders_data(data)
      order_fields = orders_data['order']['fields'] rescue nil if orders_data
      sample_barcode = nil
      if order_fields
        order_fields.each do |field|
          case field['idx']
            when 3
              barcode = get_sample_barcode_from_result(field)
              #raise TestResult::ImportError.new('Sample', "Test Result data contains information for multiple samples") if sample_barcode.present? && sample_barcode != barcode
              sample_barcode = barcode
            when 4
              barcode = get_sample_barcode_from_result(field)
              #raise TestResult::ImportError.new('Sample', "Test Result data contains information for multiple samples") if sample_barcode.present? && sample_barcode != barcode
              sample_barcode = barcode if sample_barcode.blank?
          end
        end
      else
        no_data_found
      end
      sample_barcode
    end

    def get_results(data, sample_barcode)
      orders_data = get_orders_data(data)
      results = orders_data['results']
      imported_results = []
      results.each do |res|
        test_result = { sample: sample_barcode }
        res['result']['fields'].each do |field|
          next if field['name'].blank?
          case field['name'].to_sym
            when :universal_test_id_internal
              test_result[:analyte] = get_analyte_code(field)
            when :result_value
              value = get_result_value(field)
              test_result[:value] = value unless value.nil?
            when :unit
              unit = get_unit(field)
              test_result[:unit] = unit unless unit.nil?
            when :reference_ranges
              range = get_range(field)
              test_result[:range] = range unless range.nil?
            when :abnormal_flags
              flags = get_flags(field)
              test_result[:flags] = flags unless flags.nil?
            when :result_status
              result_status = get_status(field)
              test_result[:result_status] = result_status unless result_status.nil?
            when :test_completed_at
              time_completed = get_time_completed(field)
              test_result[:time_completed] = time_completed unless time_completed.nil?
          end
        end
        comments = get_comments(res)
        test_result[:comments] = comments unless comments.blank?
        imported_results << test_result
      end
      imported_results
    end

    def get_unit(field)
      val = field['value']
      return nil if val.blank?
      unit_name = val
      #unit_name = 'mcg/dL' if unit_name == 'ug/dL' # ????
      unit_name = @unit_map.fetch(unit_name) if @unit_map.present? && @unit_map.include?(unit_name)
      unit_name = unit_name.chars.map{|b|
        b = 'u' if b.ord == 181 || b.ord == 956
        b
        }.join # μ to u

      unit_name
    end

    def get_result_value(field)
      field['value']
    end

    def get_range(field)
      field['value']
    end

    def get_flags(field)
      field['value']
    end

    def get_time_completed(field)
      date_str = field['value']
      return nil if date_str.blank?
      DateTime.strptime(date_str, '%Y%m%d%H%M%S') rescue nil
    end
    
    def get_comments(res)
      comms = res['comments']
      if comms.present?
        comments_fields = comms.first['fields']
        comments_fields[2]['value'].split('^').join("\n")
      end
    end
  end
end