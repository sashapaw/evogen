require 'indirizzo'
require 'phonelib'


module LISHL7
  extend self

  def createSegmentString(array)
    return array.join("^")
  end

  def get_address(address_text)
    return nil if address_text.blank?
    Indirizzo::Address.new(address_text)
  end

  def get_address_xad_field(address)
    address = get_address(address) unless address.is_a?(Indirizzo::Address)
    return '' if address.blank?
    city = address.city
    city = city.join(' ') if city.is_a?(Array)
    createSegmentString([address.line1,'',city,address.state,address.zip])
  end
  
  def get_xon_field(name, identifier, name_type: 'D', identifier_type: 'FI')
    createSegmentString([name,name_type,'','','','',identifier_type,'','A',identifier])
  end
  
  def get_xpn_field(full_name)
    return '' if full_name.blank?
    names = full_name.split(/[[:space:]]+/)
    middle_names = names[1...-1]
    createSegmentString([names.size > 1 ? names.last : '', names.first, middle_names.present? ? middle_names.join(' ') : ''])
  end

  def get_xcn_field(pid, full_name)
    return '' if pid.blank? && full_name.blank?
    names = full_name.split(/[[:space:]]+/)
    middle_names = names[1...-1]
    createSegmentString([pid, names.size > 1 ? names.last : '', names.first, middle_names.present? ? middle_names.join(' ') : ''])
  end

  def get_relationship_cwe_field(relationship)
    case relationship
      when 'Self'
        createSegmentString(['SEL', relationship])
      when 'Spouse'
        createSegmentString(['SPO', relationship])
      when 'Child'
        createSegmentString(['CHD', relationship])
      when 'Parent'
        createSegmentString(['PAR', relationship])
      when 'Other adult'
        createSegmentString(['OAD', relationship])
      else
        createSegmentString(['OTH', 'Other'])
    end
  end
  
  def get_phone(phone)
    #phone.area_code, phone.local_number
    phone = Phonelib.parse(phone,'us')
    if phone.nil?
      phone = Phonelib::Phone.new("")
    end
    return phone
  end

  def format_diag_codes(str)
    new_str = str.gsub(",","~")
    new_str = new_str.gsub("~ ","~")
    return new_str
  end

  def format_icd10_code(code, icd10_with_dot = false)
    if icd10_with_dot
      if code.include?('.')
        code
      else
        code.scan(/.{1,3}/).join('.')
      end
    else  # no dot is needed
      if code.include?('.')
        code.split('.').join
      else
        code
      end
    end
  end

  def remove_decimal(num)
    num.to_s.split('.')[0]
  end

  def remove_dash(str)
    str.gsub('-', '')
  end

  def is_number? string
    true if Float(string) rescue false
  end

end