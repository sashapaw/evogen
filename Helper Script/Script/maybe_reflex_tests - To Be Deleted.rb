def maybe_run_reflex_rules(req)
  pocResults = req.get_value('Point of Care Test Results')
  facility = req.get_value('Facility')
  reflexed = false
  pocResults.each do |poc_test_result|
    poc_result_entries =  poc_test_result.get_value('Point of Care Test Result Entries')
    poc_result_entries.each do |poc_test_result_entry|
      poc_test = poc_test_result_entry.get_value('Point of Care Test')
      reflex_rules = poc_test.get_value('Reflex Rules')
      facility_excluded_poc_tests = facility.get_value('Excluded Reflex POC Tests')
      if facility.get_value('Use Reflex Testing') == true && !facility_excluded_poc_tests.include?(poc_test)
        reflex_rules.each do |reflex_rule|
          rule_result = evaluate_rule(reflex_rule, poc_test, poc_test_result_entry, req)
          if !reflexed
            reflexed = rule_result 
          end
        end # end reflex_rules
      end # end check to use reflex rules
    end # end poc result entries
  end # end poc results
  reflexed
end

def evaluate_rule(reflex_rule, poc_test, poc_test_result_entry, req)
  value = reflex_rule.get_value('Value')
  operator = reflex_rule.get_value('Operator')
  test_assigned = false
  if reflex_rule.get_value('Primary POC Test').name == poc_test.name 
    if operator == 'equals' && value == poc_test_result_entry.get_value('PoC Result')
      test_assigned = assign_followup_tests(reflex_rule, poc_test, req)
    end
  end
  test_assigned
end

def assign_followup_tests(reflex_rule, poc_test, req)
  followup_tests = reflex_rule.get_value('Reflex Packaged Tests')
  test_codes = req.get_value('Tests')
  test_set = false
  if followup_tests.present?
    test_codes.concat followup_tests
    req.set_value('Tests', test_codes)
    test_set = true
  else
  end
  test_set
end
