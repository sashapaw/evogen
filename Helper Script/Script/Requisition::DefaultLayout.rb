module Requisition
  
  module DefaultLayout
    include Requisition
    include Patient::Layout
    include Physician::Layout
    include ScriptRunner::UI
    extend self
    
    
    def general_tab(subj, facility, patient_subject, system_settings)
      #portal is missing
      #: Facility Field
      #: Received Date
      # prefill to default specimen types specified in system settings, if applicable
      defaultSpecimenTypes = system_settings.get_value('Default Specimen Types') || []
      recipients = subj && subj.subject_type.name == 'Requisition' ? subj.get_value('Additional Report Recipients') : nil
  
      field_set(title:'General Information', border:'1 0 0 0', defaults:{anchor:'100%'}, items:[      
        udf('Facility', subj, filter:'terminated is false and RURO_CLIENT is null', labelWidth:160, allowCreate:true, required:true, disabled:patient_subject),
        udf('Requesting Physician', subj, addCustomFields: physician_layout(subj, 2), labelWidth:160, required:true,
          filter:"Facility = #{facility ? facility.id : 0} and terminated is false",
          react: {
            shown_when: 'value',
            change: "this.setFilter('terminated is false and Facility='+(value || 0));",
            only:'Facility'
        }),
        udf('Collection Date', subj, labelWidth:160, anchor:'50%', required:true),
        udf('Received Date', subj, labelWidth:160, anchor:'50%', required:true, hidden:patient_subject),
        udf('Specimen Types', subj, defaultValue: defaultSpecimenTypes, required:true, labelWidth:160),        
        udf('External Source ID', subj, info:'This field can be used for the Sample Barcode Entry', labelWidth:160),
        #REDMINE 8279
        udf('Reference Lab', subj, labelWidth:160, hidden:!system_settings.get_value('Use Reference Lab')),
        #End
        udf('Comments', subj, labelWidth:160),
        udf('Additional Report Recipients', subj, value: recipients, filter:"terminated is false", labelWidth:160,
          allowCreate:true, addCustomFields:encode_fields(set_level(2, udfs_for_subject_type('Contact', nil))))
      ])
    end

    def patient_tab(subj, facility, patient_subject, system_settings)
      patient = subj && subj.get_value('Patient')
      test_codes = subj.get_value('Tests') if subj
      test_codes = patient.get_value('Tests') if test_codes.blank? && patient
      field_set(title:'Patient Information', border:'1 0 0 0', items: [
          udf('Patient', subj, itemId:'patient', defaultValue:subj, labelWidth:160, required:true, addCustomFields: patient_layout(patient, 2),
            filter:"Facility = #{facility ? facility.id : 0} and terminated is false", disabled:patient_subject,
            react: patient_subject ? nil : {
              shown_when: 'value',
              change: "this.setFilter('terminated is false and Facility='+(value || 0));",
              only: 'Facility'
          }),
          patient_dependent_fields(subj, patient, test_codes)
        ],
        react: {
          shown_when: 'value',
          only: 'Facility'
      })
    end

    def requisition_layout(subj)
      # four options here:
      # 3. Created from Quick Link
      # 4. Editing
      # subj will be different in all cases and that controls the logic
      system_settings = LIS.system_settings

      facility = subj ? subj.get_value('Facility') : User.curr_user.get_value('Facility')
      patient_subject = !subj.nil? && subj.subject_type.name=="Patient"
  
      tabs = [{
        title:'General', defaults: {width:'100%'}, defaultType: 'textfield', autoScroll:true,
        items: [
          general_tab(subj, facility, patient_subject, system_settings),
          patient_tab(subj, facility, patient_subject, system_settings)
        ]
      }]
      
      if system_settings.get_value("Use Point of Care Tests")
        tabs << {
          title:'PoC Tests', defaults:{width:'100%'}, layout:'fit', items: [
            poct_results_tab(subj),
            {xtype:'hidden', name:'custom[poc_grid_results]', itemId:'poc_grid_results_id'}
          ]
        }
      end
  
      if system_settings.get_value("Use Confirmation Tests")
        tabs << {
          title:'Confirmation Tests', defaults:{width:'100%'}, layout:'fit', items: [
            confirmation_tab(subj),
            {xtype:'hidden', name:'custom[confirmation]', itemId:'confirmation_id'}
          ]
        }
      end
  
      encode_fields({
        xtype:'tabpanel', plain:true, activeTab:0, height:500, defaults:{bodyPadding:10}, items:tabs
      }.to_json)
    end
    
  end
  
end