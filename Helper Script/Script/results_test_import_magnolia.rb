# Obsolete code. Need to be rewritten to use new TestResult::Importer

require 'nokogiri'
require 'rexml/document'

def validate_file(xmlfile)
  system_settings = SubjectType.find_by_name('System Settings').subjects.first
  xsd_file = File.absolute_path(system_settings.get_value('Instrument XSD').path)

  xsd = Nokogiri::XML::Schema(File.read(xsd_file))
  doc = Nokogiri::XML(File.read(xmlfile))
  validation_errors = []
  xsd.validate(doc).each do |error|
    validation_errors << error.message
  end
  validation_errors
end

def parse_file(xmlfile, ftp)
    xmldoc = REXML::Document.new(File.read(xmlfile))

    test_results = []
	# Now get the root element
	root = xmldoc.root

    instrument_serial = ""
    xmldoc.elements.each("aiMessage/requestResult/testOrder/test/instrumentSerialNumber") {
    	|i| instrument_serial = i.text
    }

    test_code = ""
    xmldoc.elements.each("aiMessage/requestResult/testOrder/test/universalIdentifier/testIdentifier") {
		|e| test_code = e.text
	}

    specimen_sample = ""
    xmldoc.elements.each("aiMessage/requestResult/testOrder/specimen/specimenIdentifier") {
		|s| specimen_sample = s.text
	}

	xmldoc.elements.each("aiMessage/requestResult/testOrder/test/resultGroup") { |rg|
		rg.elements.each("result") { |r|
			test_result = {
				value: r.elements["value"].elements["testResult"].elements["observationValue"].text,
				analyte: r.elements["resultID"].elements["resultTestName"].text,
                analyte_code: r.elements["resultID"].elements["resultTestCode"].text,
				test_code: r.elements["resultID"].elements["resultTestCode"].text,
				test_package: rg.elements["resultGroupCode"].text,
				test_panel: test_code,
				instrument: instrument_serial,
                sample_id: specimen_sample,
                datetime: r.elements["resultDateTime"].text
			}
			begin
			test_result[:comments] = r.elements["comment"].elements["text"].text
		    rescue Exception=>e
              Rails.logger.info "[INSTRUMENT IMPORT]Something happened #{e}"
		    end
            test_results.push(test_result)
		}
	}
	return test_results
end

def convert_value(value)
  if value == "NOT_DETECT"
    return "Not Detected"
  elsif value == "DETECT"
    return "Detected"
  else
    return value
  end
end

def find_by_attrs(sub_type,attrs,values)
  begin
    query = search_query(subject_type: sub_type) do |qb|
      q = qb.and
      attrs.each_with_index { |attr,index|
       q << qb.compare(attr, :eq, values[index])
      }
      q
    end
    return find_subjects(query: query).first
  rescue Exception=>e
    Rails.logger.info "Bad query: #{query.inspect} Error: #{e}"
  end
end

def update_test_results(results)
  analyte_groups = {
    'Influenza' => ['Influenza A','Influenza A/H1','Influenza A/H3','Influenza A/2009-H1'],
    'E.coli' => ['Shiga-like toxin producing E.coli (STEC) stx1/stx2', 'E. coli 0157']
  }

  analytes_seen_in_groups = {
  }

  test_results = []
  instrument = ""
  results.each do |result|
    if instrument == ""
      instrument = find_by_attrs('Instrument', ['Serial Number'], [result[:instrument]])
    end
    #Look up instrument
    tr = find_by_attrs('Test Result',['Sample','"Analyte"->Code'],[result[:sample_id],"#{result[:analyte_code]}"])
    if tr
      tr.set_value('Value',convert_value(result[:value]))
      tr.set_value('Instrument',instrument) if instrument != "" && !instrument.nil?
      tr.set_value('Instrument Comments',result[:comments])

      # Populate analytes_seen_in_groups for analytes mentioned in analyte_groups
      analyte = tr.get_value('Analyte')
      analyte_groups.each {|group, aa|
        if aa.include?(analyte.name)
          analytes_seen_in_groups[[group,result[:sample_id]]] ||= []
          analytes_seen_in_groups[[group,result[:sample_id]]] << analyte.name
        end
      }

      test_results << tr
    end
  end

  # Process analyte groups

  analytes_seen_in_groups.each {|group_sample,analytes_seen_in_group|
    analytes = analyte_groups[group_sample[0]]
    if analytes_seen_in_group.present? && analytes_seen_in_group.size < analytes.size
        # Select analytes in group which were not seen in results from the instrument and finalize them
        analytes.select {|a| !analytes_seen_in_group.include?(a)}.each {|a|
        otr = find_by_attrs('Test Result',['Sample','Analyte'],[group_sample[1],a])
        if otr.present?
            otr.set_value('Value','Not Detected')
            otr.set_value('Instrument',instrument) if instrument != "" && !instrument.nil?
            otr.set_value('Finalized', true)
            otr.save!
        end
        }
    end
  }

  test_results
end

def process_file(file,comments,system_settings,ftp = false)
  xmlfile = File.absolute_path(file.path)
  errors = validate_file(xmlfile)

  if errors.size == 0
  	sample_import = create_subject('Sample Import') do |v|
      v.set_value('Sample Import Comments', comments) if comments.present?
    end

    start_workflow('Sample Import',sample_import)

  	test_results = parse_file(xmlfile, ftp)
    #raise "#{test_results.inspect}"
  	updated_results = update_test_results(test_results)
    sample_import.set_value('Sample Import File', file)  if file.present?

  	if updated_results.size > 0
      sample_import.set_value('Associated Test Results',updated_results)
  	end
  else
    sample_import.set_value('Sample Import Errors',errors.join("\n"))
  end
  sample_import.set_value('Test Results Imported',updated_results.size)

  complete_msg = "<strong>Import Complete</strong> - #{updated_results.size} Test Results Imported"
  if errors.size > 0
    complete_msg.concat("<br/> <span style='color:red;font-weight:bold;'>Import file contained errors - please review.</span>")
  end
  #open_subject(sample_import)
  return {subject:sample_import, msg:complete_msg}
end