module Requisition
  module DefaultReports
    extend self
    
    def test_results_report_templates
      ['Requisition Test Report']
    end
    
    def generate_requisition_report(subj)
      system_settings = LIS.system_settings
      poct_results = subj.get_value('Point of Care Test Results')
      conf_results = subj.get_value('Confirmation Test Results')
      physician_signature = subj.get_value('Requesting Physician').get_value('Physician Signature')
      patient_signature = subj.get_value('Patient Signature')

      context.run_custom_report('Requisition Report', subj, 'Requisition Report') do |extra_data|     
        extra_data['Lab Name'] = system_settings.get_value('Lab Name')      
        extra_data['Lab Logo'] = LIS.file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path))
        extra_data['System Settings'] = system_settings
        extra_data['poc_test_results'] = poct_results[0].get_value('Point of Care Test Result Entries') if poct_results.present?
        extra_data['conf_test_results'] = conf_results if conf_results.present?
        extra_data['Now'] = Time.now
        extra_data['Physician Signature'] = LIS.file_to_base64(File.absolute_path(physician_signature.path)) if physician_signature.present?
        extra_data['Patient Signature'] = LIS.file_to_base64(File.absolute_path(patient_signature.path)) if patient_signature.present?
      end # PDF
    end
    
    def find_order_number(result,type)
      order_num = ""
      test = result.get_value('Test')
      #child_test = result.get_value('Child Test')  
      test.get_value('Report Orders').each do |ro|    
        if type == 'Test'      
          #if ro.get_value('Test') && ro.get_value('Test').name == test.name
          ro_test = ro.get_value('Test')
          ro_analyte = ro.get_value('Analyte')
          ro_report_order = ro.get_value('Report Order')
          # if ro_test && child_test
          #         if ro_test && ro_test.name == child_test.name
          #           order_num = ro_report_order
          #           break
          #         end
          #       end
        elsif type == 'Analyte'    
          if ro_analyte && ro_analyte.name == ro_analyte.name        
            order_num = ro_report_order
          end
        end
      end
      return order_num
    end
    
    def sort_test_results(results)
      test_order = []
      l = 1000
      results.each do |result|
        #Find Test Package Number First
        test = result.get_value('Test')

        find = test_order.find{ |t| t[:test] == test.name }
        if !find
          lo = {l:l,test:test.name}    
          test_order.push(lo)
          l = l + 1000
        end

        ro = test.get_value('Report Order')
        if ro.to_s != ""
          ro = ro.round * 100000
        else
          ro = 1000000
        end    
        #Find Test Order Number Second
        tro = find_order_number(result,'Test')
        if tro.to_s != ""
          tro = tro.round
        else
          tro = 0
        end

        #Find Analyte Order Number Thrid
        aro = find_order_number(result,'Analyte')
        if aro.to_s != ""
          aro = aro.round
        else
          aro = 0
        end

        found = test_order.find { |t| t[:test] == test.name }
        if found
          order = found[:l] + ro + tro + aro
          #order = "#{ro}#{tro}#{aro}"
        else
          #order = "#{found[:l]}#{ro}#{tro}#{aro}"
          order = "#{ro}#{tro}#{aro}"
        end

        if order != ""
          result.set_value('Report Order',order.to_f)
        end
      end
    end

    def generate_med_string(medications)
      medications.map{|med| med.get_value('Description')}.join(', ')
    end

    def generate_req_cancellation_report(req)
      system_settings = LIS.system_settings

      if system_settings.get_value('Lab Address')
        address_array = system_settings.get_value('Lab Address').split("\n")
        if system_settings.get_value("Lab Test Results Type").eql? "Toxicology"    
          address = "#{address_array.join(' <br/> ')}"
        else
          address = "• #{address_array.join(' • ')} •"
        end
      end

      report_template = if req.get_value('Samples').present?
        "Requisition Test Report Cancellation"
      else
        "Requisition Test Report Cancellation No Sample"
      end
      now = Time.now
      req.set_value("Report Date",now)    
      filename = "Cancel_Result_#{req.name}"
      poct_results = []
      req_poct_results = req.get_value('Point of Care Test Results')
      if req_poct_results.size > 0
        poct_results = req_poct_results[0].get_value('Point of Care Test Result Entries')
      end
      file = context.run_custom_report("#{report_template}", req, 'Report File', :file_name=>filename) do |extra_data|    
        extra_data['header'] = ''
        extra_data['footer'] = ''
        extra_data['public_path'] = File.absolute_path('public')
        extra_data['now'] = now
        extra_data['system_settings'] = system_settings
        extra_data['Lab Name'] = system_settings.get_value('Lab Name')
        extra_data['Lab Logo'] = LIS.file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path))
        extra_data['Lab Logo Vertical'] = system_settings.get_value('Lab Logo Vertical')
        extra_data['Lab Address'] = address
        extra_data['Lab Director'] = system_settings.get_value('Lab Director')
        extra_data['CLIA Number'] = system_settings.get_value('CLIA Number')
        extra_data['Lab Footer Signature'] = system_settings.get_value('Lab Report Footer Signature')
        extra_data['Medications'] = generate_med_string(req.get_value('Medications'))
        extra_data['poc_test_results'] = poct_results
      end 
    end

    def generate_test_results_report(req, correction, final = false)
      if final
        non_finalized_results_count = context.count_subjects(query: "FROM \"Test Result\" WHERE Finalized = null AND Sample->Requisition = #{req.id}")
        raise "There are #{non_finalized_results_count} non-finalized Test Results" if non_finalized_results_count > 0
      end

      system_settings = LIS.system_settings
      attach_udf = final || correction && correction[:correction] ? 'Report File' : 'Preliminary Report File'

      now = Time.now
      req.set_value("Report Date",now)    
      report_template = req.get_value('Facility').get_value('Report Template').name if req.get_value('Facility').get_value('Report Template').present?
      report_template = system_settings.get_value('Report Template') if report_template.blank?
      report_template = 'Requisition Test Report' if report_template.blank?
      width = 1000

      header = context.run_custom_report("#{report_template} Header", req) do |extra_data|     
        extra_data['system_settings'] = system_settings
        extra_data['Lab Logo'] = LIS.file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path))
        extra_data['width'] = width
      end
      header_path = File.absolute_path(header.path)    

      footer = context.run_custom_report("#{report_template} Footer", req) do |extra_data|
        extra_data['system_settings'] = system_settings
        extra_data['width'] = width
      end
      footer_path = File.absolute_path(footer.path)

      filename = if correction[:correction]
        "Corrected_Result_#{req.name}_#{req.get_value('Patient').name}-#{now.strftime('%Y%m%d%H%M%S')}"
      else
        "View_Result_#{req.name}_#{req.get_value('Patient').name}"
      end
      filename.gsub!(/\W/, '_')

      poct_results = []
      req_poct_results = req.get_value('Point of Care Test Results')
      if req_poct_results.size > 0
        poct_results = req_poct_results[0].get_value('Point of Care Test Result Entries')
      end

      file = context.run_custom_report("#{report_template}", req, attach_udf, file_name: filename) do |extra_data|    
        extra_data['header'] = header_path
        extra_data['footer'] = footer_path
        extra_data['public_path'] = File.absolute_path('public')
        extra_data['system_settings'] = system_settings
        extra_data['now'] = now
        extra_data['correction'] = correction[:correction]
        extra_data['correction_user'] = correction[:user]
        extra_data['correction_time'] = correction[:time]
        extra_data['amendment_comments'] = correction[:comments]
        extra_data['Medications'] = generate_med_string(req.get_value('Medications'))
        extra_data['poc_test_results'] = poct_results

        extra_data['width'] = width
        customize_test_results_report_data(req, extra_data)
        samples = samples_for_test_results_report(req)
        if samples.present?
          extra_data['samples_data'] = samples.map{|sample|
            sample_report_data(sample, {width: width})
          }
        end
      end

      File.delete(header_path)
      File.delete(footer_path)
      File.delete(file.path)
    end

    def customize_test_results_report_data(req, extra_data)
    end

    def samples_for_test_results_report(req)
      samples = req.get_value('Samples')
      samples.sort_by!{|s| s.get_value('Collection Date')} if samples.present?
      samples
    end

    def sample_report_data(sample, opts = {})
      ret = {'sample'=> sample}
      tests = (sample.get_value('Tests') || []).reject{|t| t.get_value('Non-Reportable')}
      ret['tests'] = tests if tests.present?
      ret
    end
    
  end
end