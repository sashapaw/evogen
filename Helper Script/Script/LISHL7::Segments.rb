require 'ruby-hl7'

module LISHL7
  module Segments
    include LISHL7
    extend self
    
    def set_hl7_property(obj, prop_name, prop_value)
      obj.send("#{prop_name}=",prop_value)
    rescue NoMethodError # you can also add this
      Rails.logger.error "#{prop_name} is undefined in #{obj.class.name} \"#{obj}\"."
    end

    def add_to_hl7_segment(segment, values)
      values.each do |key, val|
        set_hl7_property(segment, key, val)
      end
      segment
    end

    def generate_message(msg_map, message = ::HL7::Message.new)
      msg_map.each do |key, val|
        begin
          segment = ::HL7::Message::Segment.const_get(key.upcase).new
        rescue NameError
          raise "Unknown HL7 segment type #{key}"
        end

        val.each do | values |
          message << add_to_hl7_segment(segment, values)
        end
      end
      return message
    end
    
    def msh(options = {})
      msh = ::HL7::Message::Segment::MSH.new
      msh.enc_chars = "^~\\&"
      msh.sending_app = 'LimitLIS.cloud^3'
      msh.time = Time.now
      msh.security = ""
      add_to_hl7_segment(msh, options)
      msh
    end
=begin
    def msh(sending_facility,sending_facility_num,recv_app,recv_facility,recv_facility_num,messagetype,triggerevent,control_id,processing_type,version)
      msh = ::HL7::Message::Segment::MSH.new
      msh.enc_chars = "^~\\&"
      msh.sending_app = "LimitLIS.cloud^3"
      msh.sending_facility = createSegmentString([sending_facility,sending_facility_num])
      msh.recv_app = recv_app
      msh.recv_facility = createSegmentString([recv_facility,recv_facility_num])
      msh.time = Time.now
      msh.security = ""
      msh.message_type = createSegmentString([messagetype,triggerevent])
      msh.message_control_id = control_id
      msh.processing_id = processing_type
      msh.version_id = version
      msh
    end
=end

    def ft1(id,collection_date,transaction_code,test,test_total,department_code,assigned_patient_location,diagnosis_codes,performed_by,physician_npi_number,physician_last_name,physician_first_name,unit_cost,procedure_code,modifer_code,order_number)
      ft1 = ::HL7::Message::Segment::FT1.new
      ft1.set_id = id
      ft1.transaction_id = ""
      ft1.transaction_batch_id = ""
      ft1.transaction_date = collection_date
      ft1.transaction_posting_date = ""
      ft1.transaction_type = "CG" #Required 6
      ft1.transaction_code = transaction_code #Required 7
      ft1.transaction_description = test
      ft1.transaction_description_alternative = test
      ft1.transaction_quantity = test_total
      ft1.transaction_amount_extended = ""
      ft1.transaction_amount_unit = ""
      ft1.department_code = department_code
      ft1.insurance_plan_id = ""
      ft1.insurance_amount = ""
      ft1.assigned_patient_location = assigned_patient_location
      ft1.fee_schedule = ""
      ft1.patient_type = ""
      ft1.diagnosis_code = format_diag_codes(diagnosis_codes)
      ft1.performed_by = performed_by
      ft1.ordered_by = createSegmentString([physician_npi_number,physician_last_name,physician_first_name]) #Required 21
      ft1.unit_cost = unit_cost
      ft1.filter_order_number = order_number
      ft1.entered_by_code = ""
      ft1.procedure_code = procedure_code #25
      ft1.procedure_code_modifer = modifer_code
      ft1
    end

    def pid(id,last_name,first_name,middle_name,dob,sex,address,phone_number,area_code,local_number,account_number)
      pid = ::HL7::Message::Segment::PID.new
      pid.set_id = 1
      pid.patient_id = ""
      pid.patient_id_list = id
      pid.alt_patient_id = id
      pid.patient_name = createSegmentString([last_name,first_name,middle_name])
      pid.mother_maiden_name = ""
      pid.patient_dob = dob.respond_to?(:strftime) ? dob.strftime('%Y%m%d') : dob
      pid.admin_sex = sex
      pid.patient_alias = ""
      pid.race = ""
      pid.address = address
      pid.country_code = ""
      pid.phone_home = createSegmentString([phone_number,"","","","",area_code,local_number])
      pid.phone_business = ""
      pid.primary_language = ""
      pid.marital_status = ""
      pid.religion = ""
      pid.account_number = account_number
      pid.social_security_num = ""
      pid.driver_license_num = ""
      pid.mothers_id = ""
      pid.ethnic_group = ""
      pid
    end

    def pv1(referring_doctor,bill_type,billing_identifier, other_healthcare_provider = nil)
      pv1 = ::HL7::Message::Segment::PV1.new
      pv1.set_id = "1"
      pv1.patient_class = "O"
      pv1.assigned_location = billing_identifier
      pv1.admission_type = ""
      pv1.preadmit_number = ""
      pv1.prior_location = ""
      pv1.attending_doctor = ""
      pv1.referring_doctor = referring_doctor
      pv1.consulting_doctor = ""
      pv1.hospital_service = ""
      pv1.temporary_location = ""
      pv1.preadmit_indicator = ""
      pv1.readmit_indicator = ""
      pv1.admit_source = ""
      pv1.ambulatory_status = ""
      pv1.vip_indicator = ""
      pv1.admitting_doctor = ""
      pv1.patient_type = ""
      pv1.visit_number = ""
      pv1.financial_class = bill_type
      pv1.charge_price_indicator = ""
      pv1.courtesy_code = ""
      pv1.credit_rating = ""
      pv1.contract_code = ""
      pv1.contract_effective_date = ""
      pv1.contract_amount = ""
      pv1.contract_period = ""
      pv1.interest_code = ""
      pv1.transfer_bad_debt_code = ""
      pv1.transfer_bad_debt_date = ""
      pv1.bad_debt_agency_code = ""
      pv1.bad_debt_transfer_amount = ""
      pv1.bad_debt_recovery_amount = ""
      pv1.delete_account_indicator = "N"
      pv1.other_healthcare_provider = other_healthcare_provider
      pv1
    end

    def dg1(id,diagnosis_code,diagnosis_description)
      dg1 = ::HL7::Message::Segment::DG1.new
      dg1.set_id = id
      dg1.diagnosis_code_set = "I10"
      dg1.diagnosis_code = diagnosis_code
      dg1.diagnosis_description = diagnosis_description
      dg1
    end

    def gt1(number,name,spouse_name,address,phone_home,phone_business,dob,sex,type,relationship)
      gt1 = ::HL7::Message::Segment::GT1.new
      gt1.set_id = "1"
      gt1.guarantor_number = number
      gt1.guarantor_name = name
      gt1.guarantor_spouse_name = spouse_name
      gt1.guarantor_address = address
      gt1.guarantor_phone_home = phone_home
      gt1.guarantor_phone_business = phone_business
      gt1.guarantor_dob = dob.respond_to?(:strftime) ? dob.strftime('%Y%m%d') : dob
      gt1.guarantor_sex = sex
      gt1.guarantor_type = type
      gt1.guarantor_relationship = relationship
      gt1
    end

    def in1(id,company_id,name,address,phone,group_num,insured_name,insured_relationship,insured_dob,insured_address,insurance_policy_num,insured_sex,insured_id)
      in1 = ::HL7::Message::Segment::IN1.new
      in1.set_id = id
      in1.insurance_plan_id = ""
      in1.insurance_company_id = company_id #remove_decimal(company_id)
      in1.insurance_company_name = name
      in1.insurance_company_address = address
      in1.insurance_company_contact_person = ""
      in1.insurance_company_phone_number = createSegmentString([phone,"","","","","",""])
      in1.group_number = group_num
      in1.group_name = ""
      in1.insureds_group_employee_id = ""
      in1.insureds_group_employee_name = ""
      in1.plan_effective_date = ""
      in1.plan_expiration_date = ""
      in1.authorization_information = ""
      in1.plan_type = ""
      in1.name_of_insured = insured_name
      in1.insureds_relationship_to_patient = insured_relationship
      in1.insureds_date_of_birth = insured_dob.respond_to?(:strftime) ? insured_dob.strftime('%Y%m%d') : insured_dob
      in1.insureds_address = insured_address
      in1.assignment_of_benefits = "Y"
      in1.coordination_of_benefits = ""
      in1.coordination_of_benefits_priority = ""
      in1.notice_of_admission_flag = ""
      in1.notice_of_admission_date = ""
      in1.report_of_eligibility_flag = ""
      in1.report_of_eligibility_date = ""
      in1.release_information_code = ""
      in1.pre_admit_cert = ""
      in1.verification_date_time = ""
      in1.verification_by = ""
      in1.type_of_agreement_code = ""
      in1.billing_status = ""
      in1.lifetime_reserve_days = ""
      in1.delay_before_lr_day = ""
      in1.company_plan_code = ""
      in1.policy_number = insurance_policy_num
      in1.policy_deductible = ""
      in1.policy_limit_amount = ""
      in1.policy_limit_days = ""
      in1.room_rate_semi_private = ""
      in1.room_rate_private = ""
      in1.insureds_employment_status = ""
      in1.insureds_sex = insured_sex
      in1.insureds_employer_address = ""
      in1.verification_status = ""
      in1.prior_insurance_plan_id = ""
      in1.coverage_type = ""
      in1.handicap = ""
      in1.insureds_id_number = insured_id
      in1
    end

    def pd1(primary_care_provider)
      pd1 = ::HL7::Message::Segment::PD1.new
      pd1.living_dependency = ""
      pd1.living_arrangement = ""
      pd1.patient_primary_facility = ""
      pd1.patient_primary_care_provider = primary_care_provider
      pd1
    end

    def nk1(id)
      nk1 = ::HL7::Message::Segment::NK1.new
      nk1.set_id = id
      nk1.name = ""
      nk1.relationship = ""
      nk1.address = ""
      nk1.phone_number = ""
      nk1
    end
    
    def obr(id, req_id, ext_req_id)
      obr = ::HL7::Message::Segment::OBR.new
      obr.set_id = id.to_s
      obr.placer_order_number = ext_req_id
      obr.filler_order_number = req_id
      obr.observation_end_date = ""
      obr.collection_volume = ""
      obr.collector_identifier = ""
      obr.specimen_action_code = ""
      obr.danger_code = ""
      obr.relevant_clinical_info = ""
      obr.order_callback_phone_number = ""
      obr.placer_field_1 = ""
      obr.placer_field_2 = ""
      obr.filler_field_1 = ""
      obr.filler_field_2 = ""
      obr.charge_to_practice = ""
      obr.diagnostic_serv_sect_id = ""
      obr.parent_result = ""
      obr
    end
    
    def obx(id, producer_id)
      obx = ::HL7::Message::Segment::OBX.new
      obx.set_id = id.to_s
      obx.probability=""
      obx.nature_of_abnormal_test=""
      obx.observation_result_status="F"
      obx.effective_date_of_reference_range=""
      obx.user_defined_access_checks=""
      obx.producer_id = producer_id
      obx.responsible_observer=""
      obx.observation_method=""
      obx.equipment_instance_id=""
      obx.analysis_date=""
      obx.observation_site=""
      obx.observation_instance_id=""
      obx.mood_code=""
      obx.performing_organization_name=""
      obx.performing_organization_address=""
      obx
    end

    def nte(index, source, comment, comment_type = '')
      nte = ::HL7::Message::Segment::NTE.new
      nte.set_id = index.to_s
      nte.source = source
      nte.comment = ActionView::Base.full_sanitizer.sanitize(comment)
      nte.comment_type = comment_type
      nte
    end


  end
end
