module TestResult
  extend self
  
  class ImportError < StandardError
    attr_accessor :type

    def initialize(type, message)
      @type = type
      super(message)
    end
  end

  def import_file(import_subject, instrument = nil, importer = nil, file = nil)
    instrument = import_subject.get_value('Instrument') if instrument.nil?
    importer = TestResult::Importer.new(instrument)if importer.nil?
    file = import_subject.get_value('File') if file.nil?
    errors = ''
    ActiveRecord::Base.connection.disable_transaction_callbacks {
      begin
        instrument_impl = instrument.type.integration_class.new(instrument, import_subject, context)
        obj = instrument_impl.import_file(file) {|result_data|
          begin
            Subject.transaction {
              instrument_impl.preprocess_record(result_data) if instrument_impl.respond_to?(:preprocess_record)
              sample, test_result = importer.import_test_result(result_data)
              import_subject.add_data(sample, test_result)
            }
          rescue Exception=>e
            Rails.logger.error("Error importing #{result_data}: #{e}\n#{e.backtrace.join("\n")}")
            errors << "line #{result_data[:row]}: #{e}\n"
          end
        }
      rescue Exception=>e
        Rails.logger.error("There was a problem processing upload. #{e}\n#{e.backtrace.join("\n")}")
        errors << "#{e}\n"
      end
    }
    qc_runs = importer.qc_runs
    if qc_runs.present?
      import_subject.add_value('Associated QC Runs', qc_runs)
      qc_runs.each do |qc_run|
        group_validation(qc_run.get_value('QC Test Result List'))
      end
    end
    import_subject.set_value("Errors", errors) if errors.present?
    import_subject.save! if import_subject.changed?
  end

  def get_invalid_groups(test_results)
    invalid_groups = {}
    test_results.each do |subj|
      qc_grouping = subj.get_value('QC Grouping')
      if subj.get_value('Passed?') == "No"
        #invalid_groups.push(subj.get_value('QC Grouping')) unless invalid_groups.include?(subj.get_value('QC Grouping'))
        invalid_groups[qc_grouping] ||= 0
        invalid_groups[qc_grouping] += 1
      end
    end
    invalid_groups
  end

  def set_invalid_groups(test_results, invalid_groups)
    test_results.each do |subj|
      qc_grouping = subj.get_value('QC Grouping')
      fail_threshold = test_results.size > 2 ? 2 : 1
      if invalid_groups.key?(qc_grouping) && invalid_groups[qc_grouping] >= fail_threshold
        subj.set_value('QC Group Valid?',"No")
      else
        subj.set_value('QC Group Valid?',"Yes")
      end
    end
  end
  
  def group_validation(test_results)
    set_invalid_groups(test_results, get_invalid_groups(test_results))
  end
  
end