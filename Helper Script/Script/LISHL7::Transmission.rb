require 'timeout'
require 'net/sftp'
require 'net/ftp'

module LISHL7

  module Transmission
    extend self

    # Obsolete:  this is for compatibility with scripts from LIS 3.0.   DO NOT USE.
    def upload_hl7_file(configuration, f, remote_file_path)
      configuration.upload_hl7_file(f, remote_file_path)
    end
    
    module SFTP
      extend self
      # Sends HL7 to provided host using path provided
      #
      # @param {host} str - Hostname for SFTP
      # @param {username} str - Username for SFTP
      # @param {password} str - Password for SFTP
      # @param {file} file - File to send to SFTP Host
      # @param {toFile} str - Path on Remote Host to place the file.
      # @param {port} num - Port Number for SFTP
      # @return {response}
      def upload_file(host, username, password, file, toFile, port)
        response = ''
        begin
          keys = {:password => password,:timeout=>20}
          keys[:port] = port if port.present?
          Net::SFTP.start(host, username, keys) do |sftp|
            #Path to Local, Path to Remote
            sftp.upload!(file, toFile)
            response = "Status Code: 0" + " OK, File successfully Uploaded to " + toFile		 
          end
        rescue Net::SFTP::StatusException => status
          response = "Status Code: " + status.code.to_s + " " + status.description + ", " + status.text
        rescue Exception => e
          response = "Failed Attempt: " + e.to_s
        end
        return response
      end
    
      # Connects to provided SFTP Host, downloads hl7 files and places them in an array,
      # Then removes collected files.
      #
      # @param {host} str - Hostname for SFTP
      # @param {username} str - Username for SFTP
      # @param {password} str - Password for SFTP
      # @param {path} str - Path used to retreive HL7 Files ex: /orders
      # @param {port} num - Port Number for SFTP
      # @return {hl7Data}
      def get_files(host, username, password, path, port = nil)
        response = ''
        begin
          entries = []
          keys = {:password => password}
          keys[:port] = port if port.present?
          Net::SFTP.start(host, username, keys) do |sftp|
            sftp.dir.foreach(path) do |entry|
              if !File.directory?(entry.name)
                #Try to download the file, timeout after an hour. 
                status = Timeout::timeout(1200) {
                  data = sftp.download!("#{path}/#{entry.name}")
                  entries.push(data)
                  sftp.remove!("#{path}/#{entry.name}")
                  }
              end
            end
          end
        rescue Net::SFTP::StatusException => status
          response = "Status Code: " + status.code.to_s + " " + status.description + ", " + status.text
        rescue Timeout::Error => e
          response = "Timeout occured while retreiving data. " + e.to_s
        rescue Exception => e
          response = "Failed Attempt: " + e.to_s
        end

        return {data: entries, response: response}
      end
    end
    
    module FTP
      extend self
      
      def upload_file(host, username, password, file, toFile, port = nil)
        response = ''
        port = 21 if port.blank?
        begin
          ftp = Net::FTP.new
          ftp.open_timeout = 20
          ftp.connect(host, port)
          begin
            ftp.login(username, password)
            ftp.passive = true
            ftp.binary = true
            ftp.chdir(toFile)
            ftp.putbinaryfile(file)
            response = 'Status Code: 0' + ' OK, File successfully Uploaded to ' + toFile
          ensure
            ftp.close rescue nil
          end
        rescue Exception => e
          Rails.logger.error("#{e.to_s}\n#{e.backtrace.join("\n")}")
          response = 'Failed Attempt: ' + e.to_s
        end

        response
      end

      def get_files(host, username, password, path, port = nil)
        response = ''
        port = 21 if port.blank?
        begin
          file_data = []
          ftp = Net::FTP.new
          ftp.open_timeout = 20
          ftp.connect(host, port)
          begin
            ftp.login(username, password)
            ftp.passive = true
            ftp.binary = true
            ftp.chdir(path)
            ftp.nlst.each do |entry|
              data = ftp.get(entry, nil)
              file_data.push(data)
              ftp.delete(entry)
            end
          ensure
            ftp.close rescue nil
          end
        rescue Exception => e
          Rails.logger.error("#{e.to_s}\n#{e.backtrace.join("\n")}")
          response = 'Failed Attempt: ' + e.to_s
        end

        { data: file_data, response: response }
      end
      
    end
    
    module FTPS
      extend self
      
      def upload_file(host, username, password, file, toFile, port = nil)
        response = ''  	
        begin
          port = 990 if port.blank?
          ftps = DoubleBagFTPS.new(nil, nil, nil, nil, DoubleBagFTPS::IMPLICIT)
          ftps.ssl_context = DoubleBagFTPS.create_ssl_context(:verify_mode => OpenSSL::SSL::VERIFY_NONE)
          ftps.passive = true
          ftps.connect(host, port)
          ftps.login(username, password)
          ftps.chdir(toFile)
          ftps.putbinaryfile(file)
          response = 'Status Code: 0' + ' OK, File successfully Uploaded to ' + toFile
          ftps.close              
        rescue Exception => e
          response = 'Failed Attempt: ' + e.to_s
        end

        response
      end

      def get_files(host, username, password, path, port = nil)
        response = ''
        begin
          file_data = []
          port = 990 if port.blank?
          ftps = DoubleBagFTPS.new(nil, nil, nil, nil, DoubleBagFTPS::IMPLICIT)
          ftps.ssl_context = DoubleBagFTPS.create_ssl_context(:verify_mode => OpenSSL::SSL::VERIFY_NONE)
          ftps.connect(host, port)
          ftps.login(username, password)
          ftps.binary = true
          ftps.passive = true
          ftps.chdir(path)
          ftps.nlst.each do |entry|
            data = ftps.get(entry, nil)
            file_data.push(data)
            ftps.delete(entry)
          end
          ftps.close  
        rescue Exception => e
          response = 'Failed Attempt: ' + e.to_s
        end

        { data: file_data, response: response }
      end
      
    end

    module MLLP
      extend self
      
      def upload_file(host, port, file)
        s = TCPSocket.new(host, port)
        s.print "\x0B"  # MLLP <SB> header
        s.puts file.read
        s.print "\x1C\x0D"  # MLLP <EB><CR> footer
        s.close
      end

    end
    
    module HTTP
      extend self
      
      def upload_file(url,username,password, file)
        uri = URI(url)
        #req = Net::HTTP::Get.new(uri)
        #req.basic_auth username, password

        #res = Net::HTTP.start(uri.hostname, uri.port) {|http|
        # http.request(req)
        #}
        response = ''
        Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
          request = Net::HTTP::Post.new uri
          request.basic_auth username, password
          request.content_type = 'plain/text'
          request.body = file.read
          response = http.request request # Net::HTTPResponse object
        end

        return response.body
      end
    end
    
  end

end