module Instrument
  class BC_AcT5 < ASTM

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @result_status_map = get_result_status_map
      @analyte_map = get_analyte_map
    end

    def get_analyte_map
      {
        '804-5' => 'White Blood Cell Count',
        '731-0' => 'Absolute Lymphocytes',
        '736-9' => 'Lymphocytes',
        '742-7' => 'Absolute Monocytes',
        '744-3' => 'Monocytes',
        '751-8' => 'Absolute Neutrophils',
        '770-8' => 'Neutrophils',
        '711-2' => 'Absolute Eosinophils',
        '713-8' => 'Eosinophils',
        '704-7' => 'Absolute Basophils',
        '706-2' => 'Basophils',
        '733-6' => 'Absolute Atypical Lymphocytes',
        '735-1' => 'Atypical Lymphocytes',
        'X-LIC' => 'Absolute Immature Plasma Cells',
        '11117-9' => 'Immature Plasma Cells',
        '789-9' => 'Red Blood Cell Count',
        '717-9' => 'Hemoglobin',
        '4544-3' => 'Hematocrit',
        '787-2' => 'Mean Corpuscular Volume',
        '785-6' => 'Mean Corpuscular HGB',
        '786-4' => 'Mean Corpuscular HGB Conc.',
        '788-0' => 'Red Cell Dist. Width',
        '777-3' => 'Platelet Count',
        '776-5' => 'Mean Platelet Volume',
        'X-PCT' => 'Plateletcrit',
        'X-PDW' => 'Platelet Distribution Width'
      }
    end

    def get_result_status_map
      {
        'W' => 'Suspicion',
        'N' => 'Rejected result',
        'F' => 'Final result',
        'X' => 'All hematology parameters except: Absolute Basophils and Basophils',
        'S' => 'Absolute Basophils and Basophils',
        'C' => 'Platelet Concentrate Mode'
      }
    end

    def get_analyte_code(field)
      field['value'].split('^').last
    end

    def get_result_value(test_result, field)
      val = field['value']
      val = 'Could not be calculated' if val == '.....'
      val = 'Exceeds the reportable range' if val == '+++++'
      test_result.set_value('Value', val) unless val.blank?
    end

    def get_range(test_result, field)
      range = field['value']
      test_result.set_value('Range', range) unless range.blank?
    end

    def get_flags(test_result, field)
      flag = field['value']
      if flag.present?
        flag.gsub!('*', '') # not sure what * means (not mentioned in documentation)
        flag[0] = 'C' if flag.size == 2
        test_result.set_value('Flag', flag)
      end
    end

    def get_status(test_result, field)
      res_stat = field['value']
      result_status = @result_status_map.fetch(res_stat) rescue nil
      test_result.set_value('Instrument Result Status', result_status) unless result_status.blank?
    end

    def get_comments(test_result, res)
      comms = res['comments']
      if comms.present?
        comments_fields = comms.first['fields']
        comments = comments_fields[2]['value'].split('^').join("\n")
        #test_result.set_value('Comments', comments) unless comments.blank?
      end
    end
  end
end