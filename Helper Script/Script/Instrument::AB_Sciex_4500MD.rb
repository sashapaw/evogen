module Instrument
  class AB_Sciex_4500MD < Common
    
    def import_file(file)
      context.open_spreadsheet_file_simple(file.path, extension: :csv, headers: ('A'..'F').to_a) {|csv|
        state = :start
        context.init_progress csv.length
        csv.each_with_index {|row, i, row_raw|
          Rails.logger.info("-->> state: #{state} data: #{row_raw.inspect}")
          context.advance_progress
          
          col0 = row_raw[0]
          case state
          when :start
            state = :header if col0 == 'Instrument Name'
          when :header
            instrument_name = col0
            acquisition_date = row_raw[1]
            state = :table_header
          when :table_header
            state = :table
          when :table
            yield({sample: row_raw[0], analyte: row_raw[1], value: row_raw[4], row: "#{i}"})
          end
        }
      }
    end
    
  end
end
