module Instrument
  class BC_LH500 < Common

    def initialize(instrument, result_entry = nil, ctx = context)
      @instrument = instrument
      @ctx = ctx
      @result_entry = result_entry
      @analyte_map = get_analyte_map
    end

    def get_analyte_map
      {
        'WBC' => 'White Blood Cell Count',
        'RBC' => 'Red Blood Cell Count',
        'HGB' => 'Hemoglobin', 
        'HCT' => 'Hematocrit',
        'MCV' => 'Mean Corpuscular Volume',
        'MCH' => 'Mean Corpuscular HGB',
        'MCHC' => 'Mean Corpuscular HGB Conc.',
        'RDW' => 'Red Cell Dist. Width',
        'PLT' => 'Platelet Count',
        'PCT' => 'Plateletcrit',
        'MPV' => 'Mean Platelet Volume',
        'PDW' => 'Platelet Distribution Width',
        'UWBC' => 'Uncorrected White Blood Cell Count',
        'RDWSD' => 'Red Cell Dist. Width-Std. Dev.',
        'MAF' => 'Microcytic Anemia Factor',
        'LY%' => 'Lymphocytes',
        'LY#' => 'Absolute Lymphocytes',
        'MO%' => 'Monocytes',
        'MO#' => 'Absolute Monocytes',
        'NE%' => 'Neutrophils',
        'NE#' => 'Absolute Neutrophils',
        'EO%' => 'Eosinophils',
        'EO#' => 'Absolute Eosinophils',
        'BA%' => 'Basophils',
        'BA#' => 'Absolute Basophils',
        'RET' => 'Reticulocytes'
      }
    end

    def get_test_map
      {
        'CBC Complete Blood Count (CBC)' => 'CBC',
        'CBCD Complete Blood Count (CBC) with differential' => 'DIFF',
        'CBCD RET' => 'CDR',
        'RET Reticulocyte Count' => 'RETIC',
        'CBC RET' => 'CR'
      }
    end

    def get_query_response(data)
      nil
    end

    def get_sample_barcode_from_result(data)
      data['groups'][0][2]['data'] rescue nil
    end


    def get_sample_from_result(data)
      barcode = get_sample_barcode_from_result(data)
      sample = get_sample(barcode)
    end

    def get_results(data, sample, test_results)   
      imported_results = []
      data['groups'].slice(1..3).each do |entry|
        entry.each do |result|
          field = result['tag']
          val = result['data']
          test_result = get_test_result(field, sample, test_results)
          if test_result
            get_result_value(test_result, val) unless val.blank?
            imported_results << test_result
          end
        end
      end
      @result_entry.link_test_results(imported_results)
      get_comments(data, sample)
    end

    def get_comments(data, sample)
      comments = ""
      data['groups'].slice(4..-1).each do |section|
        section.each do |result|
          val = result['data'] rescue nil
          comment = "#{val}" if val
          comments += comment + "\n" if comment
        end
      end
      sample.set_value('Instrument Comments', comments) unless comments.blank?
    end

    def get_result_value(test_result, val)
      result, flag = val.split(' ')
      result = 'Incomplete computation' if result == '.....'
      result = 'Parameter overrange' if result == '+++++'
      result = 'Parameter voteout' if result == '-----'
      test_result.set_value('Value', result) 
      test_result.set_value('Flag', flag) rescue nil unless ['R', '*', 'e', 'E'].include? flag
    end

    def get_analyte_code(field)
      field
    end
  end
end