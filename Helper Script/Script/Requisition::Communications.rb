module Requisition
  module Communications
    extend self
    
    def send_communication(req)  
      facility = req.get_value('Facility')
      communication = facility.get_value('Additional Report Delivery Options').to_s
      if ['Email', 'Fax & Email'].include?(communication)
        email_send(req)
      else
        # always send emails to Additional Report Recipients on requisition
        contacts_email_send(req, req.get_value('Additional Report Recipients'))
      end
      fax_send(req) if ['Fax', 'Fax & Email'].include?(communication)
      sms_send(req) if facility.get_value('SMS message when Report is ready')
    end

    def email_send(req)
      facility = req.get_value('Facility')
      send_test_results_ready_email(facility.get_value('Contact Email'), facility.get_value('Contact Name'), req)
      if facility.get_value('Also Send Physician')
        physician = req.get_value('Requesting Physician')
        send_test_results_ready_email(physician.get_value('Contact Email'), "#{physician.get_value('First Name')} #{physician.get_value('Last Name')}", req)
      end
      contacts = req.get_value('Additional Report Recipients') || []
      contacts += (facility.get_value('Additional Contacts') || [])
      contacts.uniq!
      contacts_email_send(req, contacts)
    end
    
    def contacts_email_send(req, contacts)
      contacts.each{|contact|
        send_test_results_ready_email(contact.get_value('EMail'), contact.get_value('Full Name'), req)
      } if contacts
    end

    def send_test_results_ready_email(emails, greeting, req)
      if emails.present?
        params = {'labname'=>LIS.system_settings.get_value('Lab Name'), 'greeting'=>greeting}
        context.send_email(emails.split(';'), "Test Results Ready", req, params)
      end
    end
    
    def parse_fax_numbers(fax_numbers,who,fax_array)
      if fax_numbers.present?
        faxs = fax_numbers.split(';')  
        faxs.each do |num|
          num = num.strip
          fax = {"fax":num,"who":who}
          if !fax_array.include? fax
            fax_array.push(fax)
          end
        end
      end
    end

    def fax_send(req)  
      facility = req.get_value('Facility')
      facility_fax = facility.get_value('Fax Number')
      fax_numbers = []
      parse_fax_numbers(facility_fax, facility.name, fax_numbers)
      if facility.get_value('Also Send Physician')
        physician = req.get_value('Requesting Physician')
        physician_fax = req.get_value('Requesting Physician').get_value('Fax Number')
        parse_fax_numbers(physician_fax, "#{physician.get_value('First Name')} #{physician.get_value('Last Name')}", fax_numbers)
      end
      fax_result = ''
      if fax_numbers.present?
        system_settings = LIS.system_settings
        address_array = system_settings.get_value('Lab Address').split("\n")
        address = "#{address_array.join(' <br/> ')}"
        logo = LIS.file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path))

        fax_numbers.each do |fn|
          begin
            faxNumber = fn[:fax]
            if system_settings.get_value('Use Fax Cover Sheet')
              begin
                address = address.encode("UTF-8")
              rescue Encoding::UndefinedConversionError
                # couldn't encode
              end

              coversheet = context.run_custom_report('Fax Cover Sheet', req,'Fax Cover Sheet') do |extra_data|
                extra_data['Lab Name'] = system_settings.get_value('Lab Name')
                extra_data['Lab Address'] = address
                extra_data['Lab Logo'] = logo
                extra_data['to'] = fn[:who]
                extra_data['to_fax'] = faxNumber
                extra_data['to_phone'] = ""
                extra_data['to_date'] = Time.now
                extra_data['regarding'] = "Test Results"
                extra_data['Comments'] = "Test Results are ready."
              end         
              context.send_fax(faxNumber, req.get_value('Fax Cover Sheet'), {batch:true,batch_delay:3})
              context.send_fax(faxNumber, req.get_value('Report File'),{batch:true,batch_delay:3})
              fax_result.concat("Sent Cover Sheet and Fax to #{faxNumber}, #{fn[:who]} \n");
            else
              context.send_fax(faxNumber, req.get_value('Report File'))
              fax_result.concat("Sent Fax to #{faxNumber}, #{fn[:who]} \n");
            end        
          rescue => e
            raise "Could Not Send Fax. There was an error attempting to send a Fax.<br/> #{e}"
          end
        end
      end
      return fax_result
    end

    def sms_send(req)
      facility = req.get_value('Facility')
      phones = [facility.get_value('Phone Number')]
      if facility.get_value('Also Send Physician')
        phones << req.get_value('Requesting Physician').get_value('Phone Number')
      end
      phones.each{|phone|
        begin
          context.send_sms(phone, "Results are ready for Requisition #{req.name}. To download the report, please go to our Client Portal at #{context.base_url}")
        rescue => e
          Rails.logger.error("Error sending SMS: #{e.message}\n#{e.backtrace.join("\n")}")
          raise "Could not send SMS. An error occured when attempting to send SMS:<br>#{e.message}".html_safe
        end
      }
    end
    
  end
end