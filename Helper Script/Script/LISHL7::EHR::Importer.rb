module LISHL7
  
  module EHR
    module Importer
      #include LISHL7
      extend self

      def find_by_attr(sub_type,attr,value,like = false)
        query = context.search_query(subject_type: sub_type) do |qb|
          q = qb.and
          if like
            q << qb.compare(attr, :like, "%#{value}%")
          else
            q << qb.compare(attr, :eq, value)
          end
          q
        end
        return context.find_subjects(query: query).first
      end

      def find_by_attrs(sub_type,attrs,values)
        begin
          query = context.search_query(subject_type: sub_type) do |qb|
            q = qb.and
            attrs.each_with_index { |attr,index|
              q << qb.compare(attr, :eq, values[index])
              }
            q
          end
          return context.find_subjects(query: query).first
        rescue Exception=>e
          Rails.logger.info "Bad query: #{query.inspect} Error: #{e}"
        end
      end


      def lookup_requisition(alt_req_id)
        r = find_by_attr('Requisition', 'External Source ID', alt_req_id)
        return r
      end

      def lookup_facility(facility)
        f = find_by_attr('Facility', 'name', facility)
        return f
      end

      def lookup_test_panel(test_code)
        t = find_by_attr('Test Panel', 'Code', test_code)
        return t
      end

      def lookup_test(test_code)
        t = find_by_attr('Test', 'Code', test_code)
        return t
      end

      def lookup_patient(patient_id,facility)
        if facility
          p = find_by_attrs('Patient',['MRN','Facility'],[patient_id,facility])
          if !p
            p = find_by_attrs('Patient',['External Source ID','Facility'],[patient_id,facility])
          end
        end
        return p
      end

      def lookup_physician(physician_npi, facility)
        p = if facility.present?
          find_by_attrs('Physician', ['NPI Number','Facility'], [physician_npi,facility])
        else
          find_by_attr('Physician', 'NPI Number', physician_npi)
        end
        return p
      end

      def lookup_diagnoses(diagnosis_code)
        d = find_by_attr('Diagnosis', 'Code', diagnosis_code)
        return d
      end

      def lookup_specimen_types(specimen_type_code)
        if !specimen_type_code.present?
          specimen_type_code = "UR" #Default to Urine per spec sheet.
        end
        s = find_by_attr('Specimen Type', 'name', specimen_type_code, true)
        return s
      end

      def lookup_insurance_company(insurance_company)
        i = find_by_attr('Insurance Company', 'name', insurance_company, true)
        return i
      end

      def lookup_poc_test(test)
        t = find_by_attr('Point of Care Test', 'Code', test, true)
        return t
      end

      def lookup_medication(medication)
        t = find_by_attr('Medication', 'Description', medication)
        return t
      end

      def comment_check(str)
        begin
          if str.include? "Medical Necessity"
            return true
          elsif str.split.first.include? "Medications"
            return false
          end
          return true
        rescue NoMethodError=>e
          Rails.logger.info "Comment was Nil."
        end
      end

      def get_comments(hl7_processor,msg,requisition = nil)
        comments = []
        msg[:OBR].each do |obr|
          obr[:NTE].each do |nte|
            #KIPU sends medications via comments, pull those out.
            if comment_check(nte[:comment])
              comments.push(nte[:comment])
            end
          end
        end
        if !requisition.nil?
          requisition.set_value('Comments',comments.join(','))
        end
      end

      def create_insurance_company(insurance)
        ins_name = insurance[:name]
        code = insurance[:id]
        Rails.logger.info("create_insurance_company: #{insurance.inspect}")
        address = ''
        if insurance[:address].present?
          address.concat("#{insurance[:address][0]}").concat("\n")
          address.concat("#{insurance[:address][1]} ").concat("#{insurance[:address][2]}, ")
          address.concat("#{insurance[:address][3]} ").concat("#{insurance[:address][4]}")
        end
        phone = insurance[:phone_number][0]

        ins = context.create_subject('Insurance Company', name: ins_name) do |v|
          v.set_value('Payer ID', code)
          v.set_value('Address', address) if address.present?
          v.set_value('Phone Number', phone)
          v.advance_workflow('Insurance Company', 'Active')
        end

        return ins
      end
    
      def set_insurance_information(hl7_processor, msg, target, create_ins)
        bill_to = hl7_processor.get_bill_to(msg)
        #bill_to = "Patient/Self Pay" if bill_to != "Insurance"
        Rails.logger.info("set_insurance_information: bill_to = #{bill_to}")
        if bill_to == "Insurance"
          target.set_value('Bill To',bill_to)
          insurance = hl7_processor.get_insurance_companies(msg)
          insured_name = if insurance["0"][:insured_name].is_a?(String)
            insurance["0"][:insured_name]
          elsif insurance["0"][:insured_name].is_a?(Array)
            n = insurance["0"][:insured_name][1]
            n += " #{insurance["0"][:insured_name][2]}" if insurance["0"][:insured_name][2].present?
            n += " #{insurance["0"][:insured_name][0]}"
          else
            ''
          end
          relation_to_insured = insurance["0"][:relation]
#          begin
            i = lookup_insurance_company("#{insurance["0"][:name]}")
            if i
              target.set_value('Insurance Company 1',i)
              target.set_value('Policy #1',"#{insurance["0"][:policy_number]}")
              target.set_value('Group #1',"#{insurance["0"][:group_number]}")
              target.set_value('Relation To Insured', relation_to_insured) if relation_to_insured.present?
              target.set_value('Subscriber Name', insured_name) if insured_name.present? && relation_to_insured != 'Self'
            else
              if create_ins
                new_ins = create_insurance_company(insurance["0"])
                #warnings.push("Created Insurance Company #{insurance["0"][:name]} as it was not found.")
                target.set_value('Insurance Company 1',new_ins)
                target.set_value('Policy #1',"#{insurance["0"][:policy_number]}")
                target.set_value('Group #1',"#{insurance["0"][:group_number]}")
                target.set_value('Relation To Insured', relation_to_insured) if relation_to_insured.present?
                target.set_value('Subscriber Name', insured_name) if insured_name.present? && relation_to_insured != 'Self'
              else
                raise "Could Not Find Insurance Company #{insurance["0"][:name]}"
              end
            end
=begin
 Validation will fail later if we suppress error(s) here
          rescue Exception=>e
            Rails.logger.info "[HL7 Order Import] There was an error processing insurance information: #{e.message}\n#{e.backtrace.join("\n")}"
            if !target.get_value("Relation To Insured").present?
              target.set_value("Relation To Insured","Other")
            end
          end
=end
        end
      end

      def create_patient(hl7_processor,facility,msg,create_ins)
        patient_msg = msg[:PID]
        #Facility*
        #Let the System Auto-Generate MRN
        #if !patient_msg[:patient_id]
        #  mrn = hl7_processor.find_patient_id(patient_msg) #MRN* - patient_id/alt_patient_id
        #else
        #  mrn = patient_msg[:patient_id]
        #end
        alt_mrn = hl7_processor.find_patient_id(patient_msg) #External Source ID - alt_patient_id
        firstname = patient_msg[:patient_name][1] #First Name* - patient_name[1]
        middlename = patient_msg[:patient_name][2] #Middle Name - patient_name[2]
        lastname = patient_msg[:patient_name][0] #Last Name* - patient_name[0]
        dob = patient_msg[:patient_dob] #DOB* - patient_dob
        sex = patient_msg[:admin_sex] #Sex* - admin_sex
        phone_number = patient_msg[:phone_home][0] #Phone Number* - phone_home
        address = ''
        address.concat("#{patient_msg[:address][0]}").concat("\n")
        address.concat("#{patient_msg[:address][1]} ").concat("#{patient_msg[:address][2]}, ")
        address.concat("#{patient_msg[:address][3]} ").concat("#{patient_msg[:address][4]}")

        patient = context.create_subject('Patient') do |p|
          p.set_value('First Name',firstname)
          p.set_value('Middle Name',middlename)
          p.set_value('Last Name',lastname)
          p.set_value('External Source ID',alt_mrn)
          p.set_value('Facility',facility)
          p.set_value('DOB',dob)
          p.set_value('Sex',sex)
          p.set_value('Phone Number',phone_number)
          p.set_value('Address',address)

          set_insurance_information(hl7_processor, msg, p, create_ins)
        end

        return patient
      end

      def update_patient(patient,hl7_processor,facility,msg,create_ins)
        patient_msg = msg[:PID]
        #Facility*
        firstname = patient_msg[:patient_name][1] #First Name* - patient_name[1]
        middlename = patient_msg[:patient_name][2] #Middle Name - patient_name[2]
        lastname = patient_msg[:patient_name][0] #Last Name* - patient_name[0]
        dob = patient_msg[:patient_dob] #DOB* - patient_dob
        sex = patient_msg[:admin_sex] #Sex* - admin_sex
        phone_number = patient_msg[:phone_home][0] #Phone Number* - phone_home
        address = ''
        address.concat("#{patient_msg[:address][0]}").concat("\n")
        address.concat("#{patient_msg[:address][1]} ").concat("#{patient_msg[:address][2]}, ")
        address.concat("#{patient_msg[:address][3]} ").concat("#{patient_msg[:address][4]}")

        begin
          patient.set_value('First Name',firstname)
          patient.set_value('Middle Name',middlename)
          patient.set_value('Last Name',lastname)
          patient.set_value('Facility',facility)
          patient.set_value('DOB',dob)
          patient.set_value('Sex',sex)
          patient.set_value('Phone Number',phone_number)
          patient.set_value('Address',address)
        rescue Exception=>e
          Rails.logger.info "There was a failure updating the patient. #{e}"
        end
        set_insurance_information(hl7_processor, msg, patient, create_ins)
        return patient
      end

      def get_collection_date(msg)
        collection_date = ''
        tz = Time.zone
        msg[:OBR].each do |obr|
          collection_date = DateTime.strptime(obr[:observation_date],'%Y%m%d%H%M')
          collection_date = ActiveSupport::TimeWithZone.new(nil, tz, collection_date)
          break;
        end
        collection_date
      end

      def get_alternate_req_id(msg)
        #alt_id = "#{msg[:MSH][:message_control_id]}"
        #KIPU uses placer_order_number
        alt_id = "#{msg[:ORC][:placer_order_number]}"
        alt_id
      end

      def create_req(hl7_processor,msg,system_settings)
        create_pat = system_settings[:create_patient]
        update_pat = system_settings[:update_patient]
        create_ins = system_settings[:create_insurance]
        errors = []
        warnings = []
        collection_date = get_collection_date(msg)
        #if msg[:ORC][:date_time_of_transaction]
        #   collection_date = DateTime.strptime(msg[:ORC][:date_time_of_transaction],'%Y%m%d%H%M')
        #end

        f = lookup_facility("#{msg[:MSH][:sending_facility]}")
        if !f
          errors.push("Could Not Find Facility #{msg[:MSH][:sending_facility]}\n")
        end

        req_lookup_id = get_alternate_req_id(msg)
        requisition = lookup_requisition(req_lookup_id)
        if !requisition
          requisition = context.create_subject('Requisition', skip_after_create_final: true) do |req|
            req.set_value('External Source ID',get_alternate_req_id(msg))
            req.set_value('Collection Date', collection_date)
            req.set_value('Facility',f) if f
            #req.set_value('Received Date', Time.now)
          end
        else
          warnings.push("Requisition #{msg[:MSH][:message_control_id]} already exists in the system. Updating Requisition information with import. \n")
        end

        npi_number = msg[:ORC][:ordering_provider][0]
        if npi_number
          ph = lookup_physician("#{npi_number}", f)
          if ph
            requisition.set_value('Requesting Physician',ph)
          else
            errors.push("Could Not Find Requesting Physician by NPI #{npi_number}\n")
          end
        else
          errors.push("Could Not Find Find Requesting Physician no NPI Provided.\n")
        end

        patient_id = hl7_processor.find_patient_id(msg[:PID])
        p = lookup_patient("#{patient_id}",f)
        if p
          patient_dob = Date.parse msg[:PID][:patient_dob]
          if patient_dob == p.get_value('DOB')
            if update_pat
              update_patient(p,hl7_processor,f,msg,create_ins)
            end
            requisition.set_value('Patient',p)
          else
            errors.push("Could Not Find Patient with id #{patient_id} and Date of Birth of #{patient_dob}\n")
          end
        else
          if create_pat
            if errors.size == 0
              p = create_patient(hl7_processor,f,msg,create_ins)
              requisition.set_value('Patient',p)
              warnings.push("Created Patient #{patient_id}")
            end
          else
            errors.push("Could Not Find Patient by id #{patient_id}\n")
          end
        end

        diagnoses = []
        hl7_processor.generate_diagnosis_codes(msg[:DG1]).each do |code|
          code = hl7_processor.clean_diagnosis_code(code)
          d = lookup_diagnoses(code)
          if d
            diagnoses.push(d)
          else
            warnings.push("Could Not Find Diagnosis Code #{code}")
          end
        end
        if diagnoses.size > 0
          requisition.set_value('Diagnoses',diagnoses)
        end

        specimen = []
        hl7_processor.generate_speciment_source(msg[:OBR]).each do |spec|
          s = lookup_specimen_types(spec)
          if s
            specimen.push(s)
          else
            warnings.push("Could Not Find Specimen Type #{spec}")
          end
        end
        if specimen.size > 0
          requisition.set_value('Specimen Types',specimen)
        end

        meds = []
        medications = hl7_processor.get_medications(msg)
        medications.each do |med|
          m = lookup_medication(med)
          if m
            meds.push(m)
          else
            warnings.push("Could Not Find Medication #{med}")
          end
        end
        if meds.size > 0
          requisition.set_value('Medications',meds)
        end

        tests = []
        test_panel_count = 0
        test_panel_names = []
        hl7_processor.generate_test_codes(msg[:OBR]).each do |test|
          tp = lookup_test_panel(test)
          if tp
            requisition.add_value('Test Panels',tp)
            tp.get_value('Tests').each do |tc|
              tests.push(tc)
            end
            test_panel_count = test_panel_count + 1
            test_panel_names.push("#{tp.name}")
          else
            t = lookup_test(test)
            if t
              tests.push(t)
            else
              errors.push("Could Not Find Test Code #{test}\n")
            end
          end
        end

        if tests.size > 0
          requisition.set_value('Tests', tests)
        end

        begin
          get_comments(hl7_processor,msg,requisition)
        rescue Exception=>e
          Rails.logger.info "There was a problem Retreiving comments"
        end

        set_insurance_information(hl7_processor, msg, requisition, create_ins)
        
        if errors.size > 0
          requisition.destroy
          requisition = nil
        else
          requisition.validate
          #start_workflow('Requisition',requisition)
          Requisition.submit(requisition)
          #if test_panel_count > 1
          #  requisition.set_value('Comments',"The following Test Panels were selected #{test_panel_names.join(', ')}. Only 1 Test Panel is allowed.")
          #  context.advance_workflow('Requisition','On Hold',requisition)
          #else
            context.advance_workflow('Requisition','Submitted',requisition)
          #end
        end

        return {errors:errors.join("\n"), warnings:warnings.join("\n"), req:requisition, patient:p}
      end

      #POC Tests
      def lookup_poc_result(observation_value)
        result = 'Not Tested'
        if !observation_value.nil?
          if observation_value.strip == "N"
            result = 'Negative'
          elsif observation_value.strip == "P"
            result = 'Positive'
          end
        end
        return result
      end

      def create_poc_tests(hl7_processor,msg,requisition = nil)
        tz = Time.zone
        poc_test_result = nil
        if !requisition.nil?
          patient = requisition.get_value('Patient')
          if patient
            poc_test_results = requisition.get_value('Point of Care Test Results')

            msg[:OBR].each do |obr|
              testing_date = DateTime.strptime(obr[:observation_date],'%Y%m%d%H%M')
              testing_date = ActiveSupport::TimeWithZone.new(nil, tz, testing_date)

              if !poc_test_results.nil? && poc_test_results.size > 0
                poc_test_result = poc_test_results[0]
              end
              if poc_test_result.nil?
                poc_test_result = context.create_subject('Point of Care Test Result') do |v|
                  v.set_value('Patient', patient)
                  v.set_value('Testing Date', testing_date)
                  v.set_value('Requisition', requisition)
                end
              end

              if poc_test_result.get_value("Point of Care Test Result Entries").size == 0
                poc_tests = SubjectType.find_by_name('Point of Care Test')
                poc_tests.subjects.each do |s|
                  res = context.create_subject('Point of Care Test Result Entry') do |v|
                    v.set_value('Point of Care Test Result', poc_test_result)
                    v.set_value('Point of Care Test', s)
                    v.set_value('PoC Result', 'Not Tested')
                  end
                end #Poc Tests
              end
              Rails.logger.info("===> OBXes: #{obr[:OBX].inspect}")
              #optimizing happened here
              obr[:OBX].each do |obx|
                test_lookup = lookup_poc_test(obx[:observation_id][0])
                test_result = lookup_poc_result(obx[:observation_value])
                if test_lookup
                  query = context.search_query(subject_type: 'Point of Care Test Result Entry') do |qb|
                    q = qb.and
                    q << qb.compare('Point of Care Test', :eq, test_lookup)
                    q << qb.compare('Point of Care Test Result', :eq, poc_test_result)
                    q
                  end
                  result = context.find_subjects(query: query).first
                  if result
                    result.set_value('PoC Result', test_result)
                  end
                end
              end
            end #MSG
            Requisition::ReflexRules.maybe_run_reflex_rules(requisition)
          else
            Rails.logger.info "[EMR Order Processor]::Could not find Patient Ids #{msg[:PID][:patient_id]}, #{msg[:PID][:alt_patient_id]}. POC Test Results where not processed."
          end #patient
        else
          Rails.logger.info "[EMR Order Processor]::No requisition was specified, POC Test Results where not processed."
        end #requisition
      end #def

      def importHL7(provider_configuration)

        provider_settings = {
          create_patient: provider_configuration.get_value('Create Patient'),
          update_patient: provider_configuration.get_value('Update Patient From Order'),
          create_insurance: provider_configuration.get_value('Create Insurance From Order'),
          use_poc_test: LIS.system_settings.get_value('Use Point of Care Tests')
        }
        
        hl7_processor = Processor.new
        hl7Data = provider_configuration.download_hl7_files
        message = if hl7Data[:response].present?
          hl7Data[:response].concat("\n\n")
        else
          ''
        end
        hl7Data[:data].each do |data|
          #Need to fix those nasty formatted Windows Files.
          Rails.logger.info("LISHL7::EHR::Importer: Processing HL7 Raw Data:\n#{data.inspect}")
          data = hl7_processor.cleanse(data)
          msg = hl7_processor.data_parse(data)
          Rails.logger.info("LISHL7::EHR::Importer: Processing HL7 Data:\n#{data.inspect}")
          Rails.logger.info("LISHL7::EHR::Importer: Processing HL7 Message:\n#{msg.inspect}")
          if msg[:MSH][:message_type] == "ORM^O01"
            req = {req:nil}
            begin
              Subject.transaction do
                req = create_req(hl7_processor, msg, provider_settings)
                if provider_settings[:use_poc_test]
                  create_poc_tests(hl7_processor, msg, req[:req])
                end

                message.concat("Errors:" + req[:errors] + "\n")
                if(req[:errors].present?)
                  message.concat("Requisition creation aborted due to Errors.\n")
                end
                message.concat("Warnings:" + req[:warnings])
              end
            rescue Exception=>e
              Rails.logger.error("Error \"#{e.message}\" while processing HL7 Order\n#{e.backtrace.join("\n")}")
              message += "\n" unless message.empty?
              message += "There was a problem processing the order: #{e}"
            end

            ehr_order = context.create_subject('EHR Order') do |order|
              order.set_value('EHR Server Configuration', provider_configuration)
              order.set_value('Comments', message)

              alt_req_id = get_alternate_req_id(msg) rescue nil
              order.set_value('External Source ID', alt_req_id) if alt_req_id.present?
              order.set_value('Requisition', req[:req]) if !req[:req].nil?
              order.set_value('Patient', req[:patient]) if !req[:patient].nil?
            end

            f = hl7_processor.create_file(ehr_order.name, data)
            ehr_order.set_value('File',f)
            File.delete(f)
          end  # if ORM
        end  # ...data...each
    	message
      end
      
    end
  end
end