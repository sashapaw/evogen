require 'xlsxtream'
module SpreadsheetImporter
  extend self

  def sheets
    ['Contacts', 'Facility Types', 'Facilities', 'Physicians', 'Insurance Companies', 'Patients', 'POC Tests', 'Confirmation Tests',
      'Medications', 'Specimen Types', 'Analyte Types', 'Analytes', 'Reference Ranges', 'Flags', 'Test Types', 'Test Classes', 'Tests', 'Test Panels']
  end
  
  def process(file, modes)
    c = context
    c.init_progress(modes.size + 1)
    report = Hash.new('Not processed')
    data_reader = context.open_spreadsheet_file_simple(file)
    c.advance_progress
    sheets.each{|sheet|
      mode = modes[sheet]
      if mode
        begin
          Subject.transaction{
            created, updated, skipped = self.send("process_#{sheet.downcase.tr(' ','_')}", data_reader, mode)
            report[sheet] = sheet == 'Reference Ranges' ? "#{created} created, #{updated} deleted" : "#{created} created, #{updated} updated, #{skipped} skipped"
          }
          c.advance_progress
        rescue Exception => e
          Rails.logger.error e
          report[sheet] = "Error: #{e}"
          break
        end
      end
    }
    ('<table class="client_table">' + sheets.map{|sheet| "<tr><td>#{sheet}:</td><td>#{report[sheet]}</td></tr>"}.join + '</table>').html_safe
  end
  
  def export(selected_sheets)
    c = context
    c.init_progress(selected_sheets.size)
    file = Tempfile.new(['lis','.xlsx'], Report::DIR)
    ::Xlsxtream::Workbook.open(file) do |xlsx|
      selected_sheets.each{|sheet_name|
        xlsx.write_worksheet sheet_name do |sheet|
          self.send("export_#{sheet_name.downcase.tr(' ','_')}", sheet)
          c.advance_progress
        end
        }
    end
    file
  end
  
  def process_contacts(data_reader, mode)
    data_reader.reset(sheet: 'Contacts', raw_columns: ['Phone Number'])
    c = context
    st = c.find_subject_type('Contact')
    required = required_fields(st)
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      check_required_fields(st, required, row)
      subj = subject_by_attrs(st, 'EMail'=>row['EMail'])
      if subj && mode == :skip
        skipped += 1
      else
        values = attrs(row, ['Full Name', 'EMail', 'Phone Number', 'Address', 'Comments'])
        if subj.nil?
          subj = c.create_subject(st) {|s|
            set_props(s, values)
          }
          created += 1
        else
          if update_props(subj, values)
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end

  def export_contacts(sheet)
    simple_export(sheet, 'FROM Contact WHERE terminated is false', ['Full Name', 'EMail', 'Phone Number', 'Address', 'Comments'])
  end

  def process_facility_types(data_reader, mode)
    data_reader.reset(sheet: 'Facility Types')
    simple_process(data_reader, mode, context.find_subject_type('Facility Type'), [])
  end

  def export_facility_types(sheet)
    simple_export(sheet, 'FROM "Facility Type" WHERE terminated is false', [:name])
  end

  def process_facilities(data_reader, mode)
    data_reader.reset(sheet: 'Facilities', raw_columns: ['Phone Number', 'Fax Number'])
    c = context
    facility_type_st = c.find_subject_type('Facility Type')
    contact_st = c.find_subject_type('Contact')
    simple_process(data_reader, mode, c.find_subject_type('Facility'),
      'RURO_CLIENT'=>nil, 'Contact Name'=>nil, 'Contact Email'=>nil, 'Phone Number'=>nil, 'Fax Number'=>nil, 'Address'=>nil, 'Comments'=>nil,
      'Facility Type'=>->(v){subject_by_name(facility_type_st, v, true)},
      'Additional Contacts'=>->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|v|
        s = subject_by_attrs(contact_st, 'EMail'=>v)
        raise "There is no Contact by EMail: #{v}" unless s
        s
      }}) {|subj|
      c.advance_workflow('Facility', 'Active', subj)
      Facility.find_or_create_group(subj)
    }
  end

  def export_facilities(sheet)
    simple_export(sheet, 'FROM Facility WHERE terminated is false', [:name, 'RURO_CLIENT', 'Contact Name', 'Contact Email', 'Facility Type', 'Phone Number', 'Fax Number', 'Address', 'Comments', 'Additional Contacts'],
      'Additional Contacts'=>->(v){v.map{|m|m.get_value('EMail')}.join("\n")})
  end

  def process_physicians(data_reader, mode)
    data_reader.reset(sheet: 'Physicians', raw_columns: ['Phone Number', 'Fax Number', 'NPI Number', 'UPIN Number', 'License'])
    c = context
    st = c.find_subject_type('Physician')
    required = required_fields(st)
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      check_required_fields(st, required, row)
      facility = subject_by_name('Facility', row['Facility'], true)
      subj = subject_by_attrs(st, 'Facility'=>facility, 'NPI Number'=>row['NPI Number'])
      if subj && mode == :skip
        skipped += 1
      else
        values = attrs(row, ['First Name', 'Middle Name', 'Last Name', 'Address', 'Phone Number', 'Fax Number',
          'Contact Email', 'NPI Number', 'UPIN Number', 'License']
        ).merge(
          'Facility' => facility
        )
        if subj.nil?
          subj = c.create_subject(st) {|s|
            set_props(s, values)
          }
          c.advance_workflow('Physician', 'Active', subj)
          SystemHooks.physician_created(subj)
          created += 1
        else
          if update_props(subj, values)
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end

  def export_physicians(sheet)
    simple_export(sheet, 'FROM Physician WHERE terminated is false', ['Facility', 'First Name', 'Middle Name', 'Last Name', 'Address', 'Phone Number', 'Fax Number',
          'Contact Email', 'NPI Number', 'UPIN Number', 'License'])
  end

  def process_insurance_companies(data_reader, mode)
    data_reader.reset(sheet: 'Insurance Companies', raw_columns: ['Code', 'Payer ID', 'Phone Number'])
    simple_process(data_reader, mode, context.find_subject_type('Insurance Company'), ['Code', 'Payer ID', 'Address', 'Phone Number', 'HCPCS Code G048X Billing', 'Comments'])
  end

  def export_insurance_companies(sheet)
    simple_export(sheet, 'FROM "Insurance Company" WHERE terminated is false', [:name, 'Code', 'Payer ID', 'Address', 'Phone Number', 'HCPCS Code G048X Billing', 'Comments'])
  end

  def process_patients(data_reader, mode)
    data_reader.reset(sheet: 'Patients', raw_columns: ['MRN', 'External Source ID', 'Phone Number',
      'Policy #1', 'Group #1', 'Policy #2', 'Group #2', 'Policy #3', 'Group #3', 'Primary Physician NPI'])
    c = context
    st = c.find_subject_type('Patient')
    required = required_fields(st)
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      check_required_fields(st, required, row)
      facility = subject_by_name('Facility', row['Facility'], true)
      mrn = row['MRN']
      subj = subject_by_attrs(st, 'Facility'=>facility, 'MRN'=>mrn)
      if subj && subj.get_value('DOB') != row['DOB']
        raise "Patient form '#{facility.name}' with MRN=#{mrn} has different DOB: #{subj.get_value('DOB')} vs #{row['DOB']}"
      end
      if subj && mode == :skip
        skipped += 1
      else
        values = attrs(row, ['External Source ID', 'First Name', 'Middle Name', 'Last Name', 'DOB', 'Sex', 'Address', 'Phone Number', 'Bill To',
          'Policy #1', 'Group #1', 'Policy #2', 'Group #2', 'Policy #3', 'Group #3', 'Primary Physician NPI',
          'Relation To Insured', 'Subscriber Name', 'Subscriber DOB', 'Subscriber Sex', 'Subscriber Address',
          'Billing Facility Address', 'Sent to Billing']
        ).merge(
          'Facility' => facility,
          'MRN' => mrn,
          'Insurance Company 1' => subject_by_name('Insurance Company', row['Insurance Company 1'], true),
          'Insurance Company 2' => subject_by_name('Insurance Company', row['Insurance Company 2'], true),
          'Insurance Company 3' => subject_by_name('Insurance Company', row['Insurance Company 3'], true),
          'Billing Facility' => subject_by_name('Facility', row['Billing Facility'], true)
        )
        if subj.nil?
          subj = c.create_subject(st) {|s|
            set_props(s, values)
          }
          created += 1
        else
          if update_props(subj, values)
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end
  
  def export_patients(sheet)
    simple_export(sheet, 'FROM "Patient" WHERE terminated is false', ['Facility', 'MRN',
      'External Source ID', 'First Name', 'Middle Name', 'Last Name', 'DOB', 'Sex', 'Address', 'Phone Number', 'Bill To',
      'Insurance Company 1', 'Policy #1', 'Group #1',
      'Insurance Company 2', 'Policy #2', 'Group #2',
      'Insurance Company 3', 'Policy #3', 'Group #3', 'Primary Physician NPI',
      'Relation To Insured', 'Subscriber Name', 'Subscriber DOB', 'Subscriber Sex', 'Subscriber Address',
      'Billing Facility', 'Billing Facility Address', 'Sent to Billing'])
  end

  def process_poc_tests(data_reader, mode)
    data_reader.reset(sheet: 'POC Tests', raw_columns: ['Code'])
    simple_process(data_reader, mode, context.find_subject_type('Point of Care Test'), ['Code'])
  end
  
  def export_poc_tests(sheet)
    simple_export(sheet, 'FROM "Point of Care Test" WHERE terminated is false', [:name, 'Code'])
  end

  def process_confirmation_tests(data_reader, mode)
    data_reader.reset(sheet: 'Confirmation Tests')
    simple_process(data_reader, mode, context.find_subject_type('Confirmation Test'), [])
  end
  
  def export_confirmation_tests(sheet)
    simple_export(sheet, 'FROM "Confirmation Test" WHERE terminated is false', [:name])
  end

  def process_medications(data_reader, mode)
    data_reader.reset(sheet: 'Medications', raw_columns: ['Code'])
    c = context
    st = c.find_subject_type('Medication')
    required = required_fields(st)
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      check_required_fields(st, required, row)
      code = row['Code']
      subj = subject_by_code(st, code)
      if subj && mode == :skip
        skipped += 1
      else
        if subj.nil?
          subj = c.create_subject(st) {|s|
            set_props(s, 'Code' => code, 'Description' => row['Description'])
          }
          created += 1
        else
          if update_props(subj, 'Description' => row['Description'])
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end
  
  def export_medications(sheet)
    simple_export(sheet, 'FROM Medication WHERE terminated is false', ['Code', 'Description'])
  end

  def process_specimen_types(data_reader, mode)
    data_reader.reset(sheet: 'Specimen Types', raw_columns: ['Code'])
    simple_process(data_reader, mode, context.find_subject_type('Specimen Type'), ['Code'])
  end

  def export_specimen_types(sheet)
    simple_export(sheet, 'FROM "Specimen Type" WHERE terminated is false', [:name, 'Code'])
  end

  def process_analyte_types(data_reader, mode)
    data_reader.reset(sheet: 'Analyte Types')
    simple_process(data_reader, mode, context.find_subject_type('Analyte Type'), [])
  end

  def export_analyte_types(sheet)
    simple_export(sheet, 'FROM "Analyte Type" WHERE terminated is false', [:name])
  end

  def process_analytes(data_reader, mode)
    data_reader.reset(sheet: 'Analytes', raw_columns: ['Instrument Code', 'Medications', 'Propagate Outcome To'])
    c = context
    st = c.find_subject_type('Analyte')
    required = required_fields(st)
    antype_st = c.find_subject_type('Analyte Type')
    unit_st = c.find_subject_type('Unit')
    instrument_type_st = c.find_subject_type('Instrument Type')
    medication_st = c.find_subject_type('Medication')
    specimen_st = c.find_subject_type('Specimen Type')
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      name = row['Name']
      raise "Name is required for '#{st.name}' type" if name.blank?
      subj = subject_by_name(st, name)
      if subj && mode == :skip
        skipped += 1
      else
        values = attrs(row, 'Analyte Type'=> ->(v){subject_by_name(antype_st, v, true)},
          'Unit'=> ->(v){subject_by_name(unit_st, v, true)}, 'Result Choices'=>nil, 'Instrument Code'=>nil,
          'Instrument Type'=> ->(v){subject_by_name(instrument_type_st, v, true)}, 'Decimal Places'=>nil, 'Low Limit'=>nil, 'High Limit'=>nil,
          'Medications'=> ->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|code|subject_by_code(medication_st, code, true)}},
          'Report Notes'=>nil, 'Non-Reportable'=>nil, 'Trigger Normalization'=>nil, 'Qualitative'=>nil, 'Hide Value'=>nil,
          'Propagate Outcome To'=> ->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|v|subject_by_name(st, v, true)}},
          'Specimen Type'=> ->(v){subject_by_name(specimen_st, v, true)})
        check_required_fields(st, required, values)
        if subj.nil?
          subj = c.create_subject(st, name: name) {|s|
            set_props(s, values)
          }
          created += 1
        else
          if update_props(subj, values)
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end
  
  def export_analytes(sheet)
    simple_export(sheet, 'FROM Analyte WHERE terminated is false', [:name, 'Analyte Type', 'Qualitative', 'Unit', 'Result Choices',
      'Instrument Code', 'Instrument Type', 'Decimal Places', 'Low Limit', 'High Limit', 'Medications', 'Specimen Type', 'Report Notes',
      'Non-Reportable', 'Trigger Normalization', 'Hide Value', 'Propagate Outcome To'], 'Medications'=>->(v){v.map{|m|m.get_value('Code')}.join("\n")})
  end

  def process_reference_ranges(data_reader, mode)
    data_reader.reset(sheet: 'Reference Ranges')
    c = context
    st = c.find_subject_type('Range')
    required = required_fields(st)
    created, deleted, skipped = 0, 0, 0
    ranges_by_analyte = {}
    data_reader.each{|row|
      check_required_fields(st, required, row)
      analyte = subject_by_name('Analyte', row['Analyte'], true)
      values = attrs(row, ['Critical Low', 'Low', 'High', 'Critical High', 'Age From', 'Age To', 'Age Unit', 'Gender'])
      ranges = ranges_by_analyte[analyte]
      ranges_by_analyte[analyte] = ranges = [] if ranges.nil?
      ranges << values
    }
    ranges_by_analyte.each{|analyte, ranges|
      subjs = subjects_by_attrs(st, {'Analyte'=>analyte})
      subjs.each{|subj| subj.destroy; deleted+=1} if subjs
      subjs = ranges.map{|range|
        subj = c.create_subject(st) {|s|
          set_props(s, range.merge('Analyte'=>analyte))
        }
        created += 1
        subj
      }
      Analyte::Submit.validate_ranges(subjs)
    }
    [created, deleted, skipped]
  end
  
  def export_reference_ranges(sheet)
    simple_export(sheet, 'FROM Range WHERE terminated = false AND Analyte->terminated = false', ['Analyte', 'Critical Low', 'Low', 'High', 'Critical High',
      'Age From', 'Age To', 'Age Unit', 'Gender'])
  end

  def process_flags(data_reader, mode)
    data_reader.reset(sheet: 'Flags')
    c = context
    analyte_st = c.find_subject_type('Analyte')
    simple_process(data_reader, mode, c.find_subject_type('Flag'), 'Description'=>nil, 'Triggered When Value'=>nil, 'Color for Consistent'=>nil,
      'Color for Inconsistent'=>nil, 'Qualitative'=>nil, 'Trigger Formula'=>nil, 'Outcome Rule'=>nil,
      'Analytes'=>->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|v|subject_by_name(analyte_st, v, true)}})
  end

  def export_flags(sheet)
    simple_export(sheet, 'FROM Flag WHERE terminated = false', [:name, 'Description', 'Triggered When Value', 'Color for Consistent', 'Color for Inconsistent', 'Qualitative', 'Trigger Formula', 'Outcome Rule'])
  end

  def process_test_types(data_reader, mode)
    data_reader.reset(sheet: 'Test Types', raw_columns: ['Name'])
    simple_process(data_reader, mode, context.find_subject_type('Test Type'), [])
  end

  def export_test_types(sheet)
    simple_export(sheet, 'FROM "Test Type" WHERE terminated = false', [:name])
  end

  def process_test_classes(data_reader, mode)
    data_reader.reset(sheet: 'Test Classes', raw_columns: ['Name', 'CPT Codes'])
    cpt_p = ->(v){
      s = (v || '').split("\n").map(&:strip).reject(&:blank?).join(', ')
      s.empty? ? nil : s
    }
    simple_process(data_reader, mode, context.find_subject_type('Test Class'), 'CPT Codes'=>cpt_p)
  end

  def export_test_classes(sheet)
    simple_export(sheet, 'FROM "Test Class" WHERE terminated = false', [:name, 'CPT Codes'])
  end

  def process_tests(data_reader, mode)
    data_reader.reset(sheet: 'Tests', raw_columns: ['Code', 'CPT Codes', 'Medications', 'Diagnoses', 'Test Type', 'Test Class'])
    c = context
    st = c.find_subject_type('Test')
    required = required_fields(st)
    cpt_p = ->(v){
      s = (v || '').split("\n").map(&:strip).reject(&:blank?).join(', ')
      s.empty? ? nil : s
    }
    analyte_st = c.find_subject_type('Analyte')
    analytes_p = ->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|name|subject_by_name(analyte_st, name, true)}}
    specimen_st = c.find_subject_type('Specimen Type')
    specimen_p = ->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|name|subject_by_name(specimen_st, name, true)}}
    facility_st = c.find_subject_type('Facility')
    facility_p = ->(v){subject_by_name(facility_st, v, true)}
    medication_st = c.find_subject_type('Medication')
    medications_p = ->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|code|subject_by_code(medication_st, code, true)}}
    diagnosis_st = c.find_subject_type('Diagnosis')
    diagnoses_p = ->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|code|subject_by_code(diagnosis_st, code, true)}}
    test_type_st = c.find_subject_type('Test Type')
    test_type_p = ->(v){subject_by_name(test_type_st, v, true)}
    test_class_st = c.find_subject_type('Test Class')
    test_class_p = ->(v){subject_by_name(test_class_st, v, true)}
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      check_required_fields(st, required, row)
      code = row['Code']
      subj = subject_by_code(st, code)
      if subj && mode == :skip
        skipped += 1
      else
        values = attrs(row, 'Description'=>nil, 'CPT Codes'=>cpt_p, 'Analytes'=>analytes_p, 'Specimen Types'=>specimen_p,
          'Facility'=>facility_p, 'Medications'=>medications_p, 'Diagnoses'=>diagnoses_p,
          'Non-Reportable'=>nil, 'Non-Orderable'=>nil, 'Test Type'=>test_type_p, 'Test Class'=>test_class_p, 'Comments'=>nil)
        if subj.nil?
          subj = c.create_subject(st) {|s|
            set_props(s, values.merge('Code'=>code))
          }
          created += 1
        else
          if update_props(subj, values)
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end
    
  def export_tests(sheet)
    simple_export(sheet, 'FROM Test WHERE terminated is false', ['Code', 'Description', 'CPT Codes', 'Analytes', 'Specimen Types',
      'Facility', 'Medications', 'Diagnoses', 'Non-Reportable', 'Non-Orderable', 'Test Type', 'Test Class', 'Comments'],
      'Medications'=>->(v){v.map{|m|m.get_value('Code')}.join("\n")}, 'Diagnoses'=>->(v){v.map{|m|m.get_value('Code')}.join("\n")})
  end

  def process_test_panels(data_reader, mode)
    data_reader.reset(sheet: 'Test Panels', raw_columns: ['Code', 'Tests'])
    c = context
    st = c.find_subject_type('Test Panel')
    required = required_fields(st)
    test_st = c.find_subject_type('Test')
    tests_p = ->(v){(v || '').split("\n").map(&:strip).reject(&:blank?).map{|code|subject_by_code(test_st, code, true)}}
    facility_st = c.find_subject_type('Facility')
    facility_p = ->(v){subject_by_name(facility_st, v, true)}
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      check_required_fields(st, required, row)
      code = row['Code']
      description = row['Description']
      subj = subject_by_attrs(st, 'Code'=>code, 'Description'=>description)
      if subj && mode == :skip
        skipped += 1
      else
        values = attrs(row, 'Tests'=>tests_p, 'Facility'=>facility_p)
        if subj.nil?
          subj = c.create_subject(st) {|s|
            set_props(s, values.merge('Code'=>code, 'Description'=>description))
          }
          created += 1
        else
          if update_props(subj, values)
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end
    
  def export_test_panels(sheet)
    simple_export(sheet, 'FROM "Test Panel" WHERE terminated = false', ['Code', 'Description', 'Tests', 'Facility'],
      'Tests'=>->(v){v.map{|m|m.get_value('Code')}.join("\n")})
  end

  def simple_process(data_reader, mode, st, headers)
    c = context
    required = required_fields(st)
    created, updated, skipped = 0, 0, 0
    data_reader.each{|row|
      name = row['Name']
      raise "Name is required for '#{st.name}' type" if name.blank?
      subj = c.find_subject(subject_type: st, name: name)
      if subj && mode == :skip
        skipped += 1
      else
        values = attrs(row, headers)
        check_required_fields(st, required, values)
        if subj.nil?
          subj = c.create_subject(st, name: name) {|s|
            set_props(s, values)
          }
          yield subj if block_given?
          created += 1
        else
          if update_props(subj, values)
            updated += 1
          else
            skipped += 1
          end
        end
      end
    }
    [created, updated, skipped]
  end

  def attrs(row, headers)
    h = {}
    if headers.is_a?(Hash)
      headers.each{|header, p| h[header] = p.nil? ? row[header] : p.call(row[header])}
    else
      headers.each{|header| h[header] = row[header]}
    end
    h
  end

  def set_props(subj, hash)
    hash.each{|k, v|
      if v.present?
      	subj.set_value(k, v)
      end
    }
  end

  def update_props(subj, hash)
    subj.disable_uniq_constraints do
      hash.each{|k, v|
        if v.present?
          begin
            subj.set_value(k, v)
          rescue Exception => e
            raise "Cannot update '#{k}' UDF of #{subj.subject_type.name} name='#{subj.name}': #{e}"
          end
        else
          subj.remove_prop(k)
        end
      }
    end
    changed = subj.changed?
    subj.save!
    changed
  end
  
  def subject_by_name(st, name, required = false)
    return nil if name.blank?
    subj = context.find_subject(subject_type: st, name: name)
    raise "No #{st} by name: '#{name}'" if required && subj.nil?
    subj
  end

  def subject_by_code(st, code, required = false)
    return nil if code.blank?
    subj = subject_by_attrs(st, 'Code'=>code)
    raise "No #{st} by Code: '#{code}'" if required && subj.nil?
    subj
  end

  def subject_by_attrs(subj_type, attrs)
    subjs = subjects_by_attrs(subj_type, attrs)
    if subjs && subjs.size > 1
      a = []
      attrs.each{|k,v| a << "'#{k}' = #{v}"}
      raise "There are #{subjs.size} #{subj_type.plural_name} where '#{a.join(', ')}'"
    end
    subjs ? subjs.first : nil
  end

  def subjects_by_attrs(sub_type, attrs)
    c = context
    begin
      query = c.search_query(subject_type: sub_type) do |qb|
        q = qb.and
        attrs.each {|k, v|
          q << qb.compare(k, :eq, v)
        }
        q
      end
      c.find_subjects(query: query)
    rescue Exception=>e
      Rails.logger.info "Bad query: #{query.inspect} Error: #{e}"
    end
  end

  def required_fields(st)
    st.type_property_links.reject{|tpl| !tpl.required}.map{|tpl| tpl.property.display_name}
  end
  
  def check_required_fields(st, fields, values)
    fields.each{|f|
      raise "'#{f}' field is required for '#{st.name}' type" if values[f].nil?
    }
  end
  
  def simple_export(sheet, query, udfs, serializers={})
    sheet << udfs.map{|udf| udf == :name ? 'Name' : udf}
    c = context
    props = udfs.map{|udf| udf.is_a?(String) ? c.find_prop(udf) : udf}
    count = c.count_subjects(query: query)
    n = (count - 1) / 1000 + 1
    for i in (0...n)
      offset = i * 1000
      limit = [offset + 1000, count].min - offset
      subjects = c.find_subjects(query: query, order: {id: :asc}, offset: offset, limit: limit) || []
      subjects.each{|subj|
        sheet << props.map{|prop|
          if prop == :name
            subj.name
          else
            v = subj.get_value(prop)
            if v.nil?
              nil
            else
              ser = serializers[prop.display_name]
              if ser
                ser.call(v)
              else
                case prop.gui_control
                  when Property::GC_DATE
                    v.strftime('%Y-%m-%d')
                  when Property::GC_CHECKBOX
                    v ? 'Yes' : 'No'
                  when Property::GC_LIST
                    v.join("\n")
                  when Property::GC_DATETIME
                    v.iso8601
                  when Property::GC_FILE, Property::LIMS_SUBJECT, Property::LIMS_USERGROUP
                    v.name
                  when Property::LIMS_SUBJECTS
                    v.map(&:name).join("\n")
                  when Property::LIMS_USER
                    v.username
                  when Property::LIMS_USERS
                    v.map(&:username).join("\n")
                  when Property::LIMS_PROPERTY
                    v.display_name
                  else
                    v
                end
              end
            end
          end
        }
      }
    end
  end
  
end