
# Evaluate test results for a requisition
# If a single analyte returns "Detected" the requisition is "Positive"
# Otherwise, Negative
def set_molecular_diagnostics_pass_or_fail(req)
  positive = false
  req.get_value('Samples').each do |sample|
    sample.get_value('Test Results').each do |result|
      result_value = result.get_value('Value')
      if result_value == "Detected" 
      	positive = true
        break
      end
    end
  end
  if positive == true
    req.set_value('Result','Positive')
  else
    req.set_value('Result','Negative')
  end
end

def set_toxicology_pass_or_fail(req)
  req.get_value('Samples').each do |sample|
    sample.get_value('Test Results').each do |result|
      flag = result.get_value('Flag')
      if flag.nil? || flag == "NEG"
        #If flag is already set positive we dont want to overwrite it. 
        if req.get_value('Result').nil? 
          req.set_value('Result','Negative')
        end
      else
        req.set_value('Result','Positive')
        break
      end
      #Rails.logger.info
    end
  end
end


