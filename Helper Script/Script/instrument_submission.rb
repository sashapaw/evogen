data = JSON.parse(params[:data].to_json)

begin
  submission = Instrument::Submission.new(data)
  raise "Unknown Instrument" if submission.implementation.blank?
  if data['data'].has_key? 'query'
    submission.implementation.get_query_response(data['data'])
  else
    submission.implementation.import_data(data['data'])
  end
ensure
  submission.done
end