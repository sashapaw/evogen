module Patient
  # Evogen patient form layout
  module CustomLayout
    include Patient::DefaultLayout
    extend self

    def general_panel_patient(subj, pat_subj, can_facility, system_settings)
      fields = super(subj, pat_subj, can_facility, system_settings)
      fields.shift
      patient_custom_fields(subj).each{|f| fields << f}
      fields
    end
    
    def patient_custom_fields(subj)
      [
        field_row([
          udf('EMail', subj, labelWidth:160, flex:1.75),
          udf('Evening Phone', subj, labelWidth:110)
        ]),
        field_row([
          udf('Patient Status', subj, fieldLabel:'Status', labelWidth:160, columnWidth:0.45),
          udf('Date of Death', subj, labelWidth:120, columnWidth:0.3, react:{
            shown_when: 'value == "Deceased"',
            only: 'Patient Status'
          })
        ], layout:'column'),
        udf('Ethnicities', subj, labelWidth:160, anchor:'100%', 
                  allowCreate:true, addCustomFields:encode_fields(set_level(2, udfs_for_subject_type('Ethnicity', nil)))),
        udf('Parent/Guardian Full Name', subj, labelWidth:160, anchor:'100%')
      ]
    end
    
  end
  
end