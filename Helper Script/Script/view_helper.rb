# used to generate custom view for a subject type
def each_tool_for_subject_type(subject_type, &block)
  subject_type = find_subject_type(subject_type)
  subject_type.flows.each{|flow|
    (flow.state_defs || []).each{|state_def|
      (state_def.tools || []).each{|tool|
        yield(tool, state_def, flow)
      }
    }
  }
end

def tools_by_names(subject_type, tool_names)
  subject_type = find_subject_type(subject_type)
  tools = {}
  each_tool_for_subject_type(subject_type){|tool|
    tools[tool.name] = tool if tool_names.include? tool.name
  }
  result = []
  tool_names.each{|name|
    tool = tools[name]
    result << tool if tool
  }
  result
end

def subject_tool_configs(subj, tool_names)
  tools = {}
  available_tools(subj).each{|tool|
    tools[tool.name] = tool if tool_names.include? tool.name
  }
  configs = []
  tool_names.each{|name|
    tool = tools[name]
    configs << {name: tool.name, description: tool.description, glyph: tool.get_glyph, tid: tool.id, oid: subj.id, flags: tool.flags} if tool
  }
  configs
end