module Instrument
  class BC_DxC < ASTM

    def initialize(instrument, result_entry = nil, ctx = context)
      super(instrument, result_entry, ctx)
      @flags_map = get_flag_map
      @analyte_map = get_analyte_map
      @specimen_type_map = get_specimen_type_map
    end

    def get_flag_map
      {
        'HI' => 'H',
        'LO' => 'L',
        'CL' => 'CL',
        'CH' => 'CH',
        'H2' => 'CH',
        'H3' => 'CH',
        'H4' => 'CH',
        'L2' => 'CL',
        'L3' => 'CL',
        'L4' => 'CL'
      }
    end

    def get_analyte_map
      {
        '01A' => 'Sodium',
        '01B' => 'Potassium',
        '02A' => 'CO2',
        '02B' => 'CO2 (Enzymatic)',
        '03E' => 'Creatinine',
        '03F' => 'Creatinine (Standardized)',
        '03H' => 'Creatinine (Enzymatic)',
        '04A' => 'Chloride',
        '05A' => 'Urea Nitrogen (BUN)',
        '05D' => 'Urea Nitrogen (BUNm)',
        '06A' => 'Glucose, Urine',
        '06D' => 'Glucose',
        '06F' => 'Glucose, Urine (GLUH)',
        '07A' => 'Total Protein',
        '07B' => 'Micro Total Protein',
        '07D' => 'Total Protein (modular)',
        '08A' => 'Albumin',
        '08D' => 'Albumin (modular)',
        '08E' => 'Prealbumin',
        '08M' => 'Microalbumin',
        '09D' => 'Calcium (ISE)',
        '10C' => 'Pancreatic Amylase',
        '10D' => 'Amylase (G7)',
        '11A' => 'Bilirubin, Total',
        '12A' => 'Bilirubin, Direct',
        '13B' => 'Infinity Lithium',
        '14D' => 'Hemoglobin A1c (A1c3)',
        '14F' => 'Hemoglobin, Total (Hb3)',
        '14M' => 'Hemoglobin, Total (Hb-)',
        '14N' => 'Hemoglobin A1c (A1c-)',
        '15A' => 'Homocysteine',
        '24B' => 'Apolipoprotein B',
        '24C' => 'Apolipoprotein A-1',
        '30A' => 'Aspartate Aminotransferase',
        '30B' => 'Aspartate Aminotransferase (P5P) (AST-)',
        '31A' => 'Alanine Aminotransferase',
        '31B' => 'Alanine Aminotransferase (P5P) (ALT-)',
        '32A' => 'Creatine Kinase',
        '32B' => 'Creatine Kinase (NAC) (CK-)',
        '33A' => 'Lactate Dehydrogenase (LDH)',
        '34A' => 'Lactate Dehydrogenase (LD-P)',
        '34B' => 'Low-density Lipoprotein (LDL)',
        '35A' => 'Alkaline Phosphatase',
        '36A' => 'Gamma Glutamyltransferase',
        '41A' => 'Uric Acid',
        '42B' => 'Triglycerides',
        '42C' => 'Triglycerides GPO Blanked',
        '43D' => 'Phosphorus (modular)',
        '43E' => 'Phosphorus',
        '44A' => 'Cholesterol',
        '46B' => 'Iron, Total',
        '48A' => 'Magnesium',
        '50M' => 'Haptoglobin',
        '51B' => 'Immunoglobulin G (IgG)',
        '52B' => 'Immunoglobulin A (IgA)',
        '53B' => 'Immunoglobulin M (IgM)',
        '54A' => 'Ammonia',
        '55B' => 'Lactate',
        '56A' => 'Ethanol, Urine',
        '60A' => 'Lipase',
        '61M' => 'Complement C3',
        '61R' => 'Complement C4',
        '62B' => 'Digoxin',
        '63A' => 'Methaqualone',
        '64A' => 'Methadone',
        '65A' => 'Propoxyphene',
        '66A' => 'Phencyclidine',
        '67C' => 'Phenobarbital',
        '68C' => 'Phenytoin',
        '69C' => 'Theophylline',
        '70C' => 'Tobramycin',
        '71B' => 'Transferrin',
        '72C' => 'Gentamicin',
        '72M' => 'Vancomycin',
        '73A' => 'Urea Nitrogen (UREA)',
        '73D' => 'Urea Nitrogen (UREAm)',
        '79B' => 'Iron Binding Capacity',
        '83E' => 'HDL Cholestrol',
        '84A' => 'Amphetamines',
        '85A' => 'Barbiturates',
        '86B' => 'Benzodiazepine',
        '87A' => 'Cocaine Metabolite',
        '88A' => 'Cannabinoid 100 ng',
        '88B' => 'Cannabinoid 20 ng',
        '88C' => 'Cannabinoid 50 ng',
        '89A' => 'C-Reactive Protein (CRP)',
        '89E' => 'High Sensitivity C-Reactive Protein',
        '89G' => 'C-Reactive Protein (C-RP)',
        '90A' => 'T3 Uptake',
        '91A' => 'Free T4 (Thyroxine)',
        '92A' => 'Opiate 300 ng',
        '92B' => 'Opiate 2000 ng',
        '93B' => 'Antistreptolysin-O',
        '94B' => 'Salicylates',
        '94M' => 'Acetaminophen',
        '95A' => 'Valproic Acid',
        '98A' => 'Carbamazepine',
        '99B' => 'Icterus',
        '99C' => 'Lipemia',
        '99D' => 'Hemolysis',
        '99E' => 'Cartridge Chemistry Wash Solution'
      }
    end

    def get_specimen_type_map
      {
        'Amniotic' => 'Amniotic',
        'Blood' => 'Blood',
        'Cerebrospinal Fluid' => 'CSF',
        'Cervical' => 'Cervical',
        'Other' => 'Other',
        'Plasma' => 'Plasma',
        'Saliva' => 'Saliva',
        'Serum' => 'Serum',
        'Synovial' => 'Synovial',
        'Timed Urine' => 'Timed',
        'Urethral' => 'Urethral',
        'Urine (Random)' => 'Urine'
      }
    end

    def get_patient(requisition)
      pdata = get_patient_data(requisition)
      if pdata
        {
          'type': 'P',
          'fields': [
                {'idx': 2, 'value': 1, 'name': :sequence_number},
                {'idx': 3, 'value': '', 'name': :practice_assigned_patient_id},
                {'idx': 4, 'value': pdata[:id], 'name': :laboratory_assigned_patient_id},
                {'idx': 5, 'value': '', 'name': :patient_id},
                {'idx': 6, 'value': "#{pdata[:last_name]}^#{pdata[:first_name]}", 'name': :name},
                {'idx': 7, 'value': ''},
                {'idx': 8, 'value': "#{pdata[:birth_date]}^#{pdata[:age]}^Y", 'name': :birthdate},
                {'idx': 9, 'value': pdata[:sex], 'name': :sex},
                {'idx': 10, 'value': '', 'name': :race},
                {'idx': 11, 'value': ''},
                {'idx': 12, 'value': ''},
                {'idx': 13, 'value': ''},
                {'idx': 14, 'value': '', 'name': :attending_physician},
                {'idx': 15, 'value': ''},
                {'idx': 16, 'value': ''},
                {'idx': 17, 'value': ''},
                {'idx': 18, 'value': ''},
                {'idx': 19, 'value': ''},
                {'idx': 20, 'value': ''},
                {'idx': 21, 'value': ''},
                {'idx': 22, 'value': ''},
                {'idx': 23, 'value': ''},
                {'idx': 24, 'value': ''},
                {'idx': 25, 'value': ''},
                {'idx': 26, 'value': ''}
            ]
        }
      else
        nil
      end
    end

    def get_orders(sample, test_results)
      specimen_type = get_specimen_type(sample)
      test_ids = get_test_ids(test_results)
      requested_at = get_time_requested_at

      [{
        'order': {
            'type': 'O',
            'fields': [
                {'idx': 2, 'value': 1, 'name': :sequence_number},
                {'idx': 3, 'value': "#{sample.barcode_tag}^0^0", 'name': :specimen_id},
                {'idx': 4, 'value': ''},
                {'idx': 5, 'value': test_ids, 'name': :universal_test_id_internal},
                {'idx': 6, 'value': 'R', 'name': :priority},
                {'idx': 7, 'value': requested_at, 'name': :requested_at},
                {'idx': 8, 'value': '', 'name': :collected_at},
                {'idx': 9, 'value': ''},
                {'idx': 10, 'value': ''},
                {'idx': 11, 'value': ''},
                {'idx': 12, 'value': 'N'},
                {'idx': 13, 'value': ''},
                {'idx': 14, 'value': ''},
                {'idx': 15, 'value': ''},
                {'idx': 16, 'value': specimen_type},
                {'idx': 17, 'value': ''},
                {'idx': 18, 'value': ''},
                {'idx': 19, 'value': '1^1'}
            ]
        }}]
    end

    def get_analyte_code(field)
      field['value'].match(/\^\^\^(.*?)\^\d*/)[1]
    end

    def get_result_value(test_result, field)
      val = field['value']
      test_result.set_value('Value', val) unless val.blank?
    end

    def get_range(test_result, field)
      nil
    end

    def get_flags(test_result, field)
      flag_name = field['value']
      flag = @flags_map.fetch(flag_name) rescue nil
      if flag.blank?
        @result_entry.new_message("Flag", "Could not find Flag: #{flag_name}") unless flag_name == 'NR'
      else
        test_result.set_value('Flag', flag)
      end
    end

    def get_status(test_result, field)
      nil
    end

    def get_comments(test_result, res)
      nil
    end
  end
end