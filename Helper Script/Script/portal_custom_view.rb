def custom_view(context)  
  facility = get_value('Facility')
  fid = facility.id
  req_filters = [
    UIUtils.search_filter('name', op: 'contains', label: 'Requisition ID'),
    UIUtils.search_filter('"Requesting Physician"->name', op: 'contains', label: 'Requesting Physician'),
    UIUtils.search_filter('"Samples"->name', op: 'contains', label: 'Sample  Number'),
    #Add date fields here like "Collection Date" and "Submitted" date when the date search field works
    {
      "label": "Patient Name or MRN",
      "path": UIUtils.prop_path('Patient->name'),
	  "op":"contains"
    },
    #UIUtils.search_filter('Result', op: 'contains', label: 'Pass/Fail')
  ].to_json
  
  communication_filters = [
    UIUtils.search_filter('"Requisition"->name', op: 'contains', label: 'Requisition'),
    #UIUtils.search_filter('Date Updated', op: 'eq', label: 'Date Updated'),
    # Uncomment this when searching by date fields is fixed
  ].to_json

  
  req_items = [1, 7, 31].map{|d|
    start = (Time.now - d.days).strftime('%Y-%m-%dT%H:%M:%S')
    html = subjects_in_states(context, 'Requisitions', 'Requisition', 'req', "Facility = #{fid} AND created_at >= '#{start}'", [
      {state: 'Draft', qb_filters: req_filters, bg1:'#cc4748'},
      {state: 'Submitted', qb_filters: req_filters},
      {state: 'Received', text:'In The Lab', qb_filters: req_filters},
      {state: 'Released;Accepted', qb_filters: req_filters, bg1: '#4DC441'}
    ])
    {xtype: 'box', html:html}
  }

  com_html = subjects_in_states(context, 'Communications', 'Communication', 'com', "Facility = #{fid}", [
    {state: 'Open', qb_filters: communication_filters, bg1:'#cc4748'},
    {state: 'Closed', qb_filters: communication_filters}
  ])
  
  d = Date.today
  three_days = (d+3).to_s
  sch_html = subjects_in_states(context, 'Patient Testing', 'Testing Schedule', 'sch', "Patient->Facility = #{fid} AND terminated = false", [
    {text: 'Upcoming', state: 'Scheduled', query: '"Testing Date" >= today AND "Testing Date" <= \''+ three_days +'\'', bg1:'#4DC441'},
    {text: 'Missed', state: 'Scheduled', query: '"Testing Date" < today', bg1:'#cc4748'}
  ])
  
  {xtype: 'container', style: 'background:white', layout: {type: 'hbox', align: 'stretch'}, items: [
    {xtype: 'panel', title:"Welcome, #{User.curr_user.fullname}", width: 310, layout: 'auto',
      tbar:[
        #'->',
        {text:'Day', glyph:0xf0b0, scale:'large', toggleGroup:'group1', handler:UIUtils.raw_js('function() { 
            this.ownerCt.ownerCt.down("#the_req_container").getLayout().setActiveItem(0);
            //Ext.getCmp("the_result_container").getLayout().setActiveItem(0);
        }') },
        {text:'Week', glyph:0xf0b0, scale:'large', pressed:true, toggleGroup:'group1', handler:UIUtils.raw_js('function() {
            this.ownerCt.ownerCt.down("#the_req_container").getLayout().setActiveItem(1);
            //Ext.getCmp("the_result_container").getLayout().setActiveItem(1);
        }') },
        {text:'Month', glyph:0xf0b0, scale:'large', toggleGroup:'group1', handler:UIUtils.raw_js('function() {
            this.ownerCt.ownerCt.down("#the_req_container").getLayout().setActiveItem(2);
            //Ext.getCmp("the_result_container").getLayout().setActiveItem(2);
        }') }
      ],     
      overflowX: 'hidden', overflowY: 'auto', items: [
      {xtype: 'container', width: 77*3, itemId:'the_req_container', layout:'card', activeItem:1, items:req_items},
      #result_buttons(context, fid),
      
      {xtype:'label', html:'<br>'},
      {xtype: 'panel', layout: 'hbox', items: [
        {xtype: 'box', html: com_html, hidden:!facility.get_value('Online Communication Tools').present?, width: 77*2},
        {xtype: 'box', html: sch_html, hidden:!facility.get_value('Use Patient Testing Scheduler').present?, width: 77*2}
      ]}
    ]},
    {xtype:'label', width:5},
    right_panel(context, fid)
  ]}.to_json
end

# def result_buttons(context, fid)
#   now = Time.now
#   test_results_filters = [
#      UIUtils.search_filter('"Patient"->name', op: 'contains', label:'Patient Name or MRN'),
#      UIUtils.search_filter('"Requesting Physician"->name', op: 'contains', label:'Requesting Physician'),
#   ].to_json
#
#   items = []
#   [1, 7, 31].each{|d|
#     start = (now - d.days).strftime('%Y-%m-%dT%H:%M:%S')
#     html = subjects_in_states(context, 'Test Results', 'Requisition', "req#{d}_", "Facility = #{fid} AND #state = 'Released' AND \"Report Date\" >= '#{start}'", [
#       {text: 'Positive', qb_filters: test_results_filters, query: "Result = 'Positive'", bg1: '#cc4748'},
#       {text: 'Negative', qb_filters: test_results_filters, query: "Result = 'Negative'", bg1: '#4DC441'},
#       {text: 'Not Tested', qb_filters: test_results_filters, query: "Result = null"}
#     ])
#     items << {xtype: 'box', html: html}
#   }
#   {xtype: 'container', width: 77*3, id:'the_result_container', layout: 'card', activeItem:1, items: items}
# end

def right_panel(context, fid)
  facility = get_value('Facility')
  show_sch = facility.get_value('Use Patient Testing Scheduler').present? #context.find_subjects(query: 'FROM "System Settings"').first.get_value('Use Patient Testing Scheduler')
  if show_sch
    tool_names = ['Process', 'Reschedule', 'Cancel'] #'New Requisition', 
    tools = tools_by_names(context, 'Testing Schedule', tool_names)
    buttons = tools.map{|tool|
      {xtype: 'gridflowtool', scale:'large', glyph: tool.get_glyph, tool: {id: tool.id, name: tool.name}}
    }
    schedules = context.find_subjects(query: "FROM \"Testing Schedule\" WHERE Patient->Facility = #{fid} AND terminated = false AND #state = 'Scheduled' AND \"Testing Date\" = today") || []
    data = schedules.map{|schedule|
      tools = {}
      context.available_tools(schedule).select{|tool|
        tool_names.include?(tool.name)
      }.each{|tool|
        tools[tool.name] = {text: tool.name, tid: tool.id, oid: schedule.id, flags: tool.flags}
      }
      patient = schedule.get_value('Patient')
      pat_name = "#{patient.get_value('First Name')} #{patient.get_value('Last Name')}"
      {
        a: {tools: tools}, 
        c0: schedule.get_value('Testing Date').strftime('%m/%d/%Y'), a0: {oid: schedule.id},
        c1: schedule.get_value('Patient').get_value('PIC'), a1: {oid: schedule.id},
        c2: UIUtils.h(pat_name), a2: {oid: schedule.id},
        c3: schedule.get_value_size('PoC Test'), 
        c4: schedule.get_value_size('Lab Test'), leaf: true
      } 
    }

    {xtype: 'panel', flex: 1, flex: 1, layout: {type: 'vbox', align: 'stretch'}, items: [
      #{xtype: 'box', html: sch_html}, overflowY: 'auto',
      {xtype: 'treegrid', flex: 1, autoScroll:true, viewConfig: {forceFit: true, deferEmptyText: false},
        title: 'Today Testings', tbar: buttons,
        emptyText:'<div align="center">No Testing for Today</div>', 
        columns:[
          {text:'Due', width:140}, 
          {text:'PIC', width:55}, 
          {text:'Patient'},
          {text:'PoC', width:50, renderer:UIUtils.raw_js('function(val) { return (val ? "<i style=\"color:black;\" class=\"fa fa-check\"></i>" : "") }') },
          {text:'Lab', width:50, renderer:UIUtils.raw_js('function(val) { return (val ? "<i style=\"color:black;\" class=\"fa fa-check\"></i>" : "") }') }
        ],
        data: data
      }
    ]}
  else
    data = [
     ['Primary Contact Name', UIUtils.h(facility.get_value('Contact Name') || "")],
     ['Primary Contact Email', UIUtils.h(facility.get_value('Contact Email') || "")],
     ['Phone Number', UIUtils.h(facility.get_value('Phone Number') || "")],
     ['Fax Number', UIUtils.h(facility.get_value('Fax Number') || "")],
     ['Address', UIUtils.h(facility.get_value('Address') || "")],
    ]
    if facility.get_value('Account Manager')
    	data << ['Your Account Manager', UIUtils.h(facility.get_value('Account Manager').name)]
    end

    if facility.get_value('Support Representative')
    	data << ['Your Support Representative', UIUtils.h(facility.get_value('Support Representative').name)]
    end
    
    {xtype: 'panel', title:"Your Facility Information", flex: 1, layout: {type: 'vbox', align: 'stretch'}, items: [
      {xtype: 'grid', flex: 1, hideHeaders:true, 
    	columns:[
	      {dataIndex:'name', width:'30%'},
	      {dataIndex:'value', width:'70%'}
    	],
	    store: {type: 'array', fields:['name', 'value'], data: data}
	  }
    ]}
  end
end

def subjects_in_states(context, title, subj_type_name, search_guid_prefix, query, buttons)
  a = ['<div style="display:inline-block;white-space:nowrap">',
    "<div style=\"font-size:14px; font-weight:bold; color:#888; text-align:center; margin-top:10px;\">#{title}</div>"]
  buttons.each_with_index{|button, i|
    states = button[:state].split(';')
    state = states[0]
    text = button[:text] || state
    q = []
    q << query if query
    q << button[:query] if button[:query]
    
    if states.length == 1
      q << "#state = '#{states[0]}'"
    elsif states.length == 2
      q << "(#state = '#{states[0]}' OR #state = '#{states[1]}')"
    end
    
    where = q.join(' AND ')
    
    cs_query = "FROM \"#{subj_type_name}\" WHERE #{where}"
    Rails.logger.info("CS_QUERY: #{cs_query}")
    c = context.count_subjects(query: cs_query)
    bg = (c > 0 ? button[:bg1] : button[:bg0]) || '#999'
    a << "<div style=\"padding:6px 4px;width:72px;text-align:center;color:white;border-radius:6px;display:inline-block;margin:5px 0px 5px 5px;background:#{bg}\">"
    a << "<div style=\"font-size:12px; padding: 0px 1px 4px 1px;\">#{text}</div>"
    a << "<div style=\"font-size:36px; padding: 0px 6px; color:white;\">"
    
    if c == 0
      a << '0'
    else
      opts = {}
      opts[:explorer_states] = state if state.present?
      if button[:qb_filters].present?
        opts[:need_search_panel] =  true
        opts[:qb_filters] = button[:qb_filters]
      end
      a << "<a href=\"javascript:void(0)\" style=\"color:white; text-decoration:none;\" onmouseover=\"this.style.color='black'\"  onmouseout=\"this.style.color='white'\" onclick=\"RURO.listSubjectsSearchQuery('#{search_guid_prefix}#{i}', '#{subj_type_name}', '#{UIUtils.h(UIUtils.js(where))}', '#{title} (#{text})', #{UIUtils.h(opts.to_json)})\">#{c}</a>";
    end
    a << '</div></div>'
  }
  a << '</div>'
  a.join('')
end


def each_tool_for_subject_type(context, subject_type, &block)
  subject_type = context.find_subject_type(subject_type)
  subject_type.flows.each{|flow|
    (flow.state_defs || []).each{|state_def|
      (state_def.tools || []).each{|tool|
        yield(tool, state_def, flow)
      }
    }
  }
end

def tools_by_names(context, subject_type, tool_names)
  subject_type = context.find_subject_type(subject_type)
  tools = {}
  each_tool_for_subject_type(context, subject_type){|tool|
    tools[tool.name] = tool if tool_names.include? tool.name
  }
  result = []
  tool_names.each{|name|
    tool = tools[name]
    result << tool if tool
  }
  result
end