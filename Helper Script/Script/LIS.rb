require 'base64'
require 'indirizzo/address'

module LIS
  extend self
  
  def as_admin
    cu = User.curr_user
    srcu = @user
    User.curr_user = @user = User.admin
    yield
  ensure
    @user = srcu
    User.curr_user = cu
  end

  def system_settings
    context.find_subject_type('System Settings').subjects.take
  end

  def common_config
    context.find_subject_type('Common Configuration').subjects.take
  end

  def lab_logo
    system_settings.get_value('Lab Logo')
  end

  def parse_email_field(email)
    unless email.nil?
      email.split(%r{;\s*})
    else
      email
    end
  end

  def file_to_base64(file_path)
    Base64.strict_encode64(File.open(file_path, "rb").read)
  end
  
  def validate_email(email)
    return if email.blank?
    raise "Invalid e-mail: \"#{email}\"".html_safe unless EmailValidator.valid?(email)
  end
  
  def validate_address(address_text)
    return if address_text.blank?
    address = Indirizzo::Address.new(address_text)
    raise "Address \"#{address_text}\" is not valid.<br>Please enter valid US postal address.".html_safe unless address.present? && address.valid?
  end

  def validate_address_udf(subj, address_udf_name)
    address_text = subj.get_value(address_udf_name)
    validate_address(address_text)
  end

  def format_value(value, analyte)
    dp = analyte.get_value('Decimal Places')
    begin
      dp ? "%.#{dp.to_i}f" % Float(value) : "#{value.to_s}"
    rescue
      value
    end
  end
  
  def is_lab_user?(user)
    (['Lab Users','Lab Managers','Lab Admins'] & user.user_groups.map(&:name)).present?
  end

end