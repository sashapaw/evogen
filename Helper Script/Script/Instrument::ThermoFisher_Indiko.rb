module Instrument
  class ThermoFisher_Indiko < Common
    
    def import_file(file)
      context.open_spreadsheet_file_simple(file.path, extension: :csv, csv_options: {col_sep: "\t", encoding: 'bom|utf-8'}, skip_blanks: false, headers: ('A'..'S').to_a) {|csv|
        state = :header
        context.init_progress csv.length
        csv.each_with_index {|row, i, row_raw|
          Rails.logger.info("-->> state: #{state} data: #{row_raw.inspect}")
          context.advance_progress
          
          col0 = row_raw[0]
          case state
          when :header
            if col0.blank?
              state = :address
            end
            
          when :address
            if col0.blank?
              state = :table_header
            end
            
          when :table_header
            state = :table
            
          when :table
            next if row_raw.blank? || row_raw.all? {|e| e.blank?}
            yield({sample: row_raw[0], analyte: row_raw[2], value: row_raw[4], unit: row_raw[5], completed_at: row_raw[6], row: "#{i}"})
          end
        }
      }
    end
    
    def preprocess_record(result_data)
      result_data[:completed_at] = DateTime.strptime(result_data[:completed_at], '%m/%d/%Y %I:%M') if result_data[:completed_at].present?
    end
    
  end
end
