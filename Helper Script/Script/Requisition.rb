module Requisition
  extend self

  def poc_view(data, opts={})
    {
      xtype: 'grid',
      columns:[
        {dataIndex:'name', flex:1, header:'Name'}, 
        {dataIndex:'positive', align:'center', header:'Positive', width:100},
        {dataIndex:'negative', align:'center', header:'Negative', width:100},
        {dataIndex:'not_tested', align:'center', header:'Not Tested', width:100}
        ],
      store: {
        type: 'array',
        fields:['uid', 'name', 'positive', 'negative', 'not_tested'],
        data: (data || []).sort{|r1, r2| r1[1] <=> r2[1]}
      }
    }.merge(opts)
  end
  
  def poct_results_tab(subj, height=nil)
    if subj and subj.get_value('Point of Care Test Results').present?
      result = subj.get_value('Point of Care Test Results')[0] # grab first, since this is edit
      tests = result.get_value('Point of Care Test Result Entries').reject(&:terminated?)
      data = []
      tests.each do |u|
        poct = u.get_value('Point of Care Test')
        r = u.get_value('PoC Result')
        data << [
          poct.id, 
          poct.name, 
          r=="Positive" ? "X" : "", 
          r=="Negative" ? "X" : "", 
          r=="Not Tested" ? "X" : ""
        ]
      end # tests->each
    else
      tests = SubjectType.find_by_name('Point of Care Test').subjects.reject(&:terminated?)
      data = tests.map{|u| [u.id, u.name, "", "", "X"]}
    end
    poc_grid(data, 'poc_grid_results_id', height)
  end
    
  def confirmation_tab(subj, height=nil)
    entries = subj && subj.get_value('Confirmation Test Results')
    if entries.present?
      data = []
      entries.each do |u|
        t = u.get_value('Confirmation Test')
        r = u.get_value('PoC Result')
        data << [
          t.id, 
          t.name, 
          r=='Positive' ? 'X' : '', 
          r=='Negative' ? 'X' : '', 
          r=='Not Tested' ? 'X' : ''
        ]
      end
    else
      tests = context.find_subjects(query: 'FROM "Confirmation Test" WHERE terminated = false')
      data = tests.map{|t| [t.id, t.name, '', '', 'X']}
    end
    poc_grid(data, 'confirmation_id', height)
  end
    
  def poc_grid(data, field_id, height=nil)
    data.sort!{|a, b| a[1] <=> b[1]}
    {xtype: 'grid', anchor:'100%', height:height,
      storeGridValues:UIUtils.raw_js("function() {
        var grid = this,
            field = grid.up('activeform').down('##{field_id}'),
            arr = [];
        grid.getStore().each( function(rec) {
          var pair = rec.get('id')+':';
          if (rec.get('positive')) pair+='positive';
          if (rec.get('negative')) pair+='negative';
          if (rec.get('not_tested')) pair+='not_tested';
          arr.push(pair);
        });
        field.setValue(arr.join(','));
      }"),
      tbar:[
        'Click inside the grid to set test results',
        '->',
        {text:'All Negative', handler:UIUtils.raw_js('function() {
          var grid = this.up("grid");
          grid.getStore().each( function(rec) {  rec.set("positive", "");  rec.set("negative", "X");  rec.set("not_tested", "");  });
          grid.getStore().commitChanges();
          grid.storeGridValues();
        }')},
        {text:'All Not Tested', handler:UIUtils.raw_js('function() {
          var grid = this.up("grid");
          grid.getStore().each( function(rec) {  rec.set("positive", "");  rec.set("negative", "");  rec.set("not_tested", "X");  });
          grid.getStore().commitChanges();
          grid.storeGridValues();
        }')}
      ],
      listeners:{
        afterrender:UIUtils.raw_js('
        function(grid) {
          grid.storeGridValues();
        }'),
        cellclick:UIUtils.raw_js('
        function(view, cell, cellIndex, rec, row, recIndex, e) {
          var arr = ["", "Positive", "Negative", "Not Tested"];
          var jarr = ["", "positive", "negative", "not_tested"];
          if (cellIndex > 0) {
            for (var i=1;i!=jarr.length;i++) rec.set(jarr[i], "");
            rec.set(jarr[cellIndex], "X");
            view.getStore().commitChanges();
          }
          view.up("grid").storeGridValues();
  	    }')
      },
      columns:[
        {dataIndex:'name', flex:1, header:'Name'}, 
        {dataIndex:'positive', align:'center', header:'Positive', width:100},
        {dataIndex:'negative', align:'center', header:'Negative', width:100},
        {dataIndex:'not_tested', align:'center', header:'Not Tested', width:100}
      ],
      store: {type: 'array', fields:['id', 'name', 'positive', 'negative', 'not_tested'], data: data}
    }
  end
  
  def create_poc_tests(str, patient, requisition, test_date)
    values = parse_poc_results(str)
    poc = context.create_subject('Point of Care Test Result') do |v|
      v.set_value('Patient', patient)
      v.set_value('Requisition', requisition)
      v.set_value('Testing Date', test_date)
    end

    values.each do |t, r|
      context.create_subject('Point of Care Test Result Entry') do |v|
        v.set_value('Point of Care Test Result', poc)
        v.set_value('Point of Care Test', t)
        v.set_value('PoC Result', r)
      end
    end

    poc
  end

  def edit_poc_tests(str, patient, requisition, test_date)
    poc = (requisition.get_value('Point of Care Test Results') || []).first
    if poc    
      values = parse_poc_results(str)
      poc.get_value('Point of Care Test Result Entries').each do |e|
        t = e.get_value('Point of Care Test')
        v = values[t]
        if v
          e.set_value('PoC Result', v)
          e.save
        end
      end
    else
      poc = Requisition.create_poc_tests(str, patient, requisition, test_date)
    end
    poc
  end

  def create_confirmation(str, requisition)
    values = parse_poc_results(str)
    values.each do |t, r|
      context.create_subject('Confirmation Test Result') do |v|
        v.set_value('Requisition', requisition)
        v.set_value('Confirmation Test', t)
        v.set_value('PoC Result', r)
      end
    end
  end
  
  def edit_confirmation(str, requisition)
    results = requisition.get_value('Confirmation Test Results')
    if results.present?
      values = parse_poc_results(str)
      results.each{|e|
        t = e.get_value('Confirmation Test')
        v = values[t]
        if v
          e.set_value('PoC Result', v)
          e.save
        end
      }
    else
      create_confirmation(str, requisition)
    end
  end
  
  def parse_poc_results(str)
    value_hash = {'positive'=>'Positive', 'negative'=>'Negative', 'not_tested'=>'Not Tested'}
    ret = {}
    str.split(',').map{|pair|
      p = pair.split(':')
      ret[Subject.find(p[0])] = value_hash[p[1]]
    }
    ret
  end

  def patient_dependent_fields(subj, patient, test_codes)
    if subj
      test_codes = subj.get_value('Tests')
      diagnoses = subj.get_value('Diagnoses')
      medications = subj.get_value('Medications')
    end
    facility = subj ? subj.get_value('Facility') : User.curr_user.get_value('Facility')
    facility = nil if facility && facility.get_value('RURO_CLIENT') # do not use Lab as facility
    facility_filter = facility.present? ? " and (Facility = #{facility.id} or Facility is null)" : ''
    
    patient_subject = !subj.nil? && subj.subject_type.name=="Patient"
    if patient
      if subj.get_value('Tests').blank?
        test_codes = patient.get_value('Tests')
      end
      if subj.get_value('Diagnoses').blank?
        diagnoses = patient.get_value('Diagnoses')
      end
      if subj.get_value('Medications').blank?
        medications = patient.get_value('Medications')
      end
    end
    fields = [
      udf('Address', patient, disabled:true, labelWidth:160, height:40),
      udf('Phone Number', patient, fieldLabel: 'Phone', disabled:true, labelWidth:160),
      udf('DOB', patient, fieldLabel: 'Date of Birth', disabled:true, labelWidth:160),
      udf('Test Panels', subj, filter:"terminated is false"+facility_filter, labelWidth:160, react:{
        change: 'this.setFilter(value ? "terminated is false AND (Facility = "+value+" OR Facility is null)" : "terminated is false");',
        only: 'Facility'
      }),
      udf('Tests', subj, defaultValue: test_codes, labelWidth:160, required:true,
        filter:'terminated is false and "Non-Orderable" = null'+facility_filter, react:{
          change: <<-EOJS,
            var me = this;
            if (name === 'Facility') {
              this.setFilter('terminated is false AND "Non-Orderable" = null AND (Facility = '+value+' OR Facility is null)');
            } else {
              if (Ext.isArray(value) && value.length) {
                var newPanels = me.panels ? Ext.Array.difference(value, me.panels) : value;
                if (newPanels && newPanels.length) {
                  RURO.runHelperScript("tests_in_panels", newPanels, function(result) {
                    var resultTests = Ext.decode(result);
                    var tests = data.Tests;
                    var newTests = Ext.Array.filter(resultTests, function(it) {return Ext.Array.indexOf(tests, it.id) < 0;});
                    form.setValue("Tests", tests.concat(newTests));
                  });
                }
              }
              me.panels = value;
            }
EOJS
          only: ['Facility', 'Test Panels']
        }),
      udf('Diagnoses', subj, defaultValue: diagnoses, filter:'#state=Active', labelWidth:160, required:true),
      udf('Medications', subj, defaultValue: medications, labelWidth:160),
      billing_layout(subj, true, false)
    ]
    field_container(fields, defaults: {anchor:'100%', labelWidth:160}, react:{
      init: 'this.setDisabled(!value)',
      change: '
        var me = this;
        me.setDisabled(!value);
        Ext.each(["Tests", "Diagnoses", "Medications", "Bill To", "Address", "Phone Number", "DOB"], function(udf) {
          form.setValue(udf, null);
        });
        if (Ext.isNumber(value)) {
          form.setValuesAsync({id: value, suspendListening: ["Test Panels", "Billing Facility"], udfs: [
            "Address", "Phone Number", "Test Panels", "Tests", "Diagnoses", "Medications", "Bill To",
            "Insurance Company 1", "Policy #1", "Group #1",
            "Insurance Company 2", "Policy #2", "Group #2",
            "Insurance Company 3", "Policy #3", "Group #3",
            "Self Pay Comments", "Primary Physician NPI",
            "Billing Facility", "Billing Facility Address", "Relation To Insured",
            "Subscriber Name", "Subscriber DOB", "Subscriber Sex", "Subscriber Address", "DOB"]});
        }',
      only: 'Patient'
    })
  end
  
  def error_check(params)
    raise "Collection Date can not be in the future" if params['Collection Date'].present? && params['Collection Date'] > Time.now
    raise "Received Date can not be before Collection Date." if !params['Collection Date'].nil? && !params['Received Date'].nil? && params['Collection Date'] > params['Received Date']
  end

  def release(req)
    req.get_value('Pre-Registered Samples').each {|p| p.destroy }
    
    if req.get_value('Reference Lab').present?
        # get the preliminary report out and put it in thread local storage
        ScriptRunner.cache_store[:report_file] = req.get_value('Preliminary Report File')
       # req.remove_prop('Preliminary Report File')
    	req.set_value('Report File',  ScriptRunner.cache_store[:report_file])
    elsif
        req.remove_prop('Preliminary Report File')
    	Requisition::Reports.generate_test_results_report(req, {}, true)

    end
    
  

    ctx = context
    # Release finalized samples
    samples = req.get_value('Samples')
    samples.each do |sample|
      if sample.current_states.map(&:name).include?('In Testing')
        ctx.advance_workflow('Sample', 'Released', sample)
        SystemHooks.sample_released(sample)
      end
    end

    # Release requisition
    ctx.advance_workflow('Requisition', 'Released', req)
    SystemHooks.requisition_released(req)

    # Now that all has been released, lets fax/email/sms
    Communications.send_communication(req)

    # Send results to EHR provider
    LISHL7::EHR.send_results(req,false)

    # Now Generate HL7 and Send to Billing
    patient = req.get_value('Patient')
    unless patient.get_value('Sent to Billing')
      LISHL7::Billing.create_and_send_adt(patient)
      patient.set_value('Sent to Billing',true)
    end
    ret = LISHL7::Billing.create_and_send_dft(req)
    req.save!
    ret
  end
  
  def process_just_poc_tests(patient, poc_grid_results, physician, specimen_types, testing_date)
    ctx = ScriptRunner.context
	p_test_codes = patient.get_value('Tests')
	facility = patient.get_value('Facility')
	s = ctx.create_subject('Requisition') do |v|
      v.set_value('Facility',facility)
      v.set_value('Patient',patient)
      ['Diagnoses', 'Medications', 'Bill To', 'Relation To Insured',
       'Insurance Company 1', 'Policy #1', 'Group #1','Insurance Company 2', 'Policy #2', 'Group #2','Insurance Company 3', 'Policy #3', 'Group #3',
       'Primary Physician NPI', 'Subscriber Name', 'Subscriber DOB', 'Subscriber Sex', 'Subscriber Address', 'Billing Facility', 'Billing Facility Address'].each{|udf|
         v.set_value(udf, patient[udf]) if patient[udf].present?
      }
      v.set_value('Requesting Physician', physician)
      v.set_value('Collection Date', Time.now)
      v.set_value('Specimen Types', specimen_types)
      v.set_value('Tests',p_test_codes) if !p_test_codes.nil?
    end

    poc = Requisition.create_poc_tests(poc_grid_results, patient, s, testing_date)
    #change this method to return if a reflex rule fired
    reflex_fired = Requisition::ReflexRules.maybe_run_reflex_rules(s)
  
    if reflex_fired
      ctx.advance_workflow('Requisition', 'Draft', s)
      SystemHooks.requisition_created(s)
    else
      #We do not need the requisition so lets remove it.
      s.destroy
      s = nil
    end  
    [s, poc]
  end
  
  def submit(subj)
    Requisition::Reports.generate_requisition_report(subj)#,system_settings.get_value('Lab Name'), LIS.file_to_base64(File.absolute_path(system_settings.get_value('Lab Logo').path)), address, system_settings, poct_results, physician_file,patient_file)

    #close_tab_ids(subj.id)

    prop = context.find_prop('Requisition Report')
    message = "<p>Requisition was sucessfully submitted. Requisition ID:  <strong>#{subj.name}</strong><br><br>Click to download <a target='_blank' href='/subjects/attachment/?id=#{subj.id}&pid=#{prop.id}'>Requisition Report</a></p>"

    #show_message(message.html_safe)

    SystemHooks.requisition_submitted(subj)
    message
  end

end