require_script "test_results_helper"

#*
def time_diff_milli(start, finish)
   (finish - start) * 1000.0
end

#*
def create_sample_import_filter(list)
  filter = []
  list.each do |l|    
    filter << "(id = #{l})"
  end
  filter.join(' or ')
end

#*
def open_sample_import(params,sample_imports, msg)
  params[:subject_type] = 'Sample Import'
  params[:query] = create_sample_import_filter(sample_imports)
  params[:search_guid] = "SampleImports"
  params[:tab_name] = "Imported Files"
  show_message(msg)
end

#*
def after_upload(params, subj)
  system_settings = LIS.system_settings
  complete_msg = ''
  subject_list = []
  files = UploaderUtils.parse(params['files'])
  if system_settings.get_value("Test Result Import Script")
    begin
      require_script system_settings.get_value("Test Result Import Script")
      init_progress(files.size)
      files.each_with_data{|file, data|      
        start_time = Time.now
        #Process File is the Executor or Run method in the helper script. 
        obj = process_file(file, data['Sample Import Comments'], system_settings)

        Rails.logger.info "Batch Import: File Process Completed in #{time_diff_milli(start_time, Time.now)}ms"

        subject_list.push(obj[:subject].id)
        complete_msg.concat(obj[:msg]).concat('<hr />')
        advance_progress      
      }
    rescue Exception=>e
      Rails.logger.info("There was a problem processing upload. #{e}")
    ensure
      files.remove
    end
  else
    raise "No Import Script Configured"
  end
  
  open_sample_import(params, subject_list, complete_msg)
end