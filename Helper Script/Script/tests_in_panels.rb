panel_ids = JSON.parse(params[:data])
tests = find_subjects(query:"FROM Test WHERE <-\"Test Panel\"::Tests = (#{panel_ids.join(',')})") || []
tests.map{|t| {id: t.id, name: t.name}}.to_json