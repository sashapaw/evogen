module Flag
  
  module Layout
    include ScriptRunner::UI

    def flag_layout(subj)
      the_name = subj ? subj.name : ''
      encode_fields([
        udf('Name', nil, value:the_name, required:true, anchor:'100%'),
        udf('Description', subj, required:true, anchor:'100%'),
        udf('Triggered When Value', subj, required:true, anchor:'60%', react: {
          change: UIUtils.raw_js(<<EOS)
            function(data, name, value, oldValue) {
              tf = Ext.getCmp('trigger_formula');
              if (value == 'Less Than Critical Low') tf.setValue('{value} < {Critical Low}');
              else if (value == 'Less Than Low') tf.setValue('{value} >= {Critical Low} && {value} < {Low}');
              else if (value == 'Between Low and High') tf.setValue('{value} >= {Low} && {value} <= {High}');
              else if (value == 'Greater Than High') tf.setValue('{value} > {High} && {value} <= {Critical High}');
              else if (value == 'Greater Than Critical High') tf.setValue('{value} > {Critical High}');
              else if (value == 'Other')  tf.setValue('');
            }
EOS
        }
        ),

        udf('Qualitative', subj, react: {
          shown_when: "value == 'Other'",
          only:'Triggered When Value'
        }),
        udf('Trigger Formula', subj, required:true, id:'trigger_formula', anchor:'100%'),

        udf('Outcome Rule', subj, anchor:'100%'),
        {xtype:'colorfield', fieldLabel:'Color for Consistent', required:true, name:'custom[Color for Consistent]', value:subj ? subj.get_value('Color for Consistent') : '000000' },
        {xtype:'colorfield', fieldLabel:'Color for Inconsistent', required:true, name:'custom[Color for Inconsistent]', value:subj ? subj.get_value('Color for Inconsistent') : 'FF0000' },
  
        {xtype:'label', html:'<br><b>Optionally, specify Analyte. Flag calculation will be restricted to that analyte only.</b>'},
        udf('Analytes', subj, anchor:'100%', allowCreate:false)
      ])
    end
    
    extend self
  end
  
end