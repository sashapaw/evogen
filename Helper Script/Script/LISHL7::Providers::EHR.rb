module LISHL7
  
  module Providers
    
    module EHR

      mattr_accessor :config, instance_writer: false, instance_reader: false do
        {
          generate_zps_in_oru: false,
          embed_report_in_oru_as_obx: true,
          pdf_profile: {"obx3":["PDF"],"obx4":"Base64","obx5":["","PDFReport","PDF","Base64"]},
          flag_map: { L:"L", H:"H", A:"A", N:"N", CL:"LL", CH:"HH",NEG:"NEG",POS:"POS" }
        }
      end

      def msh_options(configuration, facility)
        {
          sending_facility: LISHL7.createSegmentString([configuration.get_value('Facility Name'), configuration.get_value('Facility Number')]),
          recv_app: self.name,
          recv_facility: LISHL7.createSegmentString([facility.name, facility.get_value("EHR Account Number")]),
          processing_id: configuration.get_value('HL7 Mode') || 'P',
          version_id: self.get_value('HL7 Version')
        }
      end
      
      module KIPURECORDS
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.deep_merge({
            generate_zps_in_oru: true,
            pdf_profile: {"obx3":["PDF"],"obx4":"Base64","obx5":[""]}
          })
        end

        def msh_options(configuration, facility)
          super.merge({
            recv_facility: facility.name
            })
        end
        
        Providers.generate_config_accessors(self)
      end

      module PracticeFusion
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.deep_merge({
            pdf_profile: {"obx3":["PDF","PDF BASE64"],"obx4":"1","obx5":["","PDFReport","PDF","Base64"]},
            flag_map: { L:"L", H:"H", A:"A", N:"N", CL:"<", CH:">" }
          })
        end

        def msh_options(configuration, facility)
          super.merge({
            recv_facility: LISHL7.createSegmentString([facility.get_value("EHR Account Number"), facility.name])
            })
        end

        Providers.generate_config_accessors(self)
      end
      
      module BestNotes
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.deep_merge({
            pdf_profile: {"obx3":["PDF"],"obx4":"Base64","obx5":[""]},
            flag_map: { L:"L", H:"H", A:"A", N:"N", CL:"<", CH:">",NEG:"NEG",POS:"POS" }
          })
        end

        def msh_options(configuration, facility)
          super.merge({
            recv_app: 'ITF',
            recv_facility: 'HTI'
            })
        end

        Providers.generate_config_accessors(self)
      end
      
      module IndraCare
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.deep_merge({
            embed_report_in_oru_as_obx: false,
            flag_map: { L:"L",H:"H",A:"A", N:"N", CL:"PL", CH:"PH" }
          })
        end

        def msh_options(configuration, facility)
          super.merge({
            recv_facility: 'myOnsite'
            })
        end

        Providers.generate_config_accessors(self)
      end
      
      module ZenCharts
        mattr_accessor :config, instance_writer: false, instance_reader: false do
          parent.config.deep_merge({
            pdf_profile: {"obx3":["PDF"],"obx4":"Base64","obx5":[""]}
          })
        end

        Providers.generate_config_accessors(self)
      end
      
      module ADSC

        Providers.generate_config_accessors(self)
      end
      
      module Generic

        Providers.generate_config_accessors(self)
      end

      def generate_ehr_msh(configuration, facility)
        LISHL7::Segments.msh(msh_options(configuration, facility))
      end
      
    end
    
  end
  
end