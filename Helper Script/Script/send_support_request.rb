req = JSON.parse(params[:data])['req']

client = User.curr_user.get_value('Facility')
raise "Can't send support request. Facility is not set on a user record" unless client.present?

params['PN'] = PRODUCT_NUMBER
params['User'] = User.curr_user.fullname
params['Request'] = req

recipients = []
recipients << "support@ruro.com"
#recipients << "vlad@ruro.com"
recipients << User.curr_user.email

send_email(recipients, 'RURO Support Request', client, params)
