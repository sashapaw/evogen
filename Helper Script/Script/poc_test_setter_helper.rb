# Script for UI editing of PoC Tests in a Grid in Portal
jargs = JSON.parse(params[:data])
prop = Property.find_by_display_name('PoC Result')

if jargs['main_id'].present?
  subj = Subject.find(jargs['main_id'])
  prop2 = Property.find_by_display_name('Point of Care Test Result Entries')
  subj.get_value(prop2).each do |s|
     s.set_value(prop, jargs['val'] )
  end
  
else
  subj = Subject.find(jargs['id'])
  subj.set_value(prop, jargs['val'] ) if subj  

end