module TestResult
  class Importer

    def initialize(instrument)
      @instrument = instrument
      @ctx = context
      @auto_finalize_normal = LIS.system_settings.get_value('Auto-Finalize Test Results')
      @auto_finalize_qc = LIS.system_settings.get_value('Auto-Finalize QC Test Results')
    end
    
    def qc_runs
      @qc_run.nil? ? nil : [@qc_run]
    end
    
    def import_test_results(results)
      results.each {|result_data|
        processing_error = nil
        begin
          sample, test_result = import_test_result(result_data)
        rescue Exception=>e
          Rails.logger.error("=== Error while importing test result #{result_data.inspect}: #{e.to_s}#{e.is_a?(ImportError) ? '' : "\n" + e.backtrace.join("\n")}")
          processing_error = e
        end
        
        # Call supplied block even when test_result was not created to allow caller to implement progress reporting and/or collect result_data objects with processing errors
        yield(result_data, sample, test_result, processing_error) if block_given?
      }
    end
    
    # result_data has must have following fields:
    #  sample: (sample barcode)
    #  analyte: (Instrument Code / Name of the analyte)
    # and may optionally have following additional fields:
    #  instrument: (this will overwrite instrument specified in constructor)
    #  value:
    #  unit:
    #  flags:
    #  comments:
    #  result_status:
    #  range:
    #  completed_at:
    def import_test_result(result_data, calculate_flags: true)
      Rails.logger.info("=== Importing test result: #{result_data.inspect}")
      sample = analyte = nil
      @ctx.benchmark("Searching for sample and analyte") {
        sample = find_sample(result_data[:sample])
        analyte = find_analyte(result_data[:analyte])
      }
      
      test_result = @ctx.benchmark('obtaining test result subject') { get_test_result(sample, analyte) }
      if test_result.present?
        @ctx.benchmark('Setting all values') {
          if result_data[:value].present?
            test_result.set_value('Value', result_data[:value])
          else
            test_result.remove_prop('Value')
          end

          if result_data[:unit].present?
            unit = @ctx.find_subject({subject_type: 'Unit', name: result_data[:unit]})
            if unit.blank?
              raise ImportError.new("Unit", "Could not find Unit: #{result_data[:unit]}")
            end

            test_result.set_value('Unit', unit)
          end
          test_result.set_value('Instrument', @instrument) if @instrument.present?
          test_result.set_value('Instrument Range', result_data[:range]) if result_data[:range].present?
          test_result.set_value('Instrument Comments', result_data[:comments]) if result_data[:comments].present?
          test_result.set_value('Instrument Flags', result_data[:flags]) if result_data[:flags].present?
          test_result.set_value('Instrument Result Status', result_data[:result_status]) if result_data[:result_status].present?
          test_result.set_value('Date/Time Test Completed', result_data[:completed_at]) if result_data[:completed_at].present?
        }
        @ctx.benchmark('calculating flags') { test_result.calculate_flags } if calculate_flags
        @ctx.benchmark('finalizing test result') { finalize_result_if_needed(test_result) }
        test_result.save!
      end
      [sample, test_result]
    end


    private
    
    def finalize_result_if_needed(test_result)
      if test_result.qc? && @auto_finalize_qc
        @ctx.advance_workflow('QC Test Result', 'Finalized', test_result)
      elsif @auto_finalize_normal
        test_result.set_value('Finalized', true)
      end
    end

    def find_sample(barcode)
      raise ImportError.new("Sample", "Sample ID/Barcode is blank") if barcode.blank?
      sample = Subject.find_by_barcode_tag(barcode)
      if sample.blank?
        raise ImportError.new("Sample", "Sample #{barcode} could not be found")
      else
        if !['Sample','QC Sample'].include?(sample.subject_type.name)
          raise ImportError.new("Barcode", "Barcode #{sample.barcode_tag} not of Sample type")
          sample = nil
        else
          if sample.qc?
            raise ImportError.new("Sample", "QC Sample #{sample.barcode_tag} is Exhausted") if sample.current_states[0].name != 'Active'
          else
            raise ImportError.new("Sample", "Sample #{sample.barcode_tag} not in 'In Testing' state") if sample.current_states[0].name != 'In Testing'
          end
        end
      end
      Rails.logger.info("=== For '#{barcode}' found #{sample.present? ? sample.subject_type.name : 'nil'}: #{sample}")
      sample
    end

    def find_analyte(aname, instrument = @instrument)
      raise ImportError.new("Analyte", "Analyte Name/Code is blank.") if aname.blank?
      Rails.logger.info("=== Searching for analyte #{aname.inspect} for instrument #{instrument}")
      analytes = @ctx.find_subjects(query: @ctx.search_query(from: 'Analyte') {|qb|
        conds = qb.and( qb.or( qb.prop('Instrument Code').eq(aname),
                               qb.prop('name').eq(aname) ) )
        conds << qb.condition('terminated is false')
        if instrument.present?
          conds << qb.or( qb.prop('Instrument Type').eq(instrument.type),
                          qb.prop('Instrument Type').eq(nil) )
        end
        conds
        })
      if analytes.blank?
        raise ImportError.new('Analyte', "Analyte #{aname} could not be found.")
      end
      if analytes.size > 1
        raise ImportError.new("Analyte", "Ambiguous analytes (#{analytes.count}) found for name/code: #{aname}.")
      end
      analytes.first
    end
    
    def get_qc_run(sample)
      if @qc_run.blank?
        @qc_run = @ctx.create_subject('QC Run') do |sp|
          sp.set_value('Run Date/Time', Time.zone.now)
          sp.set_value('Run Date', Time.zone.today)
          sp.set_value('Instrument', @instrument) if @instrument.present?
        end
      end
      @qc_run
    end

    def create_qc_test_result(sample, analyte)
      @ctx.create_subject('QC Test Result') do |sp|
        sp.set_value('QC Run', get_qc_run(sample))
        sp.set_value('QC Sample', sample)
        sp.set_value('Analyte', analyte)
        sp.advance_workflow('QC Test Result', 'Data Entry', User.curr_user)
      end
    end

    def get_test_result(sample, analyte)
      if sample.qc?
        sample_analytes = sample.get_value('Analytes')
        raise ImportError.new("Test Result", "QC Result for Analyte #{analyte} is not applicable to QC Sample #{sample.barcode_tag} with Analytes #{sample_analytes.map(&:name)}") unless sample_analytes.include?(analyte)
        # Always create new test result for QC samples
        create_qc_test_result(sample, analyte)
      else
        find_test_result(sample, analyte)
      end
    end

    # Find existing open non-QC Test Result
    def find_test_result(sample, analyte)
      query = @ctx.search_query(from: 'Test Result') do |qb|
        qb.and qb.compare('Sample', :eq, sample),
               qb.compare('Analyte', :eq, analyte),
               qb.condition('Finalized is null')
      end
      qresults = @ctx.find_subjects(query: query)
      if qresults.blank?
        raise ImportError.new("Test Result", "Sample #{sample.barcode_tag} has no open Test Results for Analyte #{analyte}")
      elsif qresults.size > 1
        raise ImportError.new("Test Result", "Multiple open Test Results (#{qresults.size}) found for Sample #{sample.barcode_tag} and Analyte #{analyte}")
      else
        qresults.first
      end
    end

  end
  
end
