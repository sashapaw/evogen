module FlagCalculator
  
  def ctx
    ScriptRunner.context
  end
  
  def analyte
    @__analyte__ ||= self.get_value('Analyte')
  end

  def sample
    @__sample__ ||= self.get_value('Sample')
  end
  
  def requisition
    @__requisition__ ||= sample.get_value('Requisition')
  end
  
  def patient 
    @__patient__ ||= requisition.get_value('Patient')
  end
  
  def value 
    @__value__ ||= self.get_value('Value')
  end
  
  def calculate_flags()
    Rails.logger.info(" >>> FlagCalculator::calculate_flags: #{analyte}")

    @has_meds = _detect_medication
    self.set_value('Has Medication', @has_meds)

    if analyte.get_value('Qualitative')
      _calc_qualitative_flag(analyte.flags.select{|f| f.get_value('Qualitative')})
    else
      _calc_numeric_flag(analyte.flags.select{|f| !f.get_value('Qualitative')})
    end
  end

  def calc_outcome(flag, has_meds = @has_meds, propagate = true)
    outcome_rule = flag.get_value('Outcome Rule')
    if outcome_rule.present?
      if propagate
        # Propagate has_meds from related
        related_analytes_from = context.find_subjects(query: "FROM Analyte WHERE \"Propagate Outcome To\" = #{analyte.id}")
        related_analytes_from.each do |other_analyte|
          other_test_result = _find_related_test_result(other_analyte)
          if other_test_result.present?
            other_has_meds = other_test_result.get_value('Has Medication')
            other_outcome = other_test_result.get_value('Outcome')
            if !other_outcome.nil? && other_has_meds
              has_meds = true
            end
          end
        end
      end
    
      outcome = if outcome_rule == 'Consistent if Medication Present'
        has_meds ? 'CONSISTENT' : 'INCONSISTENT'
      else
        has_meds ? 'INCONSISTENT' : 'CONSISTENT'
      end  
      Rails.logger.error(" >>> FlagCalculator#calc_outcome : flag: #{flag}, has_meds: #{@has_meds}, outcome_rule: #{outcome_rule}, calculated outcome: #{outcome}")

      self.set_value('Outcome', outcome)

      if propagate
        # Propagate has_meds to related
        if has_meds && (related_analytes_to = analyte.get_value('Propagate Outcome To')).present?
          related_analytes_to.each do |other_analyte|
            other_test_result = _find_related_test_result(other_analyte)
            if other_test_result.present?
              other_flag = other_test_result.get_value('Flag')
              other_test_result.calc_outcome(other_flag, has_meds, false) if other_flag.present?
            end
          end
        end
      end
    else
      self.remove_prop('Outcome')
    end
  end

  private
  def _calc_numeric_flag(flags)
    range = _detect_range
    rvalue = value
    self.remove_prop('Interpretation')
    begin
      rvalue = Float(rvalue)
      interp = format_interpretation(rvalue)
      self.set_value('Interpretation', interp) if interp
    rescue Exception=>e
      instrument = self.get_value('Instrument')
      instrument_impl = instrument.type.integration_class.new(instrument) if instrument
      rvalue = instrument_impl.parse_value(rvalue) if instrument_impl
    end

    if range.present?
      flag = _detect_flag(flags, range, rvalue)
      if flag.present?
        self.set_value('Flag', flag)
        rl = range.get_value('Low')
        rh = range.get_value('High')
        if rl.present? && rh.present?
          if rl != rh
            self.set_value('Range', "#{LIS.format_value(rl, analyte)} - #{LIS.format_value(rh, analyte)}")
          else 
            self.set_value('Range', LIS.format_value(rl, analyte))
          end
        end
        calc_outcome(flag)
        if analyte.get_value('Trigger Normalization')
          _recalc_normalization(rvalue)
        else
          _calc_normalization(rvalue)
        end
      else
        self.remove_prop('Flag')
        self.remove_prop('Outcome')
        Rails.logger.error(" >>> FlagCalculator#calc_numeric_flag : Flag Not Detected:\n#{self.inspect}\n#{analyte.inspect}")
      end
    else
      Rails.logger.error(" >>> FlagCalculator#calc_numeric_flag : Range Not Detected:\n#{self.inspect}\n#{analyte.inspect}")
    end
  end
  
  def format_interpretation(rvalue)
    high_limit = analyte.get_value('High Limit')
    if high_limit && rvalue > high_limit
      return ">#{LIS.format_value(high_limit, analyte)}"
    end
    low_limit = analyte.get_value('Low Limit')
    if low_limit && rvalue < low_limit
      return "<#{LIS.format_value(low_limit, analyte)}"
    end
    nil
  end

  def _recalc_normalization(nvalue)
    q = ctx.search_query(subject_type:'Test Result') do |qb| 
      qb.and(
        qb.compare('Sample',:eq,sample),
        qb.compare('Analyte',:neq,analyte),
        qb.compare('"Analyte"->"Qualitative"',:eq,nil)
        )
    end
    ctx.find_subjects(query:q).each do |tr|
      rvalue = tr.value
      begin
        rvalue = Float(rvalue)
      rescue Exception=>e
        Rails.logger.error(" >>> FlagCalculator#recalc_normalization : #{e}: #{rvalue}\n#{tr.inspect}\n#{sample.inspect}")
        next
      end
      norm_value = ((rvalue * 100)/nvalue).round
      tr.set_value('Normalization',norm_value )
    end    
  end

  def _calc_normalization(rvalue)
    q = ctx.search_query(subject_type:'Test Result',select:'Value') do |qb| 
      qb.and(
        qb.compare('Sample',:eq,sample),
        qb.compare('"Analyte"->"Trigger Normalization"',:neq, nil)
      )
    end
    nvalue = ctx.execute_query(q,single_value:true)
    return if nvalue.blank?
    begin
      nvalue = Float(nvalue)
    rescue Exception=>e
      Rails.logger.error(" >>> FlagCalculator#calc_normalization : #{e}: #{nvalue}\n#{self.inspect}\n#{sample.inspect}")
      return
    end
    norm_value = ((rvalue * 100)/nvalue).round
    self.set_value('Normalization',norm_value )
  end

  def _detect_medication
    amed = analyte.get_value('Medications')
    if amed.present?
      all_med = requisition.get_value('Medications')
      (all_med & amed).present?
    else
      false
    end
  end
      
  def _calc_qualitative_flag(flags)
    Rails.logger.error(" >>> FlagCalculator#calc_qualitative_flag : #{flags.inspect}")
	#flag = _detect_flag(flags, nil)
    flag = flags.find {|flag| _eval_value(flag, {}) }
    if flag.present?
      self.set_value('Flag', flag)
      calc_outcome(flag)
    else
      self.remove_prop('Flag')
      self.remove_prop('Outcome')
      Rails.logger.error(" >>> FlagCalculator#calc_qualitative_flag : Flag Not Detected: #{self.inspect}   #{analyte.inspect}")
    end
  end
  
  def _find_related_test_result(other_analyte)
    context.find_subjects(query: context.search_query(from: 'Test Result') {|qb|
      qb.and qb.prop('Analyte').eq(other_analyte),
             qb.prop('Sample').eq(sample),
             qb.prop('Finalized').eq(nil)
    }).first
  end

  def _detect_flag(flags, range, rvalue)
    opts = {}
    if range
      [['Critical Low', -Float::INFINITY],['Low',0.0],['High',0.0],['Critical High',Float::INFINITY]].each do |v|
        opts[v[0]] = range.get_value(v[0]) || v[1]
      end
    end
    Rails.logger.info(">>> FlagCalculator::_detect_flag: flags: #{flags.size}, opts: #{opts.inspect}")
    flags.find {|flag| _eval_value(flag, opts, rvalue) }
  end

  def _eval_value(flag, opts, eval_value = value)
    formula = flag.get_value('Trigger Formula')
    if formula.blank?
      Rails.logger.error(">>> FlagCalculator::eval_value - blank formula for flag #{flag}")
      return false
    end
    f = formula.dup
    f = f.gsub(/{value}/,'eval_value')
    opts.keys.each do |k|
      f = f.gsub(/{#{k}}/,"opts['#{k}']")
    end
    begin 
      Rails.logger.info(">>> FlagCalculator::eval_value - formula: #{formula} : comp #{f}")
      eval(f)
    rescue Exception=>e
      Rails.logger.error(" >>> FlagCalculator#eval_value Error: formula: #{formula} : comp #{f}\n#{opts}\n#{e}\n#{flag}\n{self.inspect}")
      #"Eval Error: #{e}"
      false
    end
  end
  
  def _detect_range
    ranges = analyte.get_value('Applicable Ranges')
    Rails.logger.error(" >>> FlagCalculator#_detect_range: found #{ranges.size} ranges for analyte #{analyte}")
    ranges.each do |r|
      sex = case r.get_value('Gender')
        when 'Male'; 'M'
        when 'Female'; 'F'
        else; 'Both'
      end
      next if sex.present? && sex != 'Both' && patient.get_value('Sex') != sex

      age_unit = case r.get_value('Age Unit')
        when 'Days'; 'days'
        when 'Months'; 'months'
        else;'years'
      end
      Rails.logger.error(" >>> FlagCalculator#_detect_range: age_unit: #{age_unit}")
      if age_unit.present?
        afrom = r.get_value('Age From') || -Float::INFINITY
        ato = r.get_value('Age To') || Float::INFINITY

        age = patient.send("age_in_#{age_unit}")
        return r if age >= afrom && age <= ato
      else
        return true	# assuming this range applies to all ages
      end
    end
    nil
  end

end