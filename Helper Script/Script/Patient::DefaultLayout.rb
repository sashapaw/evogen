module Patient
  
  module DefaultLayout
    include ScriptRunner::UI
    extend self

    def insurance_comp(i, subj, bill_types)
      ins = subj.get_value("Insurance Company #{i}") if subj
      ins_comments = ins.get_value('Comments') if ins
      field_container([
          field_row([
          	udf("Insurance Company #{i}", subj, labelWidth: 160, flex: 0.6, required: i == 1,
              allowCreate:true, addCustomFields:encode_fields(set_level(2, udfs_for_subject_type('Insurance Company', nil)))),
          	udf("Policy ##{i}", subj, labelWidth: 73, flex: 0.2, required: i == 1),
          	udf("Group ##{i}", subj, labelWidth: 73, flex: 0.2)
          ]),
          udf('Comments', ins, labelWidth:160, readOnly: true, hidden: ins_comments.blank?, xtype: 'displayfield', react:{
            change: '
              var me = this;
              if (Ext.isNumber(value)) {
                form.setValuesAsync({id: value, udfs: "Comments"}, function(result) {
                  var c = result.Comments || "";
                  me.setValue(c);
                  me.setVisible(c.length > 0);
                });
              } else {
                me.hide().setValue(null);
              }',
            only: "Insurance Company #{i}"
          })
        ], 
      	react: bill_types.empty? ? nil : {
          shown_when: bill_types.map{|t| "value == '#{t}'"}.join(' || '),
          only: 'Bill To'
      	}
      )
    end

    def insurance_panel(subj)
      rel = subj && subj.get_value('Relation To Insured') || 'Self'
      field_container([
        insurance_comp(1, subj, []),
        insurance_comp(2, subj, ['Insurance']),
        insurance_comp(3, subj, ['Insurance']),
        udf('Primary Physician NPI', subj, minLength:10, maxLength:10, labelWidth:160, anchor:'50%'),
        field_container([
            udf('Relation To Insured', subj, defaultValue: rel, required:true, labelWidth:160),
            field_container([
                field_row([
                  udf('Subscriber Name', subj, fieldLabel: 'Full Name', required: true, labelWidth:160, flex: 2),
                  udf('Subscriber DOB', subj, fieldLabel: 'DOB', labelWidth: 60),
                  udf('Subscriber Sex', subj, fieldLabel: 'Sex', labelWidth: 50)
                ]),
                udf('Subscriber Address', subj, fieldLabel: 'Address', labelWidth:160,
                  anchor:'100%', height:40, validateAddress:true)
              ], 
              react: {
                shown_when: "value !== 'Self'",
                only: 'Relation To Insured'
              }
            )
          ]
        )
      ], 
      react: {
      	shown_when: "value == 'Insurance' || value == 'Medicare'",
      	only: 'Bill To'
      })
    end

    def facility_panel(subj, filter_facilities)
      facility_filter = "terminated is false"
      if filter_facilities
        f = User.curr_user.get_value('Facility')
        if f && !LIS.is_lab_user?(User.curr_user)
          facility_filter = "id = #{f.id}"
        end
      end

      field_container([
          udf('Billing Facility', subj, labelWidth:160, required: true, allowCreate:false, filter: "#{facility_filter}"),
          udf('Billing Facility Address', subj, labelWidth:160, anchor:'100%', height:40, required: true, validateAddress:true, react:{
            change:'form.setValuesAsync({id: value, udfs: {"Address":"Billing Facility Address"}})',
            only: 'Billing Facility'
          })
        ], 
        react: {
          shown_when: "value == 'Facility'",
          only: 'Bill To'
        }
      )
    end

    def general_panel_patient(subj, pat_subj, can_facility, system_settings)
      [
        udf('MRN', subj, xtype:'textfield', required:pat_subj, labelWidth:160),
        udf('External Source ID', subj, info:'Used to associate patient with external resource.', fieldLabel:'External Source ID', anchor:'55%', labelWidth:160),
        udf('Facility', subj, labelWidth:160, allowCreate:false, required:can_facility, hidden:!can_facility,
          filter:'terminated is false and RURO_CLIENT is null'),
        field_row([
          udf('First Name', subj, labelWidth:160, required:true),
          udf('Middle Name', subj, labelWidth: 100, flex:0.75),
          udf('Last Name', subj, labelWidth:110, required:true)
        ]),
        field_row([
          udf('DOB', subj, labelWidth:160, required:true),
          udf('Sex', subj, labelWidth:100, flex:0.75, required:true),
          udf('Phone Number', subj, labelWidth:110, required:true),
        ]),
        udf('Address', subj, anchor:'100%', height:40, labelWidth:160, required:true, validateAddress:true)
      ]
    end

    def billing_layout(subj, require_bill_to, filter_facilities, options = {})
      field_container([
        udf('Bill To', subj, labelWidth:160, required:require_bill_to),
        insurance_panel(subj),
        udf('Self Pay Comments', subj, anchor:'100%', height:40, labelWidth:160, react: {
          shown_when: "value == 'Patient/Self Pay'",
      	  only: 'Bill To'
        }),
        facility_panel(subj, filter_facilities)
      ], options)
    end

    def patient_layout_fields(subj)
      system_settings = SubjectType.find_by_name('System Settings').subjects.first
      pat_subj = subj && subj.subject_type.name == 'Patient'
      can_facility = LIS.is_lab_user?(User.curr_user)
      [
        field_container(general_panel_patient(subj, pat_subj, can_facility, system_settings)),
        billing_layout(subj, true, false)
      ]
    end

    def patient_layout(subj, level=nil)
      encode_fields(set_level(level, patient_layout_fields(subj)))
    end
  end
  
end