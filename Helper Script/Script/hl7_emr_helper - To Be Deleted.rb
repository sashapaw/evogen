require 'base64'
require 'street_address'
require 'phonelib'
require_script "hl7_helper"
require_script "transmit_hl7"
require_script "sftp_file"

class EMR_Generator
  #Practice Fusion - message = "OBX|#{index}|ED|PDF^PDF BASE64|1|^PDFReport^PDF^Base64^"message.concat(base64_file)
  #KIPU - message = "OBX|#{index}|ED|PDF|BASE64|message.concat(base64_file)
  PDF_PROFILE = {
    "ITF":{"obx3":["PDF"],"obx4":"Base64","obx5":[""]},
    "Practice Fusion":{"obx3":["PDF","PDF BASE64"],"obx4":"1","obx5":["","PDFReport","PDF","Base64"]},
    "KIPURECORDS":{"obx3":["PDF"],"obx4":"Base64","obx5":[""]},
    "ZenCharts":{"obx3":["PDF"],"obx4":"Base64","obx5":[""]}
  }

  FLAG_MAP = {
    IndraCare:  { L:"L",H:"H",A:"A", N:"N", CL:"PL", CH:"PH" },
    "Practice Fusion":{ L:"L", H:"H", A:"A", N:"N", CL:"<", CH:">" },
    ITF:        { L:"L", H:"H", A:"A", N:"N", CL:"<", CH:">",NEG:"NEG",POS:"POS" },
    KIPURECORDS:{ L:"L", H:"H", A:"A", N:"N", CL:"LL", CH:"HH",NEG:"NEG",POS:"POS" },
    ZenCharts:  { L:"L", H:"H", A:"A", N:"N", CL:"LL", CH:"HH",NEG:"NEG",POS:"POS" }
  }

  def initialize(context = "")
    @@context = context
  end

  def self.context
    @@context
  end

  def set_context(context)
    @@context = context
  end

  def get_flag(app,flag)
    f = "N"
    if !flag.nil?
      if FLAG_MAP.key?(app.to_sym)
        flags = FLAG_MAP[app.to_sym]
        if flags.key?(flag.to_sym)
          f = flags[flag.to_sym]
        else
          Rails.logger.info ("HL7 GENERATION(44)::: Could not find the Flag #{flag} during HL7 Generation")
        end
      else
        Rails.logger.info ("HL7 GENERATION(47)::: Could not find the application #{app} during HL7 Generation")
      end
    end
    return f
  end

  ## Add to own hl7 utils class
  def is_number? string
    true if Float(string) rescue false
  end

  def remove_decimal(num)
    num.to_s.split('.')[0]
  end

  def remove_dash(str)
    str.gsub('-', '')
  end

  def format_icd10_code(code)
    code.scan(/.{3}/).join('.')
  end

  def get_address(address_text)
    return nil if address_text.blank?
    Indirizzo::Address.new(address_text)
  end

  def get_address_xad_field(address)
    address = get_address(address) unless address.is_a?(Indirizzo::Address)
    return '' if address.blank?
    createSegmentString([address.line1,'',address.city.join(' '),address.state,address.zip])
  end

  def get_phone(phone)
    #phone.area_code, phone.local_number
    phone = Phonelib.parse(phone,'us')
    if phone.nil?
      phone = Phonelib::Phone.new("")
    end
    return phone
  end

  def set_hl7_property(obj, prop_name, prop_value)
    obj.send("#{prop_name}=",prop_value)
    rescue NoMethodError # you can also add this
      puts "#{prop_name} is undefined."
  end

  def add_to_hl7_message(segment,values)
    msg = HL7::Message.new
    values.each do |key, val|
      set_hl7_property(segment, key, val)
    end
    msg << segment
    msg.to_hl7
  end

  def file_to_base64(file_path)
    Base64.strict_encode64(File.open(file_path, "rb").read)
  end

  def createSegmentString(array)
     return array.join("^")
  end
  ## END Add to own hl7 utils class
  ## Segment Creation
  def generate_ehr_msh_map(req, app_map)
    return [{"enc_chars":"^~\\&",
      "sending_app":app_map[:sending_app], #Unknown
      "sending_facility":app_map[:sending_facility], #Unknown
      "recv_app":app_map[:recv_app],
      "recv_facility":app_map[:recv_facility], #Unknown
      "time":Time.now,
      "security":"",
      "message_type":"ORU^R01",
      "message_control_id":req.name.to_s,
      "processing_id":"P",
      "version_id":app_map[:version_id]}]
  end

  def generate_ehr_zps_map(facility, app_map)
    return [{ set_id:"1",
    "facility_footnote_code":"",
    "facility_name":"#{facility}",
    "facility_address":"",
    "facility_phone_number":"",
    "facility_contact":"",
    "facility_director":"",
    "facility_lab_code":"",
    "facility_clia_number":""}]
  end

  def generate_ehr_pid_map(patient)
    #Default to alternate id which is provided by EMR/EHR
    patient_id = patient.get_value('Alternate Patient ID').to_s
    #If one not provided we use ours
    patient_id = patient.get_value('MRN').to_s if !patient_id.present?

    patient_address = get_address_xad_field(patient.get_value('Address'))
    patient_phone = get_phone(patient.get_value('Phone Number'))

    return [{"set_id":1,
      "patient_id":patient_id,
      "patient_id_list":patient_id,
      "alt_patient_id":patient.get_value('MRN').to_s,
      "patient_name":createSegmentString([patient.get_value('Last Name').to_s,patient.get_value('First Name').to_s,patient.get_value('Middle Name').to_s]),
      "mother_maiden_name":"",
      "patient_dob":remove_dash(patient.get_value('DOB').to_s),
      "admin_sex":patient.get_value('Sex').to_s,
      "patient_alias":"",
      "race":"",
      "address":patient_address,
      "country_code":"",
      "phone_home":patient_phone.sanitized,
      "phone_business":"",
      "primary_language":"",
      "marital_status":"",
      "religion":"",
      "account_number":patient.get_value('MRN').to_s,
      "social_security_num":"",
      "driver_license_num":"",
      "mothers_id":"",
      "ethnic_group":""}]
  end

  def generate_ehr_orc_map(req,app)
    account_number = ""
    if app[:provider] == "ITF"
      account_number = req.get_value("Facility").get_value("Best Notes Account Number")
    end
    begin
      transaction_time = Time.parse(req.get_value("Released Date").to_s).strftime("%Y%m%d%H%M%S")
    rescue
      raise "Released Date is missing on the Requisition. Please correct and try again."
    end

    req_id = req.get_value('Alternate Requisition ID') || req.name.to_s
    filler_order_number = req.get_value("Samples").map(&:name).join(',')

    orc_fields = {"order_control":"RE",
         "placer_order_number":req_id,
         "filler_order_number":filler_order_number,
         "placer_group_number":"",
         "order_status":"",
         "response_flag":"D",
         "quantity_timing":"",
         "parent":"",
         "date_time_of_transaction":"#{transaction_time}",
         "entered_by":"",
         "verified_by":"",
         "ordering_provider":createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")]),
         "enterers_location":account_number,
         "call_back_phone_number":"",
         "order_effective_date_time":req.get_value("Received Date"),
         "order_control_reason":"",
         "entering_organization":"",
         "entering_device":"",
         "action_by":"",
         "advanced_beneficiary_notice_code":"",
         "ordering_facility_name":createSegmentString([req.get_value("Facility"),req.get_value("Facility").id])
      }
    orc_fields["alternate_placer_order_number"] = req.name.to_s if app[:version_id] >= '2.7'
    return [orc_fields]
  end

  def generate_obr_string(req,app,test,index,correction)
    req_id = req.get_value('Alternate Requisition ID') || req.name.to_s
    begin
      cpt_code = test.get_value("CPT Code")
    rescue
      #die sliently
      cpt_code = ''
    end
    #If no CPT Code, lets try using just the test code
    if cpt_code.empty?
      begin
        cpt_code = test.get_value("Code")
      rescue
        cpt_code = ''
      end
    end

    msg = HL7::Message.new
    obr = HL7::Message::Segment::OBR.new
    obr.set_id = index.to_s || "1"
    obr.placer_order_number = req_id
    obr.filler_order_number = req_id
    obr.universal_service_id = createSegmentString([cpt_code,test.get_value("Description"),"L"])
    obr.priority = "R"
    obr.requested_date = req.get_value("Received Date")
    obr.observation_date = req.get_value("Collection Date")
    obr.observation_end_date = ""
    obr.collection_volume = ""
    obr.collector_identifier = ""
    obr.specimen_action_code = ""
    obr.danger_code = ""
    obr.relevant_clinical_info = ""
    obr.specimen_received_date = req.get_value("Received Date")
    obr.specimen_source = "#{req.get_value("Specimen Types").join(',')}"
    obr.ordering_provider = createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")])
    obr.order_callback_phone_number = ""
    obr.placer_field_1 = ""
    obr.placer_field_2 = ""
    obr.filler_field_1 = ""
    obr.filler_field_2 = ""
    obr.results_status_change_date = req.get_value("Report Date")
    obr.charge_to_practice = ""
    obr.diagnostic_serv_sect_id = ""
    obr.result_status = correction || "F"
    obr.parent_result = ""
    obr.quantity_timing = "^^^^^R"
    obr.alternate_placer_order_number = req.name.to_s if app[:version_id] >= '2.7'
    msg << obr

    return msg.to_hl7
  end

  #Used for Toxicology labs
  def select_value(test_result)
    value = test_result.get_value("Value")
    if test_result.get_value("Interpretation").present?
       value = test_result.get_value("Interpretation")
    end
    value
  end

  def generate_obx_string(req, result, index, app, send_only_failed)
    #Producer Id
    producer_id = createSegmentString(["WST","WESTOX","18102 Sky Park South, bldg 52E","Irvine","CA","92614","Jean C Joseph, PhD, DABCC",""])
    #if result.get_value("Flag")
    if !send_only_failed || (send_only_failed && result.get_value("Flag"))
      value = select_value(result)
      value_type = "ST"
      if is_number?(value)
        value_type = "NM"
      elsif value.to_s.length > 20
        value_type = "TX"
      else
        value_type = "ST"
      end
      begin
        cpt_code = result.get_value('Test').get_value("CPT Code")
      rescue
         #die sliently
         cpt_code = ''
      end
      #If no CPT Code, lets try using just the test code
      if cpt_code.empty?
        begin
           cpt_code = result.get_value('Test').get_value("Code")
        rescue
          cpt_code = ''
        end
      end

      msg = HL7::Message.new
      obx = HL7::Message::Segment::OBX.new
      obx.set_id = index.to_s || "1"
      obx.value_type=value_type
      obx.observation_id=createSegmentString([cpt_code,result.get_value("Analyte").to_s,"","","",""])
      obx.observation_sub_id=index
      obx.observation_value=value
      obx.units=result.get_value("Unit")
      obx.references_range=result.get_value("Range")
      obx.abnormal_flags=get_flag(app,result.get_value("Flag"))
      obx.probability=""
      obx.nature_of_abnormal_test=""
      obx.observation_result_status="F"
      obx.effective_date_of_reference_range=""
      obx.user_defined_access_checks=""
      obx.observation_date=req.get_value("Report Date")
      obx.producer_id=producer_id
      obx.responsible_observer=""
      obx.observation_method=""
      obx.equipment_instance_id=""
      obx.analysis_date=""
      obx.observation_site=""
      obx.observation_instance_id=""
      obx.mood_code=""
      obx.performing_organization_name=""
      obx.performing_organization_address=""
      obx.performing_organization_medical_director=createSegmentString([req.get_value('Test Performer NPI Number'),req.get_value('Test Performer Last Name'),req.get_value('Test Performer First Name')])
      msg << obx

      return msg.to_hl7
    else
      return ""
    end
  end

  def generate_nte_string(index, source, comment, comment_type = '')
    msg = HL7::Message.new
    nte = HL7::Message::Segment::NTE.new
    nte.set_id = index.to_s
    nte.source = source
    nte.comment = ActionView::Base.full_sanitizer.sanitize(comment)
    nte.comment_type = comment_type
    msg << nte

    return msg.to_hl7
  end

  def generate_obr_poc(req,correction,index)
    req_id = req.get_value('Alternate Requisition ID') || req.name.to_s

    msg = HL7::Message.new
    obr = HL7::Message::Segment::OBR.new
    obr.set_id = index.to_s || "1"
    obr.placer_order_number = req_id
    obr.filler_order_number = req_id
    obr.universal_service_id = createSegmentString(['POC','POC Results'])
    obr.priority = "R"
    obr.requested_date = req.get_value("Received Date")
    obr.observation_date = req.get_value("Collection Date")
    obr.observation_end_date = ""
    obr.collection_volume = ""
    obr.collector_identifier = ""
    obr.specimen_action_code = ""
    obr.danger_code = ""
    obr.relevant_clinical_info = ""
    obr.specimen_received_date = req.get_value("Received Date")
    obr.specimen_source = "#{req.get_value("Specimen Types").join(',')}"
    obr.ordering_provider = createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")])
    obr.order_callback_phone_number = ""
    obr.placer_field_1 = ""
    obr.placer_field_2 = ""
    obr.filler_field_1 = ""
    obr.filler_field_2 = ""
    obr.results_status_change_date = req.get_value("Report Date")
    obr.charge_to_practice = ""
    obr.diagnostic_serv_sect_id = ""
    obr.result_status = correction || "F"
    obr.parent_result = ""
    msg << obr

    return msg.to_hl7
  end

  def generate_obx_poc(req,poc_tre,index)
    producer_id = createSegmentString(["POC","Point of Care"])

    poc_test = poc_tre.get_value('Point of Care Test')
    value = poc_tre.get_value('PoC Result')

    flag = "N"
    flag = "A" if value == "Positive"

    msg = HL7::Message.new
    obx = HL7::Message::Segment::OBX.new
    obx.set_id = index.to_s || "1"
    obx.value_type='ST'
    obx.observation_id=createSegmentString([poc_test.name,poc_test.name,"L"])
    obx.observation_sub_id=''
    obx.observation_value=value
    obx.units="ng/mL"
    obx.references_range=""
    obx.abnormal_flags=flag
    obx.probability=""
    obx.nature_of_abnormal_test=""
    obx.observation_result_status="F"
    obx.effective_date_of_reference_range=""
    obx.user_defined_access_checks=""
    obx.observation_date=req.get_value("Report Date")
    obx.producer_id=producer_id
    obx.responsible_observer=""
    obx.observation_method=""
    obx.equipment_instance_id=""
    #obx.analysis_date=""
    #obx.observation_site=""
    #obx.observation_instance_id=""
    #obx.mood_code=""
    #obx.performing_organization_name=""
    #obx.performing_organization_address=""
    #obx.performing_organization_medical_director=createSegmentString([req.get_value('Test Performer NPI Number'),req.get_value('Test Performer Last Name'),req.get_value('Test Performer First Name')])
    msg << obx

    return msg.to_hl7
  end

  def generate_poc_string(app,index,req,correction)
    messages = []
    req.get_value('Point of Care Test Results').each do |poc_tr|
      poc_tr_obr = generate_obr_poc(req,correction,index)
      messages.push(poc_tr_obr) if poc_tr_obr
      poc_tr.get_value('Point of Care Test Result Entries').each_with_index do |poc_tre,i|
        poc_tr_obx = generate_obx_poc(req,poc_tre,(i + 1))
        messages.push(poc_tr_obx) if poc_tr_obx
      end
    end
    messages.join('/n')
    return messages
  end

  def generate_obr(req,app_map,correction,send_only_failed)
    app = app_map[:recv_app]
    messages = []
    o = 1
    last = 1
    #if poc tests enabled, lets add them to the fun.
    poc = ""
    if req.get_value('Point of Care Test Results').size > 0
      poc = generate_poc_string(app, o, req, correction)
      if poc != ""
        messages.push(poc)
      end
      o = o + 1
    end

    #loop through each test
    req.get_value("Test Codes").each do |test|
      #loop through each result
      obx_messages = []
      obx_index = 1
      req.get_value("Test Results").each do |result|
        nte_index = 1
        i = 1
        #if result.get_value("Test").name.to_s == test.name
        if "#{result.get_value("Test")}" == "#{test}"
          lab = generate_obx_string(req,result,obx_index,app,send_only_failed)
          if lab != ""
            obx_messages.push(lab)
            obx_index += 1
            comments = result.get_value('Comments')
            if comments.present?
              obx_messages.push(generate_nte_string(nte_index, 'O', comments))
              nte_index += 1
            end
            outcome = result.get_value('Outcome')
            if outcome.present?
              obx_messages.push(generate_nte_string(nte_index, 'L', "Outcome: #{outcome}", 'RE'))
              nte_index += 1
            end
            i += 1
            last = i
          end
        end
      end

      if obx_messages.size > 0
        obr = generate_obr_string(req,app_map,test,o,correction)
        if obr != ""
           messages.push(obr)
           messages.push(*obx_messages)
           o += 1
        end
      end
    end
    ##Only PDF supported providers
    pdf_providers = ["Practice Fusion","ITF","KIPURECORDS","ZenCharts"]
    if pdf_providers.include?(app) && req.get_value("Report File")
      file_path = req.get_value("Report File").path
      if file_path
        obr_pdf = generate_obr_pdf_string(app, file_path, o, req, correction)
        pdf = generate_obx_pdf_string(app,file_path,1)
        if pdf != ""
          messages.push(obr_pdf)
          messages.push(pdf)
        end
      end
    end
    return messages.join("\n")
  end

  ## Add to its own class
  def generate_message(msg)
    message = ""
    segment = ""
    msg.each do |key, val|
      case key.downcase
      when "msh" #Message Header - DFT(1), SIU(1), ADT(1)
        segment = HL7::Message::Segment::MSH.new
      when "sch" #Schedule Activity Information - SIU(2) *
        segment = HL7::Message::Segment::SCH.new
      when "pid" #Patient Demographic - DFT(2), SIU(3), ADT(2)
        segment = HL7::Message::Segment::PID.new
      when "pd1" #Patient Additional Demographics - ADT(3) *
        segment = HL7::Message::Segment::PD1.new
      when "nk1" #Next of Kin - ADT(4)
        segment = HL7::Message::Segment::NK1.new
      when "rgs" #Resource Group - SIU(4) *
        segment = HL7::Message::Segment::RGS.new
      when "ail" #Location Resource - SIU(5) *
        segment = HL7::Message::Segment::AIL.new
      when "aip" #Personnel Resource - SIU(6) *
        segment = HL7::Message::Segment::AIP.new
      when "pv1" #Patient Visit - DFT(3), ADT(5) *
        segment = HL7::Message::Segment::PV1.new
      when "ft1" #Financial Transaction - DFT(4)
        segment = HL7::Message::Segment::FT1.new
      when "dg1" #Diagnosis - DFT(5)
        segment = HL7::Message::Segment::DG1.new
      when "gt1" #Guarantor - DFT(6), ADT(6)
        segment = HL7::Message::Segment::GT1.new
      when "in1" #Insurance - DFT(7), ADT(7)
        segment = HL7::Message::Segment::IN1.new
      when "acc" #Accident - DFT(8)
        segment = HL7::Message::Segment::ACC.new
      when "zdi" #Date of Current Illness - DFT(9)
        segment = HL7::Message::Segment::ZDI.new
      when "zdt" #Additional Date - DFT(10)
        segment = HL7::Message::Segment::ZDT.new
      when "orc" #EMR
        segment = HL7::Message::Segment::ORC.new
      when "obr" #EMR
        segment = HL7::Message::Segment::OBR.new
      when "obx" #EMR
        segment = HL7::Message::Segment::OBX.new
      when "nte" #EMR
        segment = HL7::Message::Segment::NTE.new
      when "zps" #EMR
        segment = HL7::Message::Segment::ZPS.new
      end

      val.each do | values |
        message.concat(add_to_hl7_message(segment,values)+"\n")
      end
      #Clear value
      segment = ""
    end
    return message
  end

  def create_hl7_message(req, app_map)
    patient = req.get_value('Patient')
    msg = {}
    msg["MSH"] = generate_ehr_msh_map(req, app_map)
    msg["PID"] = generate_ehr_pid_map(patient)
    msg["ORC"] = generate_ehr_orc_map(req,app_map)
    return msg
  end

  def generate_obr_pdf_string(app, file_path, index, req, correction)
    req_id = req.get_value('Alternate Requisition ID') || req.name.to_s

    msg = HL7::Message.new
    obr = HL7::Message::Segment::OBR.new
    obr.set_id = index.to_s || "1"
    obr.placer_order_number = req_id
    obr.filler_order_number = req_id
    obr.universal_service_id = createSegmentString(["Base64","Embedded PDF"])
    obr.priority = "R"
    obr.requested_date = ""
    obr.observation_date = req.get_value("Collection Date")
    obr.observation_end_date = ""
    obr.collection_volume = ""
    obr.collector_identifier = ""
    obr.specimen_action_code = ""
    obr.danger_code = ""
    obr.relevant_clinical_info = ""
    obr.specimen_received_date = req.get_value("Received Date")
    obr.specimen_source = "#{req.get_value("Specimen Types").join(',')}"
    obr.ordering_provider = createSegmentString([req.get_value("Requesting Physician").get_value("NPI Number"),req.get_value("Requesting Physician").get_value("Last Name"),req.get_value("Requesting Physician").get_value("First Name")])
    obr.order_callback_phone_number = ""
    obr.placer_field_1 = ""
    obr.placer_field_2 = ""
    obr.filler_field_1 = ""
    obr.filler_field_2 = ""
    obr.results_status_change_date = req.get_value("Report Date")
    obr.charge_to_practice = ""
    obr.diagnostic_serv_sect_id = ""
    obr.result_status = correction || "F"
    obr.parent_result = ""
    msg << obr

    return msg.to_hl7
  end

  def generate_zps(system_settings,app_map)
    msg = HL7::Message.new
    zps = HL7::Message::Segment::ZPS.new
    zps.set_id = "1"
    zps.facility_footnote_code = ""
    zps.facility_name = "#{system_settings.get_value('Lab Name')}"
    zps.facility_address = get_address_xad_field(system_settings.get_value('Lab Address'))
    zps.facility_phone_number = "#{system_settings.get_value('Lab Phone')}"
    zps.facility_contact = ""
    #	PhysCode^Fname^Lname^Degree^License^^^
    zps.facility_director = createSegmentString([system_settings.get_value('Lab Director Physicians Code'),system_settings.get_value('Lab Director First Name'),system_settings.get_value('Lab Director Last Name'),system_settings.get_value('Lab Director Degree'),"CLIA ID# #{system_settings.get_value('CLIA Number')}","","",""])
    zps.facility_lab_code = ""
    zps.facility_clia_number = "#{system_settings.get_value('CLIA Number')}"

    msg << zps
    return msg.to_hl7
  end

  def generate_obx_pdf_string(app, file_path, index)
    base64_file = file_to_base64(file_path)
    profile = PDF_PROFILE[app.to_sym]

    obx5 = profile[:obx5]
    if obx5.size > 1
      obx5.push(base64_file)
    else
      obx5[0] = base64_file
    end

    msg = HL7::Message.new
    obx = HL7::Message::Segment::OBX.new
    obx.set_id=index.to_s
    obx.value_type="ED"
    obx.observation_id=createSegmentString(profile[:obx3])
    obx.observation_sub_id=profile[:obx4]
    obx.observation_value=createSegmentString(obx5)
    msg << obx

    return msg.to_hl7
  end

  def create_file(ehr_record_name, message)
    filename = "#{Rails.root}/tmp/#{ehr_record_name}.hl7"

    message = message.gsub("\n","\r\n")
    begin
      message = message.encode("UTF-8")
    rescue Encoding::UndefinedConversionError
      # couldn't encode
    end

    begin
      f = File.open(filename, "wb")
      f.write(message)
    rescue IOError => e
      #some error occur, dir not writable etc.
      Rails.logger.error("Error while writing file #{filename}: #{e.to_s}")
    ensure
      f.close unless f.nil?
    end
    return f
  end

  def create_record(app_map,configuration,req,flag)
    ehr_record = @@context.create_subject('EHR Record') do |er|
      er.set_value('Requisition', req)
      er.set_value('EHR System', app_map[:provider])
    end

    msg = create_hl7_message(req,app_map)
    message = generate_message(msg)
    message = message + generate_obr(req,app_map,flag,configuration.get_value('Only Send Failed Results'))
    if app_map[:provider] == "KIPU"
      system_settings_type = SubjectType.find_by_name('System Settings')
      system_settings = system_settings_type.subjects.first

      message =  message + "\n" + generate_zps(system_settings,app_map)
    end
    f = create_file(ehr_record.name, message)
    ehr_record.set_value('File',f)
	file = File.basename f

    host = configuration.get_value('HL7 Host Name')
    username = configuration.get_value('HL7 Username')
    password = configuration.get_value('HL7 Password')
    enable_config = configuration.get_value('Enable Configuration')
    server_type = configuration.get_value('HL7 Server Type')
    if enable_config
      #If remote file path sftp,
      response = "Server Failure"
      if server_type == "SFTP"
        host_port = configuration.get_value('HL7 Host Port')
        remote_file_path = configuration.get_value('Remote File Path')
        #If not use the Global Path, then use path attached to facility
        if !configuration.get_value("Use Global File Path")
          if req.get_value('Facility').get_value('Remote File Path').present?
            remote_file_path = req.get_value('Facility').get_value('Remote File Path') #configuration.get_value('Remote File Path')
          end
        end

        if host_port
          response = @@context.sftp_file(host, username, password, f.path, remote_file_path + file, host_port.to_i)
        else
          begin
             response = @@context.sftp_file(host, username, password, f.path, remote_file_path + file, nil)
          rescue Exception => e
            response = "There was a problem sending the file, #{e}"
          end
        end
      elsif server_type == "REST"
        response = @@context.transmit_hl7_http(host,username,password,message)
      end
      ehr_record.set_value('Comments',response)
    else
      ehr_record.set_value('Comments',"#{configuration} #{configuration.get_value('HL7 Server Type')} is currently disabled.")
    end
    File.delete(f)
  end
end

def send_hl7(req,correction)
  if correction
    flag = "C"
  else
    flag = "F"
  end
  emr_generator = EMR_Generator.new(self)
  system_settings_type = SubjectType.find_by_name('System Settings')
  system_settings = system_settings_type.subjects.first

  configurations = system_settings.get_value('HL7 Server Configurations')
  configurations.each do | configuration |
    msg_type = configuration.get_value('HL7 Message Type')
    if msg_type && msg_type.include?('ORU')
      provider_type = configuration.get_value('HL7 Provider')
      if provider_type == "Practice Fusion"
        app_map = {
          sending_app:"Limfinity^6",
          sending_facility:"#{configuration.get_value('Facility Number')}^#{configuration.get_value('Facility Name')}",
          recv_app:"#{configuration.get_value('HL7 Provider')}",
          recv_facility:"#{req.get_value('Facility').get_value('Practice Fusion Account Number')}^#{req.get_value('Facility')}",
          version_id:"#{configuration.get_value('HL7 Version')}",
          provider:"Practice Fusion"
        }
        if req.get_value('Facility').get_value('Practice Fusion Account Number').present?
          if configuration.get_value('Only Send Failed Results')
            if req.get_value('Result') == 'Fail'
              emr_generator.create_record(app_map,configuration,req,flag)
            end
          else
            emr_generator.create_record(app_map,configuration,req,flag)
          end
        end
      elsif provider_type == "IndraCare"
        app_map = {
          sending_app:"Limfinity",
          sending_facility:"#{configuration.get_value('Facility Name')}",
          recv_app:"IndraCare",
          recv_facility:"myOnsite",
          version_id:"#{configuration.get_value('HL7 Version')}",
          provider:"MyOnSiteEHR"
        }
        if configuration.get_value('Only Send Failed Results')
          if req.get_value('Result') == 'Fail'
             emr_generator.create_record(app_map,configuration,req,flag)
          end
        else
           emr_generator.create_record(app_map,configuration,req,flag)
        end
      elsif configuration.get_value('HL7 Provider') == "KIPURECORDS"
        app_map = {
          sending_app:"Limfinity",
          sending_facility:"#{configuration.get_value('Facility Name')}",
          recv_app:"KIPURECORDS",
          recv_facility:"#{req.get_value('Facility').name}",
          version_id:"#{configuration.get_value('HL7 Version')}",
          provider:"KIPU"
        }
        if req.get_value('Facility').get_value('KIPU Account Number').present?
          if configuration.get_value('Only Send Failed Results')
            if req.get_value('Result') == 'Fail'
              emr_generator.create_record(app_map,configuration,req,flag)
            end
          else
            emr_generator.create_record(app_map,configuration,req,flag)
          end
        end
      elsif provider_type == "ITF"
        app_map = {
          sending_app:"QSS",
          sending_facility:"#{configuration.get_value('Facility Name')}",
          recv_app:"ITF",
          recv_facility:"HTI", #Based off sample HL7
          version_id:"#{configuration.get_value('HL7 Version')}",
          provider:"ITF"
        }
        if req.get_value('Facility').get_value('Best Notes Account Number').present?
          app_map[:recv_facility] = req.get_value('Facility').get_value('Best Notes Account Number')
          if configuration.get_value('Only Send Failed Results')
            if req.get_value('Result') == 'Fail'
               emr_generator.create_record(app_map,configuration,req,flag)
            end
          else
             emr_generator.create_record(app_map,configuration,req,flag)
          end
        end
      else
        app_map = {
          sending_app:"Limfinity",
          sending_facility:"#{configuration.get_value('Facility Name')}",
          recv_app:"#{configuration.name}",
          recv_facility:"#{req.get_value('Facility').name}",
          version_id:"#{configuration.get_value('HL7 Version')}",
          provider:provider_type
        }
        if req.get_value('Facility').get_value("#{provider_type} Account Number").present?
          if configuration.get_value('Only Send Failed Results')
            if req.get_value('Result') == 'Fail'
              emr_generator.create_record(app_map,configuration,req,flag)
            end
          else
            emr_generator.create_record(app_map,configuration,req,flag)
          end
        end
      end
    end
  end
end