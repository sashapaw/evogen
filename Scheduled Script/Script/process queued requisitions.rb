#Add Code to find Queued Requisitions, then process.
query = search_query(subject_type: "Requisition") do |qb|
  qb.compare("Queued for Release", :eq, true)
end
requisitions = find_subjects(query: query)
#Lets remove the requestion queue status first incase we get a large sample. We dont reqs processing twice
requisitions.each do |req|
  req.remove_prop("Queued for Release")
  req.set_value("Process Lock",true)
end

system_settings = LIS.system_settings
confirmation_report = ''

def finalize_results(subj)
  subj.get_value('Test Results').each do |s|
     if s.get_value('Value').present?
       s.calculate_flags
       SystemHooks.on_result_change(s)
     else
        raise "#{s.get_value('Analyte').name} does not have a value. Please enter a value in order to release the Requisition."
     end

    s.set_value('Finalized', true)
  end
  Requisition::Reports.generate_test_results_report(subj,{},true)
end

requisitions.each do |subj|
  begin
    finalize_results(subj)
    
    release_report = Requisition.release(subj)
    confirmation_report << release_report  
  rescue Exception=>e
    Rails.logger.error("Error while releasing requisition #{subj.name}: #{e.message}\n#{e.backtrace.join("\n")}")
    subj.set_value('Comments',"An Error Occured while processing requisition. #{e}")
    advance_workflow('Requisition', 'Received', subj)
    #SystemHooks.requisition_received(subj)
  ensure
    subj.remove_prop("Process Lock")
  end
end