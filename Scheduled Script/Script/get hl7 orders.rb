system_settings_type = SubjectType.find_by_name('System Settings')  
system_settings = system_settings_type.subjects.first
configurations = system_settings.get_value('HL7 EHR Providers')
configurations.each do |configuration|
  if configuration.get_value('Enabled') && configuration.get_value('Retrieve Orders')
    LISHL7::EHR::Importer.importHL7(configuration)
  end
end
