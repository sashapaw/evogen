# Obsolete code from LIS2.1

require 'net/ftp'
require_script "results_test_import_magnolia"
system_settings = SubjectType.find_by_name('System Settings').subjects.first

def get_instrument_files(host, username, password, path, port=nil)
	instrument_data = []
	if !port.nil?
		keys = {:password => password, :port => port}
	else
		keys = {:password => password}
	end
	Net::FTP.open(host, username, password) do |ftp|
		ftp.binary=true		
		ftp.passive=true
		ftp.chdir(path)
	  	ftp.nlst.each do |entry|
          data = ftp.get(entry,nil)
	  	  instrument_data.push(data)
          #ftp.delete(entry)
	  	end
	end

	return instrument_data
end

instrument_servers = SubjectType.find_by_name('Instrument Server Configuration').subjects
hn = find_prop('Hostname')
un = find_prop('Username')
pw = find_prop('Password')
rfp = find_prop('Remote File Path')
st = find_prop('Server Type')
instrument_servers.each do |config|
  hostname = config.get_value(hn)  
  username = config.get_value(un)
  password = config.get_value(pw)
  path = config.get_value(rfp)
  type = config.get_value(st)
  if type == "FTP"
  	files = get_instrument_files(hostname, username, password, path)
    if files.size == 0 
      Rails.logger.info "[Instrument Import] No new files to Process"
    else
      files.each {|file|
          process_file(file, "", system_settings,true)  
      }
    end
  end
end