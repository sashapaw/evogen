count_subjects(query:search_query(subject_type:'Communication') {|qb|
    qb.and(
      qb.state('Open'),
      qb.compare('terminated', :eq, false)
    )
})