require_script 'system_common'

system_settings = get_system_settings_subject()

reference_lab_enabled = system_settings.get_value('Use Reference Lab')

received_state_def = SubjStateDef.where(name: 'Received', subj_flow_id: SubjFlow.where(name: 'Requisition')).first
count_subjects(query:search_query(subject_type:'Requisition') {|qb|
  ready_for_release_conds = qb.or
  ready_for_release_conds << qb.and(
      qb.compare('<-Requisition<-"Test Result"::Sample', :neq, nil),
      qb.not(qb.compare('<-Requisition<-"Test Result"::Sample->Finalized', :eq, nil))
  )
  
  # if the reference lab did the testing, and the report is attached, we can also release this req
  if reference_lab_enabled
    ready_for_release_conds << qb.and(
      qb.compare('Reference Lab', :neq, nil),
      qb.compare('Preliminary Report File', :neq, nil)
      )
  end
    
  qb.and(
    qb.state(received_state_def.id),
    qb.compare('Queued for Release', :eq, nil),
    ready_for_release_conds
  )
})