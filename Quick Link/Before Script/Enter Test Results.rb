extend UI
params[:custom_fields] = encode_fields([
  field_set(title:'Narrow Down By Properities', border:'1 0 0 0', items:[       
    udf('Facility', nil, labelWidth:200, allowCreate:false, filter:"<-Requisition::Facility->#state = 'Received'"),
    udf('Requisition', nil, labelWidth:200, allowCreate:false, filter:"#state == 'Received'"),
    udf('Patient', nil, labelWidth:200, allowCreate:false, filter:"<-Requisition::Patient->#state = 'Received'"),
    udf('Sample', subj, labelWidth:200, allowCreate:false, filter:"#state = 'In Testing'"),
    udf('Test', nil, labelWidth:200, allowCreate:false, filter:"<-Sample::Tests<-\"Test Result\"::Sample->Finalized = null and Analytes != null"),
    udf('Analyte', nil, labelWidth:200, allowCreate:false, filter:"<-\"Test Result\"::Analyte->Finalized = null"),
    udf('Instrument', nil, labelWidth:200, allowCreate:false, filter:"<-\"Test Result\"::Instrument->Finalized = null"),
  ])
])


