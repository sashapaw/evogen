extend ScriptRunner::UI
params[:custom_fields] = encode_fields([
    udf('Find Requisitions', nil, labelWidth:160, required:false),
    field_set(title:'Find By Properities', border:'1 0 0 0', items:[       
        udf('External Source ID', nil, info:'This field can be used for the Sample Barcode Entry',
          allowCreate:false, labelWidth:160, anchor:'100%'),
        udf('Requisition', nil, labelWidth:160, allowCreate:false),
        udf('Patient', nil, labelWidth:160, allowCreate:false),
        udf('Requesting Physician', nil, labelWidth:160, allowCreate:false),
        udf('Sample', nil, labelWidth:160, allowCreate:false),
        udf('Facility', nil, labelWidth:160, allowCreate:false),
        udf('Received Date', nil, labelWidth:160, anchor:'50%'),
        udf('Collection Date', nil, labelWidth:160, anchor:'50%'),
        udf('Released Date', nil, labelWidth:160, anchor:'50%')
      ],
      react: {
        shown_when: 'value == "Properties"',
        only: 'Find Requisitions'
    })
  ])
