raise 'Initial Setup already completed.' if find_subject_type('Facility').subjects.size > 0

extend UI
params[:custom_fields] = encode_fields([
  {xtype:'label', html:'Initial setup is done to configure Laboratory and Default RURO user. Do not use the system without the Initial Setup Completed<br><br>'},
  {xtype:'label', html:'<b>I. System Name to show On Login Page and Main Header:</b><br>'},
  {xtype:'textfield', fieldLabel:'System Company Name', required:true, name:"custom[system]", anchor:'100%'},
  
  {xtype:'label', html:'<br><b>II. Email for new "ruro" user in the system:</b><br>'},
  {xtype:'label', html:'<b>Note:</b> Password for "ruro" user will be "ruro12345"<br>'},
  udf('EMail', nil, required:true, anchor:'100%'),

  {xtype:'label', html:'<br><b>III. Laboratory Settings:</b><br>'},
  udf('Name', nil, required:true, anchor:'100%'),
  udf('Contact Name', nil, required:true, anchor:'100%'),
  udf('Contact Email', nil, required:true, anchor:'100%'),
])