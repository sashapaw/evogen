params[:udf_config] = {
  'Requisition'=> {
    filter: '#state is Released',
    sorters: {property: 'id', direction: 'DESC'}
   }
}


params[:required] = {
  'Requisition'=> true,
  'Correction Date'=>true,
  'Reason'=>true
}
params[:allowCreate] = {
  'Requisition'=> false
}