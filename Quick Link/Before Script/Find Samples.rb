extend UI
params[:custom_fields] = encode_fields([
  udf('Find Samples', subj, labelWidth:160, required:false),
  field_set(title:'Find By Properities', border:'1 0 0 0', items:[
      udf('Sample', subj, labelWidth:160, allowCreate:false)
    ], 
    react: {
      shown_when: "value == 'Properties'",
      only: 'Find Samples'
  })
])
