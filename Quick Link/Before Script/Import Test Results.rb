extend UI
#system_settings = LIS.system_settings
#raise "No Import Script Configured" if !system_settings.get_value("Test Result Import Script")

params[:custom_fields] = encode_fields([
  udf('Instrument', nil, labelWidth:160, allowCreate:false, required: true, filter: '"Instrument Type"->"File Import Supported" is true'),
  {xtype:'uploader', name:'custom[files]', fieldLabel:'Result Files', labelWidth: 'auto', required:true, fields:['Comments']}
])

params[:background_execution] = true
params[:no_transaction] = true