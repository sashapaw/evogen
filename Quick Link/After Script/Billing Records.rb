params[:subject_type] = 'Billing Record'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:query] = '#state = Active'
params[:search_guid] = "BILLINGRECORDS"

params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label:'Name'),
  UIUtils.search_filter('"Requisition"->name', op: 'contains', label: 'Requisition'),
  UIUtils.search_filter('"Patient"->name', op: 'contains', label: 'Patient'),
  UIUtils.search_filter('"Comments"', op: 'contains', label: 'Comments')
].to_json
