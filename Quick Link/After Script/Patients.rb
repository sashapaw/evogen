params[:subject_type] = 'Patient'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:query] = '#state = Active'
params[:search_guid] = "PATIENTS"

params[:qb_filters] = [
  UIUtils.search_filter('"Facility"->name', op: 'contains', label: 'Facility'),
  UIUtils.search_filter('"MRN"', op: 'contains', label: 'MRN'),
  UIUtils.search_filter('"First Name"', op: 'contains', label:'First Name'),
  UIUtils.search_filter('"Last Name"', op: 'contains', label:'Last Name'),
].to_json
