params[:subject_type] = 'Sample'
params[:need_search_panel] = true


if params['Sample'].present?
  open_subject(params['Sample'])
else
  sample = params['Find Samples']
  if sample == "Accessioned"
     params[:query] = "#state = 'Accessioned'"
     params[:search_guid] = "ACCESSIONED"
    params[:tab_name] = "Accessioned"
  elsif sample == "In Testing"
    params[:query] = "#state = 'In Testing'"
     params[:search_guid] = "IN_TESTING"
    params[:tab_name] = "In Testing"
  elsif sample == "Stored"
    params[:query] = "#state = 'Stored'"
    params[:search_guid] = "STORED"
    params[:tab_name] = "Stored Samples"
  elsif sample == "Released"
     params[:query] = '#state = Released'
     params[:search_guid] = "RELEASED"
    params[:tab_name] = "Released Samples"
  end

  params[:qb_filters] = [
    UIUtils.search_filter('name', op: 'contains', label:'Sample ID'),
    UIUtils.search_filter('barcode_tag', op: 'contains', label: 'Sample Barcode'),
    UIUtils.search_filter('"Patient"->name', op: 'contains', label:'Patient Name'),
    UIUtils.search_filter('"Patient"->DOB', op: 'eq', label:'DOB'),
    ].to_json
   
end