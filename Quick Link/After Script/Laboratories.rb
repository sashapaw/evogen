params[:subject_type] = 'Facility'
params[:need_search_panel] = true
#params[:explorer_states] = 'Active'
params[:query] = '#state = Active and RURO_CLIENT is true'
params[:search_guid] = "CLIENTS"


params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label: 'Name'),
  UIUtils.search_filter('"Contact Name"', op: 'contains', label: 'Contact Name'),
  UIUtils.search_filter('"Contact Email"', op: 'contains', label:'Contact Email'),
  UIUtils.search_filter('"Phone Number"', op: 'contains', label:'Phone Number'),
].to_json