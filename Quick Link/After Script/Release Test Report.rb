system_settings = LIS.system_settings

reference_lab_enabled = system_settings.get_value('Use Reference Lab')

params[:subject_type] = 'Requisition'
params[:explorer_states] = 'Received'
params[:need_search_panel] = true
params[:search_guid] = "RELEASE_TEST_REPORT"
params[:tab_name] = "Release Test Report"


received_state_def = SubjStateDef.where(name: 'Received', subj_flow_id: SubjFlow.where(name: 'Requisition')).first
params[:query] = search_query do |qb|
  ready_for_release_conds = qb.or
  ready_for_release_conds << qb.and(
      qb.compare('<-Requisition<-"Test Result"::Sample', :neq, nil),
      qb.not(qb.compare('<-Requisition<-"Test Result"::Sample->Finalized', :eq, nil))
  )
  
  
 # if the reference lab did the testing, and the report is attached, we can also release this req
  if reference_lab_enabled
    ready_for_release_conds << qb.and(
      qb.compare('Reference Lab', :neq, nil),
      qb.compare('Preliminary Report File', :neq, nil)
      )
  end
  
  qb.and(
    qb.state(received_state_def.id),
    qb.compare('Queued for Release', :eq, nil),
    ready_for_release_conds
  )
end

params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label:'Requisition'),
  UIUtils.search_filter('"Patient"->name', op: 'contains', label:'Patient'),
  UIUtils.search_filter('"Requesting Physician"->name', op: 'contains', label:'Requesting Physician'),
  UIUtils.search_filter('"Facility"->name', op: 'contains', label:'Facility')
].to_json

