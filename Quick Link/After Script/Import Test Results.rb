system_settings = LIS.system_settings
instrument = params['Instrument']
instrument_type = instrument.type
importer = TestResult::Importer.new(instrument)

complete_msg = ''
import_subjects = []
files = UploaderUtils.parse(params['files'])
init_progress(files.size)
files.each_with_data{|file, data|      
  start_time = Time.now
  import_subject = create_subject('Data Import') {|sp|
    sp.set_value('Instrument', instrument)
    sp.set_value('File', file)
    sp.set_value('Comments', data['Comments'])
    sp.advance_workflow('Data Import', 'Active')
  }
  import_subjects << import_subject
  TestResult.import_file(import_subject, instrument, importer, file)
  Rails.logger.info "Batch Import: File Process Completed in #{(Time.now - start_time) * 1000.0}ms"
  advance_progress
}

params[:subject_type] = 'Data Import'
params[:query] = search_query {|qb|
  qb.prop('id').in(import_subjects.map(&:id))
}
params[:search_guid] = "DataImports"
params[:tab_name] = "Imported Files"
show_message(complete_msg)
