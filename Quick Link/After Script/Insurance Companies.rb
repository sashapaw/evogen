params[:subject_type] = 'Insurance Company'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:query] = '#state = Active or name != null'
params[:search_guid] = "INSURANCE_COMPANIES"
params[:sort_field] = 'name'
params[:sort_direction] = 'ASC'

params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label: 'Company Name'),
  UIUtils.search_filter('Payer ID', op: 'contains', label: 'Payer ID'),
  UIUtils.search_filter('"Code"', op: 'contains', label: 'Code')
].to_json