params[:subject_type] = 'Contact'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:query] = '#state = Active'
params[:search_guid] = "CONTACTS"

params[:qb_filters] = [
  UIUtils.search_filter('"Full Name"', op: 'contains', label:'Full Name'),
  UIUtils.search_filter('"EMail"', op: 'contains', label:'EMail'),
  UIUtils.search_filter('"Phone Number"', op: 'contains', label:'Phone Number')
].to_json

