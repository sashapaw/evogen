params[:subject_type] = 'Portal Document'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:query] = '#state = Active'
params[:search_guid] = "DOCUMENTS"

params[:qb_filters] = [
  UIUtils.search_filter('"name"', op: 'contains', label: 'Name')
].to_json
