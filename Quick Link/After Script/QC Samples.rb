params[:subject_type] = 'QC Sample'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:query] = '#state = Active'
params[:search_guid] = "QCSAMPLES"

params[:qb_filters] = [
  UIUtils.search_filter('"Analyte"->name', op: 'contains', label:'Analyte'),
  UIUtils.search_filter('"Lot Number"', op: 'eq', label:'Lot Number'),
  UIUtils.search_filter('"Control Type"', op: 'eq', label:'Control Type')
].to_json