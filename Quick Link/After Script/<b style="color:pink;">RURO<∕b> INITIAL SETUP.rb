# 0. Add admin to RURO Admins group
ug = UserGroup.find_by_name('RURO Admins')
User.admin.user_groups << ug unless User.admin.user_groups.include?(ug)

# 1. Create Default Facility
facility = create_subject('Facility', {:name=>params['Name']}) do |v|
  v.set_value('Contact Name', params['Contact Name'])
  v.set_value('Contact Email', params['Contact Email'])
  v.set_value('RURO_CLIENT', true)
end

# 2. Create Facility Group
ug = UserGroup.create! name: "_#{facility.subject_type.name} #{facility.name}", acts_as_configuration: false
ug.extend ScriptRunner::Helper
ug.set_value 'Facility', facility
Acl.set_permission facility, ug, Acl::READWRITE


# 3. Create RURO User
u = create_user(
  username:'ruro',
  password:'ruro12345',
  fullname:'RURO Lab Administrator',
  email:params['EMail'],
  roles:['Lab Admin'], 
  groups:['Everyone', 'RURO Admins', ug]
)


# 4. Update System settings
gen = General.first
gen.update_attributes({:company_short_name=>'LIMITLIS.cloud'})
gen.update_attributes({:company_long_name=>'<i class="fa fa-flask fa-lg"></i>  ' + params['system']})
gen.update_attributes({:login_wallpaper=>'bliss.jpg'})
gen.update_attributes({:custom_theme=>'neptune'})
gen.update_attributes({:login_font_color=>'004466'})
gen.update_attributes({:send_invitation_email=>true})
gen.update_attributes({:retrieve_username_allowed=>true})
gen.update_attributes({:password_reset_allowed=>true})
gen.update_attributes({:login_extra_text=>'<b style="font-size:110%"><img style="top:40px;left:500;position:absolute;" src="https://www.ruro.com/clientfiles/logo.png">As your drug test provider, <i>YOUR COMPANY</i> offers a full line of laboratory-based and onsite drug testing solutions</b>'})


# 5. Create LIS System Settings
ss = create_subject('System Settings')
start_workflow('System Settings', ss)



open_subject(facility)