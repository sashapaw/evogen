params[:subject_type] = 'Requisition'
params[:need_search_panel] = true

if params['Find Requisitions'] == "Properties"
  #Lets Do Properity Searches here
  query = ''
  if params['Requisition'].present?
    open_subject(params['Requisition'] )
  end

  if params['Sample'].present?
    open_subject(params['Sample'].get_value('Requisition'))
  end

  if params['Patient'].present?
  	query = search_query do |qb|
  		qb.and(
    		qb.compare('Patient', :eq, params['Patient'].id)
    	)
	end
  end

   if params['Requesting Physician'].present?
  	query = search_query do |qb|
  		qb.and(
    		qb.compare('Requesting Physician', :eq, params['Requesting Physician'].id)
    	)
	end
  end

  if params['Facility'].present?
  	query = search_query do |qb|
  		qb.and(
    		qb.compare('Facility', :eq, params['Facility'].id)
    	)
	end
  end

  if params['Received Date'].present?
  	query = search_query do |qb|
  		qb.and(
            qb.compare('Received Date', :gte, params['Received Date'].beginning_of_day),
          	qb.compare('Received Date', :lte, params['Received Date'].end_of_day)
    	)
	end
  end

    if params['Collection Date'].present?
  	query = search_query do |qb|
  		qb.and(
    		qb.compare('Collection Date', :gte, params['Collection Date'].beginning_of_day),
          	qb.compare('Collection Date', :lte, params['Collection Date'].end_of_day)
    	)
	end
  end

  if params['Released Date'].present?
  	query = search_query do |qb|
  		qb.and(
            qb.compare('Released Date', :gte, params['Released Date'].beginning_of_day),
          	qb.compare('Released Date', :lte, params['Released Date'].end_of_day)
    	)
	end
  end

  if params['External Source ID'].present?
    query = search_query do |qb|
  		qb.and(
    		qb.compare('External Source ID', :eq, params['External Source ID'])
    	)
	end
  end

  if query != '' && (!params['Requisition'].present? || !params['Sample'].present?)
     params[:query] = query
     params[:search_guid] = "FIND_REPORT"
     params[:tab_name] = "Find Report"


     params[:qb_filters] = [
      UIUtils.search_filter('name', op: 'contains', label:'Requsition ID'),
      UIUtils.search_filter('"Patient"->name', op: 'contains', label:'Patient Name'),
      UIUtils.search_filter('"Result"', op: 'eq', label:'Result'),
      UIUtils.search_filter('Released Date', op:'gt', label:'Released After'),
      UIUtils.search_filter('Released Date', op:'lt', label:'Released Before')
     ].to_json
  else
     raise "Please fill out a requisition type or a properity field to complete the search." unless (params['Requisition'].present? || params['Sample'].present?)
  end
else
  req = params['Find Requisitions']
  if req == "Open Orders"
     params[:query] = '#state = (Submitted, Received)'
     params[:search_guid] = "OPEN_ORDERS"
     params[:tab_name] = "Open Orders"
  elsif req == "On Hold"
     params[:query] = "#state = 'On Hold'"
     params[:search_guid] = "ON_HOLD"
     params[:tab_name] = "On Hold"
  elsif req == "Released"
     params[:query] = '#state = (Released, Accepted)'
     params[:search_guid] = "RELEASED"
     params[:tab_name] = "Released"
  elsif req == "Queued for Release"
     params[:query] = '"Queued for Release" = true'
     params[:search_guid] = "QUEUEDRELEASED"
     params[:tab_name] = "Queued for Release"
  end

  qb_filters = []
  qb_filters <<  UIUtils.search_filter('name', op: 'contains', label:'Requsition ID')
  qb_filters <<  UIUtils.search_filter('External Source ID', op: 'contains', label:'External Source ID')
  qb_filters <<  UIUtils.search_filter('"Patient"->name', op: 'contains', label:'Patient Name')
  if req == "On Hold" 
    qb_filters <<  UIUtils.search_filter('"On Hold Reason"', op: 'eq', label:'Hold Reason')
  end
  qb_filters <<  UIUtils.search_filter('"Result"', op: 'eq', label:'Result')

  params[:qb_filters] = qb_filters.to_json
end