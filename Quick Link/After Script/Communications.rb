params[:tab_name] = 'All Communication'
params[:subject_type] = 'Communication'
params[:explorer_states] = 'Open'
params[:query] = '#state = Open'
params[:search_guid] = "COMMUNICATIONS"
params[:need_search_panel] = true

params[:qb_filters] = [
  UIUtils.search_filter('"Requisition"->name', op: 'contains', label: 'Requisition'),
  UIUtils.search_filter('"Facility"->name', op: 'contains', label: 'Facility'),  
  UIUtils.search_filter('"Message"', op: 'contains', label:'Message')
].to_json