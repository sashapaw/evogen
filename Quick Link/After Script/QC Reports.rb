params[:subject_type] = 'QC Report'
params[:need_search_panel] = true
params[:explorer_states] = 'Ready for Approval','Approved'
params[:query] = ''
params[:search_guid] = "QCREPORTS"

params[:qb_filters] = [
  UIUtils.search_filter('name', op: 'contains', label: 'Report Name'),
  UIUtils.search_filter('"Start Date"', op: 'eq', label: 'From'),
  UIUtils.search_filter('"End Date"', op: 'eq', label: 'To'),
  UIUtils.search_filter('state', op: 'eq', label: 'Status'),
].to_json