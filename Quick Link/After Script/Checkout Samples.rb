params[:subject_type] = 'Sample'
params[:need_search_panel] = true
params[:explorer_states] = 'Stored'
params[:query] = '#state = Stored'
params[:search_guid] = "SAMPLE_REMOVE"
params[:tab_name] = "Samples in Storage"

params[:qb_filters] = [
  UIUtils.search_filter('barcode_tag', op: 'contains', label: 'Sample Barcode'),  
  UIUtils.search_filter('"Requisition"->name', op: 'contains', label: 'Requisition'),
  UIUtils.search_filter('"Specimen Type"->name', op: 'contains', label:'Specimen Type')
].to_json