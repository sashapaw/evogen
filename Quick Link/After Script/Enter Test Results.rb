params[:subject_type] = 'Test Result'
params[:need_search_panel] = true

#params[:query] = 'Finalized = null'
params[:query] = "Finalized = null and Sample->Requisition->#state != 'On Hold'"
params[:search_guid] = "TEST_RESULTS"
params[:tab_name] = "Test Results"
params[:explorer_states] = 'Data Entry'

# Handle additional filters
params[:query] += " and Sample->Requisition->Facility = '#{params['Facility']}'"  if params['Facility'].present?
params[:query] += " and Sample->Requisition = '#{params['Requisition']}'"  if params['Requisition'].present?
params[:query] += " and Sample->Requisition->Patient = '#{params['Patient']}'"  if params['Patient'].present?
params[:query] += " and Sample = '#{params['Sample']}'"  if params['Sample'].present?
params[:query] += " and Analyte<-Test::Analytes = '#{params['Test']}'"  if params['Test'].present?
params[:query] += " and Analyte = '#{params['Analyte']}'"  if params['Analyte'].present?
params[:query] += " and Instrument = '#{params['Instrument']}'"  if params['Instrument'].present?

# set grid filter controls
params[:qb_filters] = [
  UIUtils.search_filter('"Sample"->name', op: 'contains', label:'Sample'),
  UIUtils.search_filter('Analyte<-Test::Analytes->name', op: 'contains', label:'Test'),
  UIUtils.search_filter('"Analyte"->name', op: 'contains', label:'Analyte'),
  UIUtils.search_filter('"Sample"->"Requisition"->"Patient"->name', op: 'contains', label:'Patient'),
  UIUtils.search_filter('"Sample"->"Requisition"->name', op: 'contains', label:'Requisition')
].to_json
