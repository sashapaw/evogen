params[:subject_type] = 'QC Test Result'
params[:need_search_panel] = true
params[:explorer_states] = 'Data Entry'
params[:query] = '#state != Finalized'
params[:search_guid] = "QCTESTRESULTS"

params[:sort_field] = Property.find_by_display_name('Analyte').id
params[:sort_direction] = "ASC"

params[:qb_filters] = [
  UIUtils.search_filter('"QC Run"->name', op: 'contains', label:'QC Run'),
  UIUtils.search_filter('"QC Run"->"Run Date"', op: 'eq', label:'QC Run Date'),
  UIUtils.search_filter('Analyte->name', op: 'contains', label:'Analyte')
].to_json

