req = params['Requisition']
results = find_subjects(query: "FROM \"Test Result\" WHERE Sample->Requisition = #{req.id}")
subj = create_subject('Result Correction') {|s|
  s.set_value('Requisition', req)
  s.set_value('Correction Date', params['Correction Date'])
  s.set_value('Reason', params['Reason'])
  s.set_value('All Results', results)
  s.set_value('Original Report File', params['Requisition'].get_value('Report File'))
}

advance_workflow('Result Correction', 'Started', subj)

open_subject(subj)