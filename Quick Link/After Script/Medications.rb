params[:subject_type] = 'Medication'
params[:need_search_panel] = true
params[:explorer_states] = 'Active'
params[:query] = 'terminated is false'
params[:search_guid] = "MEDICATIONS"

params[:qb_filters] = [
  UIUtils.search_filter('"Code"', op: 'contains', label: 'Code'),
  UIUtils.search_filter('"Description"', op: 'contains', label: 'Description')
].to_json