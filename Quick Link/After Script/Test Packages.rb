params[:need_search_panel] = true
params[:tab_name]='Test Packages'
params[:explorer_states] = 'Active'
params[:subject_type] = "Test"
params[:query] = 'terminated is false'
params[:search_guid] = "TESTS"

params[:qb_filters] = [
  UIUtils.search_filter('"Code"', op: 'contains', label: 'Code'),
  UIUtils.search_filter('"Description"', op: 'contains', label: 'Description'),
  UIUtils.search_filter('"CPT Codes"', op: 'contains', label:'CPT Codes')
].to_json
