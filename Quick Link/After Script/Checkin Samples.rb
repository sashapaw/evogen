params[:subject_type] = 'Sample'
params[:need_search_panel] = true
params[:query] = "#state = Released or #state = Accessioned or #state = 'In Testing' or #state = 'Stored'"
params[:search_guid] = "SAMPLE_STORE"
params[:tab_name] = "Samples ready to be stored"
params[:explorer_states] = ['Accessioned','In Testing', 'Released']

params[:qb_filters] = [
  UIUtils.search_filter('barcode_tag', op: 'contains', label: 'Sample Barcode'),  
  UIUtils.search_filter('"Requisition"->name', op: 'contains', label: 'Requisition'),
  UIUtils.search_filter('"Specimen Type"->name', op: 'contains', label:'Specimen Type')
].to_json