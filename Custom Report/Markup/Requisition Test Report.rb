<!doctype html>
<html>
<head>
	<title>Test Results</title>
	<meta name="pdfkit-page_size" content="Letter"/>
	<meta name="pdfkit-header_html" content="{{extra_data['header']}}">
	<meta name="pdfkit-footer_html" content="{{extra_data['footer']}}">
    <meta name="pdfkit-header_spacing" content="2" />
    <meta name="pdfkit-footer_spacing" content="2" />
	<meta name="pdfkit-margin_top" content="1.3in"/>
	<meta name="pdfkit-margin_bottom" content="0.7in"/>
	<meta name="pdfkit-margin_left" content="0.3in"/>
	<meta name="pdfkit-margin_right" content="0.3in"/>
	<style type="text/css">
{% assign width = extra_data.width %}
* {
	font-family: arial;
	font-size: 18px;
}
h1 {
  font-size: 20px;
  font-weight: normal;
  padding: 0;
  margin: 0;
}
h2 {
  padding: 0;
  margin-bottom: 2px;
}
div.section {
  width: {{ width }}px;
}
.box {
    display: inline-block;
    width: 18px;
    height: 18px;
    border: 1px solid #000;
    margin-right: 4px;
}
tr, img, div.legend, table.signatures {
    page-break-inside: avoid !important;
}
td {
	verical-align: top;
}
table {
	border-collapse: collapse;
}
td.label {
  font-weight:bold;
  text-align: right;
  padding-right: 4px;
}
.correction {
  color: #900;
  font-weight: bold;
}
table.results th {
	border: none;
	font-weight: normal;
	text-align: left;
	padding: 4px 0px;
}
table.results th div {
	margin-bottom: 4px;
}
table.results th div.title {
	font-size: 24px;
	font-weight: bold;
}
table.results td {
	border-bottom: 1px solid #aaa;
	padding: 2px 10px 3px 0px;
}
table.results td.multi {
	color: #666;
}
tr.header td {
	border-top: 1px solid black;
	border-bottom: 1px solid black;
	font-weight: bold;
  vertical-align: baseline;
}
tr.last td {
	border-bottom: 1px solid black;
}
.continued {
	font-size: 16px;
	margin-top: 4px;
	width: {{ width }}px;
}
.sep {
  margin-top: 1.5em;
}
.data {
  border-top: 1px solid black;
  border-bottom: 1px solid #aaa;
  padding: 2px 0;
  width: {{ width }}px;
}
.comments, .legend {
	padding-top: 20px;
}
.comments-title, .legend-title {
	font-weight: bold;
}
.comment {
	font-size: 16px;
	margin-top: 4px;
	width: {{ width }}px;
}
.legend table {
	margin-top: 8px;
}
.legend td {
	font-size: 18px;
	padding: 1px 10px 1px 0px;
}
table.signatures {
	margin-top: 50px;
}
table.signatures td {
	font-size: 18px;
}
table.signatures div {
	padding-bottom: 10px;
}
.page-header {
	padding-top: 25px;
	width: {{ width }}px;
}
.page-header td {
	vertical-align: top;
}
.page-break {
	page-break-before: always;
}
.page {
	border: 1px solid green;
	width: {{ width }}px;
}
	</style>
	<script src="{{extra_data['public_path']}}/javascripts/jQuery/jquery-1.11.0.min.js"></script>
	<script>
	$(window).load(function() {
	    var body = $('body'),
		    pager = $('#pager'),
		    header = $('div.page-header').detach(),
		    sections = $('div.section'),
		    pageHeight = pager[0].scrollHeight,
			page = 0;
        //pager.height(1130);
	    sections.each(function() {
		    pager.append(header.clone());
	        var section = $(this);
		    pager.append(section.detach());
			var nextPager = null;
		    do {
				var skipRows = 0,
					scrollHeight = pager[0].scrollHeight;
				if (scrollHeight > pageHeight) {
					var table = pager.find('table.results');
				    if (table.length) {
						nextPager = pager.html();
						pager.find('div.comment').before($('<div class="continued">Continued on next page&hellip;</div>'));
				  	    var rows = table.find('tbody > tr');
						for (var i = rows.length - 1; i > 0 && scrollHeight > pageHeight; i--) {
							var row = $(rows[i]);
							row.remove();
							scrollHeight = pager[0].scrollHeight;
						}
						skipRows = i + 1;
				    } else {
				    	//Add logic for long page without table.
						//We do not have such pages now.
				    }
				} else {
					nextPager = null;
				}
			    if (page > 0) {
			  	    pager.find('div.page-header').addClass('page-break');
			    }
			    body.append(pager.html());
				page++;
			    pager.empty();
				if (nextPager) {
					pager.html(nextPager);
					var table = pager.find('table.results');
					table.find('tbody > tr').slice(0, skipRows).remove();
					scrollHeight = pager[0].scrollHeight;
				}
		    } while (nextPager);
	    });
        //pager.height(1);
        //pager.hide();
	    pager.remove();
	});
	</script>
</head>
<body>
{% assign patient = subj["Patient"] %}
{% assign samples_data = extra_data.samples_data %}
  <div id="pager" style="width:{{ width }}px;height:1130px;overflow-y:hide;left:0;top:-2000px;position:absolute"></div>
  
{% assign break_sample_cls = '' %}
{% for sample_data in samples_data %}
{% assign sample = sample_data.sample %}
<div class="section {{ break_sample_cls }}" style="padding-top:8px;border-top:2px black solid">
  <table class="sample-header" style="width:{{ width }}px">
	<tr><td class="label">Patient Name:</td><td>{{ patient["Last Name"] }}, {{ patient["First Name"] }}</td>
      <td class="label">Physician:</td><td>{{ subj["Requesting Physician"]["Last Name"] }}, {{ subj["Requesting Physician"]["First Name"] }}</td></tr>
	<tr><td class="label">Patient MRN:</td><td>{{ patient["MRN"] }}</td>
      <td class="label">Facility:</td><td>{{ subj["Facility"].name }}</td></tr>
	<tr><td class="label">Date of Birth:</td><td>{{ patient["DOB"] | date: '%m/%d/%Y' }} ({{ patient["Age"] }} years old)</td>
      <td class="label">Date Collected:</td><td>{{ subj["Collection Date"] | date: '%m/%d/%Y %I:%M:%S %p' }}</td></tr>
	<tr><td class="label">Requisition:</td><td>{{ subj.name }}</td>
      <td class="label">Received:</td><td>{{ subj["Received Date"] | date: '%m/%d/%Y %I:%M:%S %p' }}</td></tr>
  	<tr><td class="label">Sample:</td><td><h1>{{ sample.name }}</h1></td>
      <td class="label">Released:</td><td>{{ subj["Released Date"] | date: '%m/%d/%Y %I:%M:%S %p' }}</td></tr>
	<tr><td class="label">Specimen Type:</td><td>{{ sample["Specimen Type"].name }}</td>
      <td class="label">Released By:</td><td>{{ subj["Released By"] }}</td></tr>
  </table>
</div>

{% if extra_data.amendment or extra_data.correction %}
<div class="section sep">
<span class="correction">{% if extra_data.amendment %}Amended{% else %}Corrected{% endif %}</span>
{% if extra_data.correction_user %}by {{ extra_data.correction_user }}{% endif %}
on {{ extra_data.correction_time | date: "%m/%d/%Y, %I:%M %p" }}
{% if extra_data.amendment_comments %}
<div class="data">{{ extra_data.amendment_comments | multiline }}</div>
{% endif %}
</div>
{% endif %}

<div class="section sep">
I verify that the results of this test have been evaluated against the current treatment plan and explained to the patient. Please sign, and date below. (Also initial each line item as you review test results below.)
<table style="width:{{ width }}px;margin-top:0.6em"><tr>
  <td>Patient: _____________________</td><td style="text-align:center">Doctor/MA: _____________________</td><td style="text-align:right">Date: ________________.</td>
</tr></table>
</div>

<div class="section sep">
<div style="float:right">Reviewed with patient________(Patient) ________(Doctor/MA)</div> <div class="box"></div><b>Medications:</b>
<div class="data">{% if extra_data["Medications"] != '' %}{{ extra_data["Medications"] }} {% else %} <span>None Prescribed</span> {% endif %}</div>
</div>

{% if sample_data.tests %}
{% assign result_groups = sample | sample_results_by_test : sample_data.tests %}
<div class="section sep">
  <div style="float:right">Reviewed with patient________(Patient) ________(Doctor/MA)</div> <div class="box"></div><b>Results:</b>
  <table class="results" style="width:{{ width }}px;">
	<thead>
		<tr class="header">
			<td>Drug</td>
			<td style="text-align:right">Result</td>
			<td style="text-align:right">Range</td>
			<td style="text-align:right">Unit</td>
			<td style="text-align:right">Flag</td>
			<td style="text-align:right">Outcome</td>
		</tr>
	</thead>
	<tbody>
{% for group in result_groups %}
   <tr><td colspan="6">{{ group.test }}{% if group.test.['Test Type'] %} ({{ group.test.['Test Type'] }}){% endif %}</td></tr>
  {% for result in group.results %}
    {% assign analyte = result['Analyte'] %}
    {% assign flag = result['Flag'] %}
    {% assign outcome = result['Outcome'] %}
    {% if outcome == 'CONSISTENT' %}
      {% assign color = flag['Color for Consistent'] %}
    {% elsif outcome == 'INCONSISTENT' %}
      {% assign color = flag['Color for Inconsistent'] %}
    {% else %}
      {% assign color = '000' %}
    {% endif %}
    <tr>
      <td style="padding-left:2ex">{{ analyte }} {% if analyte['Report Notes'] %}<span style="color:#999">{{ analyte['Report Notes'] }}</span>{% endif %}</td>
      <td style="text-align:right">{{ result | limited_result }}</td>
      <td style="text-align:right">{{ result['Range'] }}</td>
      <td style="text-align:right">{{ analyte['Unit'] }}</td>
      <td style="text-align:right;color:#{{ color }}">{{ flag['Description'] }}</td>
      <td style="text-align:right;color:#{{ color }}">{{ outcome }}</td>
    </tr>
  {% endfor %}
{% endfor %}
	</tbody>
  </table>
</div>
{% endif %}

{% assign break_sample_cls = 'page-break' %}
{% endfor %}

</body>
</html>