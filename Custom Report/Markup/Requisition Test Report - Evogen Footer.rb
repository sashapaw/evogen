<html>
  <head>
    <!-- Evogen Requisition Test Report Footer -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<style>
{% assign width = extra_data.width %}
* {
	font-family: arial;
	font-size: 14px;
}
td {
  vertical-align: top;
}
	</style>
  </head>
  <body onload="subst()">
    <table border="0" cellpadding="0" cellspacing="0" width="{{ width }}">
		<tr>
			<td width="90%">
			  {{ extra_data.system_settings['Lab Name'] }}, {{ extra_data.system_settings['Lab Address'] }}
              &bull; CLIA {{ extra_data.system_settings["CLIA Number"] }}
			</td>
			<td width="10%" style="text-align:right">
			  {{ subj.["Report Date"] | date: '%m%Y'}}
			</td>
		</tr>
    </table>
  </body>
</html>