<html>
<head>
  <title>Requisition Report</title>
  <meta name="pdfkit-page_size" content="Letter" />
  <meta name="pdfkit-margin_top" content="0.3in" />
  <meta name="pdfkit-margin_left" content="0.3in" />
  <meta name="pdfkit-margin_right" content="0.3in" />
</head>
<body style="cursor: auto;">
<style type="text/css">
  body {
      font-family: "Liberation Sans", "Arial", arial;
      font-size:12px;
    }
    table {
    	width: 100%; 	    
    	border-collapse:collapse;
        font-size:12px;
    }
    table, th, td {
    	border: 1px solid #cccccc;
    	padding:5px;
        font-size:12px;
    }
	.col {
      width:32%;
      display: inline-block;
      vertical-align: top;
    }
    .uline {
    	border-bottom:1px solid #000
    }
    .head {
      font-size:20px;
      font-weight:bold;
    }
    .small {
      font-size: 8px;
    }
    .center {
      text-align: center;
    }
    .right {
      text-align: right;
    }
    .display-module {
    	display: inline-block;
    	margin:3px;
    	border: 2px solid #eee;
    }
    .half {
    	width:48%;
    }
    .full {
    	width:98%;
    }
    .display-head {
    	font-weight: bold;
        font-size: 14px;
    	padding:5px;
    	color: #fff;
    	background-color: #275a9e;/*#62B7E6;*/
    }
    .bottom {
    	vertical-align: bottom;
    }
    .label {
      background-color: #eee;
      font-weight: bold;
    }
</style>
<div id="page">
<header>
<div class="col"><img alt="{{ extra_data['Lab Name'] }}" height="50px" src="data:image/jpeg;base64,{{ extra_data['Lab Logo'] }}" /></div>

<div class="col center">&nbsp;</div>

<div class="col right"><span>{{ subj.barcode_tag | render_barcode: 'code-128a', 1, 20, 'jpg' }}</span><br />
<span>{{ subj.barcode_tag }}&nbsp;</span></div>
</header>

<section>
<div class="col">{{ extra_data.system_setting["Lab Address"] | multiline }}</div>

<div class="col center bottom"><span class="head">Requisition Form</span></div>

<div class="col right bottom"><span><strong>Order Priority:</strong> Routine</span><br />
<span><strong>Date &amp; Time:</strong> {{ extra_data['Now'] | date: "%m/%d/%Y, %I:%M %p"}}</span></div>
</section>

<section style="padding-bottom:5px; border-bottom: 1px solid #cccccc;">
<div class="full">&nbsp;</div>
</section>

<section>
<div class="display-module half">
<div class="display-head center">Patient Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label" style="width:145px;">Patient Name</td>
			<td>{{ subj["Patient"]["Last Name"] }}, {{ subj["Patient"]["First Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Date of Birth</td>
			<td>{{ subj["Patient"]["DOB"] | date: "%m/%d/%Y" }}</td>
		</tr>
		<tr>
			<td class="label">Sex</td>
			<td>{{ subj["Patient"]["Sex"] }}</td>
		</tr>
		<tr>
			<td class="label">Patient Record #</td>
			<td>{{ subj["Patient"]["MRN"] }}</td>
		</tr>
		<tr>
			<td class="label">Address</td>
			<td>{{ subj["Patient"]["Address"] }}</td>
		</tr>
		<tr>
			<td class="label">Patient Phone</td>
			<td>{{ subj["Patient"]["Phone Number"] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>

<div class="display-module half">
<div class="display-head center">Clinic&nbsp;Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label" style="width:165px;">Facility Name</td>
			<td>{{ subj["Facility"] }}</td>
		</tr>
		<tr>
			<td class="label" style="width:165px;">Facility Contact Name</td>
			<td>{{ subj["Facility"]["Contact Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Physician Name</td>
			<td>{{ subj["Requesting Physician"]["Last Name"] }}, {{ subj["Requesting Physician"]["First Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Physician NPI</td>
			<td>{{ subj["Requesting Physician"]["NPI Number"] }}</td>
		</tr>
		<tr>
			<td class="label">Address</td>
			<td>{{ subj["Facility"]["Address"] }}</td>
		</tr>
		<tr>
			<td class="label">Phone</td>
			<td>{{ subj["Facility"]["Phone Number"] }}</td>
		</tr>
		<tr>
			<td class="label">Fax</td>
			<td>{{ subj["Facility"]["Fax Number"] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</section>
{% if extra_data["System Settings"]["Ask for Medical Necessity"] && subj["Medical Necessity Statement"] %}

<section>
<div class="display-module full">
<div class="display-head center">Medical Necessity</div>

<div class="display-content">
<div style="padding: 5px;">{{ subj["Medical Necessity Statement"] }}</div>
</div>
</div>
</section>
{% endif %}{% if subj['Bill To'] == "Patient/Self Pay" %}

<section>
<div class="display-module full">
<div class="display-head center">Billing Information</div>

<div class="display-content">Self Pay</div>
<div class="display-content">{{ subj['Self Pay Comments'] }}</div>
</div>
</section>
{% endif %}{% if subj['Bill To'] == "Facility" %}

<section>
<div class="display-module full">
<div class="display-head center">Billing Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label" style="width:65px;">Facility Name</td>
			<td>{{ subj['Billing Facility'].name }}</td>
			<td class="label" style="width:65px;">Facility Address</td>
			<td>{{ subj['Billing Facility Address'] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</section>
{% endif %}{% if subj['Bill To'] == "Insurance" or subj['Bill To'] == "Medicare" %}

<section>
<div class="display-module full">
<div class="display-head center">Billing Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label" style="width:165px;">Insurance Company 1</td>
			<td>{{ subj["Insurance Company 1"].name }}</td>
			<td class="label" style="width:85px;">Subscriber</td>
			<td>{{ subj["Relation To Insured"] }}</td>
			<td class="label" style="width:65px;">Policy #</td>
			<td>{{ subj["Policy #1"] }}</td>
			<td class="label" style="width:65px;">Group #</td>
			<td>{{ subj["Group #1"] }}</td>
		</tr>
		<tr>
			<td class="label" style="width:165px;">Insurance Company 2</td>
			<td>{{ subj["Insurance Company 2"].name }}</td>
			<td class="label" style="width:85px;">Subscriber</td>
			<td>{{ subj["Relation To Insured"] }}</td>
			<td class="label" style="width:65px;">Policy #</td>
			<td>{{ subj["Policy #2"] }}</td>
			<td class="label" style="width:65px;">Group #</td>
			<td>{{ subj["Group #2"] }}</td>
		</tr>
		<tr>
			<td class="label" style="width:165px;">Insurance Company 3</td>
			<td>{{ subj["Insurance Company 3"].name }}</td>
			<td class="label" style="width:85px;">Subscriber</td>
			<td>{{ subj["Relation To Insured"] }}</td>
			<td class="label" style="width:65px;">Policy #</td>
			<td>{{ subj["Policy #3"] }}</td>
			<td class="label" style="width:65px;">Group #</td>
			<td>{{ subj["Group #3"] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</section>
{% endif %}

<section>
<div class="display-module full">
<div class="display-head center">Order Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label">Order Date</td>
			<td>{{ subj.created_at | date: "%m/%d/%Y" }}</td>
			<td class="label">Requisition #</td>
			<td>{{ subj.name }}</td>
			<td class="label">Alternate Requisition #</td>
		</tr>
		<tr>
			<td class="label">ICD 10 Codes</td>
			<td colspan="4">{% for d in subj["Diagnoses"] %}{{ d["Code"] }}&nbsp;&nbsp;{% endfor %}</td>
		</tr>
		<tr>
			<td class="label">Tests</td>
			<td colspan="4">{{ subj["Tests"] | join: ', ' }}</td>
		</tr>
		<tr>
			<td class="label">Comments</td>
			<td colspan="4">{{ subj["Comments"] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</section>

<section>
<div class="display-module full">
<div class="display-head center">Specimen Information</div>

<div class="display-content">
<table>
	<thead>
		<tr>
			<th>Specimen Types</th>
			<th>Collection Date</th>
			<th>Collection Time</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ subj["Specimen Types"] | join: ', '}}</td>
			<td>{{ subj["Collection Date"] | date: "%m/%d/%Y" }}</td>
			<td>{{ subj["Collection Date"] | date: "%I:%M %p" }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</section>

{% if extra_data["poc_test_results"] %}
<section>
<div class="display-module full">
<div class="display-head center">Point of Care Test Results</div>

<div class="display-content">
<table>
	<thead>
		<tr>
			<th>Test</th>
			<th>Result</th>
		</tr>
	</thead>
	<tbody>{% assign poct_results = extra_data["poc_test_results"] | poct_result_sort_abc %}{% for poct in poct_results %}
		<tr>
			<td>{{ poct["Point of Care Test"]["name"] }}</td>
			<td>{{ poct["PoC Result"] }}</td>
		</tr>
		{% endfor %}
	</tbody>
</table>
</div>
</div>
</section>
{% endif %}

{% if extra_data.conf_test_results %}
<section>
<div class="display-module full">
<div class="display-head center">Confirmation Test Results</div>

<div class="display-content">
<table>
	<thead>
		<tr>
			<th>Test</th>
			<th>Result</th>
		</tr>
	</thead>
	<tbody>{% assign entries = extra_data.conf_test_results %}{% for entry in entries %}
		<tr>
			<td>{{ entry["Confirmation Test"].name }}</td>
			<td>{{ entry["PoC Result"] }}</td>
		</tr>
		{% endfor %}
	</tbody>
</table>
</div>
</div>
</section>
{% endif %}

<footer>
<div class="full" style="font-size:8px;">{{ extra_data["System Settings"]["Signature Authorization Statement"] }}</div>
&nbsp; {% if extra_data["System Settings"]["Use Provider Practitioner Authorization Statement"] %}

<div class="full" style="font-size:8px;">{{ extra_data["System Settings"]["Provider Practitioner Authorization Statement"] }}</div>
{% endif %}

<div class="col" style="padding:5px;">
<div class="uline" style="height:60px">
{% if extra_data['Physician Signature'] %}
<img alt="" height="60px" src="data:image/jpeg;base64,{{ extra_data['Physician Signature'] }}" />
{% endif %}
</div>
<div>Physician's Signature</div>
</div>

<div class="col" style="padding:5px;">
<div class="uline" style="height:60px">
{% if extra_data['Patient Signature'] %}
<img alt="" height="60px" src="data:image/jpeg;base64,{{ extra_data['Patient Signature'] }}" />
{% endif %}
</div>
<div>Patient's Signature</div>
</div>

<div class="col right bottom"><span class="small">Limfinity LIS © RURO Inc.</span></div>
</footer>
</div>
</body>
</html>
