<!doctype html>
<html>
<head>
	<title>Report</title>
</head>
<body style="cursor: auto;">
<style type="text/css">body {
      /*font-family: arial,helvetica,verdana,sans-serif;*/
      font-family: Arial;
      font-size:9px;
    }
    section{
      padding-bottom: 5px;
      padding-top:5px;
    }
    .col {
      width:32%;
      display: inline-block;
      vertical-align: top;
    }
    .head {
      font-size:16px;
      font-weight: bold;
    }
    .small {
      font-size: 8px;
    }
    .center {
      text-align: center;
    }
    .right {
      text-align: right;
    }
    .info_head {
      font-size: 10px;
      font-weight: bold;     
    }
    .pagebreak {
        page-break-before: always;
    }        
    table {
      width:100%
    }
    .table_center {
      text-align: center;
    }
    .table_left {
      text-align: left;
    }
    .table_right {
      text-align: right;
    }
    td {
      vertical-align: top;
    }
    tr.test_head td {
      border-top:1pt solid black;
      font-weight: bold;
    }
    tr.alert td {
      color:red;
      font-weight: bold;
    }

    .rtable {
      display:table;
      width:100%;
    }
    .rhead {
      display:table-header-group;
    }
    .rbody {
      display:table-row-group;
    }
    .rrow {
      display:table-row;
    }
    .rth {
      display:table-cell;
      text-align: left;
      font-weight: bold;
      padding:2px;
    }
    .rtd {
      display:table-cell;
      padding:2px;
      white-space: nowrap;
    }
    .rtdhead {
      display:table-cell;
      padding:2px;
    }
    .row_head {
      border-top:1pt solid black;
      font-weight: bold
    }
    .correction {
       font-weight: bold;
       color: Red;
       font-size: 18px;
    }
    .amendment {
       font-weight: bold;
       color: DarkGray;
       font-size: 18px;
    }
</style>
<div id="page">{% assign samples = subj["Samples"] %}{% for sample in samples %}{% assign results = sample["Test Results"] %}
<div id="page-head">
<header>
<div class="col"><img alt="{{ extra_data['Lab Name'] }}" height="50px" src="data:image/jpeg;base64,{{ extra_data['Lab Logo'] }}" /></div>

<div class="col center"><span class="head">Laboratory Report</span> {% if extra_data["correction"] %}<br />
<span class="correction">Correction</span> {% endif %}</div>

<div class="col right small">Laboratory Director {{ extra_data["Lab Director"] }}<br />
CLIA Number: {{ extra_data["CLIA Number"] }}<br />
{{ extra_data["Lab Address"] }}<br />
{{ extra_data["system_settings"]["Lab Phone"] }}</div>
</header>

<section>&nbsp; <!--Spacer--></section>

<section>
<div class="col"><span class="info_head">Clinic Information</span><br />
<strong>Client:</strong> {{ subj["Facility"].name }}<br />
{{ subj["Facility"]["Address"] }}<br />
<strong style="font-size:10px; white-space: nowrap">Requesting Physician / Practitioner:</strong><br />
{{ subj["Requesting Physician"]["Last Name"] }}, {{ subj["Requesting Physician"]["First Name"] }}</div>

<div class="col"><span class="info_head">Patient Information</span><br />
<strong>Patient Name:</strong> {{ subj["Patient"]["Last Name"] }}, {{ subj["Patient"]["First Name"] }}<br />
<strong>Patient ID:</strong> {{ subj["Patient"]["MRN"] }}<br />
<strong>Date of Birth:</strong> {{ subj["Patient"]["DOB"] }}<br />
<strong>Male/Female:</strong> {% if subj["Patient"]["Sex"] == 'M' %} <span>Male</span> {% elsif subj["Patient"]["Sex"] == 'F' %} <span>Female</span> {% else %} <span>Unknown</span> {% endif %}</div>

<div class="col"><span class="info_head">Sample Information</span><br />
<strong>Lab Sample ID:</strong> {{ sample.name }}<br />
<strong>Requisition ID:</strong> {{ subj.name }}<br />
<strong>Specimen Type:</strong> {{ sample["Specimen Type"]["name"] }}<br />
<strong>Collected:</strong> {{ sample["Collection Date"] | date: "%m/%d/%Y, %I:%M %p" }}<br />
<strong>Received:</strong> {{ sample["Received Date"] | date: "%m/%d/%Y, %I:%M %p" }}<br />
<strong>Reported:</strong> {{ extra_data["now"] | date: "%m/%d/%Y" }}</div>
</section>
{% if extra_data["amendment_comments"] != "" %}

<section>
<div style="border-bottom: 1px solid #000"><strong>Amendment Comments</strong></div>

<div style="padding-top: 2px;">{{ extra_data["amendment_comments"] }}</div>
</section>
{% endif %}

<section>
<div style="margin-top:5px;border-bottom: 1px solid #000;"><strong>Medications Prescribed</strong></div>

<div style="padding-top: 2px;padding-bottom: 4px;">{% if extra_data["Medications"] != '' %}{{ extra_data["Medications"] }} {% else %} <span>None Prescribed</span> {% endif %}</div>
</section>
</div>

{% if render_test_codes %}
<section>
<div style="margin-top:5px;border-bottom: 1px solid #000;"><strong>Order Code(s)</strong></div>
<div style="padding-top: 2px;padding-bottom: 4px;">{% assign tests = subj["Tests"] %}{% for test in tests %}{% assign codes = test["Code"] %}{{ codes }},&nbsp;{% endfor %}</div>
</section>
{% endif %}

</div>
<!-- end page-head -->
{% endfor %}
</body>
</html>
