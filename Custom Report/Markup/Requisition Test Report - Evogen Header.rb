<!doctype html>
<html>
<head>
	<!-- Evogen Requisition Test Report Header -->
</head>
<body style="cursor: auto;">
<style type="text/css">
{% assign width = extra_data.width %}
* {
	font-family: arial;
	font-size: 16px;
}
  .phone {
    font-size: 24px;
    font-weight: bold;
    text-align: right;
  }
  .title {
    color: #007299;
    font-size: 34px;
    font-weight: bold;
    text-align: right;
  }
  .test {
    color: #007299;
    font-size: 21px;
    text-align: right;
  }
  .contacts {
    width: 100%;
  }
</style>
<header style="width:{{ width }}px">
  <table width="{{ width }}">
    <tr>
      <td width="50%" style="vertical-align:bottom"><img alt="{{ extra_data.system_settings['Lab Name'] }}" height="66px" src="data:image/png;base64,{{ extra_data['Lab Logo'] }}" /></td>
      <td width="50%" style="vertical-align:bottom;text-align:right">
        <div class="phone">{{ extra_data.system_settings['Lab Phone'] }}</div>
        <div class="title">Fragile X Analysis</div>
        <div class="test">37300 - <i style="font-size:20px">FMR1</i></div>
      </td>
    </tr>
  </table>
</header>
</body>
</html>
