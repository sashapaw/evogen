<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<style>
{% assign width = extra_data.width %}
* {
	font-family: arial;
	font-size: 18px;
}
td {
  vertical-align: top;
}
	</style>
    <script>
      function subst() {
        var vars={};
        var x=document.location.search.substring(1).split('&');
        for (var i in x) {
          var z=x[i].split('=',2);vars[z[0]] = unescape(z[1]);
        }
        var x=['frompage','topage','page','webpage','section','subsection','subsubsection', 'date'];
        for (var i in x) {
          var y = document.getElementsByClassName(x[i]);
          for (var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
        }
      }
    </script>
  </head>
  <body onload="subst()">
    <table border="0" cellpadding="0" cellspacing="0" width="{{ width }}">
		<tr>
			<td width="40%">
			  <span class="section"></span> - {{ subj["Patient"]["Last Name"] }}, {{ subj["Patient"]["First Name"] }} ({{ subj["Patient"]["MRN"] }})
			</td>
			<td width="40%">
			  Lab Director: {{ extra_data.system_settings["Lab Director"] }}<br/>
              CLIA #: {{ extra_data.system_settings["CLIA Number"] }}
			</td>
			<td width="20%" style="text-align: right">Page <span class="page"></span> of <span class="topage"></span></td>
		</tr>
    </table>
  </body>
</html>