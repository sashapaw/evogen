<!doctype html>
<html>
<head>
	<title>Report</title>
</head>
<body style="cursor: auto;">
<style type="text/css">
{% assign width = extra_data.width %}
* {
	font-family: arial;
	font-size: 18px;
}

    section{
      padding-bottom: 5px;
      padding-top:5px;
    }
    .col {
      width:40%;
      display: inline-block;
      vertical-align: top;
    }
    .head {
      font-size:16px;
      font-weight: bold;
    }
    .small {
      font-size: 10px;
    }
    .center {
      text-align: center;
    }
    .right {
      float: right;
    }
    .info_head {
      font-size: 10px;
      font-weight: bold;     
    }
    .pagebreak {
        page-break-before: always;
    }        
    .table_center {
      text-align: center;
    }
    .table_left {
      text-align: left;
    }
    .table_right {
      text-align: right;
    }
    td {
      vertical-align: top;
    }
    tr.test_head td {
      border-top:1pt solid black;
      font-weight: bold;
    }
    tr.alert td {
      color:red;
      font-weight: bold;
    }

    .rtable {
      display:table;
      width:100%;
    }
    .rhead {
      display:table-header-group;
    }
    .rbody {
      display:table-row-group;
    }
    .rrow {
      display:table-row;
    }
    .rth {
      display:table-cell;
      text-align: left;
      font-weight: bold;
      padding:2px;
    }
    .rtd {
      display:table-cell;
      padding:2px;
      white-space: nowrap;
    }
    .rtdhead {
      display:table-cell;
      padding:2px;
    }
    .row_head {
      border-top:1pt solid black;
      font-weight: bold
    }
    .correction {
       font-weight: bold;
       color: Red;
       font-size: 18px;
    }
    .amendment {
       font-weight: bold;
       color: DarkGray;
       font-size: 18px;
    }
</style>
<header style="width:{{ width }}px">
  <table width="{{ width }}">
    <tr>
      <td width="60%" style="vertical-align:bottom"><img alt="{{ extra_data.system_settings['Lab Name'] }}" height="100px" src="data:image/png;base64,{{ extra_data['Lab Logo'] }}" /></td>
      <td width="40%" style="text-align:center">
        {{ extra_data.system_settings["Lab Name"] }}<br/>
        {{ extra_data.system_settings['Lab Address'] | multiline }}<br/>
        Phone: {{ extra_data.system_settings['Lab Phone'] }}</td>
    </tr>
  </table>
</header>
</body>
</html>
