<html>
<head>
	<title></title>
	<script src="https://lis-dev.limfinity.net/javascripts/amcharts_3.14.4/amcharts/amcharts.js"></script><script src="https://lis-dev.limfinity.net/javascripts/amcharts_3.14.4/amcharts/serial.js"></script><script src="https://lis-dev.limfinity.net/javascripts/amcharts_3.14.4/amcharts/themes/patterns.js"></script><script src="https://lis-dev.limfinity.net/javascripts/amcharts_3.14.4/amcharts/plugins/export/export.js" type="text/javascript"></script>
	<link href="https://lis-dev.limfinity.net/javascripts/amcharts_3.14.4/amcharts/plugins/export/export.css" rel="stylesheet" type="text/css" />
</head>
<body>
<style type="text/css">.chartdiv {
	width		: 830px;
	height		: 520px;
	font-size	: 11px;
}

<style type="text/css">

.wrap{
    text-align:center
}
.left{
    float: left;
    text-align: left;
}
.clear {
  clear: both;
}
.right{
    float: right;
    text-align:right;
    display:inline;
}
.center{
   display:inline;
}
.info_title {
        font-weight: bold;
        font-size: 12px;
    }
.section {
    width: 100%;
}
body{
    font-size: 10px;
}
.logo_text {
    font-size:20px;
    font-style: italic;
    font-weight: bold;
}
.logo_text_align {
    vertical-align:middle;
    text-align:center;
}
.address_text{
    font-style: italic;
}
.pagebreak {
    page-break-before: always;
}
</style>
{% if extra_data["LJ Charts"].size < 1  %}
<div class="info_title">No finalized QC Test Result data found in this date range.</div>
{% else %}{% for s in extra_data["LJ Charts"] %}&nbsp;

<header>
<div style="text-align: center;">
<div class="left"><img alt="{{ extra_data['Lab Name'] }}" class="logo_text_align" height="60px" src="data:image/jpeg;base64,{{ extra_data['Lab Logo'] }}" />&nbsp;<span class="logo_text">{{ extra_data['Lab Name'] }}</span></div>

<div class="right">
<div><span class="address_text">{{ extra_data['Lab Address'] }}</span></div>
</div>

<div class="center">
<div class="info_title">Levey Jennings Report</div>

<div class="info_title">Date Range: {{s['Start Date']}} - {{s['End Date']}}</div>

<div class="info_title">Instrument: {{s['Instrument'].name}}</div>
</div>
</div>
</header>

<div class="clear">&nbsp;</div>
<script>
var div = document.createElement("div");
var divId = "chartdiv" + {{ forloop.index }}
div.setAttribute("id", divId);
div.className = "chartdiv";

// append the div to the body
document.body.appendChild(div);

// create guides
 var guides = [];
var target = {};
{% if {{s['QC Range'].['Target Value']}}  %}
target = {dashLength: 6, lineColor: '#088A08', inside: true, label: 'Target', lineAlpha: 1, value:{{s['QC Range'].['Target Value']}} }
{% endif %}
guides.push(target);

var min = {};
var max = {};
var sd1max = {};
var sd1min= {};
var mean = {};
var percentRangeMin = {}
var percentRangeMax = {}
{% if false and {{s['QC Range'].['SD']}}  %}
min= {dashLength: 8, inside: true, lineColor: '#FF0000', label: 'SD2', lineAlpha: 1, value: {{s['QC Range'].['Target Value']}} - 2*{{s['QC Range'].['SD']}} }
guides.push(min);


max = {dashLength: 8, inside: true, lineColor: '#FF0000', label: 'SD2', lineAlpha: 1, value: {{s['QC Range'].['Target Value']}} + 2*{{s['QC Range'].['SD']}} }
guides.push(max);

sd1max = {dashLength: 3, inside: true, lineColor: '#FF0000', label: 'SD1', lineAlpha: 1, value: {{s['QC Range'].['Target Value']}} + {{s['QC Range'].['SD']}} }
guides.push(sd1max);

sd1min = {dashLength: 3, inside: true, lineColor: '#FF0000', label: 'SD1', lineAlpha: 1, value: {{s['QC Range'].['Target Value']}} - {{s['QC Range'].['SD']}} }
guides.push(sd1min);
{% endif %}

{% if {{s['QC Range'].['Target Value']}}  %}
mean = {inside: true, lineColor: '#2498d2', label: 'Target Value', lineAlpha: 1, value: {{s['QC Range'].['Target Value']}} }
guides.push(mean);
{% endif %}

{% if {{s['QC Range'].['Minimum Valid Range']}}  %}
min = {dashLength: 8, inside: true, lineColor: '#FF0000', label: 'Minimum Valid Range', lineAlpha: 1, value: {{s['QC Range'].['Minimum Valid Range']}}  }
guides.push(min);
{% endif %}

{% if {{s['QC Range'].['-10% Range']}}  %}
percentRangeMin = {dashLength: 8, inside: true, lineColor: '#FF0000', label: '-10% Range', lineAlpha: 1, value:  {{s['QC Range'].['-10% Range']}}  }
guides.push(percentRangeMin);
{% endif %}

{% if {{s['QC Range'].['Maximum Valid Range']}}  %}
max = {dashLength: 8, inside: true, lineColor: '#FF0000', label: 'Maximum Valid Range', lineAlpha: 1, value: {{s['QC Range'].['Maximum Valid Range']}} }
guides.push(max);
{% endif %}

{% if {{s['QC Range'].['+10% Range']}}  %}
percentRangeMax = {dashLength: 8, inside: true, lineColor: '#FF0000', label: '+10% Range', lineAlpha: 1, value: {{s['QC Range'].['+10% Range']}} }
guides.push(percentRangeMax);
{% endif %}


//create titles
var titles = [];
var text = 'Analyte: {{s['Analyte'].name}}' + ',   Control: {{s['QC Sample'].['Control Type']}}';
var title = {text: text, size: 15};
titles.push(title);

var valueAxisTitle = "Measurement " + "{{s['QC Range'].['Analyte'].['Unit'].name}}"

//create data
var data = [];

{% for qct in s['QC Test Results'] %}
{% if {{qct['Within 20% Target?'] == 'Yes' %}
data.push({date:"{{qct['QC Run'].['Run Date/Time']}}", value: {{qct['Value']}}})
 {% else %}
data.push({date:"{{qct['QC Run'].['Run Date/Time']}}", value2: {{qct['Value']}}})

{% endif %}
{% endfor %}

</script> <script>
// sort the points on the date field
data.sort(function(a,b) {return (a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0);} );
var chart = AmCharts.makeChart(divId, {
    "type": "serial",
    "theme": "light",
    "titles":titles,
    "marginRight": 40,
   "export": {enabled: true},
    "marginLeft": 60,
    "autoMarginOffset": 20,
    "dataDateFormat": "MM-DD-YYYY",
    "legend":{
   	    "position":"right",
        "marginRight":100,
        "autoMargins":false
     },
    "valueAxes": [{
        "id": "v1",
        "title": valueAxisTitle,
        "axisAlpha": 0,
        "position": "left",
        "ignoreAxisWidth":true,
        "includeGuidesInMinMax": true,
    }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "id": "g1",
        "balloon":{
          "drop":true,
          "adjustBorderColor":false,
          "color":"#ffffff",
          "type":"smoothedLine"
        },
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 5,
        "hideBulletsCount": 50,
        "negativeLineColor": "#d1655d",
        "type": "smoothedLine",
        "lineColor": "#637bb6",
        "lineThickness": 2,
        "title": "Passed Values",
        "useLineColorForBulletBorder": true,
        "valueField": "value",
        "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
    }, {
        "id": "g2",
        "balloon":{
          "drop":true,
          "adjustBorderColor":false,
          "color":"#ffffff",
          "type":"smoothedLine"
        },
        "bullet": "round",
        "connect": false,
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "customBullet": "https://www.amcharts.com/lib/3/images/redstar.png",
        "bulletSize": 10,
        "hideBulletsCount": 50,
        "negativeLineColor": "#d1655d",
        "type": "smoothedLine",
        "lineColor": "#fbd51a",
        "lineAlpha": "0",
        "lineThickness": 2,
        "title": "Failed Values",
        "useLineColorForBulletBorder": true,
        "valueField": "value2",
        "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
    }],
    "chartCursor": {
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "cursorAlpha":0,
        "zoomable":false,
        "valueZoomable":true,
        "valueLineAlpha":0.5
    },
    "valueScrollbar":{
     "autoGridCount":true,
      "color":"#000000",
      "scrollbarHeight":50
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates":false,
        "dashLength": 1,
        "minorGridEnabled": true,
        "labelRotation": 90
    },
    "guides":guides,
    "dataProvider": data
   
});


</script> &nbsp; &nbsp;

<div class="clear" style="height:50px;">&nbsp;</div>

<footer>
<div style="text-align:center;">
<div class="left">QC Sample: {{s['QC Sample'].name}}</div>

<div class="right">Expires: {{s['QC Sample'].['Lot Expiration Date']}}</div>

<div class="center">Lot #: {{s['QC Sample'].['Lot Number']}}</div>
</div>

<div class="left">Mean: {{s['Mean'] | round}}</div>
&nbsp;&nbsp;&nbsp;&nbsp;

<div class="left">&nbsp; &nbsp;SD: {{s['SD'] | round}}</div>
</footer>
&nbsp;

<div class="left">&nbsp;</div>

<div class="pagebreak">&nbsp;</div>
{% endfor %} &nbsp;</body>
{% endif %}</html>
