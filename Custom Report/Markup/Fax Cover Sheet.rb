<html>
<head>
	<title>Fax Transmission</title>
	<style type="text/css">body {
			font-family: "Liberation Sans", "Arial", arial;
		}
		.company_name {
		    font-size: 32px;
           	font-weight:bold;
		}
		.table {
			display: table;
			width: 100%;			
		}
		.row {
			display: table-row;
		}
		.cell {
			display: table-cell;			
		}
		.bb {
			padding-top: 5px;
			padding-bottom: 5px;
			border-bottom: 1px solid #000;
		}
	</style>
</head>
<body>
<header>
<div class="table">
<div class="cell"><span class="company_name">{{ extra_data['Lab Name'] }}</span><br />
{{ extra_data['Lab Address'] }}</div>

<div class="cell"><img alt="Logo Goes Here" height="80px" src="data:image/jpeg;base64,{{ extra_data['Lab Logo'] }}" /></div>
</div>
</header>

<section>
<h2>FACSIMILE TRANSMITTAL</h2>

<div class="table">
<div class="row">
<div class="cell bb">To: {{ extra_data['to'] }}</div>

<div class="cell bb">From: {{ extra_data['Lab Name'] }}</div>
</div>

<div class="row">
<div class="cell bb">Fax: {{ extra_data['to_fax'] }}</div>

<div class="cell bb">&nbsp;</div>
</div>

<div class="row">
<div class="cell bb">Phone: {{ extra_data['to_phone'] }}</div>

<div class="cell bb">Date: {{ extra_data['to_date'] | date: "%m/%d/%Y" }}</div>
</div>
</div>

<div class="bb" style="width:100%">RE: {{ extra_data['regarding'] }}</div>

<div class="bb"><!--Spacer-->&nbsp;</div>

<div style="height:375px">Comments: {{ extra_data["Comments"] }}</div>
</section>

<div class="bb"><!--Spacer-->&nbsp;</div>

<footer style="padding-top:5px;">This facsimile transmission contains information, which is confidential and/or privileged. This information is intended for use only by the addressee indicated above. If you are not the intended recipient, please be advised that any disclosure, copying, distribution, or use of the contents of this information is strictly prohibited, and that any misdirected or improperly received information must be returned to this company immediately. Your cooperation in phoning us of erroneous receipt is requested.</footer>
</body>
</html>
