<!doctype html>
<html>
<head>
	<title>Report</title>
</head>
<body style="cursor: auto;">
<style type="text/css">body {
      font-family: helvetica,arial,verdana,sans-serif;
      font-size:10px;
    }
    section{
      padding-bottom: 5px;
      padding-top:5px;
    }
    .col {
      width:32%;
      display: inline-block;
      vertical-align: top;
    }
    .head {
      font-size:20px;
    }
    .small {
      font-size: 8px;
    }
    .center {
      text-align: center;
    }
    .right {
      text-align: right;
    }
    .info_head {
      font-size: 12px;
      font-weight: bold;
      text-decoration: underline;
    }
    table {
      width:100%
    }
    thead tr th {
      text-align: left;
    }
    td {
      vertical-align: top;
    }
    tr.test_head td {
      border-top:1pt solid black;
      font-weight: bold
    }
    tr.alert td {
      color:red;
      font-weight: bold;
    }

    .rtable {
      display:table;
      width:100%;
    }
    .rhead {
      display:table-header-group;
    }
    .rbody {
      display:table-row-group;
    }
    .rrow {
      display:table-row;
    }
    .rth {
      display:table-cell;
      text-align: left;
      font-weight: bold;
      padding:2px;
    }
    .rtd {
      display:table-cell;
      padding:2px;
      white-space: nowrap;
    }
    .rtdhead {
      display:table-cell;
      padding:2px;
    }
    .row_head {
      border-top:1pt solid black;
      font-weight: bold
    }
    .correction {
       font-weight: bold;
       color: Red;
       font-size: 18px;
    }
    .amendment {
       font-weight: bold;
       color: DarkGray;
       font-size: 18px;
    }
</style>
<header>
<div class="col"><img alt="{{ extra_data['Lab Name'] }}" height="50px" src="data:image/jpeg;base64,{{ extra_data['Lab Logo'] }}" /></div>

<div class="col center"><span class="head">Laboratory Report</span></div>

<div class="col right small">Laboratory Director {{ extra_data["Lab Director"] }}<br />
CLIA Number: {{ extra_data["CLIA Number"] }}<br />
{{ extra_data["Lab Address"]}}</div>
</header>

<section>&nbsp; <!--Spacer--></section>

<section>
<div class="col"><span class="info_head">Clinic Information</span><br />
<strong>Client:</strong> {{ subj["Facility"].name }}<br />
{{ subj["Facility"]["Address"] }}<br />
<strong>Requesting Physician / Practitioner:</strong><br />
{{ subj["Requesting Physician"]["Last Name"] }}, {{ subj["Requesting Physician"]["First Name"] }}</div>

<div class="col"><span class="info_head">Patient Information</span><br />
<strong>Patient Name:</strong> {{ subj["Patient"]["Last Name"] }}, {{ subj["Patient"]["First Name"] }}<br />
<strong>Patient ID:</strong> {{ subj["Patient"]["MRN"] }}<br />
<strong>Date of Birth:</strong> {{ subj["Patient"]["DOB"] }}<br />
<strong>Male/Female:</strong> {% if subj["Patient"]["Sex"] == 'M' %} <span>Male</span> {% elsif subj["Patient"]["Sex"] == 'F' %} <span>Female</span> {% else %} <span>Unknown</span> {% endif %}</div>

<div class="col"><span class="info_head">Sample Information</span><br />
<strong>Lab Sample ID:</strong> No Sample provided<br />
<strong>Specimen Type:</strong> No Type indicated<br />
<strong>Collected:</strong><br />
<strong>Received:</strong><br />
<strong>Reported:</strong></div>
</section>

<section>
<div style="border-bottom: 1px solid #000"><strong>Mediciations Prescribed</strong></div>

<div style="padding-top: 2px;"><span>None Prescribed</span></div>
</section>

<section><!-- Cancellation -->
<p style="font-size:14px;">No sample has been provided</p>

<p style="font-size:14px;"><span style="font-size:16px;font-weight:bold;">Cancellation Reason:</span><br />
{{ subj["Cancellation Comments"] }}</p>
</section>

<footer><!--Page Foot--></footer>
</body>
</html>
