<html>
<head>
	<title>Point of Care Result Report</title>
	<meta name="pdfkit-page_size" content="Letter" /><meta name="pdfkit-margin_top" content="0.3in" /><meta name="pdfkit-margin_left" content="0.3in" /><meta name="pdfkit-margin_right" content="0.3in" />
</head>
<body style="cursor: auto;">
<style type="text/css">body {
      font-family: "Liberation Sans", "Arial", arial;
      font-size:12px;
    }
    table {
      width: 100%;      
      border-collapse:collapse;
        font-size:12px;
    }
    table, th, td {
      border: 1px solid #cccccc;
      padding:5px;
        font-size:12px;
    }
  .col {
      width:32%;
      display: inline-block;
      vertical-align: top;
    }
    .uline {
      border-bottom:1px solid #000
    }
    .head {
      font-size:20px;
      font-weight:bold;
    }
    .small {
      font-size: 8px;
    }
    .center {
      text-align: center;
    }
    .right {
      text-align: right;
    }
    .display-module {
      display: inline-block;
      margin:3px;
      border: 2px solid #eee;
    }
    .half {
      width:48%;
    }
    .full {
      width:98%;
    }
    .display-head {
      font-weight: bold;
        font-size: 14px;
      padding:5px;
      color: #fff;
      background-color: #275a9e;/*#62B7E6;*/
    }
    .bottom {
      vertical-align: bottom;
    }
    .label {
      background-color: #eee;
      font-weight: bold;
    }
</style>
<div id="page">
<header>
<div class="col">&nbsp;</div>

<div class="col center">&nbsp;</div>

<div class="col">&nbsp;</div>
</header>

<section>
<div class="col">&nbsp;</div>

<div class="col center bottom"><span class="head">Point of Care Result Report</span></div>

<div class="col right bottom"><span><strong>Date &amp; Time:</strong> {{ extra_data['Now'] | date: "%m/%d/%Y %I:%M %p"}} </span></div>
</section>

<section style="padding-bottom:5px; border-bottom: 1px solid #cccccc;">
<div class="full">&nbsp;</div>
</section>

<section>
<div class="display-module half">
<div class="display-head center">Patient Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label">Testing Date</td>
			<td>{{ subj["Testing Date"]}}</td>
		</tr>
		<tr>
			<td class="label" style="width:145px;">Patient Name</td>
			<td>{{ subj["Patient"]["Last Name"] }}, {{ subj["Patient"]["First Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Date of Birth</td>
			<td>{{ subj["Patient"]["DOB"] | date: "%m/%d/%Y" }}</td>
		</tr>
		<tr>
			<td class="label">MRN</td>
			<td>{{ subj["Patient"]["MRN"] }}</td>
		</tr>
		<tr>
			<td class="label">Sex</td>
			<td>{{ subj["Patient"]["Sex"] }}</td>
		</tr>
		<tr>
			<td class="label">Creation Date</td>
			<td>{{ subj["created_at"] | date: "%m/%d/%Y" }}</td>
		</tr>
		<tr>
			<td class="label">Created By</td>
			<td>{{ subj["created_by"]}}</td>
		</tr>
	</tbody>
</table>
</div>
</div>

<div class="display-module half">
<div class="display-head center">Clinic&nbsp;Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label" style="width:165px;">Facility Name</td>
			<td>{{ subj["Requisition"]["Facility"].name }}</td>
		</tr>
		<tr>
			<td class="label" style="width:165px;">Facility Contact Name</td>
			<td>{{ subj["Requisition"]["Facility"]["Contact Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Physician Name</td>
			<td>{{ subj["Requisition"]["Requesting Physician"]["Last Name"] }}, {{ subj["Requisition"]["Requesting Physician"]["First Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Physician NPI</td>
			<td>{{ subj["Requisition"]["Requesting Physician"]["NPI Number"] }}</td>
		</tr>
		<tr>
			<td class="label">Address</td>
			<td>{{ subj["Requisition"]["Facility"]["Address"] }}</td>
		</tr>
		<tr>
			<td class="label">Phone</td>
			<td>{{ subj["Requisition"]["Facility"]["Phone Number"] }}</td>
		</tr>
		<tr>
			<td class="label">Fax</td>
			<td>{{ subj["Requisition"]["Facility"]["Fax Number"] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</section>
</div>

<section>
<div class="display-module full">
<div class="display-head center">Point of Care Results</div>

<div class="display-content">
<table>
	<thead>
		<tr>
			<th>Test</th>
			<th>Positive</th>
			<th>Negative</th>
			<th>Not Tested</th>
		</tr>
	</thead>
	<tbody>{% assign poct_results = extra_data["poc_test_results"] | poct_result_sort_abc %}{% for poct in poct_results %}
		<tr>
			<td>{{ poct["Point of Care Test"] }}</td>
			{% if poct["PoC Result"] == "Positive" %}
			<td>
			<center><strong>X</strong></center>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			{% elsif poct["PoC Result"] == "Negative" %}
			<td>&nbsp;</td>
			<td>
			<center><strong>X</strong></center>
			</td>
			<td>&nbsp;</td>
			{% else poct["PoC Result"] == "Negative" %}
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
			<center><strong>X</strong></center>
			</td>
			{% endif %}
		</tr>
		{% endfor %}
	</tbody>
</table>
</div>
</div>
</section>
</body>
</html>
