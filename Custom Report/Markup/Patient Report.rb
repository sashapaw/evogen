<html>
<head>
  <title>Patient Report</title>
  <meta name="pdfkit-page_size" content="Letter" />
  <meta name="pdfkit-margin_top" content="0.3in" />
  <meta name="pdfkit-margin_left" content="0.3in" />
  <meta name="pdfkit-margin_right" content="0.3in" />
</head>
<body style="cursor: auto;">
<style type="text/css">body {
      font-family: "Liberation Sans", "Arial", arial;
      font-size:10px;
    }
    table {
      width: 100%;      
      border-collapse:collapse;
        font-size:10px;        
    }
    table, th, td {
      border: 1px solid #cccccc;
      padding:5px;
        font-size:10px;
        word-break: break-all;
    }
  .col {
      width:32%;
      display: inline-block;
      vertical-align: top;
    }
    .uline {
      border-bottom:1px solid #000
    }
    .head {
      font-size:20px;
      font-weight:bold;
    }
    .small {
      font-size: 8px;
    }
    .center {
      text-align: center;
    }
    .right {
      text-align: right;
    }
    .display-module {
      display: inline-block;
      margin:3px;
      border: 2px solid #eee;
    }
    .half {
      width:48%;
    }
    .full {
      width:98%;
    }
    .display-head {
      font-weight: bold;
        font-size: 14px;
      padding:5px;
      color: #fff;
      background-color: #275a9e;/*#62B7E6;*/
    }
    .bottom {
      vertical-align: bottom;
    }
    .label {
      background-color: #eee;
      font-weight: bold;
    }
</style>
<div id="page">

<section>
  <div class="col">
    <img alt="{{ extra_data['Lab Name'] }}" height="50px" src="data:image/jpeg;base64,{{ extra_data['Lab Logo'] }}" />
    <div>{{ extra_data['System Settings']["Lab Address"] | multiline }}</div>
  </div>

  <div class="col center bottom"><span class="head">Patient Report</span></div>

  <div class="col right bottom">
  {% if extra_data['Patient Picture'] %}
    <img src="data:image/jpeg;base64,{{ extra_data['Patient Picture'] }}" style="border-radius:50%;width:60px;height:60px;" />
  {% endif %}
  </div>
</section>

<section style="padding-bottom:5px; border-bottom: 1px solid #cccccc;">
<div class="full">&nbsp;</div>
</section>

<section>
<div class="display-module half">
<div class="display-head center">Patient Information</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label" style="width:145px;">Patient Name</td>
			<td>{{ subj["First Name"] }} {{ subj["Middle Name"] }} {{ subj["Last Name"] }}</td>
		</tr>
		<tr>
			<td class="label" style="width:145px;">Facility</td>
			<td>{{ subj["Facility"]["name"] }}</td>
		</tr>
		<tr>
			<td class="label">Date of Birth</td>
			<td>{{ subj["DOB"] | date: "%m/%d/%Y" }}</td>
		</tr>
		<tr>
			<td class="label">Sex</td>
			<td>{{ subj["Sex"] }}</td>
		</tr>
		<tr>
			<td class="label">Patient Record #</td>
			<td>{{ subj["MRN"] }}</td>
		</tr>
		<tr>
			<td class="label">Alternate Record #</td>
			<td>{{ subj["External Source ID"] }}</td>
		</tr>
		<tr>
			<td class="label">Address</td>
			<td>{{ subj["Address"] }}</td>
		</tr>
		<tr>
			<td class="label">Patient Phone</td>
			<td>{{ subj["Phone Number"] }}</td>
		</tr>
		<tr>
			<td class="label">Age</td>
			<td>{{ subj["Age"] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>

<div class="display-module half">
<div class="display-head center">Patient Statistics</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label" style="width:145px;">Requisitions</td>
			<td>{{ subj["Requisitions"].size }}</td>
		</tr>
		<tr>
			<td class="label">Samples</td>
			<td>{{ subj["Patient Samples"].size }}</td>
		</tr>
		<tr>
			<td class="label">Released Requisitions</td>
			<td>{{ subj["Released Requisitions"].size }}</td>
		</tr>
		<tr>
			<td class="label">Point of Care Test Results</td>
			<td>{{ subj["Point of Care Test Results"].size }}</td>
		</tr>
		<tr>
			<td class="label">Test Results</td>
			<td>{{ extra_data["Test Results"] }}</td>
		</tr>
		<tr>
			<td class="label">Tests</td>
			<td>{{ subj["Tests"].size }}</td>
		</tr>
		<tr>
			<td class="label">Medications</td>
			<td>{{ subj["Medications"].size }}</td>
		</tr>
		<tr>
			<td class="label">Diagnoses</td>
			<td>{{ subj["Diagnoses"].size }}</td>
		</tr>
		<tr>
			<td class="label">Bill To</td>
			<td>{{ subj["Bill To"] }}</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</section>

<section>
<div class="display-module full">
<div class="display-head center">Billing Information</div>

<div class="display-content">{% if subj["Bill To"] == "Insurance" %}{% if subj["Insurance Company 1"] %}
<table>
	<tbody>
		<tr>
			<td class="label">Insurance Company 1</td>
			<td>{{ subj["Insurance Company 1"].name }}</td>
		</tr>
		<tr>
			<td class="label">Policy #1</td>
			<td>{{ subj["Policy #1"] }}</td>
		</tr>
		<tr>
			<td class="label">Group #1</td>
			<td>{{ subj["Group #1"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber</td>
			<td>{{ subj["Relation To Insured"] }}</td>
		</tr>
		{% if subj["Relation To Insured"] != "Self" %}
		<tr>
			<td class="label">Subscriber Full Name</td>
			<td>{{ subj["Subscriber Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Address</td>
			<td>{{ subj["Subscriber Address"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Date of Birth</td>
			<td>{{ subj["Subscriber DOB"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Sex</td>
			<td>{{ subj["Subscriber Sex"] }}</td>
		</tr>
		{% endif %}
	</tbody>
</table>
{% endif %}{% if subj["Insurance Company 2"] %}

<table>
	<tbody>
		<tr>
			<td class="label">Insurance Company 2</td>
			<td>{{ subj["Insurance Company 2"].name }}</td>
		</tr>
		<tr>
			<td class="label">Policy #2</td>
			<td>{{ subj["Policy #2"] }}</td>
		</tr>
		<tr>
			<td class="label">Group #2</td>
			<td>{{ subj["Group #2"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber</td>
			<td>{{ subj["Relation To Insured"] }}</td>
		</tr>
		{% if subj["Relation To Insured"] != "Self" %}
		<tr>
			<td class="label">Subscriber Full Name</td>
			<td>{{ subj["Subscriber Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Address</td>
			<td>{{ subj["Subscriber Address"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Date of Birth</td>
			<td>{{ subj["Subscriber DOB"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Sex</td>
			<td>{{ subj["Subscriber Sex"] }}</td>
		</tr>
		{% endif %}
	</tbody>
</table>
{% endif %}{% if subj["Insurance Company 3"] %}

<table>
	<tbody>
		<tr>
			<td class="label">Insurance Company 3</td>
			<td>{{ subj["Insurance Company 3"].name }}</td>
		</tr>
		<tr>
			<td class="label">Policy #3</td>
			<td>{{ subj["Policy #3"] }}</td>
		</tr>
		<tr>
			<td class="label">Group #3</td>
			<td>{{ subj["Group #3"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber</td>
			<td>{{ subj["Relation To Insured"] }}</td>
		</tr>
		{% if subj["Relation To Insured"] != "Self" %}
		<tr>
			<td class="label">Subscriber Full Name</td>
			<td>{{ subj["Subscriber Name"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Address</td>
			<td>{{ subj["Subscriber Address"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Date of Birth</td>
			<td>{{ subj["Subscriber DOB"] }}</td>
		</tr>
		<tr>
			<td class="label">Subscriber Sex</td>
			<td>{{ subj["Subscriber Sex"] }}</td>
		</tr>
		{% endif %}
	</tbody>
</table>
{% endif %}{% endif %}{% if subj["Bill To"] == "Medicare" %}

<table>
	<tbody>
		<tr>
			<td class="label">Insurance Company</td>
			<td>{{ subj["Insurance Company 1"].name }}</td>
		</tr>
		<tr>
			<td class="label">Policy #</td>
			<td>{{ subj["Policy #1"] }}</td>
		</tr>
		<tr>
			<td class="label">Group #</td>
			<td>{{ subj["Group #1"] }}</td>
		</tr>
		<tr>
			<td class="label">Primary Physician NPI</td>
			<td>{{ subj["Primary Physician NPI"] }}</td>
		</tr>
	</tbody>
</table>
{% endif %}{% if subj["Bill To"] == "Facility" %}

<table>
	<tbody>
		<tr>
			<td class="label">Billing Facility</td>
			<td>{{ subj["Billing Facility"].name }}</td>
		</tr>
		<tr>
			<td class="label">Billing Facility Address</td>
			<td>{{ subj["Billing Facility Address"] }}</td>
		</tr>
	</tbody>
</table>
{% endif %}{% if subj["Bill To"] == "Patient/Self Pay" %}
<table><tr>
  <td class="label">Bill To</td><td>Patient/Self Pay</td>
</tr></table>
{% endif %}</div>
</div>
</section>

<section>
<div class="display-module">
<div class="display-head center">Test Schedule</div>

<div class="display-content">
<table>
	<tbody>
		<tr>
			<td class="label">Use Patient Testing Scheduler</td>
			<td>{% if subj["Use Patient Testing Scheduler"] %}Yes {% else %} No {% endif %}</td>
		</tr>
		{% if subj["Start Date"] %}
		<tr>
			<td class="label">Start Date</td>
			<td>{{ subj["Start Date"] | date: "%m/%d/%Y" }}</td>
		</tr>
		{% endif %}{% if subj["End Date"] %}
		<tr>
			<td class="label">End Date</td>
			<td>{{ subj["End Date"] | date: "%m/%d/%Y" }}</td>
		</tr>
		{% endif %}{% if subj["Defined from Start Date (Lab)"] %}
		<tr>
			<td class="label">Defined from Start Date (Lab)</td>
			<td>{{ subj["Defined from Start Date (Lab)"] }}</td>
		</tr>
		{% endif %}{% if subj["Defined Schedule (Lab)"] %}
		<tr>
			<td class="label">Defined Schedule (Lab)</td>
			<td>{{ subj["Defined Schedule (Lab)"] }}</td>
		</tr>
		{% endif %}{% if subj["Randomized Schedule (Lab)"] %}
		<tr>
			<td class="label">Randomized Schedule (Lab)</td>
			<td>{{ subj["Randomized Schedule (Lab)"] }}</td>
		</tr>
		{% endif %}{% if subj["Schedule Type (Lab)"] %}
		<tr>
			<td class="label">Schedule Type (Lab)</td>
			<td>{{ subj["Schedule Type (Lab)"] }}</td>
		</tr>
		{% endif %}{% if subj["Defined from Start Date (PoC)"] %}
		<tr>
			<td class="label">Defined from Start Date (PoC)</td>
			<td>{{ subj["Defined from Start Date (PoC)"] }}</td>
		</tr>
		{% endif %}{% if subj["Defined Schedule (PoC)"] %}
		<tr>
			<td class="label">Defined Schedule (PoC)</td>
			<td>{{ subj["Defined Schedule (PoC)"] }}</td>
		</tr>
		{% endif %}{% if subj["Randomized Schedule (PoC)"] %}
		<tr>
			<td class="label">Randomized Schedule (PoC)</td>
			<td>{{ subj["Randomized Schedule (PoC)"] }}</td>
		</tr>
		{% endif %}{% if subj["Schedule Type (PoC)"] %}
		<tr>
			<td class="label">Schedule Type (PoC)</td>
			<td>{{ subj["Schedule Type (PoC)"] }}</td>
		</tr>
		{% endif %}{% if subj["Testing Schedule Records"].size > 0 %}
		<tr>
			<td class="label">Testing Schedule Records</td>
			<td>{{ subj["Testing Schedule Records"].size }}</td>
		</tr>
		{% endif %}{% if subj["Scheduled Tests"].size > 0 %}
		<tr>
			<td class="label">Scheduled Tests</td>
			<td>{{ subj["Scheduled Tests"].size }}</td>
		</tr>
		{% endif %}
	</tbody>
</table>
</div>
</div>

</section>
<!-- <section>
<div class="display-module full">
<div class="display-head center">Point of Care Tests</div>

<div class="display-content">{% for req in subj["Requisitions"] %}{% for poct in req["Point of Care Test Results"] %}
<div style="padding:10px;background-color:#ccc;"><strong>Point of Care Test Date:</strong> {{ poct["Testing Date"] | date: "%m/%d/%Y" }}</div>

<div>
<table>
	<thead>
		<tr>
			<th>Test</th>
			<th>Result</th>
		</tr>
	</thead>
	<tbody>{% for res in poct["Point of Care Test Result Entries"] %}
		<tr>
			<td>{{ res["Point of Care Test"].name }}</td>
			<td>{{ res["PoC Result"] }}</td>
		</tr>
		{% endfor %}
	</tbody>
</table>
</div>
{% endfor %}{% endfor %}</div>
</div>
</section> -->

<section>
<div class="display-module full">
<div class="display-head center">Communications</div>

<div class="display-content">
<table>
	<thead>
		<tr>
			<th style="width:15%;">Requisition</th>
			<th style="width:10%;">Type</th>
			<th style="width:40%;">Text</th>
			<th style="width:15%;">Who</th>
			<th style="width:10%;">Opened</th>
			<th style="width:10%;">Closed</th>
		</tr>
	</thead>
	<tbody>{% for req in subj["Requisitions"] %}{% for com in req["Communications"] %}
		<tr>
			<td>{{ req.name }}</td>
			<td>Message</td>
			<td>{{ com["Message"] }}</td>
			<td>&nbsp;</td>
			<td>{{ com["Date Opened"] | date: "%m/%d/%Y" }}</td>
			<td>{{ com["Date Closed"] | date: "%m/%d/%Y" }}</td>
		</tr>
		{% for rep in com["Replies"] %}
		<tr>
			<td>&nbsp;</td>
			<td>Reply</td>
			<td>{{ rep["Reply"] }}</td>
			<td>{{ rep["Reply By"] }}</td>
			<td>{{ rep.updated_at | date: "%m/%d/%Y" }}</td>
			<td>&nbsp;</td>
		</tr>
		{% endfor %}{% endfor %}{% endfor %}
	</tbody>
</table>
</div>
</div>
</section>
</div>
</body>
</html>
