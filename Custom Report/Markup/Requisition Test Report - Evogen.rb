<!doctype html>
<html>
<head>
	<title>Evogen Requisition Test Report</title>
	<meta name="pdfkit-page_size" content="Letter"/>
	<meta name="pdfkit-header_html" content="{{extra_data['header']}}">
	<meta name="pdfkit-footer_html" content="{{extra_data['footer']}}">
    <meta name="pdfkit-header_spacing" content="2"/>
    <meta name="pdfkit-footer_spacing" content="2"/>
	<meta name="pdfkit-margin_top" content="1.2in"/>
	<meta name="pdfkit-margin_bottom" content="0.4in"/>
	<meta name="pdfkit-margin_left" content="0.3in"/>
	<meta name="pdfkit-margin_right" content="0.3in"/>
	<style type="text/css">
{% assign width = extra_data.width %}
body {
  font-family: Liberation Sans Narrow;
  font-size: 16px;
}
.wide-font {
  font-family: arial; 
}
.h1 {
  font-size: 16px;
  font-weight: bold;
  color: #007299;
  margin-top: 1em;
}
.h2 {
  font-size: 16px;
  font-weight: bold;
  color: #007299;
}
div.section {
  width: {{ width }}px;
}
.correction {
  color: #900;
  font-weight: bold;
}
.box {
    display: inline-block;
    width: 18px;
    height: 18px;
    border: 1px solid #000;
    margin-right: 4px;
}
tr, img, div.legend, table.signatures {
    page-break-inside: avoid !important;
}
td {
	vertical-align: top;
}
table {
	border-collapse: collapse;
}
table.req-header {
  /*border-collapse: separate;*/
  border-spacing: 0px;
}
table.req-header td {
  font-size: 18px;
  border: 1px #999 solid;
  padding: 4px 8px;
}
table.req-header tr:first-child td { border-top: none; }
table.req-header td:last-child { border-right: none; }
table.req-header tr:last-child td { border-bottom: none; }
table.req-header td:first-child { border-left: none; }
.label {
  font-size: 14px;
}
table.results {
  max-width: {{ width / 2 - 20 }}px;
}
table.results td,table.info td {
  padding-right: 12px;
}
.analyte {
  font-style: italic;
  font-weight: bold;
  white-space: nowrap;
}
div.columns {
  margin-top:2px;
  column-count:2;
  -webkit-column-count: 2;
  column-gap:15px;
  -webkit-column-gap: 15px;
  column-rule: 1px solid #999;
  -webkit-column-rule: 1px solid #999;
}
table.columns {
  margin-top: 10px;
  width:{{ width }}px;
}
td.column:first-child {
  padding: 0 2ex 2ex 0;
  border-bottom: 2px #999 solid;
}
td.column:last-child {
  padding: 0 0 2ex 2ex;
  border-left: 2px #999 solid;
  border-bottom: 2px #999 solid;
}
.ref {
  font-size: 13px;
}
.continued {
	font-size: 16px;
	margin-top: 4px;
	width: {{ width }}px;
}
.sep {
  margin-top: 1.5em;
}
.data {
  border: 1px solid black;
  padding: 2px 4px;
}
.page-header {
	padding-top: 25px;
	width: {{ width }}px;
}
.page-break {
	page-break-before: always;
}
.page {
	width: {{ width }}px;
}
</style>
<script src="{{extra_data['public_path']}}/javascripts/jQuery/jquery-1.11.0.min.js"></script>
<script>
	$(window).load(function() {
	    var body = $('body'),
		    pager = $('#pager'),
		    header = $('div.page-header').detach(),
		    sections = $('div.section'),
		    pageHeight = pager[0].scrollHeight,
			page = 0;
        //pager.height(1130);
	    sections.each(function() {
		    pager.append(header.clone());
	        var section = $(this);
		    pager.append(section.detach());
			var nextPager = null;
		    do {
				var skipRows = 0,
					scrollHeight = pager[0].scrollHeight;
				if (scrollHeight > pageHeight) {
					var table = pager.find('table.results');
				    if (table.length) {
						nextPager = pager.html();
						pager.find('div.comment').before($('<div class="continued">Continued on next page&hellip;</div>'));
				  	    var rows = table.find('tbody > tr');
						for (var i = rows.length - 1; i > 0 && scrollHeight > pageHeight; i--) {
							var row = $(rows[i]);
							row.remove();
							scrollHeight = pager[0].scrollHeight;
						}
						skipRows = i + 1;
				    } else {
				    	//Add logic for long page without table.
						//We do not have such pages now.
				    }
				} else {
					nextPager = null;
				}
			    if (page > 0) {
			  	    pager.find('div.page-header').addClass('page-break');
			    }
			    body.append(pager.html());
				page++;
			    pager.empty();
				if (nextPager) {
					pager.html(nextPager);
					var table = pager.find('table.results');
					table.find('tbody > tr').slice(0, skipRows).remove();
					scrollHeight = pager[0].scrollHeight;
				}
		    } while (nextPager);
	    });
        //pager.height(1);
        //pager.hide();
	    pager.remove();
	});
	</script>
</head>
<body>
{% assign patient = subj["Patient"] %}
{% assign samples_data = extra_data.samples_data %}
{% assign sample = extra_data.sample %}
  <div id="pager" style="width:{{ width }}px;height:1130px;overflow-y:hide;left:0;top:-2000px;position:absolute"></div>
  
<div class="section" style="padding:0;border:1px #999 solid;border-radius:8px">
  <table class="req-header wide-font" style="width:{{ width }}px">
    <colgroup><col style="width:20%"/><col style="width:15%"/><col style="width:15%"/><col style="width:25%"/><col style="width:15%"/><col style="width:10%"/></colgroup>
	<tr>
      <td class="lt"><div class="label">Requisition Number</div>{{ subj.name }}</td>
      <td><div class="label">Received</div>{{ subj["Received Date"] | date: '%m/%d/%Y' }}</td>
      <td><div class="label">Reported</div>{{ subj["Released Date"] | date: '%m/%d/%Y' }}</td>
      <td colspan="3" class="rt"></td>
    </tr>
	<tr>
      <td><div class="label">Patient ID</div>{{ patient['MRN'] }}</td>
      <td colspan="3"><div class="label">Patient Name ( Last, First )</div>{{ patient["Last Name"] }}, {{ patient["First Name"] }}</td>
      <td><div class="label">Date of Birth</div>{{ patient['DOB'] | date: '%m/%d/%Y' }}</td>
      <td><div class="label">Gender</div>{{ patient['Sex'] }}</td>
    </tr>
	<tr>
      <td><div class="label">Sample Number</div>{{ sample.name }}</td>
      <td><div class="label">Date Collected</div>{{ sample["Collection Date"] | date: '%m/%d/%Y' }}</td>
      <td><div class="label">Date Received</div>{{ sample["Received Date"] | date: '%m/%d/%Y' }}</td>
      <td colspan="3"><div class="label">Sample Type</div>{{ sample['Specimen Type'].name }}</td>
    </tr>
	<tr>
      <td colspan="3" class="lb"><div class="label">Referring Physician ( Last, First )</div>{{ subj['Requesting Physician']['Last Name'] }}, {{ subj['Requesting Physician']['First Name'] }}</td>
      <td colspan="3" class="rb"><div class="label">Facility Address</div>{{ subj["Facility"].name }}, {{ subj["Facility"]['Address'] }}</td>
    </tr>
  </table>
</div>

<div class="section wide-font" style="margin-top:1em; border-top: 4px #007299 solid; padding-top:8px; color:#007299; font-size:22px; font-weight:bold">
  Fragile X Syndrome, Repeat and Methylation Analysis
</div>

{% if extra_data.amendment or extra_data.correction %}
<div class="section sep">
<span class="correction">Corrected</span>
{% if extra_data.correction_user %}by {{ extra_data.correction_user.fullname }}{% endif %}
on {{ extra_data.correction_time | date: "%m/%d/%Y, %I:%M %p" }}
{% if extra_data.amendment_comments %}
<div class="data">{{ extra_data.amendment_comments | multiline }}</div>
{% endif %}
</div>
{% endif %}

<div class="section">
<table class="columns">
<colgroup><col style="width:50%"/><col style="width:50%"/></colgroup>
<tr><td class="column">
  <div class="h1" style="margin-top:0">Results Summary</div>
  <div class="wide-font" style="font-size:24px;margin:12px 0">{{ extra_data.outcome }}</div>
  <table class="results">
    <tr>
      <td class="h2">Results</td>
      {% assign i = 1 %}
      {% for copy in extra_data.copy_numbers %}
        <td>Allele {{ i }}</td>
        {% assign i = i | plus: 1 %}
      {% endfor %}
    </tr>
    <tr>
      <td class="analyte" style="vertical-align:middle">CGG Repeats</td>
      {% for copy in extra_data.copy_numbers %}
        <td style="font-size:20px;">{{ copy }}</td>
      {% endfor %}
    </tr>
    {% if extra_data.methylations %}
    <tr>
      <td class="analyte">Methylation</td>
      {% for meth in extra_data.meth_statements %}
        <td>{{ meth }}</td>
      {% endfor %}
    </tr>
    {% endif %}
  </table>
  <div style="margin-top:1em">
    {% if extra_data.pcr %}<div>&bull; {{ extra_data.pcr }}</div>{% endif %}
    {% if extra_data.meth %}<div>&bull; {{ extra_data.meth }}</div>{% endif %}
  </div>
          
  <div class="h1">Interpretation</div>
  <div>{{ extra_data.interpretation }}</div>

  <div class="h1">Method / Limitations</div>
  <!--<div>Genomic DNA (gDNA) is extracted from peripheral blood cells, mixed with control material and used for two simultaneous reactions to determine repeat number and methylation percentage. To ascertain the CCG copy number gDNA is amplified using a two-primer system containing forward and FAM-labelled reverse primers. To ascertain the methylation percentage of a repeat the gDNA is predigested with a methylation-sensitive DNase which only retains the methylated alleles. The remaining material is amplified using using a two-primer system containing a forward and HEX-labelled reverse primer. After amplifica- tion the two products are pooled and analyzed directly on a validated capillary electrophoresis platform. The CCG repeat lengths are directly assessed by analyzing the FAM signal and the percent methylation is calculated using a ratio of the HEX signal relative to FAM signal peak heights. The assay has a published sensitivity of 100% [95% confidence interval (CI): 89%-100%] and specificity of 99% (95% CI:
    93%-100%) for full mutation allele detection[1].</div>-->
	<div>Genomic DNA (gDNA) is extracted from peripheral blood cells and mixed with control material. To determine CGG copy number, gDNA is amplified using a two-primer system containing FAM-labelled reverse primers. To ascertain the methylation percentage of a repeat, gDNA is predigested with a methylation-sensitive DNase. The remaining material is amplified using a two-primer system containing a HEX-labelled reverse primer. After amplification, the products from the two amplifications are pooled and analyzed directly on a validated capillary electrophoresis platform. The CGG repeat lengths are directly assessed by analyzing the FAM signal, and the percent methylation is calculated using a ratio of the HEX signal relative to FAM signal peak heights. The assay has a published sensitivity of 100% [95% confidence interval (CI):89%-100%] and specificity of 99% (95% CI: 93%-100%) for full mutation allele detection[1]. The current methods do not identify potential rare mutations in the <i>FMR1</i> gene itself.</div>
</td><td class="column">
  <!--<div style="margin-top:1em;">Fragile X is a condition predominantly by the expansion of CGG sequence in the 5’ untranslated region of the Fragile X Mental retarda- tion 1 (<i>FMR1</i>, NM_002024.4) gene[2]. Risk assessment and clinical interpretation of FXS are defined by the number of CCG repeats, as well as, the methylation status of the allele. The following ranges of CCG repeats are used to define four types of alleles. Alleles with greater than 200 CGG repeats will be annotated ≥200.</div>-->
  <div style="margin-top:1em;">Fragile X syndrome (FXS) is one of the most common genetic causes of intellectual disability and/or autism spectrum disorders. The condition is predominantly caused by the expansion of CGG sequence in the 5' untranslated region of the Fragile X Mental retardation 1 (<i>FMR1</i>, NM_002024.4) gene[2]. Risk assessment and clinical interpretation of FXS are defined by the number of CGG repeats, as well as the methylation status of the allele. The following ranges of CGG repeats are used to define four types of alleles. Alleles with greater than 200 CGG repeats will be annotated >200.</div>
          
  <div class="h1">Repeat Numbers</div>
  <table class="info">
    <tr>
      <td style="width:240px">Normal (5-44)</td>
      <td>Premutation (55-199)</td>
    </tr>
    <tr>
      <td>Intermediate (45-54)</td>
      <td>Full Mutation (&ge;200)</td>
    </tr>
  </table>
  
  <div class="h1">Methylation</div>
  <table class="info">
    <tr>
      <td style="width:240px">Unmethylated (&le;20%)</td>
      <td>Fully methylated (&ge;80%)</td>
    </tr>
    <tr>
      <td>Partially methylated (20-80%)</td>
      <td></td>
    </tr>
  </table>
  
  <div style="margin-top:1em;"><b>For more information on these disorders, see the online GeneReviews for <i>FMR1</i>-related disorders at:</b>
    <a href="https://www.ncbi.nlm.nih.gov/books/NBK1384/">www.ncbi.nlm.nih.gov/books/NBK1384/</a><br>
    <b>and the National Fragile X Foundation at:</b> <a href="https://fragilex.org/">www.fragilex.org</a></div>
      
  <div style="margin-top:1em;">This analysis evaluates only the FRAXA locus and cannot detect other possible genetic abnormalities.
      False positive or negative results may occur for reasons that include somatic or tissue-specific mosaicism, rare genetic variants, blood transfusions orbone marrow transplantation.
      Test results should be interpreted in the context of clinical findings, family history, and other laboratory data.
      Misinterpretation of results may occur if the information provided is inaccurate or incomplete.
      Used to define four types of alleles. Alleles with greater than 200 CGG repeats will be annotated ≥200.</div>

  <div class="h1">Released By</div>
  <div>{% if subj["Released By"] %}{{ subj["Released By"].fullname }}{% else %}&nbsp;{% endif %}</div>

  <table style="width:100%">
    <tr>
      <td style="width:50%"><b>Received:</b> {{ subj["Received Date"] | date: '%d %b %Y %H:%M' }}</td>
      <td style="width:50%;text-align:right"><b>Reported:</b> {{ subj["Report Date"] | date: '%d %b %Y %H:%M' }}</td>
    </tr>
  </table>
</td></tr></table>

  <table style="margin-top:6px; border-bottom: 4px #007299 solid;">
    <tr>
      <td class="h2" style="width:50%">References</td>
      <td class="h2" style="width:50%;padding-left:20px;">Disclaimer</td>
    </tr>
    <tr>
      <td style="padding:0 0 8px 0;">
        <div class="ref">1. Grasso, M., et al., <i>A novel methylation PCR that offers standardized determination of FMR1 methylation and CGG repeat length without southern blot analysis.</i> J Mol Diagn, 2014. <b>16</b>(1): p. 23-31.</div>
        <div class="ref" style="margin-top:4px">2. Verkerk, A.J., et al., <i>Identification of a gene (FMR-1) containing a CGG repeat coincident with a breakpoint cluster region exhibiting length variation in fragile X syndrome.</i> Cell, 1991. <b>65</b>(5): p. 905-14</div>
      </td>
      <td style="padding:0 0 8px 20px;">
        The test was developed and its performance characteristics have been determined by EvoScoreGX Genomics.
        The laboratory is regulated under the Clinical Laboratory Improvement Amendments of 1988 (CLIA) as qualified to perform high complexity clinical testing.
      </td>
    </tr>
  </table>
</div>
</body>
</html>