# prefill to current user/date
params[:defaults] = {
  'Approved On' => Date.today,
  'Approved By' => User.curr_user
}

params[:required] = {
  'Approved By'=>true,
  'Approved On'=> true
}

