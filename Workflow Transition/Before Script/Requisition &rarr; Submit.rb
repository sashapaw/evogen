raise "Collection Date can not be empty. Please edit the requisition before proceeding" if subj.get_value('Collection Date').nil?
raise "Collection Date can not be in the future" if subj.get_value('Collection Date') > Time.now
def compare(ins,policy,pos)
  if ins && !policy.present?
    raise "You have Insurance Company #{pos} selected. You must also provide Policy ##{pos}."
  end
  if !ins.present? && policy
    raise "You have provided Policy ##{pos}. You must also provide Insurance Company #{pos}."
  end
end
def insurance_check(req)
  ic_1 = {ins:req.get_value('Insurance Company 1'),policy:req.get_value('Policy #1'), pos:1}
  ic_2 = {ins:req.get_value('Insurance Company 2'),policy:req.get_value('Policy #2'), pos:2}
  ic_3 = {ins:req.get_value('Insurance Company 3'),policy:req.get_value('Policy #3'), pos:3}
  bill_ins = req.get_value('Bill To') == "Insurance"  
  
  insurances = [ic_1,ic_2,ic_3]
  
  if bill_ins 
    if !ic_1[:ins].present? && !ic_2[:ins].present? && !ic_3[:ins].present?
     raise "Please edit your Requisition and select an Insurance Company"
    end
  end
  
  insurances.each do |insurance|    
    compare(insurance[:ins],insurance[:policy],insurance[:pos])
  end
end

insurance_check(subj)
