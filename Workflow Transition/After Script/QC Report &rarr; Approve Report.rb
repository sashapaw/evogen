subj.set_value('Approved On', params['Approved On'])
subj.set_value('Approved By', params['Approved By'])
subj.set_value('Lab Manager Signature', params['Lab Manager Signature'])
# concatenate the old and new comments to make "blog-roll" comments
comments = subj.get_value('Comments') 
new_comments = params['Comments']
user = User.curr_user
today = Date.today.strftime('%m/%d/%Y')
if !comments.present?
  comments = ""
end
if new_comments.present?  
  comments = comments + "\n"  + "#{today} (#{user}) - " + new_comments
  subj.set_value('Comments', comments)
end

