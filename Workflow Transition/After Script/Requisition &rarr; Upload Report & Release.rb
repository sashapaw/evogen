subj.set_value('Report Date', params['Report Date'])
subj.set_value('Report File', params['Report File'])
subj.set_value('Result', params['Result'])

recepients = [subj.get_value('Facility').get_value('Contact Email')]
contacts = subj.get_value('Additional Report Recipients')
contacts.each{|contact| recepients << contact.get_value('EMail')} if contacts

LIS.as_admin {
  send_email(recepients, 'Test Results Ready', subj, params) {|ep|
    lab_logo = LIS.lab_logo
    ep.add_inline_attachment(lab_logo, 'lab_logo') unless lab_logo.nil?
  }
}