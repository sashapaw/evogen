if SystemHooks.validate_requisition_submission(subj)
  message = Requisition.submit(subj)
  show_message(message) if message.present?
  close_tab_ids(subj.id)
else
  raise "Requisition is not valid"
end