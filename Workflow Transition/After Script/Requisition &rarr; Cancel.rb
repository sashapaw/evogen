raise "The Requisition #{subj.name} is currently being processed in a batch. Cannot Cancel Requisition." if subj.get_value('Process Lock') || subj.get_value('Queued for Release')
if params['Cancellation Comments'].present?
  subj.set_value('Cancellation Comments', params['Cancellation Comments'])
end

Requisition::Reports.generate_req_cancellation_report(subj)
subj.get_value('Samples').each {|s| s.cancel}
