subj.terminated=true
subj.save

# terminate Ranges too
subj.get_value('Applicable Ranges').each do |r|
	r.terminated=true
	r.save  
end

close_tab_ids(subj.id)

