raise "Can not Archive default Facility: #{subj.name}" if subj.get_value('RURO_CLIENT').present?

subj.my_users.each do |u|
  u.update_attributes!(:disabled=>true)
end

subj.terminated=true
subj.save
SystemHooks.facility_archived(subj)
close_tab_ids(subj.id)