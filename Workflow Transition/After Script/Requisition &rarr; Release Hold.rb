subj.remove_prop('On Hold Reason')

# concatenate the old and new comments to make "blog-roll" comments
comments = subj.get_value('On Hold Comments') 
new_comments = 'Hold released'
user = User.curr_user
today = Date.today.strftime('%m/%d/%Y')
if !comments.present?
  comments = ""
end
if new_comments.present?  
  comments = comments + "\n"  + "#{today} (#{user}) - " + new_comments
  subj.set_value('On Hold Comments', comments)
end
SystemHooks.requisition_off_hold(subj)