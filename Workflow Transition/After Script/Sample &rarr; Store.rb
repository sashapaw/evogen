subj.set_value('Freezer', params['Freezer'])
subj.set_value('Rack', params['Rack'])
subj.set_value('Shelf', params['Shelf'])
subj.set_value('Box', params['Box'])
subj.set_value('Slot', params['Slot'])
subj.set_value('Date Stored', params['Date Stored'])
SystemHooks.sample_stored(subj)