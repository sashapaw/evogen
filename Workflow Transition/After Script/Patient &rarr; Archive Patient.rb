subj.my_users.each do |u|
  u.update_attributes!(:disabled=>true)
end

testing_schedule = subj.get_value('Testing Schedule Records')
if testing_schedule.present?
  testing_schedule.each do |archive|
	advance_workflow('Testing Schedule','Canceled',archive)
  end
end

subj.terminated=true
subj.save
SystemHooks.patient_archived(subj)
close_tab_ids(subj.id)
reload_tab('search_PATIENTS_tab')