resend = params['Resend Test Report']
subj.set_value('Correction Comments',params['Correction Comments'])
subj.set_value('Approved By',params['Approved By'])
subj.set_value('Approved On',params['Approved On'])
subj.set_value('Date Closed', Date.today)

req = subj.get_value('Requisition')
Requisition::Reports.generate_test_results_report(req, correction: true, comments: subj.get_value('Correction Comments'), user: User.curr_user, time:Time.now)
subj.set_value('Corrected Report File', req.get_value('Report File'))

#Now that all has been released, lets fax/email/sms
if resend
  Requisition::Communications.send_communication(req)
end
#Not sure if we should resend billing on a correction.
#send_hl7_dft(req)
LISHL7::EHR.send_results(req,true)
