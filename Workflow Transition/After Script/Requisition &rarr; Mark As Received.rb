subj.set_value('Received Date', params['Received Date'])
subj.set_value('Received By', params['Received By'])

begin
  SystemHooks.requisition_received(subj)
rescue Exception=>e
  Rails.logger.info ("ACESSIONING ERROR:: There was a problem accessioning the Requisition. ")
end