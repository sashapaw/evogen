resend = params['Resend Test Report']
subj.set_value('Date Closed', Date.today)

#Determine if we should generate PDF 
req = subj.get_value('Requisition')
#set_pass_or_fail(req)
Requisition::Reports.generate_test_results_report(req, {correction:true, user:User.curr_user, time:Time.now})
subj.set_value('Corrected Report File',req.get_value('Report File'))    

#Now that all has been released, lets fax/email/sms
if resend
  Requisition::Communications.send_communication(req)
end
#Not sure if we should resend billing on a correction.
#send_hl7_dft(req)
LISHL7::EHR.send_results(req,true)