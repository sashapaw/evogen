<%
  d = Limfinity::Lab.dashboard('week')
  samples = d[:samples]
  html = d[:requisitions] + samples[:html] + d[:specimen_types]
  samples_all = samples[:all]
  samples_all = 1 if samples_all == 0
%>
<style type="text/css" media="screen">
.dash-client-status {padding:10px 25px 10px 15px;vertical-align:top;white-space:nowrap}
.dash-client-status-title {font-size:13px; font-weight:bold; color:#888; text-align:center; margin-top:16px;}
.dash-client-status-good,.dash-client-status-warn,.dash-client-status-bad {
  font-family:"Trebuchet MS",Helvetica,sans-serif; padding:6px 4px; width:120px; margin:6px 0px 0px 6px;
  text-align:center; color:white; border-radius:6px; display:inline-block;}

.dash-client-status-good {background:#999;}
.dash-client-status-warn {background:#64B0D7;}
.dash-client-status-bad {background:#4DC441}
.dash-client-status span {font-size:12px;}
.dash-client-status-good div,.dash-client-status-warn div,.dash-client-status-bad div {font-size:36px;}
a.dash-client-status-link:link,a.dash-client-status-link:visited {color:white; text-decoration:none;}
a.dash-client-status-link:hover {color:black;}
  
table.acc_mgr {border-collapse: collapse; width: 100%; margin-top:5px;}
.acc_mgr th {font-size: 14px; text-align: left; padding: 5px 8px; background-color:#147FCC; color: white;}
.acc_mgr td {font-size: 14px; text-align: left; padding: 5px 8px 6px 8px;}
.acc_mgr tr:nth-child(even){background-color: #f2f2f2}
  
.charts path {cursor: default}
</style>

<script>
(function() {
  function load(dashPanel, range) {
    RURO.runHelperScript("lab_dashboard", {range: range}, function(result) {
      var samples = result.samples,
          html = result.requisitions + samples.html + result.specimen_types,
          samplesAll = samples.all || 1,
          gauge = dashPanel.down('#gauge').chart,
          bands = gauge.axes[0].bands,
          serial = dashPanel.down('#turnaround').chart;
      dashPanel.down('#html').update(html);
      dashPanel.down('#facilities').getStore().loadData(result.facilities);
      bands[1].setEndValue(100 * samples.accessioned / samplesAll);
      bands[1].balloonText = ''+samples.accessioned+' Sample(s)';
      bands[3].setEndValue(100 * samples.testing / samplesAll);
      bands[3].balloonText = ''+samples.testing+' Sample(s)';
      bands[5].setEndValue(100 * samples.complete / samplesAll);
      bands[5].balloonText = ''+samples.complete+' Sample(s)';
      bands[7].setEndValue(100 * samples.hold / samplesAll);
      bands[7].balloonText = ''+samples.hold+' Sample(s)';
      serial.dataProvider = result.turnaround;
      serial.validateData();
    });
  }
  var dashContainer = Ext.getCmp('dashboard_reports_tab_<%=params[:id]%>');
  var dashPanel = Ext.widget('panel', {
    tbar: [{
      text: 'Today',
      scale: 'medium',
      toggleGroup: 'range',
      allowDepress: false,
      toggleHandler: function(btn, state) {
        if (state) load(dashPanel, 'today');
      }
    },{
      text: 'Week',
      scale: 'medium',
      toggleGroup: 'range',
      pressed: true,
      allowDepress: false,
      toggleHandler: function(btn, state) {
        if (state) load(dashPanel, 'week');
      }
    },{
      text: 'Month',
      scale: 'medium',
      toggleGroup: 'range',
      allowDepress: false,
      toggleHandler: function(btn, state) {
        if (state) load(dashPanel, 'month');
      }
    },{
      text: 'All',
      scale: 'medium',
      toggleGroup: 'range',
      allowDepress: false,
      toggleHandler: function(btn, state) {
        if (state) load(dashPanel, 'all');
      }
    },'->',
    {
      text: 'LimitLIS Support',
      glyph: 'xf2bd@FontAwesome',
      scale: 'medium',
      handler: function(btn) {
        if (typeof supportDlg=='undefined' || !supportDlg.isVisible())
        supportDlg = Ext.widget('window',{
            title:'Send Support Request',
            width:800, height:350, modal:true, border:false, constrain:true, draggable:true, layout:'fit', defaultButton:0, 
            items:[{
                 xtype:'form', fieldDefaults: {labelWidth:205}, bodyPadding:10, layout:{type: 'vbox', align: 'stretch'},
                 items: [
                   {xtype:'label', text:'Email will be send to RURO Technical Support with your Product Number: <%= PRODUCT_NUMBER %>'},
                   {xtype:'label', text:'Let us know how we can help:'},
                   {xtype:'textarea', flex:1, hideLabel:true, allowBlank:false, anchor:'100%', name:'support_request'}
                 ]
            }],
            buttons: [
                { text:Text.Btn.Ok, handler:function(btn) {
                   var win = btn.up('window');
                   var form = win.down('form').getForm();
                   if (form.isValid()) {                 
                       req = form.getValues()['support_request'];
                       RURO.runHelperScript("send_support_request", {req:req}, function(result) {
                         Ext.MessageBox.show({title:Text.Lbl.Product, msg:'Your support request was successfully send. You should receive a copy of support email for your reference.', buttons:Ext.MessageBox.OK, icon:Ext.MessageBox.INFO});
                       });
                       win.close();
                   }
                }},
                { text:Ext.MessageBox.buttonText.cancel, handler: RURO.cancelDialog }
            ],
            listeners: {
                show: RURO.focusFirst
            }
        }).show();
      }
    },
    '-',
    {
      text: 'Reload',
      glyph: Glyphs.RELOAD,
      scale: 'medium',
      handler: function(btn) {
        var b = btn.ownerCt.down('button[pressed=true]');
        b.toggleHandler(b, true);
      }
    }],
    autoScroll: true,
    layout: 'anchor',//{type: 'vbox', align: 'stretch'},
    items: [{
      xtype: 'container',
      anchor: '100%',
      layout: {type: 'hbox', align: 'top'},
      items: [{
        xtype: 'box',
        itemId: 'html',
        width: 390,
        html: '<%=js html %>'
      },{
        xtype: 'container',
        flex: 1,
        layout: {type: 'vbox', align: 'stretch'},
        items: [{
          xtype: 'box',
          html:'<div class="dash-client-status-title" style="margin-top:6px">TOP FACILITIES</div>',
        },{
          xtype: 'grid',
          itemId: 'facilities',
          flex: 1,
          minWidth: 300,
          padding: '6 8 0 8',
          columns: ['Facility', 'Requisitions', 'Samples'],
          viewConfig: {forceFit: true, deferEmptyText: false},
          emptyText: '<div align="center">No requisitions in the range</div>',
          columns: [
            {dataIndex: 'facility', header: 'Facility', menuDisabled: true, flex: 1}, 
            {dataIndex: 'requisition', header: 'Requisitions', menuDisabled: true, width: 105}, 
            {dataIndex: 'sample', header: 'Samples', menuDisabled: true, width: 85}
          ],
          store: {
            type: 'array',
            fields: ['id', 'facility', 'requisition', 'sample'],
            data: <%=raw(d[:facilities].to_json)%>
          }
        }]
      }]
    },{
      xtype: 'container',
      cls: 'charts',
      anchor: '100%',
      minWidth: 750,
      height: 250,
      layout: {type: 'hbox', align: 'stretch'},
      items: [{
          xtype: 'box',
          itemId: 'gauge',
          width: 250,
          listeners: {
            boxready: function(cmp, w, h) {
              cmp.chart = AmCharts.makeChart(cmp.getEl().id, {
                type:"gauge", theme:"light",
                axes:[{
                  axisAlpha:0, tickAlpha:0, labelsEnabled:false, startValue:0, endValue:100, startAngle:0, endAngle:270,
                  bands:[
                    {color:"#eee", startValue:0, endValue:100, radius:"100%", innerRadius:"85%"}, 
                    {color:"#999", startValue:0, endValue:<%= (100 * samples[:accessioned] / samples_all).to_i %>, radius:"100%", innerRadius:"85%", balloonText:"<%= samples[:accessioned] %> Sample(s)"}, 
                    {color:"#eee", startValue:0, endValue:100, radius:"80%", innerRadius:"65%"}, 
                    {color:"#64B0D7", startValue:0, endValue:<%= (100 * samples[:testing] / samples_all).to_i %>, radius:"80%", innerRadius:"65%", balloonText:"<%= samples[:testing] %> Sample(s)"}, 
                    {color:"#eee", startValue:0, endValue:100, radius:"60%", innerRadius:"45%"}, 
                    {color:"#4DC441", startValue:0, endValue:<%= (100 * samples[:complete] / samples_all).to_i %>, radius:"60%", innerRadius:"45%", balloonText:"<%= samples[:complete] %> Sample(s)"}, 
                    {color:"#eee", startValue:0, endValue:100, radius:"40%", innerRadius:"25%"}, 
                    {color:"#cc4748", startValue:0, endValue:<%= (100 * samples[:hold] / samples_all).to_i %>, radius:"40%", innerRadius:"25%", balloonText:"<%= samples[:hold] %> Sample(s)"}
                  ]
                }],
                allLabels:[
                  {text:"Accessioned", x:"49%", y:"5%", size:15, bold:true, color:"#999", align:"right"}, 
                  {text:"In Testing", x:"49%", y:"15%", size:15, bold:true, color:"#64B0D7", align:"right"}, 
                  {text:"Released", x:"49%", y:"24%", size:15, bold:true, color:"#4DC441", align:"right"}, 
                  {text:"On Hold", x:"49%", y:"33%", size:15, bold:true, color:"#cc4748", align:"right"}
                ],
                export:{enabled:false}
              });
            },
            resize: function(cmp, w, h) {
              if (h != w) cmp.setHeight(w);
            }
          }
        },{
          xtype: 'box',
          itemId: 'watch',
          width: 250,
          listeners: {
            boxready: function(cmp, w, h) {
                var chart = AmCharts.makeChart(cmp.getEl().id, {
                      type:"gauge", theme:"light", "startDuration":0.3,
                  axes:[ { axisAlpha:0.3, endAngle:360, endValue:12, minorTickInterval:0.2, showFirstLabel:false, startAngle:0, axisThickness:1, valueInterval:1, bottomText:'L I M I T L I S ®', bottomTextYOffset:-20} ],
                  arrows:[ 
                    { radius:"50%", innerRadius:0, clockWiseOnly:true, nailRadius:10, nailAlpha:1}, 
                    {nailRadius:0, radius:"80%", startWidth:6,innerRadius:0, clockWiseOnly:true}, 
                    { color:"#CC0000", nailRadius:4, startWidth:3, innerRadius:0, clockWiseOnly:true, nailAlpha:1} 
                  ],
                  export:{enabled:false}
                });

                // update each second
                setInterval( updateClock, 1000 );
                function updateClock() {
                  if(chart.arrows.length > 0){
                    var date =  new Date();
                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var seconds = date.getSeconds();

                    if(chart.arrows[ 0 ].setValue){
                      chart.arrows[ 0 ].setValue( hours + minutes / 60 ); // set hours
                      chart.arrows[ 1 ].setValue( 12 * ( minutes + seconds / 60 ) / 60 ); // set minutes
                      chart.arrows[ 2 ].setValue( 12 * date.getSeconds() / 60 ); // set seconds
                    }
                  }
                }
            }
          }
        },{
          xtype: 'box',
          itemId: 'turnaround',
          flex: 1,
          listeners: {
            boxready: function(cmp, w, h) {
              cmp.chart = AmCharts.makeChart(cmp.getEl().id, {
                  type:"serial", theme:"light", "dataProvider":<%=raw(d[:turnaround].to_json)%>,
                  valueAxes: [{unit: "h", position: "left", title: "Requisition turnaround time"}],
                  graphs: [{
                      balloonText: "Turnaround time for [[count]] requisitons released during [[category]]<br>Maximum: <b>[[max]]</b> hours<br>Average: <b>[[avg]]</b> hours<br>Minimum: <b>[[min]]</b> hours",
                      fillAlphas: 0.5,
                      lineAlpha: 0.2,
                      title: "Maximum",
                      type: "column",
                      lineColor: "#FCD202",
                      valueField: "max"
                  },{
                      balloonText: "Turnaround time for [[count]] requisitons released during [[category]]<br>Maximum: <b>[[max]]</b> hours<br>Average: <b>[[avg]]</b> hours<br>Minimum: <b>[[min]]</b> hours",
                      fillAlphas: 0.9,
                      lineAlpha: 0.2,
                      title: "Average",
                      type: "column",
                      lineColor: "#4DC441",
                      clustered: false,
                      valueField: "avg"
                  }],
                  plotAreaFillAlphas: 0.1,
                  categoryField: "label",
                  categoryAxis: {gridPosition: "start"},
                  export: {enabled: false}
              });
            }
          }
      }]
    }]
  });
  dashContainer.down('toolbar').hide();
  dashContainer.removeAll();
  dashContainer.add(dashPanel);
  dashContainer.updateLayout();    
})();
</script>